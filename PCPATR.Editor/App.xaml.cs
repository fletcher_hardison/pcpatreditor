﻿using System.Windows;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
      private IUnityContainer _container;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var bootstrapper = new Bootstrapper();
            bootstrapper.Run();
          _container = bootstrapper.Container;
          var mainWindow = _container.Resolve<MainWindowViewModel>();

          mainWindow.RequestApplicationClose += mainWindow_RequestApplicationClose;
        }

        void mainWindow_RequestApplicationClose(object sender, System.EventArgs e)
        {
          Application.Current.Shutdown(0);
        }

        protected override void OnExit(ExitEventArgs e)
        {
          var mainWindow = _container.Resolve<MainWindowViewModel>();

          if (mainWindow.GrammarLoaded)
          {
            var results = System.Windows.MessageBox.Show("Save before exiting?", "Alert", MessageBoxButton.YesNo);

            if (results == MessageBoxResult.Yes)
            {

              try
              {
                mainWindow.SaveGrammarWithOutNotification();
              }
              catch
              {
                MessageBox.Show("Grammar could not be saved. Please see readme file for support options");
              }
            }
          }

          base.OnExit(e);
        }


    }
}

