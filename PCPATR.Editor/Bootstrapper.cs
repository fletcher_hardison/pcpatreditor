﻿//  ***************************************************************
//  File name: Bootstrapper.cs
//  Project: PCPATR.Editor
//  Created by: 
//  Created on: 09,05,2012
// ***************************************************************

using System.Windows;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using PCPATR.Common;
using PCPATR.Editor.MasterLists;
using PCPATR.DataAccess;
using PCPATR.Models;

namespace PCPATR.Editor
{
  public class Bootstrapper : UnityBootstrapper
  {
    protected override DependencyObject CreateShell()
    {
      return new Shell();
    }

    protected override void InitializeShell()
    {
      base.InitializeShell();
      Application.Current.MainWindow = (Window) Shell;
      Application.Current.MainWindow.Show();
    }

    protected override void ConfigureModuleCatalog()
    {
      base.ConfigureModuleCatalog();

      var moduleCatalog = (ModuleCatalog) ModuleCatalog;      
      moduleCatalog.AddModule(typeof (DataAccessModule));
      moduleCatalog.AddModule(typeof (ModelsModule));      
      moduleCatalog.AddModule(typeof(CoreViewModelsModule), "ModelsModule");
    }

    protected override void ConfigureContainer()
    {
      base.ConfigureContainer();
      this.Container.RegisterInstance<Actions>(new Actions());
    }
  }
}