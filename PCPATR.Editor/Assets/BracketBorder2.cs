﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows;
using System.Windows.Controls;


namespace PCPATR.Editor.Assets
{
  public class BracketBorder2 : ContentControl
  {
    // based on http://10rem.net/blog/2010/02/05/creating-customized-usercontrols-deriving-from-contentcontrol-in-wpf-4
    static BracketBorder2() {
      DefaultStyleKeyProperty.OverrideMetadata(typeof (BracketBorder2),
                                               new FrameworkPropertyMetadata(typeof (BracketBorder2)));
    }
  }
}
