<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron">
    <pattern>
        <title>
            <dir value="ltr">Rule well-formedness</dir>
        </title>
        <rule context="leftHandSide">
            <assert test="id(@symbol)[name()='nonTerminal']">The symbol of the left hand side of a phrase structure rule must be a nonTerminal.</assert>
        </rule>
        <rule context="symbolRef">
            <assert test="id(@symbol)[name()='nonTerminal' or name()='terminal']">The symbol of the right hand side of a phrase structure rule must be either a nonTerminal or a terminal.</assert>
        </rule>
        <rule context="percolationOperation/child::*[1]">
            <assert test="current()/@node=ancestor::rule/phraseStructureRule/leftHandSide/@id">The first item of a percolation operation must refer to the leftHandSide of the rule.</assert>
        </rule>
        <rule context="percolationOperation/child::*[2]">
            <assert test="current()/@node=ancestor::rule/phraseStructureRule/rightHandSide/descendant::symbolRef/@id">The second item of a percolation operation must refer to a symbol in the rightHandSide of the rule.</assert>
        </rule>
        <rule context="featurePath">
            <assert test="@node=ancestor::rule/phraseStructureRule/descendant::*/@id">There is a featurePath in a constraint in this rule which refers to a phrase structure node that is not in this rule.</assert>
            <assert  test="string-length(@featureEnd) = 0  or id(@featureEnd)[ancestor::*[@id=current()/@featureStart]] or id(@featureEnd)[name()='collectionFeature']">There is no path from the starting feature to the ending feature.</assert>
        </rule>
        <rule context="featureValueRef[preceding-sibling::*[1][name()='featurePath' and string-length(@featureEnd) > 0]]">
            <assert test="id(@value)/../@id=preceding-sibling::featurePath/@featureEnd">A feature value is not a value of the closed feature indicated by the preceding featurePath.</assert>
        </rule>
        <rule context="featureValueRef[preceding-sibling::*[1][name()='featurePath' and string-length(@featureEnd) = 0 and count(child::*)=0]]">
            <assert test="id(@value)/../@id=preceding-sibling::featurePath/@featureStart">A feature value is not a value of the closed feature indicated by the preceding featurePath.</assert>
        </rule>
        <rule context="featureValueRef[not(preceding-sibling::templateFeaturePath) and parent::templateFeaturePathDisjunction[string-length(@featureEnd) > 0]]">
            <assert test="id(@value)/../@id=../@featureEnd">A feature value is not a value of the closed feature indicated by the preceding featurePath.</assert>
        </rule>
        <rule context="featureValueRef[not(preceding-sibling::templateFeaturePath) and parent::templateFeaturePathDisjunction[string-length(@featureEnd) = 0]]">
            <assert test="id(@value)/../@id=../@featureStart">A feature value is not a value of the closed feature indicated by the preceding featurePath.</assert>
        </rule>
        <rule context="categoryValueRef">
            <assert test="@node=ancestor::rule/phraseStructureRule/descendant::*/@id">The node of a categoryValueRef must refer to a phrase structure node in the rule.</assert>
        </rule>
        <rule context="logicalConstraintRef">
            <assert test="id(@constraint)[name()='constraintTemplate']">The constraint attribute of a logicalConstraintRef must refer to a contraintTemplate.</assert>
        </rule>
    </pattern>
    <pattern>
        <title>feature template wellformedness</title>
        <rule context="terminalRef">
            <assert test="id(@terminal)[name()='terminal']">A terminalRef must refer to a terminal.</assert>
        </rule>
        <rule context="featureTemplateRef">
            <assert test="id(@template)[name()='featureTemplateName']">A featureTemplateRef must refer to a feature template.</assert>
            <assert test="ancestor::featureTemplate[preceding-sibling::featureTemplate[featureTemplateDefinition/featureTemplateName/@id=current()/@template]]">A featureTemplateRef must refer to a feature template that occurs earlier in the file.</assert>
        </rule>
        <rule context="catValue">
            <assert test="id(@symbol)[name()='nonTerminal' or name()='terminal']">The symbol of a catValue must be either a nonTerminal or a terminal.</assert>
        </rule>
        <rule context="catValuePath">
            <assert test="id(@symbol)[name()='nonTerminal' or name()='terminal']">The symbol of a catValuePath must be either a nonTerminal or a terminal.</assert>
        </rule>
        <rule context="featureRef">
            <assert test="id(@featureStart)[name()='closedFeatureDefinition' or name()='complexFeatureDefinition' or name()='collectionFeature']">The featureStart attribute of a featureRef must refer to a feature definition.</assert>
            <assert test="id(@featureEnd)[name()='closedFeatureDefinition' or name()='complexFeatureDefinition' or name()='featureValueDefinition' or name()='complexFeatureDefinitionRef']">The featureEnd attribute of a featureRef must refer to a feature definition.</assert>
            <assert  test="id(@featureEnd)[ancestor-or-self::*[@id=current()/@featureStart]]">There is no path from the starting feature to the ending feature.</assert>
        </rule>
        <rule context="complexFeatureRef">
            <assert test="id(@feature)[name()='complexFeatureDefinition']">The feature attribute of a complexFeatureRef must refer to a complexFeatureDefinition.</assert>
        </rule>
        <rule context="collectionFeatureRef">
            <assert test="id(@collectionFeature)[name()='collectionFeature']">The collectionFeature attribute of a collectionFeatureRef must refer to a collectionFeature.</assert>
        </rule>
        <rule context="templateFeaturePath">
            <assert test="id(@featureStart)[name()='closedFeatureDefinition' or name()='complexFeatureDefinition' or name()='collectionFeature']">The featureStart attribute of a templateFeaturePath must refer to a feature definition.</assert>
            <assert test="id(@featureEnd)[name()='closedFeatureDefinition' or name()='complexFeatureDefinition' or name()='complexFeatureDefinitionRef'  or name()='featureValueDefinition' or name()='collectionFeature' or name()='none']">The featureEnd attribute of a templateFeaturePath must refer to a feature definition or none.</assert>
            <assert  test="id(@featureEnd)[ancestor::*[@id=current()/@featureStart]] or @featureStart=@featureEnd or id(@featureEnd)[name()='none']">There is no path from the starting feature to the ending feature.</assert>
        </rule>
    </pattern>
    <pattern>
        <title>featureRef sequence wellformedness</title>
        <rule context="featureRef[parent::featureRef]">
            <assert test="@featureStart=id(../@featureEnd)/@complex">Invalid sequence of featureRefs: the child @featureStart must refer to the same item as the parent's @featureEnd.</assert>
        </rule>
    </pattern>
    <pattern>
        <title>feature sequence wellformedness</title>
        <rule context="feature[parent::feature]">
            <assert test="@featureStart=id(../@featureEnd)/@complex or ../@featureStart=../@featureEnd and id(../@featureStart)[name()='collectionFeature']">Invalid sequence of feature: the child @featureStart must refer to the same item as the parent's @featureEnd.</assert>
        </rule>
    </pattern>
    <pattern>
        <title>templateFeaturePath sequence wellformedness</title>
        <rule context="templateFeaturePath[parent::templateFeaturePath]">
            <assert test="@featureStart=id(../@featureEnd)/@complex or ../@featureStart=../@featureEnd and id(../@featureStart)[name()='collectionFeature'] or ../@featureStart=../@featureEnd and id(@featureStart)[name()='collectionFeature']">Invalid sequence of feature: the child @featureStart must refer to the same item as the parent's @featureEnd.</assert>
        </rule>
    </pattern>
    <pattern>
        <title>complexFeatureDefinitionRef wellformedness</title>
        <rule context="complexFeatureDefinitionRef">
            <assert test="id(@complex)[name()='complexFeatureDefinition']">A complexFeatureDefinitionRef must refer to a complexFeatureDefinition.</assert>
        </rule>
    </pattern>
</schema>
