<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" version="1.0" encoding="utf-8" indent="no"/>
    <!-- 
        PCPatrGrammar.xsl
        Transform to convert xml file conforming to the PcPatrGrammar.dtd to PC-PATR rule input format
    -->
    <!-- ===========================================================
        Keys
        =========================================================== -->
    <xsl:key name="CollectionFeatureID" match="//collectionFeature" use="@id"/>
    <xsl:key name="ConstraintTemplateID" match="//constraintTemplate" use="@id"/>
    <xsl:key name="FeatureID" match="//complexFeatureDefinition | //closedFeatureDefinition | //complexFeatureDefinitionRef | //cat | //lex | //gloss | //rootgloss | //collectionFeature" use="@id"/>
    <xsl:key name="FeatureOrFeatureValueID" match="//complexFeatureDefinition | //closedFeatureDefinition | //complexFeatureDefinitionRef | //featureValueDefinition | //terminal | //nonTerminal | //none" use="@id"/>
    <xsl:key name="FeatureValueID" match="//featureValueDefinition | //none | //nonTerminal | //terminal" use="@id"/>
    <xsl:key name="FeatureTemplateID" match="//featureTemplateName" use="@id"/>
    <xsl:key name="NodeID" match="//symbolRef | //leftHandSide" use="@id"/>
    <xsl:key name="SymbolID" match="//terminal | //nonTerminal" use="@id"/>
    <xsl:key name="SymbolRefID" match="//symbolRef | //leftHandSide" use="@id"/>
    <xsl:key name="TerminalID" match="//terminal" use="@id"/>
    <!-- ===========================================================
        Parameterized Variables
        =========================================================== -->
    <xsl:param name="bDoingDebugging" select="'N'"/>
    <xsl:param name="sCat" select="'cat'"/>
    <xsl:param name="sGloss" select="'gloss'"/>
    <xsl:param name="sLex" select="'lex'"/>
    <xsl:param name="sRootGloss" select="'rootgloss'"/>
    <xsl:param name="sSubcat" select="'subcat'"/>
    <xsl:param name="sFirst" select="'first'"/>
    <xsl:param name="sRest" select="'rest'"/>
    <xsl:param name="sNone" select="'none'"/>

    <!-- ===========================================================
        Variables
        =========================================================== -->
    <xsl:variable name="sNewLineAndIndent" select="'&#xa;          '"/>

    <!-- ===========================================================
        MAIN BODY
        =========================================================== -->
    <xsl:template match="/patrGrammar">
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        attributeOrder
    -->
    <xsl:template match="attributeOrder">
        <xsl:call-template name="OutputParameter">
            <xsl:with-param name="sName" select="'Attribute order'"/>
        </xsl:call-template>
    </xsl:template>
    <!-- 
        binaryOperation
    -->
    <xsl:template match="binaryOperation">
        <xsl:apply-templates select="factor[1]"/>
        <xsl:text>&#x20;</xsl:text>
        <xsl:choose>
            <xsl:when test="@operation='and'">
                <xsl:text>&amp;</xsl:text>
            </xsl:when>
            <xsl:when test="@operation='or'">
                <xsl:text>/</xsl:text>
            </xsl:when>
            <xsl:when test="@operation='conditional'">
                <xsl:text>-&gt;</xsl:text>
            </xsl:when>
            <xsl:when test="@operation='biconditional'">
                <xsl:text>&lt;-&gt;</xsl:text>
            </xsl:when>
        </xsl:choose>
        <xsl:text>&#x20;</xsl:text>
        <xsl:apply-templates select="comment"/>
        <xsl:apply-templates select="factor[2]"/>
    </xsl:template>
    <!-- 
        categoryFeature
    -->
    <xsl:template match="categoryFeature">
        <xsl:call-template name="OutputParameter">
            <xsl:with-param name="sName" select="'Category feature'"/>
            <xsl:with-param name="sValue" select="name"/>
        </xsl:call-template>
    </xsl:template>
    <!-- 
        categoryValueRef
    -->
    <xsl:template match="categoryValueRef">
        <xsl:param name="ruleSymbols"/>
        <xsl:text>&lt;</xsl:text>
        <xsl:value-of select="key('SymbolID',key('NodeID',@node)/@symbol)/name"/>
        <xsl:call-template name="OutputAnyIndexValue">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
            <xsl:with-param name="node" select="key('SymbolRefID',@node)"/>
        </xsl:call-template>
        <xsl:text>&gt;</xsl:text>
    </xsl:template>
    <!-- 
        catValue
    -->
    <xsl:template match="catValue">
        <xsl:if test="count(preceding-sibling::*)=0">
            <xsl:text>[</xsl:text>
        </xsl:if>
        <xsl:value-of select="$sCat"/>
        <xsl:text> : </xsl:text>
        <xsl:call-template name="OutputAnyDefaultSymbol"/>
        <xsl:value-of select="key('SymbolID', @symbol)/name"/>
        <xsl:if test="count(following-sibling::*)=0">
            <xsl:text>]</xsl:text>
        </xsl:if>
    </xsl:template>
    <!-- 
        catValuePath
    -->
    <xsl:template match="catValuePath">
        <xsl:call-template name="OutputAnyNewlineInFeatureTemplate"/>
        <xsl:value-of select="$sCat"/>
        <xsl:text>&gt;</xsl:text>
        <xsl:text> = </xsl:text>
        <xsl:call-template name="OutputAnyDefaultSymbol"/>
        <xsl:value-of select="key('SymbolID', @symbol)/name"/>
    </xsl:template>
    <!-- 
        catRef
    -->
    <xsl:template match="catRef">
        <xsl:text>&#x20;</xsl:text>
        <xsl:value-of select="$sCat"/>
    </xsl:template>
    <!-- 
        collectionFeatureRef
    -->
    <xsl:template match="collectionFeatureRef">
        <!--        <xsl:if test="parent::templateFeatureStructure or parent::complexFeatureRef or parent::featureRef">
            <xsl:text>[</xsl:text>
        </xsl:if>
        -->
        <xsl:call-template name="OutputLineBreakAndIndent"/>
        <xsl:if test="count(preceding-sibling::*)=0">
            <xsl:text>[</xsl:text>
        </xsl:if>

        <xsl:value-of select="key('CollectionFeatureID',@collectionFeature)/name"/>
        <xsl:text> : </xsl:text>
        <xsl:apply-templates/>
        <xsl:if test="count(following-sibling::*)=0">
            <xsl:text>]</xsl:text>
        </xsl:if>
        <!--   <xsl:if test="parent::templateFeatureStructure or parent::complexFeatureRef or parent::featureRef">
            <xsl:text>]</xsl:text>
        </xsl:if>-->
    </xsl:template>
    <!-- 
        comment
    -->
    <xsl:template match="comment">
        <xsl:param name="bStartWithNewline" select="'N'"/>
        <xsl:param name="bEndWithNewline" select="'Y'"/>
        <xsl:if test="$bStartWithNewline='Y'">
            <xsl:text>&#xa;</xsl:text>
        </xsl:if>
        <xsl:text>| </xsl:text>
        <xsl:value-of select="."/>
        <xsl:if test="$bEndWithNewline='Y'">
            <xsl:text>&#xa;</xsl:text>
        </xsl:if>
    </xsl:template>
    <xsl:template match="comment" mode="contentOnly">
        <xsl:text>| </xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    <!-- 
        complexFeatureRef
    -->
    <xsl:template match="complexFeatureRef">
        <xsl:choose>
            <xsl:when test="count(preceding-sibling::*[name()!='comment'])=0">
                <xsl:text>[</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>&#xa;                </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="key('FeatureID',@feature)/name"/>
        <xsl:text> : </xsl:text>
        <xsl:apply-templates/>
        <xsl:if test="count(following-sibling::*[name()!='comment'])=0">
            <xsl:text>]</xsl:text>
        </xsl:if>
    </xsl:template>
    <!-- 
        constraintDisjunction
    -->
    <xsl:template match="constraintDisjunction">
        <xsl:param name="ruleSymbols"/>
        <xsl:text>&#xa;{</xsl:text>
        <xsl:apply-templates>
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
        <xsl:text>&#xa;}</xsl:text>
    </xsl:template>
    <!-- 
        constraints
    -->
    <xsl:template match="constraints">
        <xsl:param name="ruleSymbols"/>
        <xsl:if test="count(preceding-sibling::constraints) &gt; 0">
            <xsl:text>&#xa;/</xsl:text>
        </xsl:if>
        <xsl:apply-templates>
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
    </xsl:template>
    <!-- 
        constraintTemplate
    -->
    <xsl:template match="constraintTemplate">
        <xsl:text>&#xa;Constraint </xsl:text>
        <xsl:value-of select="name"/>
        <xsl:text> is </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        constraintTemplates
    -->
    <xsl:template match="constraintTemplates">
        <!--<xsl:text>&#xa;|</xsl:text>
        <xsl:text>&#xa;| Constraint Templates</xsl:text>
        <xsl:text>&#xa;|</xsl:text>-->
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        disjunction
    -->
    <xsl:template match="disjunction">
        <xsl:param name="ruleSymbols"/>
        <xsl:text>{ </xsl:text>
        <xsl:apply-templates>
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
        <xsl:text> } </xsl:text>
    </xsl:template>
    <!-- 
        disjunctiveSymbols
    -->
    <xsl:template match="disjunctiveSymbols">
        <xsl:param name="ruleSymbols"/>
        <xsl:text> / </xsl:text>
        <xsl:apply-templates>
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
    </xsl:template>
    <!-- 
        embeddedFeaturePath
    -->
    <xsl:template match="embeddedFeaturePath">
        <xsl:if test="parent::featurePath or parent::embeddedFeaturePath">
            <xsl:text>&#x20;</xsl:text>
        </xsl:if>
        <xsl:call-template name="OutputStartToEndFeaturePath"/>
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        empty
    -->
    <xsl:template match="empty">
        <xsl:text> []</xsl:text>
    </xsl:template>
    <!-- 
        factor
    -->
    <xsl:template match="factor">
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        feature
    -->
    <xsl:template match="feature">
        <xsl:text>[</xsl:text>
        <xsl:variable name="startingFeature" select="key('FeatureID',@featureStart)"/>
        <xsl:variable name="endingFeature" select="key('FeatureOrFeatureValueID',@featureEnd)"/>
        <xsl:call-template name="OutputFeatureName">
            <xsl:with-param name="feature" select="$startingFeature"/>
        </xsl:call-template>
        <xsl:if test="@featureStart!=@featureEnd or child::*">
            <!-- [rootgloss : ^1] case -->
            <xsl:text> : </xsl:text>
        </xsl:if>
        <xsl:call-template name="OutputFeaturePath">
            <xsl:with-param name="currentFeature" select="$endingFeature/.."/>
            <xsl:with-param name="startingFeatureId" select="$startingFeature/@id"/>
            <xsl:with-param name="bOutputFeatureName" select="'Y'"/>
            <xsl:with-param name="sBeforeName" select="'['"/>
            <xsl:with-param name="sAfterName" select="' : '"/>
        </xsl:call-template>
        <!-- do embedded ones -->
        <xsl:apply-templates/>
        <xsl:if test="not(child::*)">
            <xsl:if test="@featureStart!=@featureEnd and @indexedVariable!='none'">
                <xsl:text>[</xsl:text>
            </xsl:if>
            <xsl:call-template name="OutputFeatureName">
                <xsl:with-param name="feature" select="$endingFeature"/>
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="@indexedVariable!='none'">
            <xsl:text> : ^</xsl:text>
            <xsl:value-of select="@indexedVariable"/>
            <xsl:if test="@featureStart!=@featureEnd">
                <xsl:text>]</xsl:text>
            </xsl:if>
        </xsl:if>
        <xsl:text>]</xsl:text>
        <xsl:call-template name="OutputFeaturePath">
            <xsl:with-param name="currentFeature" select="$endingFeature/.."/>
            <xsl:with-param name="startingFeatureId" select="$startingFeature/@id"/>
            <xsl:with-param name="bOutputFeatureName" select="'N'"/>
            <xsl:with-param name="sBeforeName" select="''"/>
            <xsl:with-param name="sAfterName" select="']'"/>
        </xsl:call-template>
    </xsl:template>
    <!-- 
        featureNameRef
    -->
    <xsl:template match="featureNameRef">
        <xsl:text>&#x20;</xsl:text>
        <xsl:call-template name="OutputFeatureName">
            <xsl:with-param name="feature" select="key('FeatureID',@feature)"/>
        </xsl:call-template>
    </xsl:template>
    <!-- 
        featurePath
    -->
    <xsl:template match="featurePath">
        <xsl:param name="ruleSymbols"/>
        <xsl:text> &lt;</xsl:text>
        <xsl:value-of select="key('SymbolID',key('NodeID',@node)/@symbol)/name"/>
        <xsl:call-template name="OutputAnyIndexValue">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
            <xsl:with-param name="node" select="key('SymbolRefID',@node)"/>
        </xsl:call-template>
        <xsl:text>&#x20;</xsl:text>
        <xsl:call-template name="OutputStartToEndFeaturePath"/>
        <xsl:apply-templates/>
        <xsl:text>&gt; </xsl:text>
    </xsl:template>
    <!-- 
        featureRef
    -->
    <xsl:template match="featureRef">
        <xsl:choose>
            <xsl:when test="count(preceding-sibling::*[name()!='comment'])=0">
                <xsl:text>[</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>&#xa;                </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:variable name="startingFeature" select="key('FeatureID',@featureStart)"/>
        <xsl:variable name="endingFeature" select="key('FeatureOrFeatureValueID',@featureEnd)"/>
        <xsl:call-template name="OutputFeatureName">
            <xsl:with-param name="feature" select="$startingFeature"/>
        </xsl:call-template>
        <xsl:text> : </xsl:text>
        <xsl:if test="$startingFeature != $endingFeature">
            <xsl:choose>
                <xsl:when test="child::*">
                    <xsl:call-template name="OutputFeaturePath">
                        <xsl:with-param name="currentFeature" select="$endingFeature"/>
                        <xsl:with-param name="startingFeatureId" select="$startingFeature/@id"/>
                        <xsl:with-param name="bOutputFeatureName" select="'Y'"/>
                        <xsl:with-param name="sBeforeName" select="'['"/>
                        <xsl:with-param name="sAfterName" select="' : '"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="OutputFeaturePath">
                        <xsl:with-param name="currentFeature" select="$endingFeature/.."/>
                        <xsl:with-param name="startingFeatureId" select="$startingFeature/@id"/>
                        <xsl:with-param name="bOutputFeatureName" select="'Y'"/>
                        <xsl:with-param name="sBeforeName" select="'['"/>
                        <xsl:with-param name="sAfterName" select="' : '"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
        <!-- do embedded ones -->
        <xsl:apply-templates/>
        <xsl:call-template name="OutputAnyDefaultSymbol"/>
        <xsl:if test="not(child::*)">
            <xsl:call-template name="OutputFeatureName">
                <xsl:with-param name="feature" select="$endingFeature"/>
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="count(following-sibling::*[name()!='comment'])=0">
            <xsl:text>]</xsl:text>
        </xsl:if>
        <xsl:if test="$startingFeature != $endingFeature">
            <xsl:choose>
                <xsl:when test="child::*">
                    <xsl:call-template name="OutputFeaturePath">
                        <xsl:with-param name="currentFeature" select="$endingFeature"/>
                        <xsl:with-param name="startingFeatureId" select="$startingFeature/@id"/>
                        <xsl:with-param name="bOutputFeatureName" select="'N'"/>
                        <xsl:with-param name="sBeforeName" select="''"/>
                        <xsl:with-param name="sAfterName" select="']'"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="OutputFeaturePath">
                        <xsl:with-param name="currentFeature" select="$endingFeature/.."/>
                        <xsl:with-param name="startingFeatureId" select="$startingFeature/@id"/>
                        <xsl:with-param name="bOutputFeatureName" select="'N'"/>
                        <xsl:with-param name="sBeforeName" select="''"/>
                        <xsl:with-param name="sAfterName" select="']'"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
    <!-- 
        featureTemplate
    -->
    <xsl:template match="featureTemplate">
        <xsl:if test="@enabled='yes'">
            <xsl:choose>
                <xsl:when test="$bDoingDebugging='Y' and @useWhenDebugging='yes'">
                    <xsl:apply-templates>
                        <xsl:with-param name="bStartWithNewline" select="'Y'"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:when test="$bDoingDebugging='N'">
                    <xsl:apply-templates>
                        <xsl:with-param name="bStartWithNewline" select="'Y'"/>
                    </xsl:apply-templates>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
    <!-- 
        featureTemplateDefinition
    -->
    <xsl:template match="featureTemplateDefinition">
        <xsl:text>&#xa;Let </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        featureTemplateName
    -->
    <xsl:template match="featureTemplateName">
        <xsl:value-of select="."/>
        <xsl:text> be </xsl:text>
    </xsl:template>
    <!-- 
        featureTemplateRef
    -->
    <xsl:template match="featureTemplateRef">
        <xsl:call-template name="OutputLineBreakAndIndent"/>
        <xsl:text>[</xsl:text>
        <xsl:value-of select="key('FeatureTemplateID',@template)"/>
        <xsl:text>]</xsl:text>
    </xsl:template>
    <!-- 
        featureTemplates
    -->
    <xsl:template match="featureTemplates">
        <!--        <xsl:text>&#xa;|&#xa;|  Feature templates&#xa;|&#xa;</xsl:text>-->
        <xsl:apply-templates>
            <xsl:with-param name="bStartWithNewline" select="'Y'"/>
        </xsl:apply-templates>
    </xsl:template>
    <!-- 
        featureValueRef
    -->
    <xsl:template match="featureValueRef">
        <xsl:text>&#x20;</xsl:text>
        <xsl:variable name="ref" select="key('FeatureValueID',@value)"/>
        <xsl:choose>
            <xsl:when test="name($ref)='none'">
                <xsl:value-of select="$sNone"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$ref/name"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- 
        first
    -->
    <xsl:template match="first">
        <xsl:text>&#x20;</xsl:text>
        <xsl:value-of select="$sFirst"/>
    </xsl:template>
    <!-- 
        firstRef
    -->
    <xsl:template match="firstRef">
        <xsl:if test="parent::subcatDisjunction and count(preceding-sibling::*) &gt; 0">
            <xsl:value-of select="$sNewLineAndIndent"/>
        </xsl:if>
        <xsl:text>[</xsl:text>
        <xsl:value-of select="$sFirst"/>
        <xsl:text> : </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        glossFeature
    -->
    <xsl:template match="glossFeature">
        <xsl:call-template name="OutputParameter">
            <xsl:with-param name="sName" select="'Gloss feature'"/>
            <xsl:with-param name="sValue" select="name"/>
        </xsl:call-template>
    </xsl:template>
    <!-- 
        glossRef
    -->
    <xsl:template match="glossRef">
        <xsl:text>&#x20;</xsl:text>
        <xsl:value-of select="$sGloss"/>
    </xsl:template>
    <!-- 
        glossValue
    -->
    <xsl:template match="glossValue">
        <xsl:if test="count(preceding-sibling::*)=0">
            <xsl:text>[</xsl:text>
        </xsl:if>
        <xsl:value-of select="$sGloss"/>
        <xsl:text> : </xsl:text>
        <xsl:call-template name="OutputAnyDefaultSymbol"/>
        <xsl:value-of select="."/>
        <xsl:if test="count(following-sibling::*)=0">
            <xsl:text>]</xsl:text>
        </xsl:if>
    </xsl:template>
    <!-- 
        glossValuePath
    -->
    <xsl:template match="glossValuePath">
        <xsl:call-template name="OutputAnyNewlineInFeatureTemplate"/>
        <xsl:value-of select="$sGloss"/>
        <xsl:text>&gt;</xsl:text>
        <xsl:text> = </xsl:text>
        <xsl:call-template name="OutputAnyDefaultSymbol"/>
        <xsl:value-of select="."/>
    </xsl:template>
    <!-- 
        identifier
    -->
    <xsl:template match="identifier">
        <!-- an identifier is just for the use of the user; there is no true corresonding item in a PC-PATR grammar file
        Note that we are using the id for the identifier because this guarantees it will be unique 
        Nonetheless, we do output the identifer as a comment so the user can read it -->
        <xsl:text> | </xsl:text>
        <xsl:value-of select="."/>
    </xsl:template>
    <!-- 
        leftHandSide
    -->
    <xsl:template match="leftHandSide">
        <xsl:value-of select="key('SymbolID', @symbol)/name"/>
        <xsl:variable name="symbol" select="@symbol"/>
        <xsl:variable name="sameSymbol" select="following-sibling::rightHandSide/descendant::symbolRef[@symbol=$symbol]"/>
        <xsl:if test="$sameSymbol">
            <xsl:text>_1</xsl:text>
        </xsl:if>

        <xsl:text> = </xsl:text>
    </xsl:template>
    <!-- 
        lexicalFeature
    -->
    <xsl:template match="lexicalFeature">
        <xsl:call-template name="OutputParameter">
            <xsl:with-param name="sName" select="'Lexical feature'"/>
            <xsl:with-param name="sValue" select="name"/>
        </xsl:call-template>
    </xsl:template>
    <!-- 
        lexicalRule
    -->
    <xsl:template match="lexicalRule">
        <xsl:call-template name="OutputLexicalRule"/>
    </xsl:template>
    <!-- 
        lexicalRuleDefinition
    -->
    <xsl:template match="lexicalRuleDefinition">
        <xsl:text>&lt;out </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        lexicalRuleFeaturePath
    -->
    <xsl:template match="lexicalRuleFeaturePath">
        <xsl:call-template name="OutputStartToEndFeaturePath"/>
        <xsl:text>&gt;</xsl:text>
        <xsl:choose>
            <xsl:when test="count(preceding-sibling::lexicalRuleFeaturePath)=0">
                <xsl:text> = </xsl:text>    
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>&#xa;</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- 
        lexicalRules
    -->
    <xsl:template match="lexicalRules">
        <xsl:text>&#xa;|&#xa;| Lexical Rules&#xa;|&#xa;</xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        lexRef
    -->
    <xsl:template match="lexRef">
        <xsl:text>&#x20;</xsl:text>
        <xsl:value-of select="$sLex"/>
    </xsl:template>
    <!-- 
        lexValue
    -->
    <xsl:template match="lexValue">
        <xsl:if test="count(preceding-sibling::*) &gt; 1">
            <xsl:value-of select="$sNewLineAndIndent"/>
        </xsl:if>
        <xsl:if test="count(preceding-sibling::*)=0">
            <xsl:text>[</xsl:text>
        </xsl:if>
        <xsl:value-of select="$sLex"/>
        <xsl:text> : </xsl:text>
        <xsl:call-template name="OutputAnyDefaultSymbol"/>
        <xsl:value-of select="."/>
        <xsl:if test="count(following-sibling::*)=0">
            <xsl:text>]</xsl:text>
        </xsl:if>
    </xsl:template>
    <xsl:template match="lexValue" mode="contentOnly">
        <xsl:value-of select="."/>
    </xsl:template>
    <!-- 
        lexValuePath
    -->
    <xsl:template match="lexValuePath">
        <xsl:call-template name="OutputAnyNewlineInFeatureTemplate"/>
        <xsl:value-of select="$sLex"/>
        <xsl:text>&gt;</xsl:text>
        <xsl:text> = </xsl:text>
        <xsl:call-template name="OutputAnyDefaultSymbol"/>
        <xsl:value-of select="."/>
    </xsl:template>
    <!-- 
        logicalConstraint
    -->
    <xsl:template match="logicalConstraint">
        <xsl:param name="ruleSymbols"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="featurePath">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
        <xsl:text> == </xsl:text>
        <xsl:apply-templates select="logicalExpression | logicalConstraintRef | comment"/>
    </xsl:template>
    <!-- 
        logicalConstraint
    -->
    <xsl:template match="logicalConstraintRef">
        <xsl:value-of select="key('ConstraintTemplateID',@constraint)/name"/>
    </xsl:template>
    <!-- 
        logicalExpression
    -->
    <xsl:template match="logicalExpression">
        <xsl:variable name="antecedentLogicalExpressions" select="ancestor::logicalExpression"/>
        <xsl:if test="count($antecedentLogicalExpressions) &gt; 1">
            <xsl:text>&#xa;           ( </xsl:text>
        </xsl:if>
        <xsl:apply-templates/>
        <xsl:if test="count($antecedentLogicalExpressions) &gt; 1">
            <xsl:text> )</xsl:text>
        </xsl:if>
    </xsl:template>
    <!-- 
        masterLists
    -->
    <xsl:template match="masterLists">
        <!-- these can all be ignored as far as the PC-PATR grammar file is concerned -->
    </xsl:template>
    <!-- 
        none
    -->
    <xsl:template match="none">
        <xsl:call-template name="OutputAnyDefaultSymbol"/>
        <xsl:value-of select="$sNone"/>
    </xsl:template>
    <!-- 
        noneRef
    -->
    <xsl:template match="noneRef">
        <xsl:value-of select="$sNone"/>
    </xsl:template>
    <!-- 
        noneValue
    -->
    <xsl:template match="noneValue">
        <xsl:if test="count(preceding-sibling::*) &gt; 0 and parent::templateFeatureDisjunction">
            <xsl:value-of select="$sNewLineAndIndent"/>
        </xsl:if>

        <xsl:value-of select="$sNone"/>
    </xsl:template>
    <!-- 
        noneValuePath
    -->
    <xsl:template match="noneValuePath">
        <xsl:call-template name="OutputAnyNewlineInFeatureTemplate"/>
        <xsl:text>&gt;</xsl:text>
        <xsl:text> = </xsl:text>
        <xsl:call-template name="OutputAnyDefaultSymbol"/>
        <xsl:value-of select="$sNone"/>
    </xsl:template>
    <!-- 
        optionalSymbols
    -->
    <xsl:template match="optionalSymbols">
        <xsl:param name="ruleSymbols"/>
        <xsl:text>(</xsl:text>
        <xsl:apply-templates>
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
        <xsl:text>) </xsl:text>
    </xsl:template>
    <!-- 
        parameters
    -->
    <xsl:template match="parameters">
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        percolationOperation
    -->
    <xsl:template match="percolationOperation">
        <xsl:param name="ruleSymbols"/>
        <!-- a percolation constraint is a unification constraint -->
        <xsl:call-template name="HandleUnificationConstraint">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:call-template>
    </xsl:template>
    <!-- 
        phraseStructureRule
    -->
    <xsl:template match="phraseStructureRule">
        <xsl:param name="ruleSymbols"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates>
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
    </xsl:template>
    <!-- 
        priorityUnionOperation
    -->
    <xsl:template match="priorityUnionOperation">
        <xsl:param name="ruleSymbols"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="featurePath[1]">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
        <xsl:text> &lt;= </xsl:text>
        <xsl:apply-templates select="featurePath[2] | featureValueRef | categoryValueRef | comment">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
    </xsl:template>
    <!-- 
        rest
    -->
    <xsl:template match="rest">
        <xsl:text>&#x20;</xsl:text>
        <xsl:value-of select="$sRest"/>
    </xsl:template>
    <!-- 
        restRef
    -->
    <xsl:template match="restRef">
        <xsl:text>&#xa;                       </xsl:text>
        <xsl:value-of select="$sRest"/>
        <xsl:text> : </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>]</xsl:text>
    </xsl:template>
    <!-- 
        restrictor
    -->
    <xsl:template match="restrictor">
        <xsl:variable name="sFeaturePaths">
            <xsl:for-each select="templateFeaturePath">
                <xsl:text>&lt;</xsl:text>
                <xsl:call-template name="OutputFeatureName">
                    <xsl:with-param name="feature" select="key('FeatureID',@featureStart)"/>
                </xsl:call-template>
                <xsl:call-template name="OutputFeaturePath">
                    <xsl:with-param name="currentFeature" select="key('FeatureOrFeatureValueID',@featureEnd)"/>
                    <xsl:with-param name="startingFeatureId" select="@featureStart"/>
                    <xsl:with-param name="bOutputFeatureName" select="'Y'"/>
                    <xsl:with-param name="sBeforeName" select="' '"/>
                </xsl:call-template>
                <xsl:text>&gt; </xsl:text>
            </xsl:for-each>
        </xsl:variable>
        <xsl:call-template name="OutputParameter">
            <xsl:with-param name="sName" select="'Restrictor'"/>
            <xsl:with-param name="sValue" select="$sFeaturePaths"/>
        </xsl:call-template>
    </xsl:template>
    <!-- 
        rightHandSide
    -->
    <xsl:template match="rightHandSide">
        <xsl:param name="ruleSymbols"/>
        <xsl:apply-templates>
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
    </xsl:template>
    <!-- 
        rootGlossFeature
    -->
    <xsl:template match="rootGlossFeature">
        <xsl:call-template name="OutputParameter">
            <xsl:with-param name="sName" select="'RootGloss feature'"/>
            <xsl:with-param name="sValue" select="name"/>
        </xsl:call-template>
    </xsl:template>
    <!-- 
        rootGlossRef
    -->
    <xsl:template match="rootGlossRef">
        <xsl:text>&#x20;</xsl:text>
        <xsl:value-of select="$sRootGloss"/>
    </xsl:template>
    <!-- 
        rootGlossValue
    -->
    <xsl:template match="rootGlossValue">
        <xsl:if test="count(preceding-sibling::*)=0">
            <xsl:text>[</xsl:text>
        </xsl:if>
        <xsl:value-of select="$sRootGloss"/>
        <xsl:text> : </xsl:text>
        <xsl:call-template name="OutputAnyDefaultSymbol"/>
        <xsl:value-of select="."/>
        <xsl:if test="count(following-sibling::*)=0">
            <xsl:text>]</xsl:text>
        </xsl:if>
    </xsl:template>
    <!-- 
        rootGlossValuePath
    -->
    <xsl:template match="rootGlossValuePath">
        <xsl:call-template name="OutputAnyNewlineInFeatureTemplate"/>
        <xsl:value-of select="$sRootGloss"/>
        <xsl:text>&gt;</xsl:text>
        <xsl:text> = </xsl:text>
        <xsl:call-template name="OutputAnyDefaultSymbol"/>
        <xsl:value-of select="."/>
    </xsl:template>
    <!-- 
        rule
    -->
    <xsl:template match="rule">
        <xsl:if test="@enabled='yes'">
            <xsl:choose>
                <xsl:when test="$bDoingDebugging='Y' and @useWhenDebugging='yes'">
                    <xsl:call-template name="OutputRule"/>
                </xsl:when>
                <xsl:when test="$bDoingDebugging='N'">
                    <xsl:call-template name="OutputRule"/>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
    <!-- 
        rules
    -->
    <xsl:template match="rules">
        <!--        <xsl:text>|&#xa;|&#xa;|  Phrase structure rules&#xa;|&#xa;</xsl:text>-->
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        startSymbol
    -->
    <xsl:template match="startSymbol">
        <xsl:call-template name="OutputParameter">
            <xsl:with-param name="sName" select="'Start symbol'"/>
            <xsl:with-param name="sValue" select="key('SymbolID',@symbol)/name"/>
        </xsl:call-template>
    </xsl:template>
    <!-- 
        subcat
    -->
    <xsl:template match="subcat">
        <xsl:text>&#x20;</xsl:text>
        <xsl:value-of select="$sSubcat"/>
    </xsl:template>
    <!-- 
        subcatDisjunction
    -->
    <xsl:template match="subcatDisjunction">
        <xsl:text>{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text>
    </xsl:template>
    <!-- 
        subcatPath
    -->
    <xsl:template match="subcatPath">
        <xsl:param name="ruleSymbols"/>
        <xsl:call-template name="OutputFeaturePathContent">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:call-template>
        <xsl:value-of select="$sSubcat"/>
        <xsl:if test="string-length(@subcatEnd) &gt; 0">
            <xsl:text>&#x20;</xsl:text>
            <xsl:value-of select="@subcatEnd"/>
        </xsl:if>
        <xsl:if test="string-length(@featureAfterStart) &gt; 0">
            <xsl:text>&#x20;</xsl:text>
            <xsl:call-template name="OutputStartToEndFeaturePath">
                <xsl:with-param name="sStartingFeature" select="@featureAfterStart"/>
                <xsl:with-param name="sEndingFeature" select="@featureAfterEnd"/>
            </xsl:call-template>
        </xsl:if>
        <xsl:text>&gt; </xsl:text>
    </xsl:template>
    <!-- 
        subcatRef
    -->
    <xsl:template match="subcatRef">
        <xsl:text>[</xsl:text>
        <xsl:value-of select="$sSubcat"/>
        <xsl:text> : </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>]</xsl:text>
    </xsl:template>
    <!-- 
        symbolRef
    -->
    <xsl:template match="symbolRef">
        <xsl:param name="ruleSymbols"/>
        <xsl:value-of select="key('SymbolID', @symbol)/name"/>
        <xsl:call-template name="OutputAnyIndexValue">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
            <xsl:with-param name="node" select="."/>
        </xsl:call-template>
        <xsl:text>&#x20;</xsl:text>
    </xsl:template>
    <!-- 
        templateFeatureDisjunction
    -->
    <xsl:template match="templateFeatureDisjunction">
        <xsl:if test="count(following-sibling::*[name()!='comment'])!=0 and count(preceding-sibling::*[name()!='comment' and name()!='featureTemplateName'])=0">
            <xsl:text>[</xsl:text>
        </xsl:if>
        <xsl:variable name="iChildNodes" select="count(child::*)"/>
        <xsl:variable name="iLexValueAndCommentNodes" select="count(child::*[name()='lexValue' or name()='comment'])"/>
        <xsl:choose>
            <xsl:when test="$iLexValueAndCommentNodes=$iChildNodes">
                <xsl:if test="count(preceding-sibling::*) &gt; 0">
                    <xsl:value-of select="$sNewLineAndIndent"/>
                </xsl:if>
                <xsl:value-of select="$sLex"/>
                <xsl:text> : </xsl:text>
                <xsl:text>{</xsl:text>
                <xsl:for-each select="child::*">
                    <xsl:choose>
                        <xsl:when test="name()='comment'">
                            <xsl:apply-templates mode="contentOnly"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="."/>
                            <xsl:if test="position()!=last()">
                                <xsl:text>&#x20;</xsl:text>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
                <!--                <xsl:apply-templates mode="contentOnly"/>
                for some reason, this outputs a newline and spaces for lexValue...-->
                <xsl:text>}</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>{</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="count(preceding-sibling::*[name()!='comment' and name()!='featureTemplateName'])!=0 and count(following-sibling::*[name()!='comment'])=0">
            <xsl:text>]</xsl:text>
        </xsl:if>
    </xsl:template>
    <!-- 
        templateFeaturePath
    -->
    <xsl:template match="templateFeaturePath">
        <xsl:call-template name="OutputAnyNewlineInFeatureTemplate"/>
        <xsl:call-template name="OutputFeatureName">
            <xsl:with-param name="feature" select="key('FeatureID',@featureStart)"/>
        </xsl:call-template>
        <xsl:call-template name="OutputFeaturePath">
            <xsl:with-param name="currentFeature" select="key('FeatureOrFeatureValueID',@featureEnd)/.."/>
            <xsl:with-param name="startingFeatureId" select="@featureStart"/>
            <xsl:with-param name="bOutputFeatureName" select="'Y'"/>
            <xsl:with-param name="sBeforeName" select="' '"/>
        </xsl:call-template>
        <xsl:apply-templates/>
        <xsl:if test="not(child::*)">
            <xsl:text>&gt; = </xsl:text>
            <xsl:call-template name="OutputAnyDefaultSymbol"/>
            <xsl:call-template name="OutputFeatureName">
                <xsl:with-param name="feature" select="key('FeatureValueID',@featureEnd)"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <!-- 
        templateFeaturePathDisjunction
    -->
    <xsl:template match="templateFeaturePathDisjunction">
        <xsl:text>&lt;</xsl:text>
        <xsl:call-template name="OutputFeatureName">
            <xsl:with-param name="feature" select="key('FeatureID',@featureStart)"/>
        </xsl:call-template>
        <xsl:call-template name="OutputFeaturePath">
            <xsl:with-param name="currentFeature" select="key('FeatureOrFeatureValueID',@featureEnd)"/>
            <xsl:with-param name="startingFeatureId" select="@featureStart"/>
            <xsl:with-param name="bOutputFeatureName" select="'Y'"/>
            <xsl:with-param name="sBeforeName" select="' '"/>
        </xsl:call-template>
        <xsl:for-each select="templateFeaturePath">
            <xsl:text>&#x20;</xsl:text>
            <xsl:call-template name="OutputFeatureName">
                <xsl:with-param name="feature" select="key('FeatureID',@featureStart)"/>
            </xsl:call-template>
            <xsl:call-template name="OutputFeaturePath">
                <xsl:with-param name="currentFeature" select="key('FeatureOrFeatureValueID',@featureEnd)"/>
                <xsl:with-param name="startingFeatureId" select="@featureStart"/>
                <xsl:with-param name="bOutputFeatureName" select="'Y'"/>
                <xsl:with-param name="sBeforeName" select="' '"/>
            </xsl:call-template>
        </xsl:for-each>
        <xsl:text>&gt; = {</xsl:text>
        <xsl:apply-templates select="*[name()!='templateFeaturePath']"/>
        <xsl:text>}</xsl:text>
    </xsl:template>
    <!-- 
        templateFeatureStructure
    -->
    <xsl:template match="templateFeatureStructure">
        <xsl:call-template name="OutputLineBreakAndIndent"/>
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        terminalRef
    -->
    <xsl:template match="terminalRef">
        <xsl:value-of select="key('TerminalID',@terminal)/name"/>
        <xsl:text> be </xsl:text>
    </xsl:template>
    <!-- 
        text()
    -->
    <xsl:template match="text()">
        <!-- ignore any text (i.e. whitespace) in the input file -->
    </xsl:template>
    <!-- 
        unaryOperation
    -->
    <xsl:template match="unaryOperation">
        <xsl:choose>
            <xsl:when test="@operation='negation'">
                <xsl:text>~</xsl:text>
            </xsl:when>
            <xsl:when test="@operation='existence'">
                <!-- do nothing -->
            </xsl:when>
        </xsl:choose>
        <xsl:apply-templates select="factor"/>
    </xsl:template>
    <!-- 
        unificationConstraint
    -->
    <xsl:template match="unificationConstraint">
        <xsl:param name="ruleSymbols"/>
        <!-- a percolation constraint is a unification constraint -->
        <xsl:call-template name="HandleUnificationConstraint">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:call-template>
    </xsl:template>
    <!-- 
        HandleUnificationConstraint
    -->
    <xsl:template name="HandleUnificationConstraint">
        <xsl:param name="ruleSymbols"/>
        <xsl:if test="@enabled='yes'">
            <xsl:choose>
                <xsl:when test="$bDoingDebugging='Y' and @useWhenDebugging='yes'">
                    <xsl:call-template name="OutputUnificationConstraint">
                        <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$bDoingDebugging='N'">
                    <xsl:call-template name="OutputUnificationConstraint">
                        <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
    <!-- 
        OutputAnyDefaultSymbol
    -->
    <xsl:template name="OutputAnyDefaultSymbol">
        <xsl:if test="@isDefault='yes'">
            <xsl:text>!</xsl:text>
        </xsl:if>
    </xsl:template>
    <!-- 
        OutputAnyIndexValue
    -->
    <xsl:template name="OutputAnyIndexValue">
        <xsl:param name="ruleSymbols"/>
        <xsl:param name="node"/>
        <xsl:variable name="symbol">
            <xsl:value-of select="$node/@symbol"/>
        </xsl:variable>
        <xsl:variable name="sameSymbols" select="$ruleSymbols[@symbol=$symbol]"/>
        <xsl:if test="$sameSymbols">
            <xsl:if test="count($sameSymbols) &gt; 1">
                <xsl:text>_</xsl:text>
                <xsl:variable name="thisItem" select="$node"/>
                <xsl:for-each select="$sameSymbols">
                    <xsl:if test="@id=$thisItem/@id">
                        <xsl:value-of select="position()"/>
                    </xsl:if>
                </xsl:for-each>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    <!-- 
        OutputAnyNewlineInFeatureTemplate
    -->
    <xsl:template name="OutputAnyNewlineInFeatureTemplate">
        <xsl:if test="count(preceding-sibling::*) &gt; 1">
            <xsl:value-of select="$sNewLineAndIndent"/>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="parent::templateFeaturePath">
                <xsl:text>&#x20;</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>&lt;</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- 
        OutputFeatureName
    -->
    <xsl:template name="OutputFeatureName">
        <xsl:param name="feature"/>
        <xsl:choose>
            <xsl:when test="$feature/name">
                <xsl:value-of select="$feature/name"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="name($feature)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- 
        OutputFeatureRefPath
    -->
    <xsl:template name="OutputFeaturePath">
        <xsl:param name="currentFeature"/>
        <xsl:param name="startingFeatureId"/>
        <xsl:param name="bOutputFeatureName"/>
        <xsl:param name="sBeforeName"/>
        <xsl:param name="sAfterName"/>
        <xsl:if test="$currentFeature/@id!=$startingFeatureId">
            <xsl:call-template name="OutputFeaturePath">
                <xsl:with-param name="currentFeature" select="$currentFeature/.."/>
                <xsl:with-param name="startingFeatureId" select="$startingFeatureId"/>
                <xsl:with-param name="bOutputFeatureName" select="$bOutputFeatureName"/>
                <xsl:with-param name="sBeforeName" select="$sBeforeName"/>
                <xsl:with-param name="sAfterName" select="$sAfterName"/>
            </xsl:call-template>
            <xsl:value-of select="$sBeforeName"/>
            <xsl:if test="$bOutputFeatureName='Y'">
                <xsl:value-of select="$currentFeature/name"/>
            </xsl:if>
            <xsl:value-of select="$sAfterName"/>
        </xsl:if>
    </xsl:template>
    <!-- 
        OutputFeaturePathContent
    -->
    <xsl:template name="OutputFeaturePathContent">
        <xsl:param name="ruleSymbols"/>
        <xsl:text> &lt;</xsl:text>
        <xsl:value-of select="key('SymbolID',key('NodeID',@node)/@symbol)/name"/>
        <xsl:call-template name="OutputAnyIndexValue">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
            <xsl:with-param name="node" select="key('SymbolRefID',@node)"/>
        </xsl:call-template>
        <xsl:text>&#x20;</xsl:text>
        <xsl:call-template name="OutputStartToEndFeaturePath"/>
    </xsl:template>
    <!-- 
        OutputLexicalRule
    -->
    <xsl:template name="OutputLexicalRule">
        <xsl:apply-templates select="comment"/>
        <xsl:text>&#xa;Define </xsl:text>
        <xsl:value-of select="name"/>
        <xsl:text> as &#xa;</xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <!-- 
        OutputLineBreakAndIndent
    -->
    <xsl:template name="OutputLineBreakAndIndent">
        <xsl:if test="count(preceding-sibling::*[name()!='comment' and name()!='featureTemplateName' and name()!='terminalRef']) &gt; 0">
            <xsl:value-of select="$sNewLineAndIndent"/>
        </xsl:if>
    </xsl:template>
    <!-- 
        OutputParameter
    -->
    <xsl:template name="OutputParameter">
        <xsl:param name="sName"/>
        <xsl:param name="sValue"/>
        <xsl:text>Parameter </xsl:text>
        <xsl:value-of select="$sName"/>
        <xsl:text> is </xsl:text>
        <xsl:choose>
            <xsl:when test="string-length($sValue)=0">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$sValue"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>.&#xa;</xsl:text>
        <xsl:apply-templates select="comment"/>
    </xsl:template>
    <!-- 
    OutputRule
-->
    <xsl:template name="OutputRule">
        <xsl:text>&#xa;rule {</xsl:text>
        <xsl:value-of select="@id"/>
        <xsl:text>} </xsl:text>
        <xsl:variable name="ruleSymbols" select="phraseStructureRule/leftHandSide | phraseStructureRule/rightHandSide/descendant::symbolRef"/>
        <xsl:apply-templates>
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
    </xsl:template>
    <!-- 
        OutputStartToEndFeaturePath
    -->
    <xsl:template name="OutputStartToEndFeaturePath">
        <xsl:param name="sStartingFeature" select="@featureStart"/>
        <xsl:param name="sEndingFeature" select="@featureEnd"/>
        <xsl:variable name="startingFeature" select="key('FeatureID',$sStartingFeature)"/>
        <xsl:variable name="endingFeature" select="key('FeatureID',$sEndingFeature)"/>
        <xsl:call-template name="OutputFeatureName">
            <xsl:with-param name="feature" select="$startingFeature"/>
        </xsl:call-template>
        <xsl:if test="$endingFeature">
            <xsl:call-template name="OutputFeaturePath">
                <xsl:with-param name="currentFeature" select="$endingFeature"/>
                <xsl:with-param name="startingFeatureId" select="$startingFeature/@id"/>
                <xsl:with-param name="bOutputFeatureName" select="'Y'"/>
                <xsl:with-param name="sBeforeName" select="' '"/>
                <xsl:with-param name="sAfterName" select="''"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <!-- 
        OutputUnificationConstraint
    -->
    <xsl:template name="OutputUnificationConstraint">
        <xsl:param name="ruleSymbols"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="*[1]">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
        <xsl:text> =  </xsl:text>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="ruleSymbols" select="$ruleSymbols"/>
        </xsl:apply-templates>
        <xsl:apply-templates select="comment">
            <xsl:with-param name="bEndWithNewline" select="'N'"/>
        </xsl:apply-templates>
    </xsl:template>
</xsl:stylesheet>
