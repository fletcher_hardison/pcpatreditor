﻿//  ***************************************************************
//  File name: PatrAnalyserController.cs
//  Project: PCPATR.Analyser
//  Created by: Fletcher Hardison
//  Created on: 14,02,2013
// ***************************************************************

using System;
using System.Runtime.InteropServices;
using System.Xml;
using Newtonsoft.Json.Linq;
using PCPATR.Analyser.PatrAnalyser;

namespace PCPATR.Analyser
{
  [ComVisible(true)]
  public class PatrAnalyserController
  {

    public event EventHandler NavigateToStartPage;
    public event EventHandler NavigateToResultsPage;
    public event EventHandler NavigateFeatureBrowser;
    public PatrAnalyserController(string featurePagePath) {
      _analyser = new ParseAnalyser();
      _control = new PatrParserControl();
      _options = new ParseOptionsViewModel();
      _configReader = new ConfigReader();
      _configReader.LoadConfiguration(OutsideWorld.GetPathToConfigs());
      _featurePagePath = featurePagePath;
      _notifierService = LingTreeTreeControlNotifierService.GetInstance();
      _notifierService.RegisterStartPoint(BuildNodeClickedCallBack());
      _controlViewModel = new ParserControlViewModel(_control,_analyser,_notifierService,_configReader,_options);
      
    }

    private Action<string> BuildNodeClickedCallBack() {
      return id => {
        _controlViewModel.ShowFSInfo(id,_featurePagePath);
        if (NavigateFeatureBrowser != null) {
          NavigateFeatureBrowser(this, new EventArgs());
        }
      };
    }


    public void RunParser(bool runInDebugMode) {
      if (NavigateToResultsPage != null) {      
        _controlViewModel.RunGrammarInDebugMode = runInDebugMode;
        _controlViewModel.RunParserOnSelectedFileCommandMethod();
        NavigateToResultsPage(this, new EventArgs());
      }
    }

    public void SetParserFilePaths(string grammarPath, string lexiconPath, string dataPath) {
      _controlViewModel.GrammarFile = grammarPath;
      _controlViewModel.LexiconFile = lexiconPath;
      _controlViewModel.FileToParse = dataPath;
      SetLastFilesUsed(grammarPath, lexiconPath, dataPath);
    }

    public void SetParserOptions(string maxAmbiguities, string failures, string commentChar, string sentFinalPunc, string timeLimit, bool useTimeLimit, bool checkCycles) {
      _options.MaxAmbiguities = int.Parse(maxAmbiguities);
      _options.FailuresPerSentence = int.Parse(failures);
      _options.CommentCharacter = string.IsNullOrEmpty(commentChar) ? ';' : commentChar[0] ;
      _options.SentenceFinalPunctuation = sentFinalPunc;
      _options.ParsingTimeLimitInSeconds = string.IsNullOrEmpty(timeLimit) ? 1 :long.Parse(timeLimit);
      _options.UseTimeLimitWhenParsing = useTimeLimit;
      _options.CheckCycles = checkCycles;

    }

    public void SetFeatureOptions(bool promoteFeatures, bool showGloss, bool trimEmptyFeaures, bool unificationisOn) {
      _options.PromoteAtomicLexicalFeaturesToOrdinaryStatus = promoteFeatures;
      _options.ShowGloss = showGloss;
      _options.TrimEmptyFeatures = trimEmptyFeaures;
      _options.UnificationIsOn = unificationisOn;
    }

    public void SetLexiconOptions(string recordMarker, string wordMarker, string catMarker,
                                  string glossMarker, string rootGlossMarker, string featureMarker) {
      _options.LexiconRecordMarker = recordMarker;
      _options.LexiconWordMarker = wordMarker;
      _options.LexiconCategoryMarker = catMarker;
      _options.LexiconGlossMarker = glossMarker;
      _options.LexiconRootGlossMarker = rootGlossMarker;
      _options.LexiconFeaturesMarker = featureMarker;
    }

    public string PassOptionsAsJson() {
      var jsonObject = JObject.FromObject(new {
        MaxAmbiguities = _options.MaxAmbiguities,
        FailuresPerSentence = _options.FailuresPerSentence,
        CommentCharacter = _options.CommentCharacter,
        SentenceFinalPunctuation = _options.SentenceFinalPunctuation,
        TimeLimit = _options.ParsingTimeLimitInSeconds,
        UseTimeLimit = _options.UseTimeLimitWhenParsing,
        CheckCycles = _options.CheckCycles,
        PromoteLexicalFeatures = _options.PromoteAtomicLexicalFeaturesToOrdinaryStatus,
        ShowGloss = _options.ShowGloss,
        TrimEmptyFeatures = _options.TrimEmptyFeatures,
        IsUnificationOn = _options.UnificationIsOn,
        RecordMarker  = _options.LexiconRecordMarker,
        WordMarker = _options.LexiconWordMarker,
        CatMarker = _options.LexiconCategoryMarker,
        GlossMarker = _options.LexiconGlossMarker,
        RootGlossMarker =_options.LexiconRootGlossMarker,
        FeaturesMarker = _options.LexiconFeaturesMarker
      });

      return jsonObject.ToString();
    }

    public string PassLastFilesAsJson() {
      var jsonObject = JObject.FromObject(new {
        LastGrammarFile = _configReader[OutsideWorld.LAST_GRAMMAR_FILE],
        LastLexiconFile = _configReader[OutsideWorld.LAST_LEXICON],
        LastDataFile = _configReader[OutsideWorld.LAST_DATA_FILE]
      });

      return jsonObject.ToString();
    }

    public string PassParseResultsAsHtmlString() {
      return _controlViewModel.GetParseResultsAsHtml();
    }

   


    public void SetSentenceAndParseFromIndex(int sentenceIndex, int parseIndex) {
      _controlViewModel.SetParseAndSentenceFromIndecies(sentenceIndex,parseIndex);
    }

    private void SetLastFilesUsed(string grammarFile, string lexiconFile, string dataFile) {
      _configReader[OutsideWorld.LAST_GRAMMAR_FILE] = grammarFile;
      _configReader[OutsideWorld.LAST_LEXICON] = lexiconFile;
      _configReader[OutsideWorld.LAST_DATA_FILE] = dataFile;
      _configReader.SaveConfigurations(OutsideWorld.GetPathToConfigs());
    }
	  
    private readonly ParseAnalyser _analyser;
    private readonly PatrParserControl _control;
    private readonly ParseOptionsViewModel _options;
    private readonly ParserControlViewModel _controlViewModel;
    private readonly ConfigReader _configReader;
    private readonly LingTreeTreeControlNotifierService _notifierService;
    private readonly string _featurePagePath;

    public void NavigateToStart() {
      if (NavigateToStartPage == null) return;
      NavigateToStartPage(this,new EventArgs());
    }
  }
}