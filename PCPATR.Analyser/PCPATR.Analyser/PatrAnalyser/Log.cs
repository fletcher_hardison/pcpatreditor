using System.Collections.Generic;

namespace PCPATR.Analyser.PatrAnalyser
{
    public class Log 
    {
        public bool FailuresExit { get; set; }
        public List<PatrSentenceAnalysis> Sentences { get; set; }
    }
}
