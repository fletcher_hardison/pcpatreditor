﻿//  ***************************************************************
//  File name: OutsideWorld.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 01,01,2013
// ***************************************************************

namespace PCPATR.Analyser.PatrAnalyser
{
  public class OutsideWorld
  {
    public  const string DEBUG_CONFIG_PATH = @"..\..\pcpatr_editor_configurations.txt";
    public const string RELEASE_CONFIG_PATH = @"pcpatr_editor_configurations.txt";
    public const string CURRENT_FILE_PATH = "current_file_path";
    public const string LAST_LEXICON = "last_lexicon";
    public const string LAST_DATA_FILE = "last_data_file";
    public const string LAST_GRAMMAR_FILE = "last_grammar_file";
    public const string DEBUG_PATH_TO_TRANSFORM = @"..\..\..\PCPATR.Editor.FeatureSystem\PatrParser\PcPatrGrammar.xsl";
    public const string RELEASE_PATH_TO_TRANSFORM = "PcPatrGrammar.xsl";
    public const string DEBUG_DTD_LOCATION = @"..\..\Assets\PcPatrGrammar.dtd";
    public const string RELEASE_DTD_LOCATION = @"Assets\PcPatrGrammar";
   
    public static string GetPathToTransform()
    {
#if DEBUG
      return DEBUG_PATH_TO_TRANSFORM;
#else
      return RELEASE_PATH_TO_TRANSFORM;
#endif
    }

    public static string GetPathToConfigs()
    {
#if DEBUG
      return DEBUG_CONFIG_PATH;
#else
      return RELEASE_CONFIG_PATH;
#endif
    }

    public static string GetDtdLocation()
    {
     #if DEBUG
      return DEBUG_DTD_LOCATION;
#else
      return RELEASE_DTD_LOCATION;
#endif 
    }

    public OutsideWorld(ConfigReader configReader)
    {
      _configReader = configReader;

#if DEBUG
      _configReader.LoadConfiguration(DEBUG_CONFIG_PATH);
#else
      _configReader.LoadConfiguration(RELEASE_CONFIG_PATH);
#endif
    }

    private readonly ConfigReader _configReader;
  }
}