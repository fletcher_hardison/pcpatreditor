﻿//  ***************************************************************
//  File name: ParserControlViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 23,01,2013
// ***************************************************************

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.Linq;
using System.Xml.XPath;
using PCPATR.Editor.MasterLists.PatrParser.ParserComponents;
using PCPATR.Editor.MasterLists.ViewModels;
using SIL.PcPatrBrowser;

namespace PCPATR.Analyser.PatrAnalyser
{
  /// <summary>
  /// Allow user to select files and sentences to execute parser
  /// </summary>
  public class ParserControlViewModel
  {
    public ParserControlViewModel(PatrParserControl parserControl, ParseAnalyser analyser, LingTreeTreeControlNotifierService notifierService, ConfigReader configReader, ParseOptionsViewModel optionsViewModel) {

      _notifierService = notifierService;      
      _parserControl = parserControl;
      _analyser = analyser;
      _configReader = configReader;
      _optionsViewModel = optionsViewModel;
      FileToParse = _configReader[OutsideWorld.LAST_DATA_FILE];
      LexiconFile = _configReader[OutsideWorld.LAST_LEXICON];
      m_fsTransform = new XslCompiledTransform();
      m_fsTransform.Load("ShowFS.xsl");
      
      //SelectLexiconFile = new DelegateCommand(ChooseLexiconFile);
      //SelectFileToParse = new DelegateCommand(ChooseFileToParse);
      //RunParser = new DelegateCommand(RunParserOnSelectedFileCommandMethod);
      ////SelectGrammarFile = new DelegateCommand(ChooseGrammarFile);
      //LoadPreviousParseOutput = new DelegateCommand(ChoosePreviousParseDataToLoad);
      //DebugGrammar = new DelegateCommand(RunParseInDebugModeOnSelectFile);
      //OpenLexiconFileInTextEditor = new DelegateCommand(() => OpenFileInTextEditor(_lexiconFile));
      //OpenDataFileInTextEditor = new DelegateCommand(() => OpenFileInTextEditor(_fileToParse));
      //RunParserWithSelectedGrammar = new DelegateCommand(RunWithSelectedGrmFile);

    }

    private readonly XslCompiledTransform  m_fsTransform;
    

    public string FileToParse {
      get { return _fileToParse; }
      set {
        _fileToParse = value;
        _configReader[OutsideWorld.LAST_DATA_FILE] = value;
        SaveConfigurations(_configReader);

      }
    }

    public string LexiconFile {
      get { return _lexiconFile; }
      set {
        _lexiconFile = value;
        _configReader[OutsideWorld.LAST_LEXICON] = value;
        SaveConfigurations(_configReader);

      }
    }

    public string GrammarFile {
      get { return _grammarFile; }
      set { _grammarFile = value; }
    }

    public bool IsParserRunning { get; private set; }

    public bool ParsingFinished {
      get { return _parsingFinished; }
      private set {
        _parsingFinished = value;

      }
    }


    public ObservableCollection<PatrSentenceAnalysis> Sentences {
      get { return _analysisSentences; }
      set {
        _analysisSentences = value;

      }
    }

    public PatrSentenceAnalysis CurrentAnalysisSentence {
      get { return _selectedAnalysisSentence; }
      set {
        if (_selectedAnalysisSentence != null) {
          _selectedAnalysisSentence.SelectedParseChanged -= SelectedAnalysisSentenceSelectedParseChanged;
        }
        _selectedAnalysisSentence = value;
        _selectedAnalysisSentence.SelectedParseChanged += SelectedAnalysisSentenceSelectedParseChanged;

        GetCurrentPatrSentence();
      }
    }

    public PatrParseAnalysis CurrentAnalysisParse {
      get {
        return _selectedAnalysisSentence.SelectedParse;
      }
    }

    public ParseOptionsViewModel ParseOptions {
      get { return _optionsViewModel; }
    }

    public bool RunGrammarInDebugMode { get; set; }

    public void ChoosePreviousParseDataToLoad() {
      var result = string.Empty;//_fileSelector();
      if (result == null) return;
      GetAnalysisFromParseOutput(result);
    }

    

    public  void RunParserOnSelectedFileCommandMethod() {
      ParsingFinished = false;

      var outputPath = RunParserOnSelectedFile(_fileToParse, _lexiconFile, _grammarFile, _analyser, _parserControl, _optionsViewModel, 0, false);
      GetAnalysisFromParseOutput(outputPath);
      ParsingFinished = true;
    }

    public void RunParseInDebugModeOnSelectFile() {
      ParsingFinished = false;
      var outputPath = RunParserOnSelectedFile(_fileToParse, _lexiconFile, _grammarFile, _analyser, _parserControl, _optionsViewModel, 1, false);
      GetAnalysisFromParseOutput(outputPath);
      ParsingFinished = true;
    }

    /// <summary>
    /// computes the parser output from the file to parse, lexicon file, xml grammar file and returns path to parser output.
    /// </summary>
    /// <returns></returns>
    public static string RunParserOnSelectedFile(string fileToParse, string lexiconFile, string pathToXmlFile,
                                                 ParseAnalyser analyser,
                                                 PatrParserControl parserControl, ParseOptionsViewModel options,
                                                 long debugLevel, bool useDebugGrammar) {
      if (string.IsNullOrEmpty(fileToParse) | string.IsNullOrEmpty(lexiconFile)) return string.Empty;
      analyser.LoadDataFile(fileToParse);
      var pathToGrm = pathToXmlFile;
      if (pathToXmlFile.ToUpper().Contains(".XML")) {
        pathToGrm = pathToXmlFile.Replace(".xml", ".grm");
        var parameters = ParametersParsingMethods.ParseParameters(pathToXmlFile);
        IOHelperMethods.TransformGrammarXmlToGrmFile(pathToXmlFile, OutsideWorld.GetPathToTransform(), pathToGrm, useDebugGrammar,
                                                     parameters);
      }

      return RunParserOnSelectedGrmFile(fileToParse, lexiconFile, pathToGrm, parserControl, analyser, options, debugLevel);
    }

    public static string RunParserOnSelectedGrmFile(string fileToParse, string lexiconFile, string pathToGrmFile, PatrParserControl parserControl, Analyser.PatrAnalyser.ParseAnalyser analyser, ParseOptionsViewModel options, long debugLevel) {
      if (string.IsNullOrEmpty(fileToParse) | string.IsNullOrEmpty(lexiconFile)) return string.Empty;

      parserControl.ParseFile(fileToParse, lexiconFile, pathToGrmFile, PARSE_OUTPUT, options, debugLevel);

      return PARSE_OUTPUT;
    }



    private void GetAnalysisFromParseOutput(string pathToOutput) {
      _analysisModel = _analyser.AnalysePatrOutput(pathToOutput);

      string grammarFile;
      _pcPatrDocument = new PcPatrDocument(pathToOutput, out grammarFile);
      Sentences = new ObservableCollection<PatrSentenceAnalysis>(_analysisModel.PatrSentences);
    }

    public PatrParseNode CurrentNode {
      get { return _selectedNode; }
      set {
        _selectedNode = value;
      }
    }

    public string GetParseResultsAsHtml() {
      var sb = new StringBuilder();
      var sentenceNumber = 0;
      foreach (var sentenceAnalysis in Sentences) {
        sentenceNumber++;
        sb.Append(PatrParseAnalysisMethods.ConvertSentenceAnalysisToHtml(sentenceAnalysis, sentenceNumber));
      }

      return sb.ToString();
    }

    public string CurrentNodeFeatureStructure {
      get {
        if (_selectedNode == null) return string.Empty;
        if (_selectedNode.IsLeafNode) {
          return _selectedNode.FeatureStructure.FormattedRepresentation +
                 Environment.NewLine + "Lexical Features" + Environment.NewLine +
                 _selectedNode.LexicalFeatureStructure.FormattedRepresentation;
        }

        return _selectedNode.FeatureStructure.FormattedRepresentation;
      }
    }

    public void SetSenteceCurrentSentence(int index) {
      _selectedAnalysisSentence = _analysisSentences[Math.Min(index-1, _analysisSentences.Count - 1)];
    }

    public void SetCurrentParse(int index) {
      _selectedAnalysisSentence.SelectedParse =
        _selectedAnalysisSentence.Parses[Math.Min(index-1, _selectedAnalysisSentence.Parses.Count-1)];
      _notifierService.ShowParse(GetCurrentPatrSentence(), GetCurrentParseAnalysis());

    }
    

    public void SetParseAndSentenceFromIndecies(int sentenceIndex, int parseIndex) {
      SetSenteceCurrentSentence(sentenceIndex);
      SetCurrentParse(parseIndex);
    }

    private void SelectedAnalysisSentenceSelectedParseChanged(object sender, EventArgs e) {
      _notifierService.ShowParse(GetCurrentPatrSentence(), GetCurrentParseAnalysis());
    }


    private PcPatrSentence GetCurrentPatrSentence() {
      var index = _analysisModel.PatrSentences.IndexOf(_selectedAnalysisSentence) + 1;
      return _pcPatrDocument.GoToSentence(index);
      //RaisePropertyChanged("CurrentPatrSentence");
    }

    private PcPatrParse GetCurrentParseAnalysis() {
      var currentSentence = GetCurrentPatrSentence();
      if (currentSentence == null) return null;
      return currentSentence.GoToParse(_selectedAnalysisSentence.SelectedParseIndex);
    }

    public void ShowFSInfo(string sId, string filePath) {
      var parse = GetCurrentParseAnalysis();
      XmlNode featStruct = GetFeatureStructureAtNode(sId, parse);
      if (featStruct == null)
        return; // avoid crash

      XmlDocument doc1 = new XmlDocument();
      doc1.LoadXml(featStruct.OuterXml);
      XPathNavigator nav = doc1.CreateNavigator();

      XmlTextWriter writer = new XmlTextWriter(filePath, null);
      m_fsTransform.Transform(nav, null, writer, null);
      writer.Close();

      
    }


    public XmlNode GetFeatureStructureAtNode(string sNodeId, PcPatrParse parse) {
      string sId = sNodeId;
      string sFeatureStructureElement = "Fs";
      if (sNodeId.EndsWith("gloss")) {
        sId = sNodeId.Substring(0, sNodeId.Length - 5);
        sFeatureStructureElement = "Lexfs";
      }
      if (sNodeId.EndsWith("lex")) {
        sId = sNodeId.Substring(0, sNodeId.Length - 3);
        sFeatureStructureElement = "Lexfs";
      }
      string sAtId = "[@id='" + sId + "']/" + sFeatureStructureElement;
      string sChosenNodeXPath = "//Node" + sAtId + " | //Leaf" + sAtId;
      XmlNode featStructBasic = parse.Node.SelectSingleNode(sChosenNodeXPath);
      XmlNode featStruct = FleshOutFVals(featStructBasic, parse);
      return featStruct;
    }

    private XmlNode FleshOutFVals(XmlNode basic, PcPatrParse parse) {
      if (basic == null)
        return null;
      XmlNodeList nodes = basic.SelectNodes("descendant::F/@fVal");
      foreach (XmlNode node in nodes) {
        string sFVal = node.InnerText;
        XmlNode fleshed = GetFeatureStructureAtFs(sFVal, parse);
        if (fleshed != null) {
          // append fleshed to basic at right spot
          XmlNode f = node.SelectSingleNode("..");
          f.InnerXml = fleshed.OuterXml;
        }
      }
      return basic;
    }
    

    private XmlNode GetFeatureStructureAtFs(string sFsId, PcPatrParse parse) {
      string sFs = "//Fs[@id='" + sFsId + "']";
      if (parse.Node == null)
        return null; // avoid crash
      XmlNode featStructBasic = parse.Node.SelectSingleNode(sFs);
      XmlNode featStruct = FleshOutFVals(featStructBasic, parse);
      return featStruct;
    }





    private Action<string> BuildFailurePointCallBack() {
      return id => {
        var matchingFailurePoint = _selectedAnalysisSentence.SelectedParse.FailurePoints.FirstOrDefault(f => f.Id == id);
        // if (matchingFailurePoint == null) return;
        _selectedAnalysisSentence.SelectedParse.SelectedFailurePoint = matchingFailurePoint;
        var matchingNode = _selectedAnalysisSentence.SelectedParse.Nodes.FirstOrDefault(f => f.Id == id);
        CurrentNode = matchingNode;

        ShowFSInfo(id, _featurePagePath);

      };

    }

    private void SaveConfigurations(ConfigReader reader) {
#if DEBUG
      reader.SaveConfigurations(OutsideWorld.DEBUG_CONFIG_PATH);
#else
      reader.SaveConfigurations(OutsideWorld.RELEASE_CONFIG_PATH);
#endif
    }

    private void OpenFileInTextEditor(string filePath) {
      if (filePath == null) return;
      try {
        Process.Start(filePath);
      } catch {
        Process.Start("notepad.exe", filePath);
      }
    }

    private string _fileToParse;
    private string _lexiconFile;
    //private string _grammarFile;
    private bool _parsingFinished;
    private readonly PatrParserControl _parserControl;
    private readonly Analyser.PatrAnalyser.ParseAnalyser _analyser;
    private PatrParseNode _selectedNode;
    private PatrAnalysisData _analysisModel;
    private PcPatrDocument _pcPatrDocument;
    private PatrSentenceAnalysis _selectedAnalysisSentence;
    private ObservableCollection<PatrSentenceAnalysis> _analysisSentences;
    private readonly LingTreeTreeControlNotifierService _notifierService;

    private readonly ConfigReader _configReader;
    private readonly ParseOptionsViewModel _optionsViewModel;

    private string _grammarFile;

    private const string PARSE_OUTPUT = "ParseLog.log";
  }


  public class LingTreeTreeControlNotifierService
  {
    private static LingTreeTreeControlNotifierService _instnace;

    public static LingTreeTreeControlNotifierService GetInstance() {
      if (_instnace == null) {
        _instnace = new LingTreeTreeControlNotifierService();
      }
      return _instnace;
    }

    private LingTreeTreeControlNotifierService() {

    }

    public void RegisterEndPoint(LingTreeTreeWrapperControl endpoint) {
      _endPoint = endpoint;
    }

    public void RegisterStartPoint(Action<string> callBack) {
      _callBack = callBack;
    }

    public void ShowParse(PcPatrSentence sentence, PcPatrParse parse) {
      if (_endPoint == null) return;
      _endPoint.Refresh();
      _endPoint.ShowParseTree(sentence, parse);
    }

    public void SendIdToStartPoint(string id) {
      if (_callBack == null) return;
      _callBack(id);
    }

    private LingTreeTreeWrapperControl _endPoint;

    private Action<string> _callBack;
  }

  public static class ParametersParsingMethods
  {
    public static Dictionary<string, string> ParseParameters(string filePath) {
      var doc = XDocument.Load(filePath);
      var toParse = doc.Descendants("parameters");
      if (!toParse.Any()) return new Dictionary<string, string>();
      var parameters = toParse.First();
      var catFeature = parameters.Elements("categoryFeature").FirstOrDefault();
      var glossFeature = parameters.Elements("glossFeature").FirstOrDefault();
      var rootGlossFeature = parameters.Elements("rootGlossFeature").FirstOrDefault();
      var lexicalFeature = parameters.Elements("lexicalFeature").FirstOrDefault();


      var args = new Dictionary<string, string>();

      if (catFeature != null && !string.IsNullOrEmpty(catFeature.Value)) {
        args.Add("sCat", catFeature.Value);
      }
      if (glossFeature != null && !string.IsNullOrEmpty(glossFeature.Value)) {
        args.Add("sGloss", glossFeature.Value);
      }
      if (lexicalFeature != null && !string.IsNullOrEmpty(lexicalFeature.Value)) {
        args.Add("sLex", lexicalFeature.Value);
      }
      if (rootGlossFeature != null && !string.IsNullOrEmpty(rootGlossFeature.Value)) {
        args.Add("sRootGloss", rootGlossFeature.Value);
      }

      return args;

    }

  }
}