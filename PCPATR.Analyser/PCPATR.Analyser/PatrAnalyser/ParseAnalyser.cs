/* ***************************************************************
// * File name: ParseAnalyser.cs
// * Project: PCPATR.Services
// * Created by: 
// * Created on: 21,04,2012
// * In the name of Christ with thanks to God 
// */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using PCPATR.Editor.MasterLists.PatrParser;
using PCPATR.Editor.MasterLists.PatrParser.DataModel;

namespace PCPATR.Analyser.PatrAnalyser
{
  public class ParseAnalyser
  {
    public ParseAnalyser()
    {
      InputSentences = new List<string>();
    }

    /// <summary>
    /// holds sentences sent to parser
    /// </summary>
    public List<string> InputSentences { get; set; }

    /// <summary>
    /// Loads the data sent to the parser
    /// </summary>
    /// <param name="pathToInputData">path to parser input sentences</param>
    public void LoadDataFile(string pathToInputData)
    {
      if (pathToInputData == null) throw new ArgumentNullException("pathToInputData");
      if (System.IO.File.Exists(pathToInputData) == false)
        throw new
            ApplicationException(string.Format("File does not exist: {0}", pathToInputData));

      using (var reader = new System.IO.StreamReader(pathToInputData))
      {
        string line;

        while ((line = reader.ReadLine()) != null)
        {
          if (line == string.Empty) continue;

          if (line.Contains("|"))
          {
            line = line.Remove(line.IndexOf("|"));
            if (line == String.Empty) continue;
          }

          InputSentences.Add(line);
        }
      }
    }

    /// <summary>
    /// Analyses the parser output file.
    /// </summary>
    /// <param name="pathToOutput">Path to parser output file.</param>
    /// <returns>PatrAnalysisData object holing analysis results</returns>
    public PatrAnalysisData AnalysePatrOutput(string pathToOutput)
    {
      if (pathToOutput == null) throw new ArgumentNullException("pathToOutput");
      var analysisData = new PatrAnalysisData();
      if (!System.IO.File.Exists(pathToOutput)) return analysisData;

      var contents = LoadFileIntoString(pathToOutput);
      var placeInFile = MovePastFileHeader(contents);
      Tuple<string, string> analysisTuple;
      while ((analysisTuple = GetNextSentenceAndComments(contents, ref placeInFile)) != null)
      {
        var sentenceAnalysis = LoadSentenceAndCommentsIntoClass(analysisTuple.Item1);
        analysisData.PatrSentences.Add(AnalyzePatrSentence(analysisTuple.Item2, sentenceAnalysis));
      }

      var otherMessages = CheckForOtherMessages(contents, (analysisData.PatrSentences.Count < 1));
      if (otherMessages != null)
      {
        analysisData.PatrSentences.Add(otherMessages);
      }
      return analysisData;
    }

    public static PatrSentenceAnalysis CheckForOtherMessages(string analysisData, bool noOtherDataFound)
    {
      var errorsHolder = new PatrSentenceAnalysis();
      errorsHolder.Failed = true;
      errorsHolder.Sentence = "Parser Messages";
      var lexicalErrors = CheckForLexiconErrors(analysisData);

      if (lexicalErrors != null)
      {
        var parse = new PatrParseAnalysis();
        var node = new PatrParseNode { Cat = "Parser Messages" };
        node.FailedFeatures = lexicalErrors.Select(f => WrapErrorinParseFeature("Lexical", f)).ToList();
        parse.Nodes.Add(node);
        errorsHolder.Parses.Add(parse);
      }

      if (noOtherDataFound)
      {
        if (errorsHolder.Parses.Count < 1)
        {
          errorsHolder.Parses.Add(CreateNoParseDataFoundNode());
        }
      }
      return errorsHolder;
    }

    private static PatrParseAnalysis CreateNoParseDataFoundNode()
    {
      var parse = new PatrParseAnalysis();
      var node = new PatrParseNode { Cat = "No data found!" };
      //Thank you to Amy Hardison for the humor of this next statement.
      var feature = new PatrParseFeature("EPIC ", "", "FAIL - no parse data could be extracted.",
                                         new List<PatrParseFeature>());
      node.FailedFeatures.Add(feature);
      parse.Nodes.Add(node);

      return parse;
    }

    private static PatrParseFeature WrapErrorinParseFeature(string errorType, string errorContent)
    {
      return new PatrParseFeature { Content = "FAIL:" + errorContent, Name = errorType };
    }

    public static IEnumerable<string> CheckForLexiconErrors(string analysisData)
    {
      var startOfAnalysisData = analysisData.IndexOf("in the lexicon");
      if (startOfAnalysisData < 0) return null;

      var lexicalMessage = analysisData.Substring(startOfAnalysisData);

      var reader = new StringReader(lexicalMessage);
      var result = new List<string>();
      string line;
      while (!string.IsNullOrEmpty((line = reader.ReadLine())))
      {
        if (!result.Contains(line))
        {
          result.Add(line);
        }
      }

      return result;
    }

    public static int MovePastFileHeader(string sDoc)
    {
      return sDoc.IndexOf("\r\n") + 2; //header should be on first line
    }

    private const string ANALYSIS_CLOSE_MARKER = "</Analysis>";
    private const string ANALYSIS_OPEN_MARKER = "<Analysis";

    private static Tuple<string, string> GetNextSentenceAndComments(string sDoc, ref int startLoc)
    {

      if (sDoc.Length < startLoc | startLoc < 0)
      {
        startLoc = -1; //nothing there
        return null;
      }

      var workingDoc = sDoc.Substring(startLoc);
      var iAnalysisStart = workingDoc.IndexOf(ANALYSIS_OPEN_MARKER);
      if (iAnalysisStart == -1) return null;
      var iAnalysisEnd = workingDoc.IndexOf(ANALYSIS_CLOSE_MARKER) + 13;  // 13 is for skipping past the </Analysis> + nl
      var headerStart = iAnalysisStart - 1 < 0 ? iAnalysisStart : iAnalysisStart - 1;
      var header = workingDoc.Substring(0, headerStart);
      startLoc += iAnalysisEnd;
      var analysis = workingDoc.Substring(iAnalysisStart, Math.Max(iAnalysisEnd - iAnalysisStart, 1));

      return Tuple.Create(header, analysis);
    }

    public static PatrSentenceAnalysis LoadSentenceAndCommentsIntoClass(string header)
    {
      var sentence = new PatrSentenceAnalysis();
      using (var reader = new StringReader(header))
      {

        string line = null;

        while ((line = reader.ReadLine()) != null)
        {
          if ((string.IsNullOrEmpty(line) || line[0] == '|')) continue;
          if (!(line.Contains("****") | line.Contains("|")))
          {
            sentence.Sentence = line;
          }
          else
          {
            sentence.Comments.Add(line);
          }
        }
      }

      return sentence;
    }


    ///// <summary>
    ///// Get XML of next analysis element in document
    ///// </summary>
    ///// <param name="sDoc">Document text file</param>
    ///// <param name="iBegin">index of where to begin looking;  is set at end of the next analysis; if negative, it indicates nothing was found</param>
    ///// <returns>xml node containing the analysis element</returns>
    ///// From PcPatrDocument.cs by H. Andrew Black
    //protected XmlNode GetNextSentence(string sDoc, ref int iBegin)
    //{
    //  if (iBegin >= 0)
    //  {
    //    string sText = sDoc.Substring(iBegin);
    //    int iBeg = sText.IndexOf("<Analysis");
    //    int iEnd = sText.IndexOf("</Analysis>") + 13;  // 13 is for skipping past the </Analysis> + nl
    //    if (iBeg > 0)
    //    {
    //      iBegin += iEnd;
    //      string sXml = sText.Substring(iBeg, iEnd - iBeg);
    //      XmlDocument doc = new XmlDocument();
    //      doc.LoadXml(sXml);
    //      return doc.SelectSingleNode("/");
    //    }
    //    else
    //    {
    //      iBegin = -1; // nothing to find; make sure index is negative
    //    }
    //  }
    //  return null;
    //}


    private string LoadFileIntoString(string filePath)
    {
      return System.IO.File.ReadAllText(filePath);
    }

    /// <summary>
    /// Reads xAnalysis data from xml node into a new PatrSentenceAnalysis object
    /// </summary>
    /// <param name="sentence">Xml Node holding data to be read</param>
    /// <returns>new PatrSentenceAnalysis object with data loaded</returns>
    private static PatrSentenceAnalysis AnalyzePatrSentence(string sentenceString, PatrSentenceAnalysis analysis)
    {
      var sentence = LoadStringIntoXmlNode(sentenceString);

      var xmlAna = sentence.SelectSingleNode("./" + PatrNames.ANALYSIS);

      if (xmlAna != null)
      {
        ReadAnalysisAttributes(ref analysis, xmlAna);
        var isBush = CheckForBush(sentence, ref analysis);
        ReadPatrAnalysis(ref analysis, xmlAna, isBush);
      }

      return analysis;
    }

    private static bool CheckForBush(XmlNode sentence, ref PatrSentenceAnalysis analysis)
    {
      var bushTop = sentence.SelectSingleNode("./Analysis/Parse/Node/@cat");
      if (bushTop == null) return false;
      if (bushTop.Value == "?")
      {
        analysis.Failed = true;
        return true;
      }

      return false;
    }

    private static XmlNode LoadStringIntoXmlNode(string x)
    {
      var doc = new XmlDocument();
      try
      {
        doc.LoadXml(x);
      }
      catch (XmlException ex)
      {
        return null;
      }

      return doc.SelectSingleNode("/");
    }


    private static void ReadPatrAnalysis(ref PatrSentenceAnalysis sentence, XmlNode xAnalysis, bool isBush)
    {
      if (xAnalysis == null) throw new ArgumentNullException("xAnalysis");
      if (sentence == null) throw new ArgumentNullException("sentence");

      XmlNodeList parses = xAnalysis.SelectNodes(".//" + PatrNames.PARSE);
      if (parses == null) return;

      foreach (XmlNode parse in parses)
      {
        sentence.Parses.Add(AnalysePatrParse(parse, isBush, xAnalysis));
      }
    }

    private static PatrParseAnalysis AnalysePatrParse(XmlNode parse, bool isBush, XmlNode doc)
    {
      if (parse == null) throw new ArgumentException("parse");
      var analysis = new PatrParseAnalysis { Nodes = GetChildNodes(parse, doc) };

      //analysis.FailurePoints = GetParseFailurePoints(parse);


      if (isBush)
      {
        analysis.FailurePoints.Add(BushFailurePoint(parse));
      }

      return analysis;
    }



    private static List<PatrParseNode> GetChildNodes(XmlNode parse, XmlNode doc)
    {
      var xNodes = parse.SelectNodes(".//Node|.//Leaf");
      if (xNodes == null) return new List<PatrParseNode>();

      var nodes = FEs.Map(xNodes, f => LoadDataIntoPatrParseNode(f, doc));

      return new List<PatrParseNode>(nodes);
    }

    private static PatrParseNode BushFailurePoint(XmlNode parse) // no reflection on any presidents intended :-)
    {
      var failurePoint = new PatrParseNode();
      var node = parse.SelectSingleNode("./Node");
      if (node == null)
      {
        failurePoint.Cat = "?";

        return failurePoint;
      }

      var cat = node.Attributes.GetNamedItem("cat");
      var id = node.Attributes.GetNamedItem("id");

      failurePoint.Cat = cat == null ? "?" : cat.Value;
      failurePoint.Id = id == null ? string.Empty : id.Value;

      return failurePoint;

    }

    private static List<PatrParseNode> GetParseFailurePoints(XmlNode xNode, XmlDocument doc)
    {
      var failures = new List<PatrParseNode>();

      var xFailNodes = xNode.SelectNodes(".//Node[@fail= 'true']");

      if (xFailNodes == null) return failures;

      foreach (XmlNode node in xFailNodes)
      {
        if (node.Attributes == null) continue;
        var failure = LoadDataIntoPatrParseNode(node, doc);

        GetFailedFeatures(ref failure, node, doc);

        failures.Add(failure);
      }

      return failures;
    }

    private static PatrParseNode LoadDataIntoPatrParseNode(XmlNode node, XmlNode doc)
    {
      var newNode = new PatrParseNode
      {
        Id = node.Attributes[PatrNames.ID] != null ? node.Attributes[PatrNames.ID].Value : null,
        RuleName = node.Attributes[PatrNames.RULE] != null ? node.Attributes[PatrNames.RULE].Value : null,
        All = node.Attributes[PatrNames.ALL] != null ? node.Attributes[PatrNames.ALL].Value : null,
        Cat = node.Attributes[PatrNames.CAT] != null ? node.Attributes[PatrNames.CAT].Value : null,
      };
      newNode.FeatureStructure = WalkFeatures(GetFeaturesNode(node), ref newNode, doc);
      newNode.FailedFeatures = GetFailedFeatures(newNode.FeatureStructure);

      if (node.Name == "Leaf")
      {
        newNode.LexicalFeatureStructure = WalkFeatures(GetLexicalFeaturesNode(node), ref newNode, doc);
      }
      return newNode;
    }

    private static XmlNode GetLexicalFeaturesNode(XmlNode node)
    {
      return node.SelectSingleNode("./Lexfs");
    }

    private static List<PatrParseFeature> GetFailedFeatures(PatrParseFeature structure)
    {
      var failedList = new List<PatrParseFeature>();
      if (structure.Failed)
      {
        failedList.Add(structure);
      }

      structure.ChildFeatures.ForEach(f => failedList.AddRange(GetFailedFeatures(f)));

      return failedList;
    }


    /// <summary>
    /// Reads failure point for which features failed.
    /// </summary>
    /// <param name="failurePoint">the PatrPArseFailurePoint object to hold data</param>
    /// <param name="xNode">The xml node holding data to be read to be read</param>
    private static void GetFailedFeatures(ref PatrParseNode failurePoint, XmlNode xNode, XmlDocument doc)
    {
      if (failurePoint == null) throw new ArgumentNullException("failurePoint");
      if (xNode == null) throw new ArgumentNullException("xNode");

      failurePoint.FeatureStructure = WalkFeatures(GetFeaturesNode(xNode), ref failurePoint, doc);
    }

    private static PatrParseFeature WalkFeatures(XmlNode features, ref PatrParseNode failurePoint, XmlNode doc)
    {
      var failure = new PatrParseFeature();
      failure.ChildFeatures = WalkChildFeatures(features, ref failurePoint, doc);

      return failure;
    }

    private static List<PatrParseFeature> WalkChildFeatures(XmlNode features, ref PatrParseNode failurePoint, XmlNode doc)
    {
      var children = new List<PatrParseFeature>();
      for (var i = 0; i < features.ChildNodes.Count; i++)
      {
        var node = features.ChildNodes[i];
        var child = LoadFeatureWithBasicData(node);
        EvaluateFeatureValue(node, ref child, ref failurePoint, doc);
        if (child.Failed) failurePoint.FailedFeatures.Add(child);
        children.Add(child);
      }

      return children;
    }

    private static PatrParseFeature LoadFeatureWithBasicData(XmlNode feature)
    {
      var failure = new PatrParseFeature();

      if (feature.Attributes == null) return failure;
      var name = feature.Attributes.GetNamedItem("name");
      if (name != null)
      {
        failure.Name = name.Value;
      }
      var id = feature.Attributes.GetNamedItem("id");
      if (id != null)
      {
        failure.Id = id.Value;
      }

      return failure;
    }


    private static XmlNode GetFeaturesNode(XmlNode source)
    {
      return source.SelectSingleNode("./Fs");
    }

    private static void EvaluateFeatureValue(XmlNode feature, ref PatrParseFeature failure, ref PatrParseNode failurePoint, XmlNode doc)
    {
      var childNode = feature.FirstChild;
      if (childNode == null)
      {
        var attFval = feature.Attributes.GetNamedItem("fVal");
        if (attFval == null) failure.Content = "Could not parse!";
        var referencedNode = doc.SelectSingleNode(string.Format("//*[@id='{0}']", attFval.Value));
        if (referencedNode == null) failure.Content = "Could not parse!";

        failure.ChildFeatures = WalkChildFeatures(referencedNode, ref failurePoint, doc);

        return;
      }

      switch (childNode.Name)
      {
        case "Fs":
          var Fs = WalkFeatures(childNode, ref failurePoint, doc);
          failure.ChildFeatures.AddRange(Fs.ChildFeatures);
          break;
        case "Str":
          failure.Content = GetStrNodeContent(childNode);
          break;
        case "Plus":
          failure.Content = "+";
          break;
        case "Minus":
          failure.Content = "-";
          break;
        case "Any":
          failure.Content = "Any";
          break;
        case "None":
          failure.Content = "None";
          break;
        case "Sym":
          failure.Content = "Symbol";
          break;
        case "Nbr":
          if (childNode.Attributes == null)
          {

            var childAtt = childNode.Attributes.GetNamedItem("value");
            if (childAtt == null) failure.Content = "Number";

            else failure.Content = childAtt.Value;
          }
          else failure.Content = "Number";
          break;
      }
    }

    private static string GetStrNodeContent(XmlNode strNode)
    {
      return strNode.InnerText;
    }


    /// <summary>
    /// Reads the attributes on the Analysis node from a sentence parse result
    /// </summary>
    /// <param name="sentence">PatrSentenceAnalysis object to hold data</param>
    /// <param name="xAnalysis">Xml Analysis node holding data to be read</param>
    private static void ReadAnalysisAttributes(ref PatrSentenceAnalysis sentence, XmlNode xAnalysis)
    {
      var xCountAtt = xAnalysis.Attributes[PatrNames.COUNT];
      if (xCountAtt != null)
      {
        sentence.AnalysesCount = Convert.ToInt64(xCountAtt.Value);
      }
      //todo, figure out why this attribut is null on sentence four
      var xFailAtt = xAnalysis.Attributes[PatrNames.FAIL];
      if (xFailAtt != null)
      {
        sentence.Failed = (xFailAtt.Value == "true");
      }




    }



  }

  public static class FEs
  {
    public static IEnumerable<T> Map<T>(XmlNodeList list, Func<XmlElement, T> func) {
      var output = new List<T>();
      foreach (XmlElement elem in list) {
        output.Add(func(elem));
      }

      return output;
    }
  }
}