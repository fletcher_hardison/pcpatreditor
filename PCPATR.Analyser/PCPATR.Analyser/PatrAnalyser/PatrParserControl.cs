using System;
using System.IO;
using System.Text;
using PCPATR.Editor.MasterLists.PatrParser;

namespace PCPATR.Analyser.PatrAnalyser
{
  public class PatrParserControl
  {
    /// <summary>
    /// Initializes a new instance of the PatrParserControl class.
    /// </summary>
    public PatrParserControl()
    {
      Parser = new PatrParser
                   {
                     CommentChar = '|',
                     CodePage = Encoding.UTF8.CodePage
                   };
    }

    private PatrParser Parser { get; set; }

    public string GrammarPath { get; set; }

    public string LexPath { get; set; }

    public bool ParserReady { get; private set; }

    public string Log { get; set; }

    public string WritePath { get; set; }

    public bool FailuresOn { get; set; }

    /// <summary>
    /// Sets up parser
    /// </summary>
    public void ReadyParser()
    {
      Parser.LoadGrammarFile(GrammarPath);
      Parser.LoadLexiconFile(LexPath, 0);
      
      Parser.Failures = 1;
      Parser.MaxAmbiguity = 5;
      Parser.TreeDisplay = (long)PATRDataDataTypes.eTreeDisplay.PATR_XML_TREE;

      ParserReady = true;
    }



    /// <summary>
    /// Parses the file provided by the file path property.
    /// </summary>
    /// <param name="filePath">string path to the file to be parsed</param>
    /// <remarks>prints the data on each line, any messages from the parser, then parse results</remarks>
    public string ParseFile(string filePath, string lexiconFile, string grammarFile, string logFile, Analyser.PatrAnalyser.ParseOptionsViewModel options, long debugLevel)
    {
      if (filePath == null)
        throw new ArgumentNullException("filePath");
      Log = logFile;
      if (Log != null)
        Parser.OpenLog(Log);
      GrammarPath = grammarFile;
      LexPath = lexiconFile;
      Parser.Clear();
      Parser.LoadGrammarFile(grammarFile);
      Parser.LoadLexiconFile(lexiconFile, 0); //0 here means not to append, but to remove all previous lexicon files then add this one.
      options.ReadyParser(Parser, debugLevel);
      //ReadyParser();
      
      if (File.Exists(filePath) == false)
        throw new ApplicationException(string.Format("File does not exist: {0}", filePath));

      const string xmlOut = "parserOutput.xml";
      
      Parser.ParseFile(filePath, xmlOut);

      if (Log != null)
        Parser.CloseLog();

      return xmlOut;
    }
  }
}