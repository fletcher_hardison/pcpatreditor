﻿//  ***************************************************************
//  File name: ParseOptionsViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 31,01,2013
// ***************************************************************

using System.Linq;
using PCPATR.Editor.MasterLists.PatrParser;

namespace PCPATR.Analyser.PatrAnalyser
{
  public class ParseOptionsViewModel 
  {
    public ParseOptionsViewModel()
    {
      //set up defaults
      _checkCycles = true;
      _commentCharacter = '|';
      _failuresPerSentence = 0;
      _maxAmbiguities = 10;
      _promoteDefaultAtoms = true;
      _showGloss = true;
      _sentenceFinalPunctuation = ". ; ! ?";
      _timeLimitInSeconds = 0;
      _unificationIsOn = true;
      _useTopDownFiltering = true;
      _useTimeLimit = false;
      _trimEmptyFeatures = true;
      _lexiconCategoryMarker = "\\c";
      _lexiconFeaturesMarker = "\\f";
      _lexiconGlossMarker = "\\g";
      _lexiconRecordMarker = "\\w";
      _lexiconRootGlossMarker = "\\r";
      _lexiconWordMarker = "\\w";
    }

    public bool CheckCycles
    {
      get { return _checkCycles; }
      set
      {
        _checkCycles = value;
        
      }
    }

    public char CommentCharacter
    {
      get { return _commentCharacter; }
      set
      {
        _commentCharacter = value;
      }
    }

    public int FailuresPerSentence
    {
      get { return _failuresPerSentence; }
      set
      {
        _failuresPerSentence = value;
      }
    }

    public string LexiconRecordMarker
    {
      get { return _lexiconRecordMarker; }
      set
      {
        _lexiconRecordMarker = value;
      }
    }

    public string LexiconWordMarker
    {
      get { return _lexiconWordMarker; }
      set
      {
        _lexiconWordMarker = value;
      }
    }

    public string LexiconRootGlossMarker
    {
      get { return _lexiconRootGlossMarker; }
      set
      {
        _lexiconRootGlossMarker = value;
      }
    }

    public string LexiconGlossMarker
    {
      get { return _lexiconGlossMarker; }
      set
      {
        _lexiconGlossMarker = value;
      }
    }

    public string LexiconFeaturesMarker
    {
      get { return _lexiconFeaturesMarker; }
      set
      {
        _lexiconFeaturesMarker = value;
      }
    }

    public string LexiconCategoryMarker
    {
      get { return _lexiconCategoryMarker; }
      set
      {
        _lexiconCategoryMarker = value;
      }
    }

    public int MaxAmbiguities
    {
      get { return _maxAmbiguities; }
      set
      {
        _maxAmbiguities = value;
      }
    }

    public long ParsingTimeLimitInSeconds
    {
      get { return _timeLimitInSeconds; }
      set
      {
        _timeLimitInSeconds = value;
      }
    }

    public bool PromoteAtomicLexicalFeaturesToOrdinaryStatus
    {
      get { return _promoteDefaultAtoms; }
      set
      {
        _promoteDefaultAtoms = value;
      }
    }

    public string SentenceFinalPunctuation
    {
      get { return _sentenceFinalPunctuation; }
      set
      {
        _sentenceFinalPunctuation = EnforceEachCharacterIsSeperatedByASpace(value);
      }
    }

    public bool ShowGloss
    {
      get { return _showGloss; }
      set
      {
        _showGloss = value;
      }
    }

    public bool TrimEmptyFeatures
    {
      get { return _trimEmptyFeatures; }
      set
      {
        _trimEmptyFeatures = value;
      }
    }

    public bool UnificationIsOn
    {
      get { return _unificationIsOn; }
      set
      {
        _unificationIsOn = value;
      }
    }

    public bool UseTopDownFiltering
    {
      get { return _useTopDownFiltering; }
      set
      {
        _useTopDownFiltering = value;
      }
    }


    public bool UseTimeLimitWhenParsing
    {
      get { return _useTimeLimit; }
      set
      {
        _useTimeLimit = value;
      }
    }

    public void ReadyParser(PatrParser parser, long debugingLevel)
    {
      parser.CheckCycles = _checkCycles ? 1 : 0;
      parser.CommentChar = (byte)_commentCharacter;
      parser.DebuggingLevel = 0;//debugingLevel;
      parser.Failures = _failuresPerSentence;
      parser.Gloss = _showGloss ? 1 : 0;
      parser.Unification = _unificationIsOn ? 1 : 0;
      parser.TopDownFilter = _useTopDownFiltering ? 1 : 0;
      parser.MaxAmbiguity = _maxAmbiguities;
      parser.PromoteDefaultAtoms = _promoteDefaultAtoms ? 1 : 0;
      parser.TimeLimit = _useTimeLimit ? _timeLimitInSeconds : 0;
      parser.TrimEmptyFeatures = _trimEmptyFeatures ? 0 : 32;
      parser.LexWordMarker = _lexiconWordMarker;
      parser.LexRootGlossMarker = _lexiconRootGlossMarker;
      //parser.LexRecordMarker = _lexiconRecordMarker;
      parser.LexGlossMarker = _lexiconGlossMarker;
      parser.LexFeaturesMarker = _lexiconFeaturesMarker;
      parser.LexCategoryMarker = _lexiconCategoryMarker;
      parser.SentenceFinalPunctuation = _sentenceFinalPunctuation;
      parser.TreeDisplay = (long)PATRDataDataTypes.eTreeDisplay.PATR_XML_TREE;
    }

    private string EnforceEachCharacterIsSeperatedByASpace(string input)
    {
      var inputList = input.Replace(" " ,"").ToCharArray();
      
      return inputList.Aggregate("",(working, nextChar) => working + " " + nextChar).Trim();
    }

    private bool _trimEmptyFeatures;
    private long _timeLimitInSeconds;
    private bool _promoteDefaultAtoms;
    private bool _useTopDownFiltering;
    private bool _checkCycles;
    private char _commentCharacter;
    private int _failuresPerSentence;
    private int _maxAmbiguities;
    private bool _showGloss;
    private bool _unificationIsOn;
    private bool _useTimeLimit;
    private string _sentenceFinalPunctuation;
    private string _lexiconWordMarker;
    private string _lexiconRootGlossMarker;
    private string _lexiconRecordMarker;
    private string _lexiconGlossMarker;
    private string _lexiconFeaturesMarker;
    private string _lexiconCategoryMarker;
  }
}