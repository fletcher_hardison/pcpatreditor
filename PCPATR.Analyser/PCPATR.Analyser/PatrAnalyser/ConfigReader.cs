﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCPATR.Analyser.PatrAnalyser
{
  public class ConfigReader
  {
    private Dictionary<string, string> _configurations;
    
    public void LoadConfiguration(string file_path)
    {
      _configurations = new Dictionary<string, string>();
      foreach (var value in GetFileContetns(file_path).Select(x => x.Split(new[]{"::"},StringSplitOptions.None))) {
        _configurations.Add(value[0], value[1]);
      }
    }

    public void SaveConfigurations(string file_path)
    {
      var builder = new StringBuilder();
      foreach (var kvp in _configurations) {
        builder.AppendLine(string.Format("{0}::{1}", kvp.Key, kvp.Value));
      }

      System.IO.File.WriteAllText(file_path, builder.ToString());
    }

    public string this[string key]
    {
      get { return _configurations[key]; }
      set { _configurations[key] = value; }
    }

    public string GetConfig(string key)
    {
      return _configurations[key];
    }

    private IEnumerable<string> GetFileContetns(string file_path)
    {
      return new List<string>(System.IO.File.ReadAllLines(file_path));
    }
  }
}
