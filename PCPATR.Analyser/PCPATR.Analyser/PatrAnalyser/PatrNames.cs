// /* ***************************************************************
// * File name: PatrNames.cs
// * Project: PCPATR.Services
// * Created by: 
// * Created on: 23,04,2012
// * In the name of Christ with thanks to God 
// */
namespace PCPATR.Editor.MasterLists.PatrParser
{
    public class PatrNames
    {
        public const string RULE = "rule";
        public const string ALL = "all";
        public const string CAT = "cat";
        public const string ANALYSIS = "Analysis";
        public const string PARSE = "Parse";
        public const string FAIL = "fail";
        public const string ID = "id";
        public const string SENTENCE = "sentence";
        public const string COUNT = "count";
        public const string NAME = "name";



    }
}