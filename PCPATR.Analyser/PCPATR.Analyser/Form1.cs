﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PCPATR.Analyser
{
  public partial class Form1 : Form
  {
    public Form1() {
      InitializeComponent();
      _controller = new PatrAnalyserController(Path.Combine(Application.StartupPath, @"InitFeature.htm"));
     
      _startPage = new StartPageControl(_controller) {Dock = DockStyle.Fill};
      _resultsPage = new ResultsPage(_controller) {Dock = DockStyle.Fill};
      NavPanel.Controls.Add(_startPage);
      _controller.NavigateToResultsPage += ControllerNavigateToResultsPage;
      _controller.NavigateToStartPage += ControllerNavigateToStartPage;
    }

    void ControllerNavigateToStartPage(object sender, EventArgs e) {
      if (NavPanel.Controls.Contains(_resultsPage)) {
        NavPanel.Controls.Remove(_resultsPage);
      }
      NavPanel.Controls.Add(_startPage);
    }

    void ControllerNavigateToResultsPage(object sender, EventArgs e) {
      if (NavPanel.Controls.Contains(_startPage)) {
        NavPanel.Controls.Remove(_startPage);
      }
      NavPanel.Controls.Add(_resultsPage);
      _resultsPage.Activate();
    }


    private readonly ResultsPage _resultsPage;
    private readonly StartPageControl _startPage;
    private readonly  PatrAnalyserController _controller;
  }
}
