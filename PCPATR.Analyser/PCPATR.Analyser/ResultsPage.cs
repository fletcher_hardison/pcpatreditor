﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PCPATR.Analyser.PatrAnalyser;
using PCPATR.Editor.MasterLists.PatrParser.ParserComponents;

namespace PCPATR.Analyser
{
  public partial class ResultsPage : UserControl
  {
    public ResultsPage(PatrAnalyserController controller) {
      InitializeComponent();
      _notifierService= LingTreeTreeControlNotifierService.GetInstance();

      _treeControl = new LingTreeTreeWrapperControl();
      _treeControl.Dock = DockStyle.Fill;
      _treeControl.m_tree.m_LingTreeNodeClickedEvent += MTreeMLingTreeNodeClickedEvent;
      //TODO figure out how to handle node click event
      _notifierService.RegisterEndPoint(_treeControl);
      splitContainer2.Panel1.Controls.Add(_treeControl);
      _controller = controller;
      resultsBrowser.ObjectForScripting = _controller;
      featureBrowser.ObjectForScripting = _controller;

      _controller.NavigateFeatureBrowser += (sender, args) => featureBrowser.Navigate(Path.Combine(Application.StartupPath, @"InitFeature.htm"));

    }

    void MTreeMLingTreeNodeClickedEvent(object sender, LingTree.LingTreeNodeClickedEventArgs e) {
      var node = e.Node;
      _notifierService.SendIdToStartPoint(node.Id);
    }


    public void Activate() {
      featureBrowser.Navigate(Path.Combine(Application.StartupPath, @"InitFeature.htm"));
      resultsBrowser.Navigate(Path.Combine(Application.StartupPath, @"InitResults.html"));
    }

    
    private readonly LingTreeTreeWrapperControl _treeControl;
    private readonly PatrAnalyserController _controller;
    private readonly LingTreeTreeControlNotifierService _notifierService;

    private void toolStripBack_Click(object sender, EventArgs e) {
      _controller.NavigateToStart();
    }
  }
}
