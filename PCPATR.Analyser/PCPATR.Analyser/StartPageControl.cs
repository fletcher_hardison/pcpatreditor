﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PCPATR.Analyser
{
  public partial class StartPageControl : UserControl
  {
    public StartPageControl(PatrAnalyserController controller) {
      InitializeComponent();
      _controller = controller;
      webBrowser1.ObjectForScripting = _controller;
      var navpoint = Path.Combine(Application.StartupPath, @"Start.html");
      webBrowser1.Navigate(navpoint);
    }
    
    private readonly PatrAnalyserController _controller;
  }
}
