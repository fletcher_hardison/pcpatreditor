﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.BlockEditor;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.Models.Rules;

namespace PCPATR.Editor.MasterLists.LexicalRules
{
  public class LexicalRulesRepositoryViewModel : ViewModelBase
  {
    public LexicalRulesRepositoryViewModel(ModelService<LexicalRule> modelService, IUnityContainer container, BlockMapper mapper, NewIdServiceViewModel idService)
    {
      _container = container;
      _mapper = mapper;
      _modelService = modelService;
      _idService = idService;
      _idIdentifiersList  = new ObservableCollection<string>();
      _modelService.SetManagedIdIdentifierCollection(_idIdentifiersList);
      _addRule = new DelegateCommand(AddLexicalRuleToList);
      _removeRule = new DelegateCommand(RemoveSelectedRuleFromList);
      _cancelAdd = new DelegateCommand(ClearProposedRuleFields);
      _saveEdits = new DelegateCommand(SaveEditsMethod);
      _blockEditor = new BlockEditorViewModel();
    }

    private ObservableCollection<string> _idIdentifiersList;
    public ObservableCollection<string> IdIdentifiersList
    {
      get { return _idIdentifiersList; }
      set
      {
        _idIdentifiersList = value;
        _modelService.SetManagedIdIdentifierCollection(_idIdentifiersList);
        RaisePropertyChanged("IdIdentifiersList");
      }
    }

    private string _selectedIdentifier;
    public string SelectedIdentifier
    {
      get { return _selectedIdentifier; }
      set
      {
        _selectedIdentifier = value;
        RaisePropertyChanged("SelectedIdentifier");
      }
    }

    private BlockEditorViewModel _blockEditor;
    public BlockEditorViewModel BlockEditor
    {
      get { return _blockEditor; }
      set
      {
        _blockEditor = value;
        RaisePropertyChanged("BlockEditor");
      }
    }

    public NewIdServiceViewModel IdServiceViewModel
    {
      get { return _idService; }
    }

    public DelegateCommand AddLexicalRule
    {
      get { return _addRule; }
    }

    public DelegateCommand RemoveLexicalRule
    {
      get { return _removeRule; }
    }

    public string ProposedIdentifier
    {
      get { return _proposedIdentifier; }
      set
      {
        _proposedIdentifier = value;
        RaisePropertyChanged("ProposedIdentifier");
      }
    }
    
    public DelegateCommand CancelAdd
    {
      get { return _cancelAdd; }
    }

    public DelegateCommand SaveEdits
    {
      get { return _saveEdits; }
    }

    private void AddLexicalRuleToList()
    {
      if (string.IsNullOrEmpty(_idService.ProposedId)) return;
      
      var rule = new LexicalRule { Id = _idService.ProposedId };

      _modelService.PersistModel(rule);
    }


    private void RemoveSelectedRuleFromList()
    {
      if (string.IsNullOrEmpty(_selectedIdentifier)) return;
      var id = _modelService.ExtractIdFromIdentifierIdString(_selectedIdentifier);
      _modelService.RemoveRule(id);
    }

    private void ClearProposedRuleFields()
    {
      _idService.ProposedId = string.Empty;
      ProposedIdentifier = string.Empty;
    }

    private void SaveEditsMethod()
    {
      var rule = _mapper.MapBlockToModelObject(BlockEditor.Content) as LexicalRule;
      _modelService.PersistModel(rule);
    }

    public IEnumerable<LexicalRule> GetLexicalRuleModels()
    {
      return _modelService.ModelDictionary.Values;
    }

    public void TryShowSelectedRule()
    {
      if (string.IsNullOrEmpty(_selectedIdentifier)) return;

      var rule = _modelService.GetModelFromIdentiferIdString(_selectedIdentifier);
      
      if (rule == null) return;

      var initContext = new BlockInitializationContext();
      initContext.AddContextElement<IUnityContainer>(_container);
      initContext.AddContextElement<LexicalRulesRepositoryViewModel>(this);
      BlockEditor.Context = initContext;

      BlockEditor.Content = _mapper.MapToBlock(rule, null, initContext) as ContainerBlock;
    }

    public void LoadModels(IEnumerable<LexicalRule> models)
    {
      _modelService.LoadModels(models);
    }

    private readonly DelegateCommand _addRule;
    private readonly DelegateCommand _removeRule;
    private readonly BlockMapper _mapper;
    private readonly IUnityContainer _container;
    private readonly ModelService<LexicalRule> _modelService;
    private readonly NewIdServiceViewModel _idService;
    private string _proposedIdentifier;
    private readonly DelegateCommand _cancelAdd;
    private readonly DelegateCommand _saveEdits;
  }
}