﻿using System.Windows.Input;

namespace PCPATR.Editor.MasterLists.LexicalRules
{
  /// <summary>
  /// Interaction logic for LexicalRulesView.xaml
  /// </summary>
  public partial class LexicalRulesView
  {
    public LexicalRulesView(LexicalRulesRepositoryViewModel viewModel)
    {
      InitializeComponent();
      _viewModel = viewModel;
      DataContext = _viewModel;
    }

    private readonly LexicalRulesRepositoryViewModel _viewModel;

    private void TempalateModelsList_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      _viewModel.TryShowSelectedRule();
    }
  }
}
