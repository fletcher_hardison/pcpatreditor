﻿using System;
using System.Windows.Input;
using PCPATR.Common;
using PCPATR.Editor.MasterLists.ViewModels;

/// <author>Fletcher Hardison</author>
namespace PCPATR.Editor.MasterLists
{
       /// <summary>
    /// Description of CommandViewModel.
    /// </summary>
    /// <remarks>
    /// from http://msdn.microsoft.com/en-us/magazine/dd419663.aspx#id0090055
    /// by Josh Smith.
    /// </remarks>
    public class CommandViewModel : ViewModelBase, ICommandViewModel
    {
        #region Constructors

        public CommandViewModel(string displayName, ICommand command)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            base.DisplayName = displayName;
            this.Command = command;
        }

        #endregion Constructors

        #region Properties

        public ICommand Command
        {
            get;
            private set;
        }

        #endregion Properties
    }
}