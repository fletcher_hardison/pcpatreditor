// /* ***************************************************************
// * File name: RulesRepositoryViewModel.cs
// * Project: RGE.Editor
// * Created by:
// * Created on: 09,05,2012
// * In the name of Christ with thanks to God
// */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using PCPATR.Common;
using PCPATR.Editor.MasterLists.BlockEditor;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.Models.Rules;
using PCPATR.Models.Rules.Constraints;
using PCPATR.Models.Rules.PSRElements;

namespace PCPATR.Editor.MasterLists.Rules.ViewModels
{
  public class RulesRepositoryViewModel : ViewModelBase
  {
    public event EventHandler ShowRuleDetails;
    private readonly BlockMapper _blockMapper;
    //private readonly PhraseStructureRuleMapper _psrMapper;
    private RuleViewModel _selectedRuleAsViewModel;

    private readonly IUnityContainer _container;

    private readonly ModelService<Rule> _ruleService;
    private readonly NewIdServiceViewModel _newIdService;

    public RulesRepositoryViewModel(IUnityContainer container, ModelService<Rule> ruleService, BlockMapper mapper, NewIdServiceViewModel newIdService)
    {
      _container = container;
      _newIdService = newIdService;
      _blockMapper = mapper;
      //_psrMapper = psrMapper;
      _ruleService = ruleService;
      _ruleIdentifiers = new ObservableCollection<string>();
      _ruleService.SetManagedIdIdentifierCollection(_ruleIdentifiers);
      AddRule = new DelegateCommand(AddRuleToList);
      RemoveRule = new DelegateCommand(RemoveSelected);
      CancelAdd = new DelegateCommand(NulloutProposedRuleData);
    }

    public DelegateCommand AddRule { get; set; }

    public DelegateCommand RemoveRule { get; set; }

    public DelegateCommand CancelAdd { get; private set; }


    public bool CanRemove { get { return !string.IsNullOrEmpty(_selectedIdentifier); } }

    public RuleViewModel SelectedRuleAsViewModel
    {
      get
      {
        return _selectedRuleAsViewModel;
      }
      set
      {
        _selectedRuleAsViewModel = value;
        RaisePropertyChanged("SelectedRuleAsViewModel");
      }
    }


    public NewIdServiceViewModel IdServiceViewModel { get { return _newIdService; } }

    private string _proposedIdentifier;
    public string ProporsedIdentifier
    {
      get { return _proposedIdentifier; }
      set
      {
        _proposedIdentifier = value;
        RaisePropertyChanged("ProporsedIdentifier");
      }
    }

    public IEnumerable<Rule> GetRuleModels()
    {
      return _ruleService.ModelDictionary.Values;
    }


    public IEnumerable<Rule> Repository
    {
      get
      {
        return _ruleService.ModelDictionary.Values;
      }
      set
      {
        LoadRuleService(value);
        RaisePropertyChanged("Repository");
      }
    }

    private string _selectedIdentifier;
    public string SelectedRuleIdentifier
    {
      get { return _selectedIdentifier; }
      set
      {
        _selectedIdentifier = value;
        RaisePropertyChanged("SelectedRuleIdentifier");
        RaisePropertyChanged("CanRemove");
      }
    }
    private ObservableCollection<string> _ruleIdentifiers;
    public ObservableCollection<string> RuleIdentifiers
    {
      get { return _ruleIdentifiers; }
      set
      {
        _ruleIdentifiers = value;
        RaisePropertyChanged("RuleIdentifiers");
      }
    }

    public ModelService<Rule> ModelService
    {
      get { return _ruleService; }
    }


    private void NulloutProposedRuleData()
    {
      _newIdService.ProposedId = string.Empty;
      ProporsedIdentifier = string.Empty;
    }

    public void LoadRuleService(IEnumerable<Rule> ruleList)
    {

      _ruleService.LoadModels(ruleList);
    }

    private void RemoveSelected()
    {
      if (!CanRemove) return;
      var id = _ruleService.ExtractIdFromIdentifierIdString(_selectedIdentifier);
      _ruleService.RemoveRule(id);
      SelectedRuleAsViewModel = null;
      RaisePropertyChanged("RuleIdentifiers");
    }

    private void AddRuleToList()
    {
      if (!_newIdService.IsValid | string.IsNullOrEmpty(_newIdService.ProposedId)) return;
      var newRule = _container.Resolve<Rule>();
      newRule.Id = _newIdService.ProposedId;
      newRule.Identifier = _proposedIdentifier;
      
      FillRuleWithDefaults(newRule);
      
      _ruleService.PersistModel(newRule);

      RaisePropertyChanged("RuleIdentifiers");

      NulloutProposedRuleData();
    }

    public void FillRuleWithDefaults(Rule rule)
    {
      var lhsId = rule.Id + "_lhs";
      var headId = rule.Id + "_head";
      
      var psr = _container.Resolve<PhraseStructureRule>();
      rule.PhraseStructureRule = psr;
      rule.PhraseStructureRule.LeftHandSide.Id = lhsId;
      
      var symbolRef = _container.Resolve<SymbolRef>();
      symbolRef.Id = headId;
      rule.PhraseStructureRule.RightHandSide.Add(symbolRef);
      
      var percolationOperation = _container.Resolve<PercolationOperationModel>();
      var leftPath = _container.Resolve<FeaturePathModel>();
      var rightPath = _container.Resolve<FeaturePathModel>();
      leftPath.node = new NameIdRef(lhsId);
      leftPath.feature_start = new NameIdRef("(feature ref)");
      rightPath.node = new NameIdRef(headId);
      rightPath.feature_start = new NameIdRef("(feature ref)");
      
      percolationOperation.left = leftPath;
      percolationOperation.right = rightPath;
      percolationOperation.enabled = true;
      rule.Constraints.Add(percolationOperation);
    }
    
    public void TryShowDetailsForSelectedRule()
    {
      if (string.IsNullOrEmpty(_selectedIdentifier)) return;
      var rule = _ruleService.GetModelFromIdentiferIdString(_selectedIdentifier);
      SelectedRuleAsViewModel = RuleHelperMethods.MapRuleToViewModel(rule, _container, _blockMapper);
      if (ShowRuleDetails != null)
      {
        ShowRuleDetails(this, new EventArgs());
      }
    }

    public void TryMapSelectedRuleToModel()
    {
      if (_selectedRuleAsViewModel != null)
      {
        _ruleService.PersistModel(RuleHelperMethods.MapRuleViewModelToModel(_selectedRuleAsViewModel, _container, _blockMapper));
      }
    }

    public void HandleListDoubleClick(object sender, MouseButtonEventArgs e)
    {
      TryShowDetailsForSelectedRule();
    }
  }



  public static class RuleHelperMethods
  {
    public static RuleViewModel MapRuleToViewModel(Rule source, IUnityContainer container, BlockMapper blockMapper)
    {
      var result = container.Resolve<RuleViewModel>();
      result.Id = source.Id;
      result.Identifier = source.Identifier;

      FillListFromSource(result.Comments.Comments, source.Comments);
      FillListFromSource(result.ConstraintsComments.Comments, source.ConstraintsComments);

      result.Enabled = source.enabled;
      result.UseWhenDebugging = source.use_when_debugging;

      var psrContext = new BlockInitializationContext();
      psrContext.AddContextElement<RuleViewModel>(result);
      psrContext.AddContextElement<IUnityContainer>(container);

      result.PsrEditor.Context = psrContext;

      if (source.PhraseStructureRule == null)
      {
        result.PsrEditor.Content = blockMapper.MapToBlock(new PhraseStructureRule(), null, psrContext) as ContainerBlock;
      }
      else
      {
        result.PsrEditor.Content =
          blockMapper.MapToBlock(source.PhraseStructureRule, null, psrContext) as ContainerBlock;
      }


      var constraintsContext = new BlockInitializationContext();
      constraintsContext.AddContextElement<RuleViewModel>(result);
      constraintsContext.AddContextElement<IUnityContainer>(container);

      result.ConstraintsEditor.Context = constraintsContext;

      if (source.Constraints == null)
      {
        result.ConstraintsEditor.Content = blockMapper.MapToBlock(new List<ConstraintModelBase>(), null, constraintsContext) as ContainerBlock;
      }
      else
      {
        result.ConstraintsEditor.Content =
          blockMapper.MapToBlock(source.Constraints, null, constraintsContext) as ContainerBlock;
      }

      return result;
    }

    public static Rule MapRuleViewModelToModel(RuleViewModel source,
                                               IUnityContainer container, BlockMapper blockMapper)
    {
      var result = container.Resolve<Rule>();
      result.Id = source.Id;
      result.Identifier = source.Identifier;
      FillListFromSource(result.Comments, source.Comments.Comments);
      FillListFromSource(result.ConstraintsComments, source.ConstraintsComments.Comments);

      result.use_when_debugging = source.UseWhenDebugging;
      result.enabled = source.Enabled;

      if (source.PsrEditor.Content != null)
      {
        result.PhraseStructureRule = blockMapper.MapBlockToModelObject(source.PsrEditor.Content) as PhraseStructureRule;
      }

      if (source.ConstraintsEditor.Content != null)
      {
        var resultList = new List<ConstraintModelBase>();
        foreach (var child in source.ConstraintsEditor.Content.Children)
        {
          resultList.Add(blockMapper.MapBlockToModelObject(child) as ConstraintModelBase);
        }
        result.Constraints = resultList;
      }

      return result;
    }

    private static void FillListFromSource<T>(ICollection<T> destination, IEnumerable<T> source)
    {
      destination.Clear();

      foreach (var comment in source)
      {
        destination.Add(comment);
      }
    }
  }
}