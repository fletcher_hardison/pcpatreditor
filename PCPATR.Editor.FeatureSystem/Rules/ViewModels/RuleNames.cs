﻿//  ***************************************************************
//  File name: RuleNames.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 11,01,2013
// ***************************************************************
namespace PCPATR.Editor.MasterLists.Rules.ViewModels
{
  public class RuleNames
  {
    public const string SYMBOL_REF = "Symbol Reference";
    public const string OPTIONAL_SYMBOLS = "Optional Symbols";
    public const string DISJUNCTION = "Disjunction";
    public const string DISJUNCT = "Disjunct";

  }
}