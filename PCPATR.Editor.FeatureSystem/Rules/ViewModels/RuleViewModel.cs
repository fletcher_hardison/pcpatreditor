//  ***************************************************************
//  File name: RuleViewModel.cs
//  Project: PCPATR.Editor.Rules
//  Created by: 
//  Created on: 15,06,2012
// ***************************************************************

using System;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.Commands;
using PCPATR.Editor.MasterLists.BlockEditor;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.Editor.MasterLists.Constraints.ViewModels;

namespace PCPATR.Editor.MasterLists.Rules.ViewModels
{
  public class RuleViewModel : ViewModelBase
  {
    public static Predicate<RuleViewModel> HasIdentifier = p => !string.IsNullOrEmpty(p.Identifier);
    public static ObservableCollection<string> ConstraintTypes = new ObservableCollection<string>{
          ConstraintViewModelNames.UNIFICATION_CONSTRAINT,
          ConstraintViewModelNames.PERCOLATION_OPERATION,
          ConstraintViewModelNames.PRIORITY_UNION_CONSTRAINT,
          ConstraintViewModelNames.LOGICAL_CONSTRAINT,
          ConstraintViewModelNames.CONSTRAINT_DISJUNCTION
        };
   
    public RuleViewModel(ValidNewIdGetter idService)
    {
      _idService = idService;
      Comments = new CommentsField();
      ConstraintsComments = new CommentsField();
      SetId = new DelegateCommand(SetNewId);
      ConstraintsEditor = new BlockEditorViewModel();
      PsrEditor = new BlockEditorViewModel();
    }


    public DelegateCommand remove_selected_constraint { get; private set; }

    public DelegateCommand SetId { get; private set; }

    public CommentsField Comments { get; private set; }

    public CommentsField ConstraintsComments { get; private set; }

    public BlockEditorViewModel ConstraintsEditor { get; set; }

    public BlockEditorViewModel PsrEditor { get; set; }

    public string Id
    {
      get { return _id; }
      set
      {
        _id = value;
        RaisePropertyChanged("Id");
      }
    }

    public string Identifier
    {
      get { return _identifier; }
      set
      {
        _identifier = value;
        RaisePropertyChanged("Identifier");
      }
    }

    public bool UseWhenDebugging
    {
      get { return _useWhenDebugging; }
      set
      {
        _useWhenDebugging = value;
        RaisePropertyChanged("UseWhenDebugging");
      }
    }

    public bool Enabled
    {
      get { return _enabled; }
      set
      {
        _enabled = value;
        RaisePropertyChanged("Enabled");
      }
    }

    private void SetNewId()
    {
      var newId = _idService();
      if (string.IsNullOrEmpty(newId)) return;
      Id = newId;
    }


    private bool _useWhenDebugging;
    private bool _enabled;
    private string _id;
    private string _identifier;
    private readonly ValidNewIdGetter _idService;
  }
}