using System.Windows.Controls;
using System.Windows.Input;
using PCPATR.Editor.MasterLists.BlockEditor;
using PCPATR.Editor.MasterLists.Rules.ViewModels;

namespace PCPATR.Editor.MasterLists.Rules.Views
{
  public partial class RuleDetailView 
  {
    public RuleDetailView() {
      
      InitializeComponent();
      KeepAlive = true;
    }

   

    public void LoadRule(RuleViewModel data)
    {
      DataContext = data;
    }

     
  }
}