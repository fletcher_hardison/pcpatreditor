﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PCPATR.Editor.MasterLists.Rules.ViewModels;
namespace PCPATR.Editor.MasterLists.Rules.Views
{
  /// <summary>
  /// Interaction logic for RulesListPage.xaml
  /// </summary>
  public partial class RulesListPage : Page
  {
    public RulesListPage(RulesRepositoryViewModel viewModel) {
      InitializeComponent();
      _viewModel = viewModel;
      DataContext = _viewModel;

      _viewSource = CollectionViewSource.GetDefaultView(_viewModel.RuleIdentifiers);
      _viewSource.Filter = Filter;
    }

    private bool Filter(object o)
    {
      if (string.IsNullOrEmpty(_rulesFilter)) return true;
      var ident = o as string;      
      return ident.Contains(_rulesFilter);
    }


    private void LboxRulesListBoxOnMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      _viewModel.HandleListDoubleClick(sender,e);
    }
    
    private void TboxRulesFilter_OnTextChanged(object sender, TextChangedEventArgs e)
    {
      _rulesFilter = TboxRulesFilter.Text;
      if (!string.IsNullOrEmpty(_rulesFilter))
      {
        _viewSource.Refresh();
      }
    }


    private readonly ICollectionView _viewSource;
    private readonly RulesRepositoryViewModel _viewModel;
    private string _rulesFilter;

    private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
    {
      _rulesFilter = string.Empty;
      _viewSource.Refresh();
    }
  }
}
