using System;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Models.Rules;
using PCPATR.Editor.MasterLists.Rules.ViewModels;

namespace PCPATR.Editor.MasterLists.Rules.Views
{
  /// <summary>
  /// Interaction logic for RulesMasterView.xaml
  /// </summary>
  public partial class RulesMasterView : INavigationAware, IRegionMemberLifetime
  {
    public RulesMasterView(RulesRepositoryViewModel rep, INavigationManager manager, IUnityContainer container)
    {
      InitializeComponent();
      _manager = manager;
      _viewModel = rep;
      DataContext = _viewModel;
      _listPage = container.Resolve<RulesListPage>();
      _detailView = container.Resolve<RuleDetailView>();
      RulesFrame.Navigate(_listPage);      
      _viewModel.ShowRuleDetails += ViewModelShowRuleDetails;

      _container = container;
    }

    void RulesFrameNavigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
    {
       if (e.Content.Equals(_listPage))
       {
         _viewModel.TryMapSelectedRuleToModel();
       }
    }

    private void ViewModelShowRuleDetails(object sender, EventArgs e)
    {
      _detailView.LoadRule(_viewModel.SelectedRuleAsViewModel);
      RulesFrame.Navigate(_detailView);
    }


    #region INavigationAware Members

    public void OnNavigatedTo(NavigationContext navigationContext)
    {      
      
    }

    public bool IsNavigationTarget(NavigationContext navigationContext)
    {
      return true;
    }

    public void OnNavigatedFrom(NavigationContext navigationContext)
    {
      IRegion region = _manager.RegionManager.Regions["DetailsRegion"];
      if (region is SingleActiveRegion) {
        object activeView = region.ActiveViews.SingleOrDefault();
        if (activeView != null) region.Deactivate(activeView);
      }
    }

    #endregion

    #region IRegionMemberLifetime Members

    public bool KeepAlive
    {
      get { return true; }
    }

    #endregion

    

  

    private readonly INavigationManager _manager;
    private readonly RulesRepositoryViewModel _viewModel;       
    private IUnityContainer _container;
    private RuleDetailView _detailView;
    private RulesListPage _listPage;
  }
}