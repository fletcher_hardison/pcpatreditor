﻿//  ***************************************************************
//  File name: CoreViewModelsModule.cs
//  Project: PCPATR.Editor.FeatureSystem
//  Created by: T. Fletcher Hardison
//  Created on: 25,05,2012
// ***************************************************************

using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using NMatrix.Schematron;
using PCPATR.Common;
using PCPATR.Common.Interfaces;
using PCPATR.Editor.MasterLists.BlockEditor;
using PCPATR.Editor.MasterLists.LexicalRules;
using PCPATR.Editor.MasterLists.PatrParser;
using PCPATR.Editor.MasterLists.Rules.ViewModels;
using PCPATR.Editor.MasterLists.Rules.Views;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.Editor.MasterLists.Views;
using PCPATR.Editor.MasterLists.Views.Dialogs;
using PCPATR.Editor.MasterLists.FeatureTemplateViewModels;
using PCPATR.Models.MasterLists;
using RelayCommand = PCPATR.Editor.MasterLists.Rules.ViewModels.RelayCommand;
using MessageBox = System.Windows.MessageBox;

namespace PCPATR.Editor.MasterLists
{
  public class CoreViewModelsModule : IModule
  {
    public CoreViewModelsModule(IRegionManager regionManager, IUnityContainer container)
    {
      _regionManager = regionManager;
      _container = container;
    }

    #region IModule Members

    public void Initialize()
    {
      //TODO uncomment the mainregion view stuff.
      RegisterViewModelsWithIoC();


      _regionManager.RegisterViewWithRegion("MainRegion", typeof (MainWindowView));
      _regionManager.RegisterViewWithRegion("DetailsRegion", typeof (StartPage));
      _regionManager.RegisterViewWithRegion("DetailsRegion", typeof (MasterListsView));
      _regionManager.RegisterViewWithRegion("DetailsRegion", typeof (RulesMasterView));
      _regionManager.RegisterViewWithRegion("DetailsRegion", typeof (RuleDetailView));
      _regionManager.RegisterViewWithRegion("DetailsRegion", typeof (ConstraintTemplatesView));
      _regionManager.RegisterViewWithRegion("DetailsRegion", typeof (FeatureTemplatesView));
      //LogicalFeature System Views
      _regionManager.RegisterViewWithRegion("TreeRegion", typeof (FeatureSystemTreeView));
      //_regionManager.RegisterViewWithRegion("FeatureInfoRegion", typeof(SelectedFeatureInfoView));

      //_regionManager.RegisterViewWithRegion("ActionsRegion", typeof(FeatureSystemActions));
    }

    #endregion

    public void RegisterViewModelsWithIoC()
    {
      _container.RegisterType<StartPage>(CoreConstants.START_PAGE_VIEW, new ContainerControlledLifetimeManager());
      _container.RegisterInstance<ValidNewIdGetter>(BuildValidNewIdGetter());
      _container.RegisterInstance<FeatureCreator>(() => _container.Resolve<FeatureViewModel>());
      _container.RegisterInstance<FeatureCreatorWithArg>(
        model => _container.Resolve<FeatureViewModel>(new ParameterOverride("feature", model)));
      LoadBlockGrammar();
      RegisterParserComponrntsWithIoC();
      RegisterServiceClassesWithIoC();
      RegisterGeneralViewModelClassesWithIoC();
      RegisterConstraintViewModelsWithIoC();
      RegisterRuleViewModelsWithIoC();
      RegisterConstraintTemplateClassesWithIoC();
      RegisterCollectionFeatureClassesWithIoC();
      RegisterMasterListsViewModelsWithIoC();
      RegisterParemetersClassesWithIoC();
      RegisterFeatureTemplateClassesWithIoC();
      RegisterLexicalRuleClassesWithIoC();
      RegisterViewsWithIoC();
      RegisterFunctionDelegatesWithIoC();
      _container.RegisterType<MainWindowViewModel>(new ContainerControlledLifetimeManager());

    }

    private void LoadBlockGrammar()
    {
      var grammar = BlockGrammar.GetInstance();

      BlockGrammarLoadMethods.ConfigureRuleConstraints(grammar);
      BlockGrammarLoadMethods.ConfigureRuleConstraintsPathElements(grammar);
      BlockGrammarLoadMethods.ConfigureLogicalConstraintTemplateElements(grammar);
      BlockGrammarLoadMethods.ConfigureLexicalReferenceElements(grammar);
      BlockGrammarLoadMethods.ConfigureFeatureStructureAndElements(grammar);
      BlockGrammarLoadMethods.ConfigureFeatureElements(grammar);
      BlockGrammarLoadMethods.ConfigureFeatureTemplates(grammar);
      BlockGrammarLoadMethods.ConfigureConstraintTemplatesAndElements(grammar);
      BlockGrammarLoadMethods.ConfigureLexicalRulesAndElements(grammar);
      BlockGrammarLoadMethods.ConfigurePhraseStructureRuleAndElements(grammar, _container);
      BlockGrammarActionLoaderMethods.LoadLexicalReferenceActions(grammar, _container);

      _container.RegisterInstance(grammar);
    }

    private void RegisterParserComponrntsWithIoC()
    {
      _container.RegisterInstance<ParseAnalyser>(new ParseAnalyser(OutsideWorld.GetPathToShowFsTransForm()));
      _container.RegisterInstance<LingTreeTreeControlNotifierService>(LingTreeTreeControlNotifierService.GetInstance());
      _container.RegisterType<IPatrParser, PatrParser.PatrParser>(new ContainerControlledLifetimeManager());
      _container.RegisterType<PatrParserControl>(new ContainerControlledLifetimeManager());
      _container.RegisterType<ParserControlViewModel>(new ContainerControlledLifetimeManager());
      _container.RegisterType<object, PatrParserView>(CoreConstants.PATR_PARSER_VIEW, new ContainerControlledLifetimeManager());

    }

    private void RegisterServiceClassesWithIoC()
    {
      var validator = new Validator();
      validator.AddSchema(System.IO.File.OpenRead(OutsideWorld.ReleaseSchematronLocation));
      _container.RegisterInstance<Validator>(validator);

      _container.RegisterInstance<ICargoBus>(new CargoBus());
      _container.RegisterType<INavigationManager, NavigationManager>(new ContainerControlledLifetimeManager());
      _container.RegisterType<NewIdService>(new ContainerControlledLifetimeManager());
      _container.RegisterType<IMessageRepository, MessageRepository>(new ContainerControlledLifetimeManager());
      _container.RegisterInstance<IMessageBus>(new MessageBus());
      _container.RegisterType<IMessageCenter, MessageCenter>(new ContainerControlledLifetimeManager());
      _container.RegisterType<BlockToObjectMapper>(new ContainerControlledLifetimeManager());
      _container.RegisterType<ObjectToBlockMapper>(new ContainerControlledLifetimeManager());
      _container.RegisterType<BlockMapper>(new ContainerControlledLifetimeManager());

    }

   

    private void RegisterCollectionFeatureClassesWithIoC()
    {
      _container.RegisterInstance<CollectionFeatureCreator>(() => _container.Resolve<CollectionFeatureViewModel>());
      _container.RegisterType<CollectionFeaturesMapper>(new ContainerControlledLifetimeManager());
      _container.RegisterType<CollectionFeatureViewModel>();
      _container.RegisterType<CollectionFeaturesRepositoryViewModel>(new ContainerControlledLifetimeManager());
    }

    private void RegisterLexicalRuleClassesWithIoC()
    {
      _container.RegisterType<LexicalRulesRepositoryViewModel>(new ContainerControlledLifetimeManager());
    }

    private void RegisterParemetersClassesWithIoC()
    {
      _container.RegisterType<ParametersViewModel>(new ContainerControlledLifetimeManager());
    }


    public void RegisterGeneralViewModelClassesWithIoC()
    {
      _container.RegisterType<OutsideWorld>(new ContainerControlledLifetimeManager());
      _container.RegisterType<RelayCommand>();
      _container.RegisterType<ICommandViewModel, CommandViewModel>();
      _container.RegisterType<ConfigReader>(new ContainerControlledLifetimeManager());
      _container.RegisterType<NewIdServiceViewModel>();
      _container.RegisterType<ParametersMapper>(new ContainerControlledLifetimeManager());
      _container.RegisterType<EnableDisableElementsViewModel>(new ContainerControlledLifetimeManager());
    }

    public void RegisterRuleViewModelsWithIoC()
    {
      //_container.RegisterType<PhraseStructureRuleMapper>();

      _container.RegisterType<RulesRepositoryViewModel>(new ContainerControlledLifetimeManager());
      _container.RegisterType<RuleViewModel, RuleViewModel>();
    }


    private void RegisterConstraintViewModelsWithIoC()
    {
    }

    private void RegisterConstraintTemplateClassesWithIoC()
    {
      _container.RegisterType<ConstraintTemplatesViewModel>(new ContainerControlledLifetimeManager());
    }



    private void RegisterMasterListsViewModelsWithIoC()
    {
      _container.RegisterType<FeatureViewModel>();
      _container.RegisterType<FeatureSystemViewModel>(new ContainerControlledLifetimeManager());
      _container.RegisterType<FeatureSystemViewModel>(new ContainerControlledLifetimeManager());

      _container.RegisterType<SymbolViewModel>();
      _container.RegisterType<SymbolsRepositoryViewModel>(new ContainerControlledLifetimeManager());
      _container.RegisterType<BuiltInPrimitivesViewModel>();
      _container.RegisterType<FeatureMapper>(new ContainerControlledLifetimeManager());
      _container.RegisterType<MasterListsViewModel>(new ContainerControlledLifetimeManager());
    }

    private void RegisterViewsWithIoC()
    {
      _container.RegisterType<object, RuleDetailView>(CoreConstants.RULE_DETAIL_VIEW);
      _container.RegisterType<object, RulesMasterView>(CoreConstants.RULES_MASTER_VIEW);
      _container.RegisterType<object, ConstraintTemplatesView>(CoreConstants.CONSTRAINT_TEMPLTES_VIEW);
      _container.RegisterType<AddSymbolRefDialog>();
      _container.RegisterType<FeatureTreeViewPopUp>();
      _container.RegisterType< ComboPopUp>();
      _container.RegisterType<NameIdRefListDialog>();
      _container.RegisterType<NameIdListDialog>();
      _container.RegisterType< NameIdListDialog>();
      _container.RegisterType<FeatureSystemTreeView>(new ContainerControlledLifetimeManager());
      _container.RegisterType<ListBoxPopup>();
      _container.RegisterType<FeatureValueDialog>();
      _container.RegisterType<FeatureValueControl>();
      _container.RegisterType<FilteredFeatureDialog>();
      _container.RegisterType<EnterNewIdWithValidationDialog>();
      _container.RegisterType<AddNewIdWithValidationControl>();
      _container.RegisterType<object, ParametersView>(CoreConstants.PARAMETERS_VIEW);
      _container.RegisterType<object, LexicalRulesView>(CoreConstants.LEXICAL_RULES_VIEW);
      _container.RegisterType<object, MasterListsView>(CoreConstants.MASTER_LISTS_VIEW);
      _container.RegisterType<object, SymbolsEditView>(CoreConstants.SYMBOLS_VIEW);
      _container.RegisterType<object, FeatureSystemView>(CoreConstants.FEATURE_SYSTEM_VIEW);
      _container.RegisterType<object, CollectionFeaturesView>(CoreConstants.COLLECTION_FEATURES_VIEW);
      _container.RegisterType<object, EnableDisableElementsView>(CoreConstants.ENABLE_DISABLE_ELEMENTS_VIEW);
    }

    private void RegisterFeatureTemplateClassesWithIoC()
    {
      _container.RegisterType<FeatureTemplatesRepositoryViewModel>(new ContainerControlledLifetimeManager());
    }

    private void RegisterFunctionDelegatesWithIoC()
    {
      var selector = NameIdListSelector();
      _container.RegisterInstance<NameIdListSelector>(selector);
      var refSelector = BuildNameIdRefListSelector();
      _container.RegisterInstance<NameIdRefListSelector>(refSelector);
      _container.RegisterInstance<EditSymbol>(BuildEditSymbol());
      _container.RegisterInstance<ConfirmationGetter>(BuildConfirmationGetter());
      _container.RegisterInstance<SymbolCreator>(() => _container.Resolve<Symbol>());
      var symbols = _container.Resolve<SymbolsRepositoryViewModel>();
      _container.RegisterInstance(symbols.GetNonTerminalSymbolSelector());
      var osw = _container.Resolve<OutsideWorld>();
      _container.RegisterInstance<IO<XmlDocument>>(osw.LoadGrammarCreator());
      _container.RegisterInstance<FeatureSelector>(CreateFeatureSelector());
      //var masterLists = _container.Resolve<MasterListsViewModel>();

      _container.RegisterInstance<NewFilePicker>(BuildNewFilePicker());
    }

    private ValidNewIdGetter BuildValidNewIdGetter()
    {
      return () => {
        var dialog = _container.Resolve<EnterNewIdWithValidationDialog>();
        var result = dialog.ShowDialog();
        if (result.HasValue && result.Value) {

          return dialog.ResultId;
        }

        return null;
      };
    }

    private NameIdListSelector NameIdListSelector()
    {
      return list => {
        var dialog = _container.Resolve<NameIdListDialog>();
        dialog.data = list;
        var result = dialog.ShowDialog();
        if (result == null) return null;

        return dialog.selected;
      };
    }

    private NameIdRefListSelector BuildNameIdRefListSelector()
    {
      return list => {
        var dialog = _container.Resolve<NameIdRefListDialog>();
        dialog.Data = new ObservableCollection<NameIdRef>(list);
        var result = dialog.ShowDialog();
        if (result == null) return null;

        return dialog.Selected;
      };
    }

    private FeatureSelector CreateFeatureSelector()
    {
      return () => {
        var dialog = _container.Resolve<FilteredFeatureDialog>();
        var featureSystem = _container.Resolve<FeatureSystemViewModel>();
        dialog.Features = featureSystem.Features;
        dialog.ShowDialog();

        if (dialog.SelectedFeature == null) return null;
        var selection = dialog.SelectedFeature;
        return new NameIdRef(selection.Id);
      };
    }



    private EditSymbol BuildEditSymbol()
    {
      return (symbol) => {
        var dialog = _container.Resolve<SymbolAddEditPopup>();
        var viewModel = _container.Resolve<SymbolViewModel>();
        viewModel.Data = symbol.Clone() as Symbol;
        dialog.Symbol = viewModel;
        dialog.ShowDialog();
        if (dialog.SaveResults) {

          return viewModel.Data;
        }

        return null;
      };
    }

    private ConfirmationGetter BuildConfirmationGetter()
    {
      return str => {
        var result = MessageBox.Show(str, "Confirm Action", MessageBoxButton.YesNo);
        if (result == MessageBoxResult.Yes) return true;

        return false;
      };
    }



    private NewFilePicker BuildNewFilePicker()
    {
      return () => {
        var dialog = new SaveFileDialog();
        dialog.Filter = "Xml files (*.xml) |*.xml";
        var result = dialog.ShowDialog();
        if (result == DialogResult.OK) return dialog.FileName;

        return null;
      };
    }


    private readonly IUnityContainer _container;
    private readonly IRegionManager _regionManager;
  }
}
