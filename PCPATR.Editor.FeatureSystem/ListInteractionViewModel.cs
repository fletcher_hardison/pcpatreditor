﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace PCPATR.Editor.MasterLists
{
    public class ListInteractionViewModel<T> : ViewModels.ViewModelBase
    {
        private ObservableCollection<T> _data;
        public ObservableCollection<T> Data
        { get { return _data; }
        set
        {
            if (_data == value) return;
            _data = value;
            this.RaisePropertyChanged("Data");
        }}

        private T _selectedElement;

        public T SelectedElement
        {
            get { return _selectedElement; }
            set
            {
                _selectedElement = value;
                this.RaisePropertyChanged("SelectedElement");
            }
        }

    }
}
