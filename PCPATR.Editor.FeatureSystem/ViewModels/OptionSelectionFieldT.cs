﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class OptionSelectionField<T> : ViewModelBase
  {
    public OptionSelectionField(IEnumerable<T> optionsList) {
      Options = new ObservableCollection<T>(optionsList);
    }


    public T OptionSelected {
      get { return _optionSelected; }
      set {

        _optionSelected = value;
        RaisePropertyChanged("OptionSelected");
        ShowPopup = false;
        RaisePropertyChanged("ShowPopup");
        RaiseSelectedOptionChanged(value);
        _optionSelected = default(T);
        RaisePropertyChanged("OptionSelected");
        RaiseClearSelection();
      }
    }

    public bool ShowPopup { get; set; }

    public bool DisplayField {
      get { return _displayField; }
      set {
        _displayField = value;
        RaisePropertyChanged("DisplayField");
      }
    }

    public ObservableCollection<T> Options { get; private set; }
    public event EventHandler<GenericEventArgs<T>> SelectedOptionChanged;
    public event EventHandler ClearSelection;


    private void RaiseSelectedOptionChanged(T value) {
      if (SelectedOptionChanged == null) return;

      SelectedOptionChanged(this, new GenericEventArgs<T>(value));
    }

    private void RaiseClearSelection() {
      if (ClearSelection != null) {
        ClearSelection(this, new EventArgs());
      }
    }

    private T _optionSelected;
    private bool _displayField;
  }
}