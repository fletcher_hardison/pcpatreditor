﻿//  ***************************************************************
//  File name: IOHelperMethods.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: Fletcher Hardision
//  Created on: 31,01,2013
// ***************************************************************

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using PCPATR.Common;
using PCPATR.Models;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public static class IOHelperMethods
  {
    /// <summary>
    /// computes the .grm file for a given xml grammar file using the specified xml transform. 
    /// </summary>
    /// <returns></returns>
    public static void TransformGrammarXmlToGrmFile(string xmlPath, string transformLocation, string outputPath,
                                                    bool debugging, Dictionary<string, string> parameters)
    {
      var xslt = new XslCompiledTransform(true);
     // Console.WriteLine("Transform found " + File.Exists(transformLocation));
      xslt.Load(transformLocation);

      var settings = new XmlReaderSettings
      {
        DtdProcessing = DtdProcessing.Parse,
        //ValidationType = ValidationType.DTD
      };

     // Console.WriteLine("DTD found " + File.Exists(Path.Combine(Path.GetDirectoryName(xmlPath), "PcPatrGrammar.dtd")));

      var args = CreateGrammarTransformArgumentList(debugging, parameters);

      if (File.Exists(outputPath))
      {
        File.Delete(outputPath);
      }
      

      using (var xmlReader = XmlReader.Create(xmlPath, settings))
      {
        
        using (var textWriter = File.CreateText(outputPath))
        {
          if (args == null) xslt.Transform(xmlReader, null, textWriter);
          else xslt.Transform(xmlReader, args, textWriter);

          textWriter.Flush();

          textWriter.Close();
        }
        xmlReader.Close();
      }

    }

    /// <summary>
    /// Creates the XsltArgumentsList to the grammar transfrom by reading the parameters values
    /// </summary>
    /// <param name="isDebugging"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public static XsltArgumentList CreateGrammarTransformArgumentList(bool isDebugging, Dictionary<string, string> parameters)
    {
      var parameterAdded = false;
      var args = new XsltArgumentList();
      if (isDebugging)
      {
        args.AddParam("bDoingDebugging", string.Empty, "Y");
        parameterAdded = true;
      }

      if (parameters != null)
      {
        foreach (var keyValuePair in parameters)
        {
          args.AddParam(keyValuePair.Key, string.Empty, keyValuePair.Value);
          parameterAdded = true;
        }
      }

      if (parameterAdded) return args;

      return null;
    }

    public static string CreateNewGrammarFile(NewFilePicker filePathChooser)
    {
      var result = filePathChooser();
      if (string.IsNullOrEmpty(result)) return null;
      var minimumDoc = GenerateMinimumGrammarXmlDocument();
      minimumDoc.Save(AppendXmlExtensionIfMissing(result));

      return result;
    }

    public static void CopyDtdFileToDir(string pathToGrammer)
    {
      var targetDir = Path.GetDirectoryName(pathToGrammer);
      var dtdLocation = OutsideWorld.GetDtdLocation();
      var dtdName = Path.GetFileName(dtdLocation);
      var targetFilePath = Path.Combine(targetDir, dtdName);
      if (File.Exists(targetFilePath))
      {
        File.Delete(targetFilePath);
      }
      File.Copy(dtdLocation, targetFilePath);
    }

    public static string AppendXmlExtensionIfMissing(string path)
    {
      var index = path.IndexOf(".xml");
      if (index < 0) path += ".xml";

      return path;
    }

    public static XDocument GenerateMinimumGrammarXmlDocument()
    {
      var doc = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"),
                              new XDocumentType("patrGrammar", null, "PcPatrGrammar.dtd", null),
                              new XElement(DataModelNames.PATR_GRAMMAR,
                                           new XElement(DataModelNames.RULES,
                                                        new XElement(DataModelNames.RULE,
                                                                     new XAttribute(DataModelNames.ID, "ruleS"),
                                                                     new XAttribute(DataModelNames.ENABLED, "yes"),
                                                                     new XAttribute(DataModelNames.USE_WHEN_DEBUGGING,
                                                                                    "no"),
                                                                     new XElement(DataModelNames.PHRASE_STRUCTURE_RULE,
                                                                                  new XElement(
                                                                                    DataModelNames.LEFT_HAND_SIDE,
                                                                                    new XAttribute(DataModelNames.ID,
                                                                                                   "ruleSlhs"),
                                                                                    new XAttribute(
                                                                                      DataModelNames.SYMBOL,
                                                                                      "S")
                                                                                    ),
                                                                                  new XElement(
                                                                                    DataModelNames.RIGHT_HAND_SIDE,
                                                                                    new XElement(
                                                                                      DataModelNames.SYMBOL_REF,
                                                                                      new XAttribute
                                                                                        (DataModelNames.ID,
                                                                                         "ruleSNP"),
                                                                                      new XAttribute
                                                                                        (DataModelNames.SYMBOL,
                                                                                         "NP")
                                                                                      ),
                                                                                    new XElement(
                                                                                      DataModelNames.SYMBOL_REF,
                                                                                      new XAttribute
                                                                                        (DataModelNames.ID,
                                                                                         "ruleSVP"),
                                                                                      new XAttribute
                                                                                        (DataModelNames.SYMBOL,
                                                                                         "VP")
                                                                                      )
                                                                                    )
                                                                       )
                                                          )
                                             ),
                                           new XElement(DataModelNames.MASTER_LISTS,
                                                        new XElement(DataModelNames.NONTERMINALS,
                                                                     new XElement(DataModelNames.NONTERMINAL,
                                                                                  new XAttribute(DataModelNames.ID, "S"),
                                                                                  new XElement(DataModelNames.NAME, "S")
                                                                       ),
                                                                     new XElement(DataModelNames.NONTERMINAL,
                                                                                  new XAttribute(DataModelNames.ID, "NP"),
                                                                                  new XElement(DataModelNames.NAME, "NP")
                                                                       ),
                                                                     new XElement(DataModelNames.NONTERMINAL,
                                                                                  new XAttribute(DataModelNames.ID, "VP"),
                                                                                  new XElement(DataModelNames.NAME, "VP")
                                                                       )
                                                          ),
                                                        new XElement(DataModelNames.TERMINALS,
                                                                     new XElement(DataModelNames.TERMINAL,
                                                                                  new XAttribute(DataModelNames.ID, "N"),
                                                                                  new XElement(DataModelNames.NAME, "N")
                                                                       ),
                                                                     new XElement(DataModelNames.TERMINAL,
                                                                                  new XAttribute(DataModelNames.ID, "V"),
                                                                                  new XElement(DataModelNames.NAME, "V")
                                                                       )),
                                                        new XElement(DataModelNames.BUILT_IN_PRIMATIVES,
                                                                     new XElement(DataModelNames.CAT,
                                                                                  new XAttribute(DataModelNames.ID,
                                                                                                 "pcpatrGrammarCat")),
                                                                     new XElement(DataModelNames.LEX,
                                                                                  new XAttribute(DataModelNames.ID,
                                                                                                 "pcpatrGrammarLex")),
                                                                     new XElement(DataModelNames.GLOSS,
                                                                                  new XAttribute(DataModelNames.ID,
                                                                                                 "pcpatrGrammarGloss")),
                                                                     new XElement(DataModelNames.ROOT_GLOSS,
                                                                                  new XAttribute(DataModelNames.ID,
                                                                                                 "pcpatrGrammarRootGloss")),
                                                                     new XElement(DataModelNames.NONE,
                                                                                  new XAttribute(DataModelNames.ID,
                                                                                                 "pcpatrGrammarNone"))
                                                          )
                                             )
                                )
        );

      return doc;
    }

    public static string GenerateTempCopyAndCleanFileToParse(string file_to_parse, char commentChar)
    {
      var sentenceMarker = commentChar + "@@sentence";
      var tempFileName = Path.GetTempFileName();
      var fileContents = File.ReadAllText(file_to_parse);

      fileContents = fileContents.Replace("\\t", string.Empty).Replace("\r", string.Empty);

      var lines = fileContents.Split('\n').Where(f => !string.IsNullOrWhiteSpace(f)).Where(f => !f.Contains("\\id"));
          
      //var result = lines.Aggregate<string>((working,nextStr) => working += nextStr.Trim() + " " + sentenceMarker + Environment.NewLine);

      File.WriteAllLines(tempFileName, lines);

      return tempFileName;
    }
  }
}