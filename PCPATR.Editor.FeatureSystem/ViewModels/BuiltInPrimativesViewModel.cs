﻿//  ***************************************************************
//  File name: BuiltInPrimatives.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: Fletcher Hardison
//  Created on: 09,06,2012
// ***************************************************************

using System.Collections.Generic;
using PCPATR.Common;
using PCPATR.Models.MasterLists;

namespace PCPATR.Editor.MasterLists.ViewModels
{
    public class BuiltInPrimitivesViewModel :ViewModelBase
    {
      public BuiltInPrimitivesViewModel(BuiltInPrimitives model)
      {
          DisplayName = "Built In Primatives";
        _model = model;
        _builtInPrimitivesListWithOutNull = new List<NameIdRef>(new []
        {
          new NameIdRef(_model.CatId.id), 
          new NameIdRef(_model.LexId.id), 
          new NameIdRef(_model.GlossId.id), 
          new NameIdRef(_model.RootGlossId.id)          
        });
      }

      public BuiltInPrimitives BuiltInPrimitives { get; set; }

      public List<NameIdRef> BuiltInPrimitivesWithOutNone { get { return _builtInPrimitivesListWithOutNull; } }

      private readonly List<NameIdRef> _builtInPrimitivesListWithOutNull;
      private readonly BuiltInPrimitives _model;
    }
}
