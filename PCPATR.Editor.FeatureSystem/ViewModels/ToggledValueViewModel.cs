﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class ToggledValueViewModel : ViewModelBase
  {
    public ToggledValueViewModel()
    {
      Comments = new CommentsField();
    }

    public bool HasValue
    {
      get { return _hasValue; }
      set
      {
        _hasValue = value;
        RaisePropertyChanged("HasValue");
      }
    }

    public string Value
    {
      get { return _value; }
      set
      {
        _value = value;
        RaisePropertyChanged("Value");
      }
    }

    public CommentsField Comments { get; set; }


    private bool _hasValue;
    private string _value;
  }
}
