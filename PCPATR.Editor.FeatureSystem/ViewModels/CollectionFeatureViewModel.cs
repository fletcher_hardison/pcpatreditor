﻿//  ***************************************************************
//  File name: CollectionFeatureViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 01,01,2013
// ***************************************************************

using Microsoft.Practices.Prism.Commands;
using PCPATR.Common;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class CollectionFeatureViewModel : ViewModelBase
  {
    public CollectionFeatureViewModel(ValidNewIdGetter idGetter)
    {
      _identifier = new NameId("New Collection Feature", "cfNew");
      _idGetter = idGetter;
      Comments = new CommentsField();
      SetId = new DelegateCommand(SetIdValue);
    }

    public CommentsField Comments { get; set; }

    public string Name
    {
      get { return _identifier.name; }
      set
      {
        _identifier.update_name(value);
        RaisePropertyChanged("Name");
      }
    }

    public string Id
    {
      get { return _identifier.id; }
      set
      {
        _identifier.id = value;
        RaisePropertyChanged("Id");
      }
    }

    public NameId Identifier
    {
      get { return _identifier; }
      set
      {
        _identifier = value;
        RaisePropertyChanged("Name");
        RaisePropertyChanged("Id");
      }
    }

    public string Description
    {
      get { return _description; }
      set
      {
        _description = value;
        RaisePropertyChanged("Description");
      }
    }

    public DelegateCommand SetId { get; set; }


    private void SetIdValue()
    {
      var result = _idGetter();
      if (string.IsNullOrEmpty(result)) return;
      Id = result;
    }


    private NameId _identifier;
    private string _description;
    private readonly ValidNewIdGetter _idGetter;
  }
}