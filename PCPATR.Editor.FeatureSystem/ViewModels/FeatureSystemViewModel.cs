//  ***************************************************************
//  File name: FeatureSystemViewModel.cs
//  Project: PCPATR.Editor.FeatureSystem
//  Created by: Fletcher Hardison
//  Created on: 25,05,2012
// ***************************************************************

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using System.Windows.Forms;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.Properties;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Editor.MasterLists.Views;





using PCPATR.Models.FeatureSystem;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public delegate void FeatureSystemChanged(FeatureSystemViewModel viewModel);

  public class FeatureSystemViewModel : ViewModelBase
  {
    public event FeatureSystemChanged FeaturesChanged;

    /// <summary>
    ///   Initializes a new instance of the FeatureSystemViewModel class.
    /// </summary>
    public FeatureSystemViewModel(ValidNewIdGetter id_getter,
                                  IUnityContainer container,
                                  FeatureCreator creator, FeatureCreatorWithArg creatorWithArg, NewIdService idService)
    {
      _container = container;
      _featureCreator = creator;
      _featureCreatorWithArg = creatorWithArg;
      _idService = idService;
      //_data = system;
      //LoadViewModels(system.Features, _featuresFlatList);
      _idGetter = id_getter;

      DisplayName = "Logical Feature System";

      AddNewSibling = new CommandViewModel("Add Sibling Feature", new RelayCommand(f => AddSibling()));
    }



    private XmlDocument _grammarDoc;
    public XmlDocument GrammarDoc
    {
      get { return _grammarDoc; }
      set
      {
        _grammarDoc = value;
        RaisePropertyChanged("GrammarDoc");
        FeaturesXmlNode = _grammarDoc.SelectSingleNode("//featureSystem");
      }
    }

    private XmlNode _featuresXmlNode;
    public XmlNode FeaturesXmlNode {
      get {return _featuresXmlNode ; }
      set{ _featuresXmlNode = value;
        RaisePropertyChanged("FeaturesXmlNode");}
    }
    
    public FeatureViewModel TargetFeatureForMovingFeature
    {
      get { return _targetFeatureForMoving; }
      set
      {
        _targetFeatureForMoving = value;
        RaisePropertyChanged("TargetFeatureForMovingFeature");
      }
    }

    #region IFeatureSystemViewModel Members

    public void FlattenFeaturesSystem(List<FeatureViewModel> dict, FeatureViewModel feature)
    {
      if (dict.All(f => f.Id != feature.Id)) dict.Add(feature);
      feature.Children.ForEach(f => FlattenFeaturesSystem(dict, f));
    }

    public FeatureSystem Data
    {
      get { return _data; }
      set
      {
        if (_data != value)
        {
          _data = value;
          RaisePropertyChanged("Data");
        }
      }
    }



    /// <summary>
    ///   Gets or sets the collection of FeatureViewModels for this class.
    /// </summary>
    public ObservableCollection<FeatureViewModel> Features
    {
      get { return _features; }
      set
      {
        if (value != _features)
        {
          _features = value;
          RaisePropertyChanged("Features");
        }
      }
    }

    /// <summary>
    ///   Gets or sets the selected feature for the view model
    /// </summary>
    public FeatureViewModel SelectedFeature
    {
      get { return _selectedFeature; }
      set
      {
        if (value != _selectedFeature & !_creatingFeatureReference)
        {
          _selectedFeature = value;
          RaisePropertyChanged("SelectedFeature");
        }
      }
    }



    #endregion


    /// <summary>
    ///   Loops through children of the searchSpace to find the parent feature of the target feature.
    /// </summary>
    /// <param name="searchSpace"> the node whose descendants are to be searched </param>
    /// <param name="target"> the FeatureViewModel being searched for </param>
    /// <returns> The LogicalFeature View Model containing the target as one of its children </returns>
    public static FeatureViewModel SearchFeaturesForParnetOfTarget(FeatureViewModel searchSpace, FeatureViewModel target)
    {
      if (searchSpace.Children.Contains(target)) return searchSpace;

      //foreach (var child in searchSpace.Children) {
      //  var result = SearchFeaturesForParnetOfTarget(child, target);
      //  if (result != null) return result;
      //}

      return searchSpace.Children.FirstOrDefault(f => SearchFeaturesForParnetOfTarget(f, target) != null);
    }

    public IEnumerable<FeatureViewModel> SearchFeatures(Predicate<FeatureViewModel> pred)
    {
      var result = new List<FeatureViewModel>();
      foreach (var feature in Features)
      {
        result.AddRange(SearchFeaturesRecusively(feature, pred));
      }

      return result;
    }

    public IEnumerable<FeatureViewModel> SearchFeaturesRecusively(FeatureViewModel searchSpace, Predicate<FeatureViewModel> pred)
    {

      var result = new List<FeatureViewModel>();
      if (pred(searchSpace))
      {
        result.Add(searchSpace);
      }

      foreach (var child in searchSpace.Children)
      {
        result.AddRange(SearchFeaturesRecusively(child, pred));
      }

      return result;
    }

    
    /// <summary>
    ///   Searches feature system for the feature with a given Identifier
    /// </summary>
    /// <param name="searchSpace"> LogicalFeature system to search </param>
    /// <param name="targetId"> Identifier to search for </param>
    /// <returns> FeatureViewModel Object with the target Identifier </returns>
    public static FeatureViewModel SearchFeaturesForId(FeatureViewModel searchSpace, string targetId)
    {
      foreach (FeatureViewModel child in searchSpace.Children)
      {
        if (child.Id == targetId) return child;
      }

      foreach (FeatureViewModel child in searchSpace.Children)
      {
        FeatureViewModel result = SearchFeaturesForId(child, targetId);

        if (result != null) return result;
      }

      return null;
    }

    //private void HandleFeatureInfoRq(IFeatureInfoRq rq)
    //{
    //  List<FeatureViewModel> results = SearchFeatureSystemByPredicate(Features, p => p.Id == rq.Id);

    //  if (results.Count > 1)
    //  {
    //    _messageBus.Publish(
    //      new PopupMessage("Grammar file Invalid: contains more than one feature with the same Identifier"));
    //  }

    //  if (results.Count == 1) rq.DisplayName = results[0].Name;
    //}


    public static void MoveExistingFeature(FeatureViewModel source, FeatureViewModel destination,
                                           FeatureViewModel featureToMove)
    {
      if (source == null)
      {
        throw
          new ArgumentNullException("source");
      }
      if (destination == null) throw new ArgumentNullException("destination");
      if (featureToMove == null) throw new ArgumentNullException("featureToMove");


      if (featureToMove == destination) return;
      if (destination.Children.Contains(featureToMove)) return;
      if (SearchFeaturesForParnetOfTarget(featureToMove, destination) != null) return;

      //source.Feature.Children.Remove(featureToMove.Feature);
      source.Children.Remove(featureToMove);
      //destination.Feature.Children.Add(featureToMove.Feature);
      destination.Children.Add(featureToMove);
    }


    #region Commands

    /// <summary>
    ///   Gets the list of commands this view model contains
    /// </summary>
    public ObservableCollection<ICommandViewModel> Commands
    {
      get
      {
        if (_commands == null)
        {
          _commands =
            new ObservableCollection<ICommandViewModel>
          {
            AddNewFeature,
            AddNewSibling,
            MoveFeature,
            AddBinaryFeature,
            RemoveFeature,
            CreateFeatureRef,
            //this.Save
          };
        }
        return _commands;
      }
    }

    public ICommandViewModel AddNewFeature
    {
      get
      {
        if (_addFeatureVm == null)
        {
          _addFeatureVm = new CommandViewModel("Add LogicalFeature",
                                               new RelayCommand(p => AddFeature()));
        }
        return _addFeatureVm;
      }
    }

    public ICommandViewModel MoveFeature
    {
      get
      {
        if (_moveFeature == null)
        {
          _moveFeature = new CommandViewModel("Move LogicalFeature",
                                              new RelayCommand(p => MoveExistingFeature()));
        }
        return _moveFeature;
      }
    }

    public ICommandViewModel AddBinaryFeature
    {
      get
      {
        if (_addBinaryFeature == null)
        {
          _addBinaryFeature = new CommandViewModel("Make LogicalFeature Binary",
                                                   new RelayCommand(p => CreateBinaryFeature(),
                                                                    p => CanMakeBinary));
        }
        return _addBinaryFeature;
      }
    }

    public ICommandViewModel RemoveFeature
    {
      get
      {
        if (_removeFeature == null)
        {
          _removeFeature = new CommandViewModel("Remove Selected LogicalFeature",
                                                new RelayCommand(p => RemoveExistingFeature(),
                                                                 p => CanEdit));
        }
        return _removeFeature;
      }
    }


    public ICommandViewModel CreateFeatureRef
    {
      get
      {
        if (_addFeatureRef == null)
        {
          _addFeatureRef = new CommandViewModel("Create LogicalFeature Reference",
                                                new RelayCommand(p => ReferToExistingFeature()));
        }
        return _addFeatureRef;
      }
    }

    public ICommandViewModel AddNewSibling { get; private set; }
    public bool CanEdit
    {
      get { return (SelectedFeature != null); }
    }

    public bool CanMakeBinary
    {
      get { return CanBeBinary(); }
    }

    /// <summary>
    ///   A feature can be made binary IF it does not have any children and it is not alread a plus or minus of a binary feature
    /// </summary>
    /// <returns> </returns>
    public bool CanBeBinary()
    {
      if (_selectedFeature == null) return false;
      if (_selectedFeature.Name == "+" | _selectedFeature.Name == "-") return false;
      if (_selectedFeature.Name.ToUpper() == "PLUS" | _selectedFeature.Name.ToUpper() == "MINUS") return false;
      if (_selectedFeature.IsReference) return false;
      if (_selectedFeature.Children.Count == 0) return true;

      return false;
    }

    private void AddSibling()
    {
      var newFeature = CreateNewFeatureMethod();

      if (_selectedFeature != null)
      {
        var parent = SearchFeatures(f => f.Children.Contains(_selectedFeature)).FirstOrDefault();
        if (parent != null)
        {
          parent.Children.Add(newFeature);
        }
        else
        {
          Features.Add(newFeature);
        }
      }
      else
      {
        Features.Add(newFeature);
      }

    }

    public void AddFeature()
    {
      var newFeature = CreateNewFeatureMethod();
      if (_selectedFeature != null)
      {
        _selectedFeature.Children.Add(newFeature);
      }
      else
      {
        Features.Add(newFeature);
      }
      RaiseFeaturesChanged();
    }

    private FeatureViewModel CreateNewFeatureMethod()
    {
      FeatureViewModel newFeature = _featureCreator();
      newFeature.Name = "newFeature";
      newFeature.Id = "newFeature";
      return newFeature;
    }

    /// <summary>
    ///   Removes an existing feature from the feature system. Child features are either deleleted as well or Reattached to the removed feature's parent.
    /// </summary>
    public void RemoveExistingFeature()
    {
      //Ask user "remove Children or just this node?"
      DialogResult answer =
        MessageBox.Show(Resources.FeatureSystemViewModel_RemoveExistingFeature_Remove_Child_features_,
                        Resources.FeatureSystemViewModel_RemoveExistingFeature_Remove_Feature,
                        MessageBoxButtons.YesNo);

      FeatureViewModel parent = SearchFeatures(f => f.Children.Contains(_selectedFeature)).FirstOrDefault();

      if (parent == null)
      {
        if (answer == DialogResult.No)
        {
          foreach (var featureViewModel in _selectedFeature.Children)
          {
            Features.Add(featureViewModel);
          }
        }

        _features.Remove(_selectedFeature);
      } else
      {
        if (answer == DialogResult.No)
        {
          foreach (FeatureViewModel child in _selectedFeature.Children)
          {
            //parent.Feature.Children.Add(child.ToFeature());
            parent.Children.Add(child);
          }
        }

        //parent.Feature.Children.Remove(_selectedFeature.ToFeature());
        parent.Children.Remove(_selectedFeature);
      }

      RaiseFeaturesChanged();
    }

    /// <summary>
    ///   Allows user to create FeatureRef to existing feature;
    /// </summary>
    public void ReferToExistingFeature()
    {
      _creatingFeatureReference = true;
      var referingFeature = _selectedFeature;
      var dialog = _container.Resolve<FeatureTreeViewPopUp>();
      dialog.ShowDialog();
      FeatureViewModel target = dialog.Result;
      if (target != null)
      {
        var reference = FeatureViewModel.CreateFeatureReference(target, referingFeature.Id, _idGetter,
                                                                _featureCreatorWithArg, _idService);
        reference.Id = _idGetter();
        referingFeature.Children.Add(reference);
      }
      _creatingFeatureReference = false;
      RaisePropertyChanged("Features");
      RaiseFeaturesChanged();
    }

    public void MoveExistingFeature()
    {
      if (_selectedFeature == null | _targetFeatureForMoving == null) return;

      if (_selectedFeature == _targetFeatureForMoving) return;

      if (FeatureChildrenContainsTarget(_selectedFeature, _targetFeatureForMoving)) return;

      MoveExistingFeature(SearchFeaturesForParnetOfTarget(Features[0], _selectedFeature), _targetFeatureForMoving,
                          _selectedFeature);
      RaiseFeaturesChanged();
    }

    public void CreateBinaryFeature()
    {
      var plus = _container.Resolve<FeatureViewModel>();
      var minus = _container.Resolve<FeatureViewModel>();

      plus.Name = "+";
      minus.Name = "-";

      plus.Id = _selectedFeature.Id + "plus";

      minus.Id = _selectedFeature.Id + "minus";

      SelectedFeature.Children.Add(plus);
      //_selectedFeature.Feature.Children.Add(plus.ToFeature());
      SelectedFeature.Children.Add(minus);
      //_selectedFeature.Feature.Children.Add(minus.ToFeature());
      RaiseFeaturesChanged();
    }

    private void RaiseFeaturesChanged()
    {
      if (FeaturesChanged != null)
      {
        FeaturesChanged(this);
      }
    }

    private static bool FeatureChildrenContainsTarget(FeatureViewModel searchSpace, FeatureViewModel target)
    {
      if (SearchFeaturesForParnetOfTarget(searchSpace, target) != null) return true;

      return false;
    }

    #endregion

    private readonly IUnityContainer _container;
    private readonly FeatureCreator _featureCreator;
    private readonly FeatureCreatorWithArg _featureCreatorWithArg;
    private readonly ValidNewIdGetter _idGetter;
    private ICommandViewModel _addBinaryFeature;
    private ICommandViewModel _addFeatureRef;
    private ICommandViewModel _addFeatureVm;
    private ObservableCollection<ICommandViewModel> _commands;
    private FeatureSystem _data;
    private ObservableCollection<FeatureViewModel> _features = new ObservableCollection<FeatureViewModel>();
    private ICommandViewModel _moveFeature;
    private ICommandViewModel _removeFeature;
    private FeatureViewModel _selectedFeature;
    private FeatureViewModel _targetFeatureForMoving;
    private bool _creatingFeatureReference;
    private readonly NewIdService _idService;
  }
}