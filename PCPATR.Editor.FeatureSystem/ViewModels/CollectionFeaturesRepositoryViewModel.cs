﻿//  ***************************************************************
//  File name: CollectionFeaturesRepositoryViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 01,01,2013
// ***************************************************************

using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.Commands;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class CollectionFeaturesRepositoryViewModel : ViewModelBase
  {
    public CollectionFeaturesRepositoryViewModel(CollectionFeatureCreator creator)
    {
      _features = new ObservableCollection<CollectionFeatureViewModel>();
      _creator = creator;
      AddCollectionFeature = new DelegateCommand(AddNewCollectionFeature);
      RemoveCollectionFeature = new DelegateCommand(RemoveSelectedCollectionFeature);
    }

    public ObservableCollection<CollectionFeatureViewModel> Features
    {
      get { return _features; }
      set
      {
        _features = value;
        RaisePropertyChanged("Features");
      }
    }
    public CollectionFeatureViewModel SelectedFeature
    {
      get { return _selectedFeature; }
      set
      {
        _selectedFeature = value;
        RaisePropertyChanged("SelectedFeature");
        RaisePropertyChanged("CanEdit");
      }
    }

    public bool CanEdit { get { return _selectedFeature != null; } }

    public DelegateCommand AddCollectionFeature { get; private set; }
    
    public DelegateCommand RemoveCollectionFeature { get; private set; }



    private void AddNewCollectionFeature()
    {
      _features.Add(_creator());
      RaisePropertyChanged("Features");
    }

    private void RemoveSelectedCollectionFeature()
    {
      if (_selectedFeature == null) return;
      _features.Remove(_selectedFeature);
      RaisePropertyChanged("Features");
      SelectedFeature = null;
    }

    private ObservableCollection<CollectionFeatureViewModel> _features;
    private CollectionFeatureViewModel _selectedFeature;
    private readonly CollectionFeatureCreator _creator;
  }
}