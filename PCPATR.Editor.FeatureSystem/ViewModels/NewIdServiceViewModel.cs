﻿//  ***************************************************************
//  File name: NewIdServiceViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 09,01,2013
// ***************************************************************

using PCPATR.Editor.MasterLists.Services;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class NewIdServiceViewModel : ViewModelBase
  {
    public NewIdServiceViewModel(NewIdService idService)
    {
      _idService = idService;
    }

    public bool IsValid
    {
      get { return _idService.IsProposedIdVaild(_proposedId); }
    }

    public string ProposedId
    {
      get { return _proposedId; }
      set
      {
        _proposedId = value;
        RaisePropertyChanged("ProposedId");
        RaisePropertyChanged("IsValid");
      }
    }

    public void RemoveId(string id)
    {
      _idService.RemoveId(id);
    }

    private string _proposedId;

    private readonly NewIdService _idService;
  }
}