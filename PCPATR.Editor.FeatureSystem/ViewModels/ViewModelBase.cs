﻿//  ***************************************************************
//  File name: ViewModelBase.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 25,05,2012
// ***************************************************************
using System.ComponentModel;
using System;


namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class ViewModelBase : INotifyPropertyChanged
  {
    private string _displayName;

    public string DisplayName {
      get { return _displayName; }
      set {
        _displayName = value;
        this.RaisePropertyChanged("DisplayName");
      }
    }

    #region INotifyPropertyChanged Members

    public event PropertyChangedEventHandler PropertyChanged;

    #endregion

    public void RaisePropertyChanged(string propertyName) {
      if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }

    public object Clone() {
      return this.MemberwiseClone();
    }
  }
}