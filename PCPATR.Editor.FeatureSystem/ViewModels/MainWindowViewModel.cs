﻿//  ***************************************************************
//  File name: MainWindowViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: Fletcher Hardison
//  Created on: 07,09,2012
// ***************************************************************

using System;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.FeatureTemplateViewModels;
using PCPATR.Editor.MasterLists.LexicalRules;
using PCPATR.Editor.MasterLists.Rules.ViewModels;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Models.MasterLists;
using PCPATR.Models.Parameters;
using Microsoft.Practices.Prism.Commands;
using PCPATR.Models.Rules;
using PCPATR.Common;
using PCPATR.Models.FeatureTemplates;
using System.Collections.Generic;
using NMatrix.Schematron;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class MainWindowViewModel : ViewModelBase
  {


    public MainWindowViewModel(IUnityContainer container,
                               INavigationManager nav_manager,
                               ConfigReader configReader,
                               EnableDisableElementsViewModel enableDisable,
                               IOService ioService,
                               ParametersMapper parametersMapper,
                               FeatureMapper featureMapper,
                               CollectionFeaturesMapper collectionFeaturesMapper,
                               NewFilePicker filePicker, Validator schematronChecker)
    {
      _container = container;
      _nav_manager = nav_manager;
      _newFilePicker = filePicker;
      _ioService = ioService;
      _reader = configReader;
      _parametersMapper = parametersMapper;
      _enableDisableElementsViewModel = enableDisable;
      _featureMapper = featureMapper;
      _collectionFeaturesMapper = collectionFeaturesMapper;
      _notifyUser = str => System.Windows.Forms.MessageBox.Show(str, "PCPATR.Editor: info");
      _schematronChecker = schematronChecker;
      //#if DEBUG
      //      LoadGrammar();
      //#endif
      PathToGrammarFile = _reader[OutsideWorld.CURRENT_FILE_PATH];

      ChooseGrammarFile = new DelegateCommand(ChooseGrammarFileMethod);
      show_rules = new DelegateCommand(NavigateToRulesPage);
      save = new DelegateCommand(SaveGrammarAndNotifyUser);
      show_constraint_templates = new DelegateCommand(NavigateToConstraintTemplatesPage);
      show_feature_templates = new DelegateCommand(NavigateToFeatureTemplates);
      ShowParameters = new DelegateCommand(NavigateToParameters);
      ShowLexicalRules = new DelegateCommand(NavigateToLexicalRules);
      ShowMasterLists = new DelegateCommand(NavigateToMasterLists);
      ShowParserPage = new DelegateCommand(NavigateToParserPage);
      ShowEnableDisableElementsPage = new DelegateCommand(NavigateToEnableDisableElementsPage);
      NewGrammar = new DelegateCommand(CreateNewGrammer);
      ExitApplication = new DelegateCommand(ExitApplicationMethod);

      GrammarLoaded = false;
    }

    public event EventHandler RequestApplicationClose;
    
    public bool GrammarLoaded { get; set; }

    public DelegateCommand ChooseGrammarFile { get; private set; }

    public DelegateCommand show_rules { get; private set; }

    public DelegateCommand show_constraint_templates { get; private set; }

    public DelegateCommand show_feature_templates { get; private set; }

    public DelegateCommand ShowParameters { get; private set; }

    public DelegateCommand ShowLexicalRules { get; private set; }

    public DelegateCommand ShowMasterLists { get; private set; }

    public DelegateCommand ShowParserPage { get; private set; }

    public DelegateCommand ShowEnableDisableElementsPage { get; private set; }

    public DelegateCommand save { get; private set; }

    public DelegateCommand NewGrammar { get; private set; }

    public DelegateCommand ExitApplication { get; private set; }

    public ObservableCollection<ICommandViewModel> Commands
    {
      get { return _commands; }
      set
      {
        if (_commands != value)
        {
          _commands = value;
          RaisePropertyChanged("Commands");
        }
      }
    }

    /// <summary>
    /// gets the parameters for the grammar.
    /// </summary>
    public ParametersViewModel Parameters
    {
      get { return _parametersViewModel; }
    }

    public string PathToGrammarFile { get; private set; }

    private void ExitApplicationMethod()
    {
      if (RequestApplicationClose != null)
      {
        RequestApplicationClose(this, new EventArgs());
      }
    }

    private void NavigateToRulesPage()
    {
      if (!GrammarLoaded) return;
      _nav_manager.RegionManager.Regions[MainWindowRegionNames.DETAILS_REGION].RequestNavigate(new Uri(CoreConstants.RULES_MASTER_VIEW, UriKind.Relative), f => f.Result.ToString());
    }

    private void NavigateToConstraintTemplatesPage()
    {
      if (!GrammarLoaded) return;
      _nav_manager.RegionManager.Regions[MainWindowRegionNames.DETAILS_REGION].RequestNavigate(new Uri(CoreConstants.CONSTRAINT_TEMPLTES_VIEW, UriKind.Relative), f => f.Result.ToString());

    }

    private void NavigateToFeatureTemplates()
    {
      if (!GrammarLoaded) return;
      _nav_manager.RegionManager.Regions[MainWindowRegionNames.DETAILS_REGION].RequestNavigate(new Uri(CoreConstants.FEATURE_TEMPLATES_VIEW, UriKind.Relative), f => f.ToString());
    }

    private void NavigateToParameters()
    {
      if (!GrammarLoaded) return;
      _nav_manager.RegionManager.Regions[MainWindowRegionNames.DETAILS_REGION].RequestNavigate(new Uri(CoreConstants.PARAMETERS_VIEW, UriKind.Relative), f => f.ToString());
    }

    private void NavigateToLexicalRules()
    {
      if (!GrammarLoaded) return;
      _nav_manager.RegionManager.Regions[MainWindowRegionNames.DETAILS_REGION].RequestNavigate(new Uri(CoreConstants.LEXICAL_RULES_VIEW, UriKind.Relative), f => f.ToString());
    }

    private void NavigateToMasterLists()
    {
      if (!GrammarLoaded) return;
      _nav_manager.RegionManager.Regions[MainWindowRegionNames.DETAILS_REGION].RequestNavigate(new Uri(CoreConstants.MASTER_LISTS_VIEW, UriKind.Relative), f => f.ToString());
    }

    private void NavigateToEnableDisableElementsPage()
    {
      if (!GrammarLoaded) return;
      _enableDisableElementsViewModel.LoadRules(_rules.GetRuleModels());
      _enableDisableElementsViewModel.LoadFeatureTemplates(_featureTemplatesRepository.GetTemplateModels());
      _nav_manager.RegionManager.Regions[MainWindowRegionNames.DETAILS_REGION].RequestNavigate(new Uri(CoreConstants.ENABLE_DISABLE_ELEMENTS_VIEW, UriKind.Relative),f => f.ToString());
    }

    private PatrParser.PatrParserView _parserPage;

    private void NavigateToParserPage()
    {
      if (_parserPage == null)
      {
        _parserPage  = _container.Resolve<PatrParser.PatrParserView>();
      }
      
      if (_parserPage.IsVisible)
      {
        _parserPage.Activate();
      } else
      {
        _parserPage = _container.Resolve < PatrParser.PatrParserView>();
        _parserPage.Show();
      }
      //_nav_manager.RegionManager.Regions[MainWindowRegionNames.DETAILS_REGION].RequestNavigate(new Uri(CoreConstants.PATR_PARSER_VIEW, UriKind.Relative), f => f.ToString());
    }

    public void LoadGrammar()
    {
      //_ioService.BeginIOOperation();
      //_ioService.LoadRules(BootstrapRules);
      //_ioService.LoadConstriantTemplates(BootstrapConstriantTemplates);
      //_ioService.LoadFeatureTemplates(BootstrapFeatureTemplates);
      //_ioService.LoadParameters(BootstrapParameters);
      //_ioService.LoadLexicalRules(BootstrapLexicalRules);
      //_ioService.LoadMasterLists(BootstrapMasterLists);
      //_notifyUser(_ioService.LoadGrammar());

      var patrGrammar = _ioService.LoadGrammarFromXmlDoc(PathToGrammarFile);
      _rules = _container.Resolve<RulesRepositoryViewModel>();
      _constraintTemplates = _container.Resolve<ConstraintTemplatesViewModel>();
      _featureTemplatesRepository = _container.Resolve<FeatureTemplatesRepositoryViewModel>();
      _parametersViewModel = _container.Resolve<ParametersViewModel>();
      _lexicalRulesViewModel = _container.Resolve<LexicalRulesRepositoryViewModel>();

      if (patrGrammar != null)
      {
        BootstrapRules(patrGrammar.Rules);
        BootstrapConstriantTemplates(patrGrammar.ConstraintTemplates);
        BootstrapFeatureTemplates(patrGrammar.FeatureTemplates);
        BootstrapParameters(patrGrammar.Parameters);
        BootstrapLexicalRules(patrGrammar.LexicalRules);
      }

      //needs to run anyway to make sure master list view model elements are set up correctly
      //has check to see if grammar is null internally
      BootstrapMasterLists(patrGrammar);

      _notifyUser("Data loaded sucessfully");
      _enableDisableElementsViewModel.PersistFeatureTemplatesChangesAction =
        PersistEnableDisableFeatureTemplates;
      _enableDisableElementsViewModel.PersistRuleChangesAction = PersistEnableDisableRules;

      GrammarLoaded = true;
      RaisePropertyChanged("GrammarLoaded");
    }

    private void PersistEnableDisableFeatureTemplates(IEnumerable<FeatureTemplateModel> changes)
    {
      foreach (var featureTemplateModel in changes)
      {
        _featureTemplatesRepository.ModelService.PersistModel(featureTemplateModel);
      }
    }

    private void PersistEnableDisableRules(IEnumerable<Models.Rules.Rule> changes)
    {
      foreach (var rule in changes)
      {
        _rules.ModelService.PersistModel(rule);
      }
    }

    private void SaveGrammarAndNotifyUser()
    {
      _notifyUser(SaveGrammarWithOutNotification());
    }

    public string SaveGrammarWithOutNotification()
    {
      //_ioService.BeginIOOperation();
      //_ioService.SaveFeatureTemplates(_featureTemplatesRepository.GetTemplateModels());
      //_ioService.SaveConstraintTemplates(_constraintTemplates.GetTemplateModels());
      //_ioService.SaveRules(_rules.Repository);
      ////_ioService.SaveFeatures(_featureMapper.MapRepositoryToModel(_masterListViewModel.FeatureSystem));
      //_ioService.SaveParameters(_parametersMapper.MapToModel(_parametersViewModel));
      //_ioService.SaveLexicalRules(_lexicalRulesViewModel.GetLexicalRuleModels());
      //_ioService.SaveMasterLists(GetMasterListsToSave());
      if (!GrammarLoaded) return "No file loaded!";
      var patrGrammar = new IOService.PcPatrGrammar
      {
        Rules = _rules.GetRuleModels(),
        ConstraintTemplates = _constraintTemplates.GetTemplateModels(),
        FeatureTemplates = _featureTemplatesRepository.GetTemplateModels(),
        LexicalRules = _lexicalRulesViewModel.GetLexicalRuleModels(),
        MasterLists = GetMasterListsToSave(),
        Parameters = _parametersMapper.MapToModel(_parametersViewModel)
      };

      var saveResult = _ioService.SaveGrammar(patrGrammar, PathToGrammarFile);
      try{
        //      var doc = new XPathDocument(PathToGrammarFile );
        _schematronChecker.Validate(System.IO.File.OpenRead(PathToGrammarFile));
        
        if (_schematronChecker.Context.HasErrors)
        {
          System.IO.File.WriteAllText(PathToGrammarFile.Replace(".xml", ".txt"), _schematronChecker.Context.Messages.ToString());
          
          return "Grammar is contains content errors";
        }
      }
      catch (Exception ex){
        System.IO.File.WriteAllText(PathToGrammarFile.Replace(".xml",".txt"), ex.ToString() );
      }
      
      return (saveResult) ? "Save sucessful" : "Save Filed";
    }

    private MasterListsModel GetMasterListsToSave()
    {
      var masterLists = _container.Resolve<MasterListsModel>();
      masterLists.GrammarFeatureSystem = _featureMapper.MapRepositoryToModel(_masterListViewModel.FeatureSystem);
      masterLists.GrammarCollectionFeatures =
        _collectionFeaturesMapper.MapRepositoryToModel(_masterListViewModel.CollectionFeatures);
      masterLists.Terminals = new List<Symbol>(_masterListViewModel.Symbols.TerminalSymbols);
      masterLists.NonTerminals = new List<Symbol>(_masterListViewModel.Symbols.NonTerminalSymbols);

      return masterLists;
    }

    private void BootstrapMasterLists(IOService.PcPatrGrammar grammar)
    {
      _masterListViewModel = _container.Resolve<MasterListsViewModel>();
      _masterListViewModel.Symbols = _container.Resolve<SymbolsRepositoryViewModel>();
      if (grammar != null)
      {
        var masterLists = grammar.MasterLists;
        var featureSystem = _featureMapper.MapRepositoryToViewModel(masterLists.GrammarFeatureSystem);        
        featureSystem.GrammarDoc = grammar.GrammarDoc;
        var symbols = _masterListViewModel.Symbols;
        symbols.NonTerminalSymbols = new ObservableCollection<Symbol>(masterLists.NonTerminals);
        symbols.TerminalSymbols = new ObservableCollection<Symbol>(masterLists.Terminals);
        _masterListViewModel.FeatureSystem = featureSystem;
        _masterListViewModel.CollectionFeatures =
          _collectionFeaturesMapper.MapRepositoryToViewModel(masterLists.GrammarCollectionFeatures);
      }
      if (_masterListViewModel.CollectionFeatures == null)
      {
        _masterListViewModel.CollectionFeatures = _container.Resolve<CollectionFeaturesRepositoryViewModel>();
      }
      if (_masterListViewModel.FeatureSystem == null)
      {
        _masterListViewModel.FeatureSystem = _container.Resolve<FeatureSystemViewModel>();
      }
    }

    private void BootstrapRules(IEnumerable<Models.Rules.Rule> rules)
    {
      _rules.LoadRuleService(rules);
    }

    private void BootstrapConstriantTemplates(IEnumerable<ConstraintTemplateModel> templates)
    {

      if (templates == null) return;
      _constraintTemplates.LoadModels(templates);
    }

    private void BootstrapFeatureTemplates(IEnumerable<FeatureTemplateModel> templates)
    {
      if (templates == null) return;
      _featureTemplatesRepository.LoadModels(templates);
    }

    private void BootstrapParameters(ParametersModel model)
    {
      _parametersViewModel = _parametersMapper.MapToViewModels(model);
    }

    private void BootstrapLexicalRules(IEnumerable<LexicalRule> repository)
    {

      if (repository == null) return;

      _lexicalRulesViewModel.LoadModels(repository);
    }

    private void ChooseGrammarFileMethod()
    {

      var openFileDialog = new OpenFileDialog();
      openFileDialog.Filter = "XML files (*.xml, *.XML)|*.xml;*.XML|All Files (*.*)|*.*";

      openFileDialog.ShowDialog();

      var choice = openFileDialog.FileName;

      if (string.IsNullOrEmpty(choice)) return;
      PathToGrammarFile = choice;
      LoadGrammar();
    }

    private void CreateNewGrammer()
    {
      var result = IOHelperMethods.CreateNewGrammarFile(_newFilePicker);
      if (string.IsNullOrEmpty(result)) return;

      PathToGrammarFile = result;
      IOHelperMethods.CopyDtdFileToDir(result);
      LoadGrammar();
    }

    

    
    //backing fields//
    private ObservableCollection<ICommandViewModel> _commands;
    //data//
    private RulesRepositoryViewModel _rules;
    private ConstraintTemplatesViewModel _constraintTemplates;
    private FeatureTemplatesRepositoryViewModel _featureTemplatesRepository;
    private ParametersViewModel _parametersViewModel;
    private LexicalRulesRepositoryViewModel _lexicalRulesViewModel;

    //private IMasterListsViewModel _masterLists;
    //services//
    private readonly IUnityContainer _container;
    private readonly INavigationManager _nav_manager;
    private readonly ConfigReader _reader;
    //functions//

    private readonly Action<string> _notifyUser;
    private readonly IOService _ioService;
    private readonly ParametersMapper _parametersMapper;
    private readonly FeatureMapper _featureMapper;
    private readonly CollectionFeaturesMapper _collectionFeaturesMapper;
    private MasterListsViewModel _masterListViewModel;
    private readonly NewFilePicker _newFilePicker;
    private readonly EnableDisableElementsViewModel _enableDisableElementsViewModel;
    private readonly Validator _schematronChecker;
  }
}