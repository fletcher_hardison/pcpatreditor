﻿//  ***************************************************************
//  File name: MasterListsViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: Fletcher Hardison
//  Created on: 09,06,2012
// ***************************************************************

using System;
using Microsoft.Practices.Prism.Commands;
using PCPATR.Editor.MasterLists.Services;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class MasterListsViewModel : ViewModelBase
  {
    public MasterListsViewModel(FeatureSystemViewModel featureSystem,
                                BuiltInPrimitivesViewModel builtInPrimitives,
                                SymbolsRepositoryViewModel symbols,
                                CollectionFeaturesRepositoryViewModel collectionFeatures,
                                INavigationManager navigationManager)
    {
      _navigationManager = navigationManager;
      _featureSystem = featureSystem;
      _builtInPrimitives = builtInPrimitives;
      _collectionFeatures = collectionFeatures;
      _symbols = symbols;
      ShowSymbols = new DelegateCommand(NavigateToSymbols);
      ShowFeatureSystem = new DelegateCommand(NavigateToFeatures);
      ShowCollectionFeatures = new DelegateCommand(NavigateToCollectionFeatures);
    }


    public DelegateCommand ShowSymbols { get; private set; }

    public DelegateCommand ShowFeatureSystem { get; private set; }

    public DelegateCommand ShowCollectionFeatures { get; private set; }


    public BuiltInPrimitivesViewModel BuiltInPrimitives
    {
      get { return _builtInPrimitives; }
      set
      {
        if (_builtInPrimitives != value) {
          _builtInPrimitives = value;
          RaisePropertyChanged("BuiltInPrimitives");
        }
      }
    }

    public FeatureSystemViewModel FeatureSystem
    {
      get { return _featureSystem; }
      set
      {
        if (_featureSystem != value) {
          _featureSystem = value;
          RaisePropertyChanged("FeatureSystem");
        }
      }
    }

    public SymbolsRepositoryViewModel Symbols
    {
      get { return _symbols; }
      set
      {
        if (_symbols != value) {
          _symbols = value;
          RaisePropertyChanged("Symbols");
        }
      }
    }

    public CollectionFeaturesRepositoryViewModel CollectionFeatures
    {
      get { return _collectionFeatures; }
      set
      {
        _collectionFeatures = value;
        RaisePropertyChanged("CollectionFeatures");
      }
    }


    private void NavigateToSymbols()
    {
      _navigationManager.RegionManager.Regions[MainWindowRegionNames.MASTER_LISTS_DETAIL_REGION].RequestNavigate(
        new Uri(CoreConstants.SYMBOLS_VIEW, UriKind.Relative), f => f.ToString());
    }

    private void NavigateToFeatures()
    {
      _navigationManager.RegionManager.Regions[MainWindowRegionNames.MASTER_LISTS_DETAIL_REGION].RequestNavigate(
        new Uri(CoreConstants.FEATURE_SYSTEM_VIEW, UriKind.Relative), f => f.ToString());
    }
    
    private void NavigateToCollectionFeatures()
    {
      _navigationManager.RegionManager.Regions[MainWindowRegionNames.MASTER_LISTS_DETAIL_REGION].RequestNavigate(
        new Uri(CoreConstants.COLLECTION_FEATURES_VIEW, UriKind.Relative), f => f.ToString());
    }

    private readonly INavigationManager _navigationManager;

    private BuiltInPrimitivesViewModel _builtInPrimitives;
    private CollectionFeaturesRepositoryViewModel _collectionFeatures;
    private FeatureSystemViewModel _featureSystem;
    private SymbolsRepositoryViewModel _symbols;
  }
}