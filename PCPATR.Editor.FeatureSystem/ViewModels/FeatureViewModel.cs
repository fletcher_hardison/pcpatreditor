//  ***************************************************************
//  File name: FeatureViewModel.cs
//  Project: PCPATR.Editor.UI
//  Created by: Fletcher Hardison
//  Created on: 18,05,2012
// ***************************************************************

using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.Commands;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Models.FeatureSystem;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class FeatureViewModel : ViewModelBase
  {
    private readonly NewIdService _idService;
    public FeatureViewModel(ValidNewIdGetter idGetter, FeatureCreatorWithArg creator, NewIdService idService)
    {
      _idGetter = idGetter;      
      _featureCreator = creator;
      _idService = idService;

      SetId = new DelegateCommand(SetIdValue);
      AddComment = new DelegateCommand(AddNewComment);
      RemoveComment = new DelegateCommand(RemoveSelectedComment);
      LoadData();

    }


    public DelegateCommand AddComment { get; private set; }

    public DelegateCommand RemoveComment { get; private set; }

    public DelegateCommand SetId { get; private set; }

    private string _id;
    public string Id
    {
      get { return _id; }
      set
      {
        if (_idService.ContainsId(_id))
        {
          _idService.RemoveId(_id);
        }
        
        _id = value;

        _idService.AddId(_id,_name);

        RaisePropertyChanged("Id");
      }
    }


    private string _description;
    public string Description
    {
      get { return _description; }
      set
      {
        _description = value;
        RaisePropertyChanged("Description");
      }
    }

    private string _name;
    public string Name
    {
      get { return _name; }
      set
      {
        _name = value;
        RaisePropertyChanged("Name");
      }
    }           

    public string NameOrId
    {
      get { return string.IsNullOrEmpty(Name) ? Id : Name; }
    }
    /// <summary>
    ///   Gets a value that determines whether the feature has comments.
    /// </summary>
    public bool HasComments
    {
      get
      {
        if (_comments.Count <= 0) return false;
        return true;
      }
    }

    public string Type
    {
      get { return (_children.Count > 0) ? "Non-terminal" : "Terminal"; }
    }

    public ObservableCollection<string> Comments
    {
      get
      {

        return _comments;
      }
      set
      {
        _comments = value;
        RaisePropertyChanged("Comments");
        RaisePropertyChanged("HasComments");
      }
    }

    public string SelectedComment
    {
      get { return _selectedComment; }
      set
      {
        _selectedComment = value;
        RaisePropertyChanged("SelectedComment");
      }
    }

    public string CommentToAdd
    {
      get { return _commentToAdd; }
      set
      {
        _commentToAdd = value;
        RaisePropertyChanged("CommentToAdd");
      }
    }

    public ObservableCollection<FeatureViewModel> Children
    {
      get { return _children; }
      set
      {
        if (IsReference == false & _children != value)
        {
          _children = value;
          RaisePropertyChanged("Children");
        }
      }
    }

    /// <summary>
    /// gets/set/ if the feature is reference to another feature.
    /// </summary>
    public bool IsReference
    {
      get { return _isReference; }
      set
      {
        _isReference = value;
        RaisePropertyChanged("IsReference");

      }
    }

    private bool _isReference;

    /// <summary>
    ///   Gets or sets the Identifier of the reature refered to by the
    /// </summary>
    public string ReferenceTargetId
    {
      get { return _isReference ? _targetId : null; }
      set
      {
        _targetId = value;
        RaisePropertyChanged("ReferenceTargetId");
      }
    }

    private string _targetId;
    public string TargetId
    {
      get { return _targetId; }
      set
      {
        _targetId = value;
        RaisePropertyChanged("TargetId");
      }
    }  

    public void AddNewComment()
    {
      if (string.IsNullOrEmpty(_commentToAdd)) return;
      _feature.Comments.Add(_commentToAdd);
      RaisePropertyChanged("Comments");
    }

    public void RemoveSelectedComment()
    {
      if (string.IsNullOrEmpty(_selectedComment)) return;
      _feature.Comments.Remove(_selectedComment);
      RaisePropertyChanged("Comments");
    }

    public bool CanRemove
    {
      get { return (_comments.Count > 0); }
    }

    private void SetIdValue()
    {
      string result = _idGetter();
      if (string.IsNullOrEmpty(result)) return;
      Id = result;
    }


    /// <summary>
    ///   Loads the Models data into this view model
    /// </summary>
    private void LoadData()
    {
      if (_feature == null) return;

      _comments = new ObservableCollection<string>(_feature.Comments);

      if (_feature.IsReference & _feature.Name == null) _feature.Name = "References:" + _feature.TargetId;

      _children.Clear();

      foreach (Feature child in _feature.Children)
      {
        FeatureViewModel childVm = _featureCreator(child);
        _children.Add(childVm);
      }
    }


    public static FeatureViewModel CreateFeatureReference(FeatureViewModel reference, string parentId, ValidNewIdGetter idGetter, FeatureCreatorWithArg creator, NewIdService idService)
    {
      

      var featVm = new FeatureViewModel(idGetter, creator,idService)
      {
        Name = "References: " + reference.TargetId,
        TargetId = reference.Id,
        Id = parentId + reference.Id,
        Description = string.Format("A reference to {0} (Edit should be made there)", string.IsNullOrEmpty(reference.Name) ? reference.Id : reference.Name),
        IsReference = true
      };


      return featVm;
    }

    private readonly FeatureCreatorWithArg _featureCreator;
    private readonly ValidNewIdGetter _idGetter;

    private ObservableCollection<FeatureViewModel> _children = new ObservableCollection<FeatureViewModel>();
    private string _commentToAdd;
    private ObservableCollection<string> _comments = new ObservableCollection<string>();
    private Feature _feature;
    private string _selectedComment;
  }


  public static class FeatureHelperMethods
  {
    public static bool IsClosedFeature(FeatureViewModel viewModel)
    {
      if (viewModel.Children.Count < 1) return false;

      foreach (var child in viewModel.Children)
      {
        if (!IsFeatureValue(child)) return false;
      }

      return true;
    }

    public static bool IsFeatureValue(FeatureViewModel viewModel)
    {
      return viewModel.Children.Count < 1;
    }

    public static bool IsComplexFeature(FeatureViewModel viewModel)
    {
      foreach (var child in viewModel.Children)
      {
        if (IsClosedFeature(child)) return true;
      }

      return false;
    }
  }
}