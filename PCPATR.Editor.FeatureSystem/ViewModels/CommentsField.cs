﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Commands;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class CommentsField : ViewModelBase
  {
    public CommentsField()
    {
      _comments = new ObservableCollection<string>();
      AddComment = new DelegateCommand(AddCommentToList);
      RemoveComment = new DelegateCommand(RemoveCommentFromList);
    }


    public DelegateCommand AddComment { get; private set; }

    public DelegateCommand RemoveComment { get; private set; }

    public ObservableCollection<string> Comments
    {
      get { return _comments; }
      set
      {
        _comments = value;
        RaisePropertyChanged("Comments");
      }
    }

    public string SelectedComment
    {
      get { return _selectedComment; }
      set
      {
        _selectedComment = value;
        RaisePropertyChanged("SelectedComment");
      }
    }

    public string CommentToAdd
    {
      get { return _commentToAdd; }
      set
      {
        _commentToAdd = value;
        RaisePropertyChanged("CommentToAdd");
      }
    }

    
    private void AddCommentToList()
    {
      if (string.IsNullOrEmpty(_commentToAdd)) return;
      _comments.Add(_commentToAdd);
      RaisePropertyChanged("Comments");
      CommentToAdd = null;
    }

    private void RemoveCommentFromList()
    {
      if (!_comments.Contains(_selectedComment)) return;
      _comments.Remove(_selectedComment);
      RaisePropertyChanged("Comments");
      SelectedComment = null;
    }


    private string _selectedComment;
    private string _commentToAdd;
    private ObservableCollection<string> _comments;
  }
}
