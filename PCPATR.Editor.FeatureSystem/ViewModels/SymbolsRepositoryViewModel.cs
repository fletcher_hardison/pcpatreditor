﻿//  ***************************************************************
//  File name: SymbolsRepositoryViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: Fletcher Hardison
//  Created on: 11,06,2012
// ***************************************************************

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using PCPATR.Common;

using PCPATR.Models.MasterLists;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class SymbolsRepositoryViewModel : ViewModelBase
  {
    /// <summary>
    ///   Initializes a new instance of the SymbolsRepositoryViewModel class
    /// </summary>
    public SymbolsRepositoryViewModel(NameIdRefListSelector selector, EditSymbol symbolEditor,
                                      ConfirmationGetter confirmationGetter, SymbolCreator creator)
    {
      _terminals = new ObservableCollection<Symbol>();
      _nonTerminals = new ObservableCollection<Symbol>();
      _listSelector = selector;
      _symbolEditor = symbolEditor;
      _symbolCreator = creator;
      _getConfirmation = confirmationGetter;
      
      //_messageBus.Subscribe<ISymbolsRq>(HandleSymbolsRequest);
      DisplayName = "Symbols";
    }

    #region SymbolsViewModel Members

    /// <summary>
    ///   Gets or sets the collection of terminals symbols.
    /// </summary>
    public ObservableCollection<Symbol> TerminalSymbols
    {
      get { return _terminals; }
      set
      {
        if (_terminals != value) {
          _terminals = value;
          RaisePropertyChanged("TerminalSymbols");
        }
      }
    }

    /// <summary>
    ///   Gets or sets the selected terminal symbol.
    /// </summary>
    public Symbol SelectedTerminal
    {
      get { return _selectedTerminal; }
      set
      {
        if (_selectedTerminal != value) {
          _selectedTerminal = value;
          SelectedNonTerminal = null;
          RaisePropertyChanged("SelectedTerminal");
          RaisePropertyChanged("SelectedSymbol");
        }
      }
    }

    /// <summary>
    ///   Gets or sets the Collection of non terminals symbols.
    /// </summary>
    public ObservableCollection<Symbol> NonTerminalSymbols
    {
      get { return _nonTerminals; }
      set
      {
        if (_nonTerminals != value) {
          _nonTerminals = value;
          RaisePropertyChanged("NonTerminalSymbols");
        }
      }
    }

    /// <summary>
    ///   Gets or sets the selected non terminal symbol
    /// </summary>
    public Symbol SelectedNonTerminal
    {
      get { return _selectedNonTerminal; }
      set
      {
        if (_selectedNonTerminal != value) {
          _selectedNonTerminal = value;
          SelectedTerminal = null;
          RaisePropertyChanged("SelectedNonTerminal");
          RaisePropertyChanged("SelectedSymbol");
        }
      }
    }

    /// <summary>
    ///   Gets a value repsetening which symbols is selected regardless of its type.
    /// </summary>
    public Symbol SelectedSymbol
    {
      get
      {
        if (_selectedTerminal == null) return _selectedNonTerminal;
        return _selectedTerminal;
      }
    }

    public Func<NameId> get_select_symbol_function(Func<List<NameId>, NameId> function)
    {
      Func<NameId> result = () => {
        IEnumerable<NameId> list = from x in _terminals.Union(_nonTerminals)
                                   select new NameId(x.Name, x.Id);

        return function(new List<NameId>(list));
      };

      return result;
    }

    #endregion


    public NonTerminalSymbolSelector GetNonTerminalSymbolSelector()
    {
      return () => {
        IEnumerable<NameIdRef> NT = from x in _nonTerminals
                                    select new NameIdRef(x.Id);

        return _listSelector(NT);
      };
    }


    //private void HandleSymbolsRequest(ISymbolsRq r)
    //{
    //  r.Symbols = this;
    //}

    #region Commands

    /// <summary>
    ///   Gets the list of commands affecting NonTerminal Symbols.
    /// </summary>
    public ObservableCollection<ICommandViewModel> NonTerminalCommands
    {
      get
      {
        if (_nonTerminalCommands == null) {
          _nonTerminalCommands = new ObservableCollection<ICommandViewModel>
          {
            AddNonTerminal,
            EditNonTerminal,
            RemoveNonTerminal
          };
        }
        return _nonTerminalCommands;
      }
    }

    /// <summary>
    ///   Gets the list of commands affecting Terminal Symbols
    /// </summary>
    public ObservableCollection<ICommandViewModel> TerminalCommands
    {
      get
      {
        if (_terminalCommands == null) {
          _terminalCommands = new ObservableCollection<ICommandViewModel>
          {
            AddTerminalSymbol,
            EditTerminal,
            RemoveTerminal
          };
        }
        return _terminalCommands;
      }
    }

    public ICommandViewModel AddTerminalSymbol
    {
      get
      {
        if (_addTerminal == null) {
          _addTerminal = new CommandViewModel("Add Terminal Symbol",
                                              new RelayCommand(p => AddSymbol(true)));
        }
        return _addTerminal;
      }
    }

    public ICommandViewModel AddNonTerminal
    {
      get
      {
        if (_addNonTerminal == null) {
          _addNonTerminal = new CommandViewModel("Add Non-Terminal Symbol",
                                                 new RelayCommand(p => AddSymbol(false)));
        }
        return _addNonTerminal;
      }
    }

    public ICommandViewModel EditTerminal
    {
      get
      {
        if (_editTerminal == null) {
          _editTerminal = new CommandViewModel("Edit Terminal Symbol",
                                               new RelayCommand(p => EditSymbol(true),
                                                                p => _selectedTerminal != null));
        }
        return _editTerminal;
      }
    }

    public ICommandViewModel EditNonTerminal
    {
      get
      {
        if (_editNonTerminal == null) {
          _editNonTerminal = new CommandViewModel("Edit Non Terminal Symbol",
                                                  new RelayCommand(p => EditSymbol(false),
                                                                   p => _selectedNonTerminal != null));
        }
        return _editNonTerminal;
      }
    }

    public ICommandViewModel RemoveTerminal
    {
      get
      {
        if (_removeTerminal == null) {
          _removeTerminal = new CommandViewModel("Remove Terminal Symbol",
                                                 new RelayCommand(p => RemoveSelectedSymbol(true),
                                                                  p => _selectedTerminal != null));
        }
        return _removeTerminal;
      }
    }


    public ICommandViewModel RemoveNonTerminal
    {
      get
      {
        if (_removeNonTerminal == null) {
          _removeNonTerminal = new CommandViewModel("Remove NonTerminal Symbol",
                                                    new RelayCommand(
                                                      p => RemoveSelectedSymbol(false),
                                                      p => _selectedNonTerminal != null));
        }
        return _removeNonTerminal;
      }
    }

    public void AddSymbol(bool isTerminal)
    {
      if (isTerminal) {
        Symbol result = _symbolEditor(_symbolCreator());
        if (result == null) return;
        _terminals.Add(result);
        RaisePropertyChanged("TerminalSymbols");
      } else {
        Symbol result = _symbolEditor(_symbolCreator());
        if (result == null) return;
        _nonTerminals.Add(result);
        RaisePropertyChanged("NonTerminalSymbols");
      }
    }

    public void EditSymbol(bool isTerminal)
    {
      if (isTerminal) {
        Symbol result = _symbolEditor(_selectedTerminal);
        InsertSymbolEditResult(_selectedTerminal, result, _terminals);
      } else {
        Symbol result = _symbolEditor(_selectedNonTerminal);
        InsertSymbolEditResult(_selectedNonTerminal, result, _nonTerminals);
      }
    }

    public void RemoveSelectedSymbol(bool isTerminal)
    {
      if (isTerminal) {
        RemoveSymbol(_selectedTerminal, _terminals);
        RaisePropertyChanged("TerminalSymbols");
      } else {
        RemoveSymbol(_selectedNonTerminal, _nonTerminals);
        RaisePropertyChanged("NonTerminalSymbols");
      }
    }

    private void InsertSymbolEditResult(Symbol oldSymbol, Symbol newSymbol, ObservableCollection<Symbol> symbolList)
    {
      if (newSymbol == null) return;
      int index = symbolList.IndexOf(oldSymbol);
      symbolList.RemoveAt(index);
      symbolList.Insert(index, newSymbol);
    }

    private void RemoveSymbol(Symbol doomed, ObservableCollection<Symbol> list)
    {
      bool result = _getConfirmation("Remove selected symbol?");
      if (result) list.Remove(doomed);
    }

    #endregion

    private readonly ConfirmationGetter _getConfirmation;

    private readonly NameIdRefListSelector _listSelector;    
    private readonly SymbolCreator _symbolCreator;
    private readonly EditSymbol _symbolEditor;
    private ICommandViewModel _addNonTerminal;
    private ICommandViewModel _addTerminal;
    private ICommandViewModel _editNonTerminal;
    private ICommandViewModel _editTerminal;
    private ObservableCollection<ICommandViewModel> _nonTerminalCommands;
    private ObservableCollection<Symbol> _nonTerminals;
    private ICommandViewModel _removeNonTerminal;
    private ICommandViewModel _removeTerminal;

    private Symbol _selectedNonTerminal;
    private Symbol _selectedTerminal;
    private ObservableCollection<ICommandViewModel> _terminalCommands;

    private ObservableCollection<Symbol> _terminals;
  }
}