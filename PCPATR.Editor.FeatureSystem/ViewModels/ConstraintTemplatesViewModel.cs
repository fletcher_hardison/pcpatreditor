﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.BlockEditor;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Models.FeatureTemplates;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PCPATR.Models.Rules.Constraints;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class ConstraintTemplatesViewModel : ViewModelBase
  {
    public ConstraintTemplatesViewModel(BlockMapper blockMapper, ModelService<ConstraintTemplateModel> modelService, NewIdServiceViewModel idService, IUnityContainer container)
    {
      _container = container;
      _idService = idService;
      _modelService = modelService;
      _blockMapper = blockMapper;
      BlockEditor = new BlockEditorViewModel();

      AddTemplate = new DelegateCommand(AddNewConstraintTemplate);
      RemoveTemplate = new DelegateCommand(RemoveFeatureTemplate);
      CancelAdd = new DelegateCommand(CancelAddMethod);
      SaveSelected = new DelegateCommand(TryPersistSelectedViewModelModel);
    }

    public BlockEditorViewModel BlockEditor { get; private set; }

    private string _selectedIdentifier;
    public string SelectedIdentifier
    {
      get { return _selectedIdentifier; }
      set
      {
        _selectedIdentifier = value;
        RaisePropertyChanged("SelectedIdentifier");
      }
    }


    private ObservableCollection<string> _idIdentifierList = new ObservableCollection<string>();
    public ObservableCollection<string> IdIdentifiersList
    {
      get { return _idIdentifierList; }
      set
      {
        _idIdentifierList = value;
        _modelService.SetManagedIdIdentifierCollection(_idIdentifierList);
        RaisePropertyChanged("IdIdentifiersList");
      }
    }



    //public ConstraintTemplateViewModel SelectedConstraint
    //{
    //  get
    //  {
    //    return _selectedConstraint;
    //  }
    //  set
    //  {
    //    _selectedConstraint = value;
    //    RaisePropertyChanged("SelectedConstraint");
    //  }
    //}

    public DelegateCommand AddTemplate { get; private set; }

    public DelegateCommand RemoveTemplate { get; private set; }

    public DelegateCommand CancelAdd { get; private set; }

    public DelegateCommand SaveSelected { get; private set; }

    public NewIdServiceViewModel IdService { get { return _idService; } }

    private string _proposedIdentifier;
    public string ProposedIdentifier
    {
      get { return _proposedIdentifier; }
      set
      {
        _proposedIdentifier = value;
        RaisePropertyChanged("ProposedIdentifier");
      }
    }


    private void CancelAddMethod()
    {
      IdService.ProposedId = string.Empty;
      ProposedIdentifier = string.Empty;
    }


    private void AddNewConstraintTemplate()
    {
      var result = _container.Resolve<ConstraintTemplateModel>();
      result.LogicalExpression = _container.Resolve<LogicalExpressionModel>();
      result.Id = IdService.ProposedId;
      result.Identifier = _proposedIdentifier;

      _modelService.PersistModel(result);
      IdService.ProposedId = string.Empty;
      ProposedIdentifier = string.Empty;
    }

    private void RemoveFeatureTemplate()
    {
      if (string.IsNullOrEmpty(_selectedIdentifier)) return;
      _modelService.RemoveRule(_selectedIdentifier);

    }

    public void LoadModels(IEnumerable<ConstraintTemplateModel> models)
    {
      _modelService.SetManagedIdIdentifierCollection(_idIdentifierList);
      _modelService.LoadModels(models);
    }

    public void ShowSelectedIdentifier()
    {
      if (string.IsNullOrEmpty(_selectedIdentifier)) return;
      var initializationContext = new BlockInitializationContext();
      initializationContext.AddContextElement<ConstraintTemplatesViewModel>(this);
      initializationContext.AddContextElement<IUnityContainer>(_container);
      BlockEditor.Context = initializationContext;

      BlockEditor.Content = _blockMapper.MapToBlock(
        _modelService.GetModelFromIdentiferIdString(_selectedIdentifier), null, initializationContext) as ContainerBlock;

    }


    public void TryPersistSelectedViewModelModel()
    {
      if (BlockEditor == null) return;
      var model = _blockMapper.MapBlockToModelObject(BlockEditor.Content) as ConstraintTemplateModel;
      _modelService.PersistModel(model);
    }

    public IEnumerable<ConstraintTemplateModel> GetTemplateModels()
    {
      return _modelService.ModelDictionary.Values;
    }

    //private ConstraintTemplateViewModel _selectedConstraint;
    private readonly BlockMapper _blockMapper;
    private readonly ModelService<ConstraintTemplateModel> _modelService;
    private readonly NewIdServiceViewModel _idService;
    private readonly IUnityContainer _container;
  }
}
