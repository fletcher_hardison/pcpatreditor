﻿//  ***************************************************************
//  File name: SymbolViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 11,06,2012
// ***************************************************************

using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.Practices.Prism.Commands;
using PCPATR.Models.MasterLists;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class SymbolViewModel : ViewModelBase
  {


    public SymbolViewModel(Symbol symbol, ValidNewIdGetter idGetter)
    {
      this._symbol = symbol;
      _idGetter = idGetter;
      SetId = new DelegateCommand(SetIdValue);
      AddComment = new DelegateCommand(AddNewComment);
      RemoveComment = new DelegateCommand(RemoveSelectedComment);
    }


    public string Name
    {
      get { return this._symbol.Name; }
      set
      {
        if (this._symbol.Name != value)
        {
          this._symbol.Name = value;
          RaisePropertyChanged("Name");
        }
      }
    }
    public string Description
    {
      get { return this._symbol.Description; }
      set
      {
        if (this._symbol.Description != value)
        {
          this._symbol.Description = value;
          RaisePropertyChanged("Description");
        }
      }
    }

    public string Id
    {
      get { return this._symbol.Id; }
      set
      {
        if (this._symbol.Id != value)
        {
          this._symbol.Id = value;
          RaisePropertyChanged("Id");
          RaisePropertyChanged("HasId");
        }
      }
    }

    public bool HasId
    {
      get { return _symbol.Id != null; }
    }

    public DelegateCommand SetId { get; private set; }

    public DelegateCommand AddComment { get; private set; }

    public DelegateCommand RemoveComment
    {
      get;
      private set;
    }

    public bool CanRemoveComment
    {
      get { return (this._selectedComment != null); }
    }

    public ObservableCollection<string> Comments
    {
      get
      {
        if (this._comments == null) this._comments = new ObservableCollection<string>(this._symbol.Comments);
        return this._comments;
      }
      set
      {
        if (this._comments != value)
        {
          this._comments = value;
          RaisePropertyChanged("Comments");
        }
      }
    }

    public string SelectedComment
    {
      get { return this._selectedComment; }
      set
      {
        if (this._selectedComment != value)
        {
          this._selectedComment = value;
          CommentToAdd = value;
          RaisePropertyChanged("SelectedComment");
        }
      }
    }

    public string CommentToAdd
    {
      get { return _commentToAdd; }
      set
      {
        _commentToAdd = value;
        RaisePropertyChanged("CommentToAdd");
      }
    }

    public Symbol ToSymbol()
    {
      if (this._symbol == null) this._symbol = new Symbol();

      this._symbol.Comments.Clear();

      foreach (string comment in this.Comments)
      {
        this._symbol.Comments.Add(comment);
      }

      return this._symbol;
    }

    public Symbol Data
    {
      get
      {
        _symbol.Comments = _comments.ToList();
        return this._symbol;
      }
      set
      {
        if (_symbol != value)
        {
          this._symbol = value;
          Comments = new ObservableCollection<string>(_symbol.Comments);
          RaisePropertyChanged("Data");
        }
      }
    }

    private  void AddNewComment()
    {
      if (string.IsNullOrEmpty(_commentToAdd)) return;
      _comments.Add(_commentToAdd);
      RaisePropertyChanged("Comments");
    }

    private  void RemoveSelectedComment()
    {
      if (string.IsNullOrEmpty(_selectedComment)) return;
      _comments.Remove(_selectedComment);
      RaisePropertyChanged("Comments");
      SelectedComment = null;
    }

    private void SetIdValue()
    {
      var newId = _idGetter();
      if (string.IsNullOrEmpty(newId)) return;
      this.Id = newId;
    }

    private string _commentToAdd;
    private ObservableCollection<string> _comments;
    private string _selectedComment;
    private Symbol _symbol;
    private readonly ValidNewIdGetter _idGetter;
  }
}