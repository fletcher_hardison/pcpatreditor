﻿using System;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class GenericEventArgs<T> : EventArgs
  {
    public GenericEventArgs(T payload) {
      Payload = payload;
    }

    public T Payload { get; protected set; }
  }
}