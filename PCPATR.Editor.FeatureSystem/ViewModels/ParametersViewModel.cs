﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.Practices.Prism.Commands;
using PCPATR.Common;
using PCPATR.Models.Parameters;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class ParametersViewModel : ViewModelBase
  {
    public ParametersViewModel(NonTerminalSymbolSelector selector, FeatureSelector featureSelector)
    {
      _nonTerminalSelector = selector;
      _featureSelector = featureSelector;
      _attributeOrder = new ObservableCollection<AttributeOrderViewModel>();
      _categoryFeature = new ToggledValueViewModel { DisplayName = ParameterFieldNames.CAT };
      _lexicalFeature = new ToggledValueViewModel() { DisplayName = ParameterFieldNames.LEX };
      _glossFeature = new ToggledValueViewModel() { DisplayName = ParameterFieldNames.GLOSS };
      _rootGlossFeature = new ToggledValueViewModel { DisplayName = ParameterFieldNames.ROOT_GLOSS };
      _availableAttributeOrderItems = SetupPossibleAttributeOrderList();
      _nonFeatureAttributeOrderItems = SetupPossibleAttributeOrderList().ToList();
      SetStartSymbol = new DelegateCommand(MutateStartSymbol);
      AddAttributeOrderItem = new DelegateCommand(AddAttributeOrderItemToList);
      RemoveAttributeOrderItem = new DelegateCommand(RemoveAttributeOrderItemFromList);
      AddFeatureNameRef = new DelegateCommand(AddFeatureNameRefToAttributeOrder);
      MoveAttributeDown = new DelegateCommand(MoveAttributeDownOne);
      MoveAttributeUp = new DelegateCommand(MoveAttributeUpOne);
      Comments = new CommentsField();
      StartSymbolComments = new CommentsField();
      AttributeOrderComments = new CommentsField();
    }


    public DelegateCommand SetStartSymbol { get; private set; }

    public DelegateCommand AddAttributeOrderItem { get; private set; }

    public DelegateCommand RemoveAttributeOrderItem { get; private set; }

    public DelegateCommand AddFeatureNameRef { get; private set; }

    public DelegateCommand MoveAttributeUp { get; private set; }

    public DelegateCommand MoveAttributeDown { get; private set; }

    public NameIdRef StartSymbol
    {
      get { return _startSymbol; }
      set
      {
        _startSymbol = value;
        RaisePropertyChanged("StartSymbol");
      }
    }

    public ObservableCollection<AttributeOrderViewModel> AttributeOrder
    {
      get { return _attributeOrder; }
      set
      {
        _attributeOrder = value;
        RaisePropertyChanged("AttributeOrder");
        SyncAvailableListWithAttributeOrderList();
      }
    }

    public AttributeOrderViewModel SelectedUsedAttribute
    {
      get { return _selectedUsedAttribute; }
      set
      {
        _selectedUsedAttribute = value;
        RaisePropertyChanged("SelectedUsedAttribute");
        RaisePropertyChanged("IsUsedAttributeSelected");      
      }
    }

    public bool IsUsedAttributeSelected
    {
      get { return _selectedUsedAttribute != null; }
    }

    public ObservableCollection<AttributeOrderViewModel> AvailableAttributes
    {
      get { return _availableAttributeOrderItems; }
      set
      {
        _availableAttributeOrderItems = value;
        RaisePropertyChanged("AvailableAttributes");
      }
    }

    public AttributeOrderViewModel SelectedAvailableAttribute
    {
      get { return _selectedAvailableAttribute; }
      set
      {
        _selectedAvailableAttribute = value;
        RaisePropertyChanged("SelectedAvailableAttribute");
      }
    }

    public ToggledValueViewModel CategoryFeature
    {
      get { return _categoryFeature; }
      set
      {
        _categoryFeature = value;
        _categoryFeature.DisplayName = ParameterFieldNames.CAT;
        RaisePropertyChanged("CategoryFeature");
      }
    }

    public ToggledValueViewModel LexicalFeature
    {
      get { return _lexicalFeature; }
      set
      {
        _lexicalFeature = value;
        _lexicalFeature.DisplayName = ParameterFieldNames.LEX;
        RaisePropertyChanged("LexicalFeature");
      }
    }

    public ToggledValueViewModel GlossFeature
    {
      get { return _glossFeature; }
      set
      {
        _glossFeature = value;
        _glossFeature.DisplayName = ParameterFieldNames.GLOSS;
        RaisePropertyChanged("GlossFeature");
      }
    }

    public ToggledValueViewModel RootGlossFeature
    {
      get { return _rootGlossFeature; }
      set
      {
        _rootGlossFeature = value;
        _rootGlossFeature.DisplayName = ParameterFieldNames.ROOT_GLOSS;
        RaisePropertyChanged("RootGlossFeature");
      }
    }

    public CommentsField Comments { get; private set; }

    public CommentsField StartSymbolComments { get; private set; }

    public CommentsField AttributeOrderComments { get; private set; }

    public Dictionary<string, string> GetXsltArgsListFromParameters()
    {
      var args = new Dictionary<string, string>();

      if (!string.IsNullOrEmpty(CategoryFeature.Value))
      {
        args.Add("sCat", CategoryFeature.Value);
      }
      if (!string.IsNullOrEmpty(GlossFeature.Value))
      {
        args.Add("sGloss",  GlossFeature.Value);
      }
      if (!string.IsNullOrEmpty(LexicalFeature.Value))
      {
        args.Add("sLex", LexicalFeature.Value);
      }
      if (!string.IsNullOrEmpty(RootGlossFeature.Value)) {
        args.Add("sRootGloss", RootGlossFeature.Value);
      }

      return args;
    }


    //MutateStartSymbol : this, selector -> this
    //Purpose: compute the new state of this by executing selector to mutate the startSymbol on this.
    private void MutateStartSymbol()
    {
      var result = _nonTerminalSelector();
      StartSymbol = result != null ? result : StartSymbol;
    }

    //SyncAvailableListWithAttributeOrderList: list -> list
    //Purpose: compute the available attribute order items from list of currently used attribute order items
    private void SyncAvailableListWithAttributeOrderList()
    {
      foreach (var attribute in AttributeOrder) {
        if (_availableAttributeOrderItems.Contains(attribute)) {
          _availableAttributeOrderItems.Remove(attribute);
        }
      }

      RaisePropertyChanged("AvailableAttributes");
    }


    //SetupPossibleAttributeOrderList: () -> list;
    //Purpose: initializes the list or possibilities or attribute order items.
    private ObservableCollection<AttributeOrderViewModel> SetupPossibleAttributeOrderList()
    {
      //catRef | lexRef | glossRef | rootGlossRef | featureNameRef | subcat | first | rest)
      var list = new ObservableCollection<AttributeOrderViewModel>
      {
        new AttributeOrderViewModel{Name = ParameterAttributeOrderNames.CAT, IdRef = new NameIdRef("pcpatrGrammarCat")},
        new AttributeOrderViewModel{Name = ParameterAttributeOrderNames.LEX, IdRef=new NameIdRef("pcpatrGrammarLex")},
        new AttributeOrderViewModel{Name=ParameterAttributeOrderNames.GLOSS, IdRef = new NameIdRef("pcpatrGrammarGloss")},
        new AttributeOrderViewModel{ Name=ParameterAttributeOrderNames.ROOT_GLOSS, IdRef =   new NameIdRef("pcpatrGrammarRootGloss")},
        new AttributeOrderViewModel{ Name=ParameterAttributeOrderNames.NONE,IdRef= new NameIdRef("pcpatrGrammarNone")},
        new AttributeOrderViewModel{Name =ParameterAttributeOrderNames.SUBCAT},
        new AttributeOrderViewModel{ Name = ParameterAttributeOrderNames.FIRST},
        new AttributeOrderViewModel{ Name = ParameterAttributeOrderNames.REST}
      };

      return list;
    }

    //AddAttributeOrderItemToList: list, selectedItem -> list
    //Purpose: computes the available and the used item by moving the selectedItem from in avaliable list to the used list.
    private void AddAttributeOrderItemToList()
    {
      MoveItemFromListToList(_availableAttributeOrderItems, _attributeOrder, _selectedAvailableAttribute);
      SelectedAvailableAttribute = null;
      RaisePropertyChanged("AttributeOrder");
      RaisePropertyChanged("AvailableAttributes");
    }

    //RemoveAttributeOrderItemFromList: list, selectedItem -> list
    //Purpose: computes the available and used items by moving the selectedItem from the used list to the available list.
    private void RemoveAttributeOrderItemFromList()
    {
      //If item is a feature name ref then simple remove it and return.
      if (!_nonFeatureAttributeOrderItems.Contains(_selectedUsedAttribute))
      {
        _attributeOrder.Remove(_selectedUsedAttribute);
      }
      else
      {
        MoveItemFromListToList(_attributeOrder, _availableAttributeOrderItems, _selectedUsedAttribute);
      }
      SelectedUsedAttribute = null;
      RaisePropertyChanged("AttributeOrder");
      RaisePropertyChanged("AvailableAttributes");
    }

    //MoveItemFromListToList: source, target, item -> target, source
    //Purpose: computes the new target list by moving the item from the source to the target.
    private void MoveItemFromListToList<T>(ObservableCollection<T> source, ObservableCollection<T> target, T item)
    {
      if (!source.Contains(item)) return;
      source.Remove(item);
      target.Add(item);
    }

    //AddFeatureNameRefToAttributeOrder: selector, list -> list
    //Purpose: computes a new version of the attribute order list by executing selector and add the result if it's not null.
    private void AddFeatureNameRefToAttributeOrder()
    {
      var result = _featureSelector();
      if (result == null) return;
      var feature = new AttributeOrderViewModel { Name = ParameterAttributeOrderNames.FEATURE_NAME_REF, IdRef = result };
      _attributeOrder.Add(feature);
      RaisePropertyChanged("AttributeOrder");
    }

    private void MoveAttributeUpOne()
    {
      MoveAttribute(-1);
    }

    private void MoveAttributeDownOne()
    {
      MoveAttribute(1);
    }

    //MoveAttribute: list, attribute, offset -> list
    //Purpose: computes the mutated version of the attributes list from list, attribute, and offset
    private void MoveAttribute(int offset)
    {
      if (_selectedUsedAttribute == null) return;
      if (!_attributeOrder.Contains(_selectedUsedAttribute)) return;
      var attribute = _selectedUsedAttribute;
      var index = _attributeOrder.IndexOf(_selectedUsedAttribute);
      var newLocation = index + offset;
      if (newLocation < 0) newLocation = 0;
      if (newLocation > _attributeOrder.Count - 1) newLocation = _attributeOrder.Count - 1;
      _attributeOrder.RemoveAt(index);
      _attributeOrder.Insert(newLocation, attribute);
      RaisePropertyChanged("AttributeOrder");
    }


    private NameIdRef _startSymbol;
    private ObservableCollection<AttributeOrderViewModel> _attributeOrder;
    private ObservableCollection<AttributeOrderViewModel> _availableAttributeOrderItems;
    private readonly List<AttributeOrderViewModel> _nonFeatureAttributeOrderItems;
    private AttributeOrderViewModel _selectedUsedAttribute;
    private AttributeOrderViewModel _selectedAvailableAttribute;
    private ToggledValueViewModel _categoryFeature;
    private ToggledValueViewModel _lexicalFeature;
    private ToggledValueViewModel _glossFeature;
    private ToggledValueViewModel _rootGlossFeature;
    private readonly NonTerminalSymbolSelector _nonTerminalSelector;
    private readonly FeatureSelector _featureSelector;
  }

  public class AttributeOrderViewModel : ViewModelBase
  {
    public NameIdRef IdRef
    {
      get { return _idRef; }
      set
      {
        _idRef = value;
        RaisePropertyChanged("IdRef");
      }
    }

    public string Name
    {
      get { return _name; }
      set
      {
        _name = value;
        RaisePropertyChanged("Name");
      }
    }


    public override bool Equals(object obj)
    {
      var other = obj as AttributeOrderViewModel;
      if (other == null) return false;
      if (other.IdRef == null & IdRef != null) return false;
      if (IdRef == null & ! string.IsNullOrEmpty(other.Name)) {
        return this.Name == other.Name;
      }

      return IdRef.IdRef == other.IdRef.IdRef;
    }

    public override int GetHashCode()
    {
      return IdRef != null ? IdRef.IdRef.GetHashCode() : base.GetHashCode();
    }

    private NameIdRef _idRef;
    private string _name;
  }
}
