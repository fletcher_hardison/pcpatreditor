﻿//  ***************************************************************
//  File name: EnableDisableElementsViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 20,03,2013
// ***************************************************************

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.Commands;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Models.Rules;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class EnableDisableElementsViewModel : ViewModelBase
  {

    public EnableDisableElementsViewModel(INavigationManager navManager)
    {
      _navManager = navManager;
      OkCommand = new DelegateCommand(OkCommandMethod);
      CancelCommand = new DelegateCommand(NavigateToMainWindow);
    }


    private ObservableCollection<RuleWrapper> _rulesList = new ObservableCollection<RuleWrapper>();
    public ObservableCollection<RuleWrapper> RulesList
    {
      get { return _rulesList; }
      set
      {
        _rulesList = value;
        RaisePropertyChanged("RulesList");
      }
    }

    private ObservableCollection<FeatureTemplateWrapper> _selectedFeatureTemplates;
    public ObservableCollection<FeatureTemplateWrapper> SelectedFeatureTemplates
    {
      get { return _selectedFeatureTemplates; }
      set
      {
        _selectedFeatureTemplates = value;
        RaisePropertyChanged("SelectedFeatureTemplates");
      }
    }

    private ObservableCollection<FeatureTemplateWrapper> _featureTemplates = new ObservableCollection<FeatureTemplateWrapper>();
    public ObservableCollection<FeatureTemplateWrapper> FeatureTemplates
    {
      get { return _featureTemplates; }
      set
      {
        _featureTemplates = value;
        RaisePropertyChanged("FeatureTemplates");
      }
    }

    public Action<IEnumerable<FeatureTemplateModel>> PersistFeatureTemplatesChangesAction { get; set; }
    public Action<IEnumerable<Rule>> PersistRuleChangesAction { get; set; }

    public void LoadRules(IEnumerable<Rule> rules)
    {
      _rulesList.Clear();
      foreach (var rule in rules)
      {
        _rulesList.Add(new RuleWrapper(rule));
      }
    }

    public void LoadFeatureTemplates(IEnumerable<FeatureTemplateModel> templates)
    {
      _featureTemplates.Clear();
      foreach (var featureTemplateModel in templates)
      {
        _featureTemplates.Add(new FeatureTemplateWrapper(featureTemplateModel));
      }
    }

    private void TryPersistChanges()
    {
      if (PersistFeatureTemplatesChangesAction != null)
      {
        var fts = new List<FeatureTemplateModel>();
        foreach (var featureTemplateWrapper in _featureTemplates)
        {
          fts.Add(featureTemplateWrapper.Model);
        }

        PersistFeatureTemplatesChangesAction(fts);
      }

      if (PersistRuleChangesAction != null)
      {
        var rules = new List<Rule>();
        foreach (var ruleWrapper in _rulesList)
        {
          rules.Add(ruleWrapper.Model);
        }
        PersistRuleChangesAction(rules);
      }

      //keep data from hanging around when not needed
      _rulesList.Clear();
      _featureTemplates.Clear();
    }

    private void OkCommandMethod()
    {
      TryPersistChanges();
      NavigateToMainWindow();
    }

    private void NavigateToMainWindow()
    {
      _navManager.RegionManager.Regions[MainWindowRegionNames.DETAILS_REGION].RequestNavigate(new Uri(CoreConstants.START_PAGE_VIEW, UriKind.Relative), f => f.Result.ToString());
    }

    public void ToggleUseWhenDebuggingRulesMethod(IList rules)
    {

      ApplyToSelected<RuleWrapper>(rules, f => f.UseWhenDebugging = !f.UseWhenDebugging);
    }

    public void ToggleUseWhenDebuggingFeatureTemplatesMethod(IList fts)
    {
      ApplyToSelected<FeatureTemplateWrapper>(fts, f => f.UseWhenDebugging = !f.UseWhenDebugging);
    }

    public void ToggleSelectedRulesMethod(IList rules)
    {
      ApplyToSelected<RuleWrapper>(rules, f => f.Enabled = !f.Enabled);
    }

    public void ToggleSelectedFeatureTemplatesMethod(IList fts)
    {
      ApplyToSelected<FeatureTemplateWrapper>(fts, f => f.Enabled = !f.Enabled);
    }

    private void ApplyToSelected<T>(IList list, Action<T> toggle)
    {
      foreach (var element in list)
      {
        toggle((T)element);
      }
    }

    public DelegateCommand OkCommand { get; private set; }
    public DelegateCommand CancelCommand { get; private set; }
    public DelegateCommand<IList> ToggleEnabledStatus { get; private set; }
    public DelegateCommand<IList> ToggleEnabledStatusFeatureTemplates { get; private set; }
    public DelegateCommand<IList> ToggleUseWhenDebuggingRulesStatus { get; private set; }
    public DelegateCommand<IList> ToggleUseWhenDebuggingFeatureTemplates { get; private set; }

    private readonly INavigationManager _navManager;
  }

  public class RuleWrapper : ViewModelBase
  {
    public RuleWrapper(Rule model)
    {
      Model = model;
    }

    public Rule Model { get; set; }

    public string Identifier { get { return Model.Identifier; } }


    public bool Enabled
    {
      get { return Model.enabled; }
      set
      {
        Model.enabled = value;
        RaisePropertyChanged("Enabled");
      }
    }

    public bool UseWhenDebugging
    {
      get { return Model.use_when_debugging; }
      set
      {
        Model.use_when_debugging = value;
        RaisePropertyChanged("UseWhenDebugging");
      }
    }
  }

  public class FeatureTemplateWrapper : ViewModelBase
  {
    public FeatureTemplateWrapper(FeatureTemplateModel model)
    {
      Model = model;
    }

    public FeatureTemplateModel Model { get; set; }
    
    public string Identifier { get { return Model.Identifier; } }

    public bool Enabled
    {
      get { return Model.Enabled; }
      set
      {
        Model.Enabled = value;
        RaisePropertyChanged("Enabled");
      }
    }

    public bool UseWhenDebugging
    {
      get { return Model.UseWhenDebugging; }
      set
      {
        Model.UseWhenDebugging = value;
        RaisePropertyChanged("UseWhenDebugging");
      }
    }
  }
}