﻿using System.Collections.Generic;

namespace PCPATR.Editor.MasterLists.ViewModels
{
  public class OptionSelectionFieldString : OptionSelectionField<string>
  {
    public OptionSelectionFieldString(IEnumerable<string> optionsList)
      : base(optionsList) {
      Content = @"\/";
      DisplayField = true;
      }

    public object Content { get; set; }
  }
}