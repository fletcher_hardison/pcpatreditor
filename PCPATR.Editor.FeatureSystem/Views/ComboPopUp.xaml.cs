using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;


namespace PCPATR.Editor.MasterLists.Views
{
    /// <summary>
    /// Interaction logic for ComboPopUp.xaml
    /// </summary>
    public partial class ComboPopUp : Window
    {
        public ComboPopUp()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            SaveResults = true;
            Close();
        }

        private void Discard(object sender, RoutedEventArgs e)
        {
            SaveResults = false;
            Close();
        }

        
        public ObservableCollection<string> Values { get; set; }

        /// <summary>
        /// Result of the dialog
        /// </summary>
        public string Result { get; set; }
        
        /// <summary>
        /// Description displayed above input box
        /// </summary>
        public string Description { get; set; }

        public bool SaveResults { get; set; }
    }
}
