﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PCPATR.Editor.MasterLists.FeatureTemplateViewModels;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for UserControl1.xaml
  /// </summary>
  public partial class OptionSelectionFieldControl : UserControl
  {
    private OptionSelectionFieldString _field;

    public OptionSelectionFieldControl() {
      InitializeComponent();
      
    }
    


    private void OptionSelectionFieldControl_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
      _field = DataContext as OptionSelectionFieldString;
      if (_field == null) return;
      _field.ClearSelection += (s, a) => OptionsListBox.UnselectAll();  
    }
  }
}
