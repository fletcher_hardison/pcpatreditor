﻿using System;
using System.Windows;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.ViewModels;



namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for FeatureTreeViewPopUp.xaml
  /// </summary>
  // ReSharper disable RedundantExtendsListEntry
  public partial class FeatureTreeViewPopUp
  // ReSharper restore RedundantExtendsListEntry
  {
    private readonly FeatureSystemTreeView _tree;


    private FeatureTreeViewPopUp()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Initializes a new instance of the FeatureTreeViewPopUp class.
    /// This class lets the user select a feature from the feature system,
    /// and returns it as its Result property.
    /// </summary>
    /// <param name="container">IoC Container used to resolve dependencies within the control</param>
    public FeatureTreeViewPopUp(IUnityContainer container)
      : this()
    {
      _tree = container.Resolve<FeatureSystemTreeView>(new ParameterOverride("BindToSelected", false));

      TreeGrid.Children.Add(_tree);
    }

    /// <summary>
    /// Gets or sets the LogicalFeature View Model that is the result of the popup window
    /// </summary>
    public FeatureViewModel Result { get; set; }

    /// <summary>
    /// Filters what part of the feature system is displayed;
    /// </summary>
    public Predicate<FeatureViewModel> Filter
    {
      get { return _tree != null ? null : _tree.Filter; }
      set { if (_tree != null) _tree.Filter = value; }
    }


    public Predicate<FeatureViewModel> SelectionalRestriction { get; set; }

    private readonly Func<FeatureViewModel, Predicate<FeatureViewModel>, bool> _matchesRestriction =
        (val, res) => res(val);

    /// <summary>
    /// Handles Button "Selected" click.
    /// Sets result to selected property of tree view.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void SelectClick(object sender, RoutedEventArgs e)
    {
      if (SelectionalRestriction != null)
      {
        if (_matchesRestriction(_tree.Selected, SelectionalRestriction))
        {
          Result = _tree.Selected;
          Close();
        }
      }
      else
      {
        Result = _tree.Selected;
        Close();
      }

    }

    /// <summary>
    /// Handles button "Cancel" click.
    /// Sets Result = null.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CancelClick(object sender, RoutedEventArgs e)
    {
      Result = null;
      Close();
    }
  }
}