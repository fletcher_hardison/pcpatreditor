using System.Windows.Controls;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.Views
{
    /// <summary>
    /// Interaction logic for FeatureSystemActions.xaml
    /// </summary>
    public partial class FeatureSystemActions : UserControl
    {
        private FeatureSystemViewModel _featureSystem;

        public FeatureSystemActions()
        {
            InitializeComponent();
        }

        public  FeatureSystemActions(FeatureSystemViewModel featureSystem)
        {
            _featureSystem = featureSystem;
            DataContext = _featureSystem;
        }
    }
}
