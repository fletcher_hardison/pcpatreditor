﻿using System.Collections.ObjectModel;
using System.Windows;


namespace PCPATR.Editor.MasterLists.Views
{
    /// <summary>
    /// Interaction logic for ListBoxPopup.xaml
    /// </summary>
    public partial class ListBoxPopup
    {
        public ListBoxPopup()
        {
            InitializeComponent();
            DataContext = this;
        }

        /// <summary>
        /// Data to be interacted with.
        /// </summary>
        public ObservableCollection<object> Data { get; set; }

        /// <summary>
        /// Data template to apply to data.
        /// </summary>
        public DataTemplate DataTemplate { get; set; }

        /// <summary>
        /// The selected item
        /// </summary>
        public object Result { get; set; }

        public bool SaveResult { get; set; }

        private void SelectClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            Result = this.LboxContent.SelectedItem;
            this.Close();
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            Result = null;
            this.Close();
        }

        
    }
}
