﻿using System.Windows;
using PCPATR.Editor.MasterLists.ViewModels;



namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for SymbolAddEditPopup.xaml
  /// </summary>
  public partial class SymbolAddEditPopup 
  {
    public SymbolAddEditPopup()
    {
      InitializeComponent();
      DataContext = this;
    }

    public SymbolViewModel Symbol { get; set; }

    public bool SaveResults { get; set; }    

    private void Save(object sender, RoutedEventArgs e)
    {
      SaveResults = true;
      Close();
    }

    private void Discard(object sender, RoutedEventArgs e)
    {
      SaveResults = false;
      Close();
    }

  }
}
