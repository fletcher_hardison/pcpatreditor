using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Xml;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.DAL;


namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for FeatureSystemTreeView.xaml
  /// </summary>
  public partial class FeatureSystemTreeView : UserControl
  {
    private readonly FeatureSystemViewModel _viewModel;

    private Predicate<FeatureViewModel> _filter;

    /// <summary>
    /// Initializes a new instance of the FeatureSystemTreeView class.
    /// This view displays the feature system and lets client classes
    /// So client classes can get the selected feature.
    /// </summary>
    /// <param name="fstVm"></param>
    /// <param name="container"></param>
    /// <param name="bindToSelected"> </param>
    public FeatureSystemTreeView(FeatureSystemViewModel fstVm, IUnityContainer container)
    {
      InitializeComponent();
      _viewModel = fstVm;
      _container = container;
      features = new ObservableCollection<FeatureViewModel>(_viewModel.Features);
//      FeaturesXmlNode = _viewModel.FeaturesXmlNode;
      DataContext = this;

      featureTree.SelectedItemChanged += delegate { Selected = (FeatureViewModel) featureTree.SelectedItem; };  
        //{ Selected = ParseXmlToSelected((XmlNode) featureTree.SelectedItem); };
    }

    public FeatureSystemViewModel ViewModel
    {
      get { return _viewModel; }
    }

    private IUnityContainer _container;

//    private FeatureViewModel ParseXmlToSelected(XmlNode node)
//    {
//      var temp = _container.Resolve<FeatureViewModel>();
//
//      temp.Name = node.SelectSingleNode("./name").InnerText;
//      temp.Id = node.Attributes.GetNamedItem("id").Value;
//
//      return temp;
//    }
//
//    public XmlNode FeaturesXmlNode { get; set; }

    public void RefershFeatures()
    {
      features.Clear();
      foreach (var feature in _viewModel.Features) {
        features.Add(feature);
      }
    }
    
    private void ViewModelFeaturesChanged(FeatureSystemViewModel viewmodel)
    {
      features.Clear();
      foreach (var featureViewModel in viewmodel.Features)
      {
        features.Add(featureViewModel);
      }
    }

    public FeatureViewModel Selected { get; set; }

    public ObservableCollection<FeatureViewModel> features { get; private set; }

    public Predicate<FeatureViewModel> Filter
    {
      get { return _filter; }
      set
      {
        _filter = value;
        DataContext = _viewModel.FilterFeatureSystem(_filter);
      }
    }

    public void FilterFeatures(Predicate<FeatureViewModel> pred)
    {
      features.Clear();
      foreach (var feat in _viewModel.SearchFeatures(pred))
      {
        features.Add(feat);
      }
    }
  }


  public static class FeatureSystemViewModelExtensions
  {
    public static FeatureSystemViewModel FilterFeatureSystem(this FeatureSystemViewModel system,
                                                             Predicate<FeatureViewModel> predicate)
    {
      var result = (FeatureSystemViewModel) system.Clone();
      result.Features.Clear();

      foreach (FeatureViewModel feature in system.Features) {
        if (predicate(feature)) result.Features.Add(feature);
        else {
          List<FeatureViewModel> recusiveStep = feature.Children.FindAll(predicate);
          if (recusiveStep != null) result.Features.AddRange(recusiveStep);
        }
      }

      return result;
    }
  }
}