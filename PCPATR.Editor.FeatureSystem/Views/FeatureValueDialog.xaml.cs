﻿using System.Collections.Generic;
using System.Windows;
using PCPATR.Common;
using PCPATR.Editor.MasterLists.ViewModels;


namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for FeatureValueDialog.xaml
  /// </summary>
  public partial class FeatureValueDialog
  {
    public FeatureValueDialog()
    {
      InitializeComponent();
      DataContext = this;
    }

    public NameIdRef ResultNameId { get; set; }

    public IEnumerable<FeatureViewModel> ClosedFeatures { get; set; }
    public FeatureViewModel SelectedFeature { get; set; }


    private void CloseDialog(bool result)
    {
      DialogResult = result;
      Close();
    }

    private void BtnOk_OnClick(object sender, RoutedEventArgs e)
    {
      if (SelectedFeature == null) ResultNameId = null;
      ResultNameId = new NameIdRef(SelectedFeature.Id);
      CloseDialog(true);
    }

    private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
    {
      ResultNameId = null;
      CloseDialog(false);
    }
  }
}
