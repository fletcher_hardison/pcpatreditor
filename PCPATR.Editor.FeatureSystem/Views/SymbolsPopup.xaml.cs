﻿using System.Collections.ObjectModel;
using System.Windows;
using PCPATR.Common.Interfaces;



namespace PCPATR.Editor.MasterLists.Views
{
    /// <summary>
    /// Interaction logic for SymbolsPopup.xaml
    /// for selecting symbols from the grammar
    /// </summary>
    public partial class SymbolsPopup 
    {
        public SymbolsPopup()
        {
            InitializeComponent();
            
            DataContext = this;
        }

        /// <summary>
        /// Sets the Property that will be dispayed
        /// </summary>
        public ObservableCollection<ISymbol> Symbols { get; set; }

        public ISymbol Result { get; set; }

        public bool SaveResult { get; set; }

        private void Save(object sender, RoutedEventArgs e)
        {
            SaveResult = true;
            
            Close();
        }

        private void Discard(object sender, RoutedEventArgs e)
        {
            SaveResult = false;
            
            Close();
        }
        
        
    }
}
