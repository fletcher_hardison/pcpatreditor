using System.Windows;
using System.Windows.Controls;
using PCPATR.Editor.MasterLists.ViewModels;


namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for FeatureSystemView.xaml
  /// </summary>
  public partial class FeatureSystemView : UserControl
  {
    private readonly FeatureSystemViewModel _featureSystem;

   public FeatureSystemView(FeatureSystemViewModel features)
    {
      InitializeComponent();
      _featureSystem = features;
      DataContext = _featureSystem;
    }

    private void MoveFeatureTreeView_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      if (_featureSystem == null) return;
      _featureSystem.TargetFeatureForMovingFeature = e.NewValue as FeatureViewModel;
    }

    private void FeatureTree_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      if (_featureSystem != null)
      {
        _featureSystem.SelectedFeature = e.NewValue as FeatureViewModel;
      }
    }
  }
}
