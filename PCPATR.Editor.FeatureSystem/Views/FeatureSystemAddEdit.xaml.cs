﻿using System.Windows;
using PCPATR.Editor.MasterLists.ViewModels;



namespace PCPATR.Editor.MasterLists.Views
{
    /// <summary>
    /// Interaction logic for FeatureSystemAddEdit.xaml
    /// </summary>
    public partial class FeatureSystemAddEdit
    {
        private FeatureViewModel _feature;      

        public FeatureSystemAddEdit(FeatureCreator creator)
        {
            InitializeComponent();
            DataContext = _feature;
          _creator = creator;
        }        

        public static DependencyProperty SourceProperty = DependencyProperty.Register("ParentFeature", typeof (FeatureViewModel),
                                                                                      typeof (FeatureSystemAddEdit));
      private FeatureCreator _creator;

      public FeatureViewModel ParentFeature 
        { 
            get { return (FeatureViewModel) GetValue(SourceProperty); } 
            set { SetValue(SourceProperty,value);}
        }

        public FeatureViewModel Item
        {
            get
            {
                if (_feature == null) _feature = _creator();
                return _feature;
            }
            set
            {
                _feature = value;
                DataContext = _feature;
            }
        }

        public bool SaveResults { get; set; }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            SaveResults = true;
        }

        private void BtnDiscard_Click(object sender, RoutedEventArgs e)
        {
            _feature = null;
            SaveResults = false;
            this.Close();
            
        }      
    }
}