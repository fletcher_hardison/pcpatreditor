﻿using System.Windows.Controls;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for ParametersView.xaml
  /// </summary>
  public partial class ParametersView : UserControl
  {
    public ParametersView(ParametersViewModel viewModel)
    {
      InitializeComponent();
      _viewModel = viewModel;
      DataContext = _viewModel;
    }

    private readonly ParametersViewModel _viewModel;
  }
}
