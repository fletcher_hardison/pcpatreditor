using System.Windows;


namespace PCPATR.Editor.MasterLists.Views
{
    /// <summary>
    /// Interaction logic for PopUpBox.xaml
    /// </summary>
    public partial class PopUpBox : Window
    {
        public PopUpBox()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            SaveResults = true;
            Close();
        }

        private void Discard(object sender, RoutedEventArgs e)
        {
            SaveResults = false;
            Close();
        }

        /// <summary>
        /// Result of the dialog
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Description displayed above input box
        /// </summary>
        public string Description { get; set; }

        public bool SaveResults { get; set; }
    }
}