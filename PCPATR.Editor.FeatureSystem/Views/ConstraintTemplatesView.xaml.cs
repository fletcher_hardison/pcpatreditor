﻿using PCPATR.Editor.MasterLists.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for ConstraintTemplatesView.xaml
  /// </summary>
  public partial class ConstraintTemplatesView : UserControl
  {
    public ConstraintTemplatesView(ConstraintTemplatesViewModel viewModel)
    {
      InitializeComponent();
      _viewModel = viewModel;
      DataContext = _viewModel;
    }

    private ConstraintTemplatesViewModel _viewModel;

    private void TemplateListBoxMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      _viewModel.ShowSelectedIdentifier();
    }
  }
}
