﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PCPATR.Editor.MasterLists.ViewModels;



namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for FilteredFeatureDialog.xaml
  /// </summary>
  public partial class FilteredFeatureDialog
  {
    public FilteredFeatureDialog()
    {
      InitializeComponent();
      DataContext = this;

      this.FeaturesTreeView.SelectedItemChanged += FeaturesTreeViewSelectedItemChanged;
    }

    void FeaturesTreeViewSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      SelectedFeature = (FeatureViewModel) FeaturesTreeView.SelectedItem;
    }

    public FeatureViewModel SelectedFeature { get; set; }

    public IEnumerable<FeatureViewModel> Features { get; set; }

    private void BtnOkClick(object sender, RoutedEventArgs e)
    {
      DialogResult = true;
      Close();
    }

    private void BtnCancelClick(object sender, RoutedEventArgs e)
    {
      DialogResult = false;
      SelectedFeature = null;
      Close();
    }


  }
}
