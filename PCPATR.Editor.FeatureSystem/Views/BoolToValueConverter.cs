//  ***************************************************************
//  File name: BoolToValueConverter.cs
//  Project: PCPATR.Editor.UI
//  Created by: Fletcher Hardison
//  From: Anthony W. Jones (see http://geekswithblogs.net/codingbloke/archive/2010/05/28/a-generic-boolean-value-converter.aspx)
//  Created on: 25,05,2012
// ***************************************************************

using System;
using System.Globalization;
using System.Windows.Data;

namespace PCPATR.Editor.MasterLists.Views
{
    public class BoolToValueConverter<T> :IValueConverter
    {
        public T FalseValue { get; set; }
        public T TrueValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return FalseValue;
            else
                return (bool)value ? TrueValue : FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null ? value.Equals(TrueValue) : false;
        }
    }

    public class BoolToStringConverter : BoolToValueConverter<string> { }
}