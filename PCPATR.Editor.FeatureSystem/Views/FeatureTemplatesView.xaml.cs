using System.Windows.Controls;
using System.Windows.Input;
using PCPATR.Editor.MasterLists.FeatureTemplateViewModels;

namespace PCPATR.Editor.MasterLists.Views
{
	/// <summary>
	/// Interaction logic for FeatureTemplatesView.xaml
	/// </summary>
	public partial class FeatureTemplatesView : UserControl
	{
		public FeatureTemplatesView(FeatureTemplatesRepositoryViewModel viewModel)
		{
			InitializeComponent();
		  _viewModel = viewModel;
		  DataContext = viewModel;
		}

	  private FeatureTemplatesRepositoryViewModel _viewModel;

	  private void TempalateModelsList_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
	  {
	    _viewModel.ShowSelectedIdentifier();
	  }
	}
}