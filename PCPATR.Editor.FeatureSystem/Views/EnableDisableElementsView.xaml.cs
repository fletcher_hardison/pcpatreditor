using System.Windows;
using System.Windows.Controls;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for EnableDisableElementsView.xaml
  /// </summary>
  public partial class EnableDisableElementsView : UserControl
  {
    public EnableDisableElementsView(EnableDisableElementsViewModel viewModel)
    {
      InitializeComponent();
      _viewModel = viewModel;
      DataContext = _viewModel;
    }

    private readonly EnableDisableElementsViewModel _viewModel;

    private void MenuItem_OnClick(object sender, RoutedEventArgs e)
    {
      _viewModel.ToggleSelectedRulesMethod(RulesListBox.SelectedItems);
    }

    private void ToggleFeatureTemplateUseWhenDebuggingMenuItem_OnClick(object sender, RoutedEventArgs e)
    {
      _viewModel.ToggleUseWhenDebuggingFeatureTemplatesMethod(FeatureTemplatesListBox.SelectedItems);
    }

    private void ToggleFeatureTemplateEnabledMenuItem_OnClick(object sender, RoutedEventArgs e)
    {
      _viewModel.ToggleSelectedFeatureTemplatesMethod(FeatureTemplatesListBox.SelectedItems);
    }

    private void ToggleRuleUseWhenDebuggingMenu_OnClick(object sender, RoutedEventArgs e)
    {
      _viewModel.ToggleUseWhenDebuggingRulesMethod(RulesListBox.SelectedItems);
    }
  }
}
