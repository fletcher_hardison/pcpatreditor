using System.Windows.Controls;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.Views
{
    /// <summary>
    /// Interaction logic for SelectedFeatureInfoView.xaml
    /// </summary>
    public partial class SelectedFeatureInfoView : UserControl
    {
        private FeatureSystemViewModel _feature;

        public SelectedFeatureInfoView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the selecteFeatureInfoView class 
        /// and sets the data context to the passed parameter.
        /// </summary>
        /// <param name="featureViewModel">view model to be data context</param>
        public SelectedFeatureInfoView(FeatureSystemViewModel featureViewModel): this()
        {
            _feature = featureViewModel;

        }
    }
}
