﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.Views
{
    /// <summary>
    /// Interaction logic for MasterListsView.xaml
    /// </summary>
    public partial class MasterListsView : UserControl
    {
        public MasterListsView(MasterListsViewModel masterLists)
        {
            InitializeComponent();
            MasterLists = masterLists;
            DataContext = MasterLists;
        }

        public MasterListsViewModel MasterLists { get; set; }
    }
}
