﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for CollectionFeaturesView.xaml
  /// </summary>
  public partial class CollectionFeaturesView : UserControl
  {
    public CollectionFeaturesView(CollectionFeaturesRepositoryViewModel viewModel)
    {
      InitializeComponent();
      _viewModel = viewModel;
      DataContext = _viewModel;
    }

    private readonly CollectionFeaturesRepositoryViewModel _viewModel;
  }
}
