﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.Views.Dialogs
{
  /// <summary>
  /// Interaction logic for AddNewIdWithValidationControl.xaml
  /// </summary>
  public partial class AddNewIdWithValidationControl : UserControl
  {
    public AddNewIdWithValidationControl(NewIdServiceViewModel viewModel)
    {
      InitializeComponent();
      _viewModel = viewModel;
      DataContext = _viewModel;
    }

    public bool IsValid { get { return _viewModel.IsValid; } }

    public string PropsedId
    {
      get { return _viewModel.ProposedId; }
      set { _viewModel.ProposedId = value; }
    }

    private readonly NewIdServiceViewModel _viewModel;
  }
}
