﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PCPATR.Common;

namespace PCPATR.Editor.MasterLists.Views.Dialogs
{
  /// <summary>
  /// Interaction logic for NameIdRefListDialog.xaml
  /// </summary>
  public partial class NameIdRefListDialog 
  {
    public NameIdRefListDialog()
    {
      InitializeComponent();
      DataContext = this;
    }

    public ObservableCollection<NameIdRef> Data { get; set; }

    public NameIdRef Selected { get; set; }

    private void ButtonClick1(object sender, RoutedEventArgs e)
    {
      if (this.Selected == null) return;
      this.Close();
    }

    private void BtnCancelClick(object sender, RoutedEventArgs e)
    {
      this.Selected = null;
      this.Close();
    }
  }
}
