﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using PCPATR.Common.Interfaces;




namespace PCPATR.Editor.MasterLists.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddSymbolRefDialog.xaml
    /// </summary>
    public partial class AddSymbolRefDialog : Window, INotifyPropertyChanged
    {
        
        public AddSymbolRefDialog()
        {
            this.InitializeComponent();
            this.DataContext = this;
        }


        private ObservableCollection<ISymbol> _data;

        public ObservableCollection<ISymbol> Data
        {
            get { return _data; }
            set { _data = value;
                this.RaisePropertyChanged("Data");
            }
        }

        private ISymbol _selectedItem;

        public ISymbol SelectedItem
        {
            get { return _selectedItem; }
            set { 
                _selectedItem = value;
                this.RaisePropertyChanged("SelectedItem");
            }
        }

        private bool _save;
        public bool SaveResult
        {
            get { return _save; }
            set { _save = value; }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged!= null) {
                this.PropertyChanged(this,new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
         Close();   
        }
    }
}
