﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.Views.Dialogs
{
  /// <summary>
  /// Interaction logic for EnterNewIdWithValidationDialog.xaml
  /// </summary>
  public partial class EnterNewIdWithValidationDialog
  {
    public EnterNewIdWithValidationDialog(NewIdServiceViewModel viewModel)
    {
      InitializeComponent();
      _viewModel = viewModel;
      DataContext = _viewModel;
    }

    public string ResultId { get; set; }

    private void BtnOk_OnClick(object sender, RoutedEventArgs e)
    {
      DialogResult = true;
      ResultId = _viewModel.ProposedId;
      Close();
    }

    private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
    {
      DialogResult = false;
      ResultId = null;
      Close();
    }

    private readonly NewIdServiceViewModel _viewModel;
  }
}
