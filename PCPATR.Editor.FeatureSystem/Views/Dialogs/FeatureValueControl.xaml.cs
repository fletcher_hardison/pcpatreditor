﻿using System.Collections.Generic;
using System.Windows;
using PCPATR.Common;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.Views.Dialogs
{
  /// <summary>
  /// Interaction logic for FeatureValueDialog.xaml
  /// </summary>
  public partial class FeatureValueControl 
  {
    public FeatureValueControl()
    {
      InitializeComponent();
      DataContext = this;
    }

    public NameIdRef ResultNameId { get; set; }

    public IEnumerable<FeatureViewModel> ClosedFeatures { get; set; }
    public FeatureViewModel SelectedFeature { get; set; }         
  }
}
