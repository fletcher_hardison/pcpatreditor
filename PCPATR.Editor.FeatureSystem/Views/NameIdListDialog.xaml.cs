﻿using System.Collections.Generic;
using System.Windows;
using PCPATR.Common;


namespace PCPATR.Editor.MasterLists.Views
{
  /// <summary>
  /// Interaction logic for NameIdListDialog.xaml
  /// </summary>
  public partial class NameIdListDialog 
  {
    public NameIdListDialog()
    {
      InitializeComponent();
      DataContext = this;
    }

    public IEnumerable<NameId> data { get; set; }

    public NameId selected { get; set; }

    private void ButtonClick1(object sender, RoutedEventArgs e)
    {
      if (this.selected == null) return;
      this.Close();
    }

    private void BtnCancelClick(object sender, RoutedEventArgs e)
    {
      this.selected = null;
      this.Close();
    }

    
  }
}
