using System.Windows.Input;

namespace PCPATR.Editor.MasterLists
{
  public interface ICommandViewModel
  {
    ICommand Command { get; }
    string DisplayName { get; set; }
  }
}
