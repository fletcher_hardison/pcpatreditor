﻿//  ***************************************************************
//  File name: MainWindowRegionNames.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 07,09,2012
// ***************************************************************
namespace PCPATR.Editor.MasterLists
{
    public static class MainWindowRegionNames
    {
        public const string MAIN_MISC_REGION = "MainMiscRegion";

        public const string MAIN_NAVIGATION_REGION = "MainNavigationRegion";

        public const string MASTER_REGION = "MasterRegion";

        public const string DETAILS_REGION = "DetailsRegion";

      public const string MASTER_LISTS_DETAIL_REGION = "MasterListsDetailRegion";
    }
}