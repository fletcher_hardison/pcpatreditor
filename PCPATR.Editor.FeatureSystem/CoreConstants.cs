﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;

namespace PCPATR.Editor.MasterLists
{
  public static class CoreConstants
  {
    public const string START_PAGE_VIEW = "StartPage";
    public const string ENABLE_DISABLE_ELEMENTS_VIEW = "EnableDisableElementsView";
    public const string  PATR_PARSER_VIEW = "PatrParserView"; 
    public const string FEATURE_SYSTEM_VIEW = "FeatureSystemView";

    public const string MASTER_LISTS_VIEW = "MasterListsView";

    public const string LEXICAL_RULES_VIEW = "LexicalRulesView";

    public const string RULE_NAV_PARAMETER = "RuleModelData";

    public const string RULE_DETAIL_VIEW = "RuleDetailView";

    public const string RULES_MASTER_VIEW = "RulesMasterView";

    public const string CONSTRAINT_VIEW = "ConstraintView";

    public const string CONSTRAINT_TEMPLTES_VIEW = "ConstraintTemplatesView";

    public const string FEATURE_TEMPLATES_VIEW = "FeatureTemplatesView";

    public const string PARAMETERS_VIEW = "ParametersView";

    public const string SYMBOLS_VIEW = "SymbolsEditView";

    public const string COLLECTION_FEATURES_VIEW = "CollectionFeaturesView";
  }
}
