﻿//  ***************************************************************
//  File name: IOService.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 11,01,2013
// ***************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using PCPATR.Common;
using PCPATR.DataAccess.Parser;
using PCPATR.DataAccess.Writer;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.Models;
using PCPATR.Models.FeatureSystem;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Models.MasterLists;
using PCPATR.Models.Parameters;
using PCPATR.Models.Rules;

namespace PCPATR.Editor.MasterLists.Services
{
  /// <summary>
  /// Responcible for loading and saving grammar files
  /// </summary>
  public class IOService
  {
    public IOService(DataParser parser, DataWriter writer, IO<XmlDocument> docLoader)
    {
      _parser = parser;
      _writer = writer;
      _docLoader = docLoader;
    }

    public bool OperationInProgress { get; set; }

    public void BeginIOOperation()
    {
      OperationInProgress = true;
      _ioOperations = new List<Action<XmlDocument>>();

    }

    public PcPatrGrammar LoadGrammarFromXmlDoc(string filePath)
    {
      var grammmar = new PcPatrGrammar();
      var doc = new XmlDocument();
      try
      {
        doc.Load(filePath);
      }
      catch
      {
        return null;
      }
      
      grammmar.GrammarDoc = doc;

      grammmar.Rules = _parser.ParseRulesRepository(doc.SelectSingleNode("//rules"));

      grammmar.FeatureTemplates = _parser.ParseFeatureTemplatesCollection(doc.SelectSingleNode("//featureTemplates"));

      grammmar.ConstraintTemplates = _parser.ParseConstraintTemplates(doc.SelectSingleNode("//constraintTemplates"));

      grammmar.MasterLists = _parser.ParseMasterLists(doc.SelectSingleNode("//masterLists"));

      grammmar.Parameters = _parser.ParseParameters(doc.SelectSingleNode("//parameters"));

      grammmar.LexicalRules = _parser.ParseLexicalRules(doc.SelectSingleNode("//lexicalRules")).Rules;

      return grammmar;
    }

    public bool SaveGrammar(PcPatrGrammar grammar, string filePath)
    {
      //var xdoc = IOHelperMethods.GenerateMinimumGrammarXmlDocument();
      IOHelperMethods.CopyDtdFileToDir(filePath);
      //xdoc.Save(IOHelperMethods.AppendXmlExtensionIfMissing(filePath));

      var doc = new XmlDocument();
      doc.Load(filePath);

      var docType = doc.CreateDocumentType("patrGrammar", null, OutsideWorld.GetDtdLocation(), null);
      if (doc.DocumentType != null)
      {

        doc.RemoveChild(doc.DocumentType);
      }

      doc.InsertBefore(docType, doc.DocumentElement);

      ReplaceOrAddNode(DataModelNames.PATR_GRAMMAR, _writer.WriteRulesRepository(doc, grammar.Rules), doc);
      ReplaceOrAddNode(DataModelNames.PATR_GRAMMAR, _writer.WriteConstraintTemplates(doc, grammar.ConstraintTemplates),
                       doc);
      ReplaceOrAddNode(DataModelNames.PATR_GRAMMAR,
                       _writer.WriteFeatureTemplatesCollection(doc, grammar.FeatureTemplates), doc);
      ReplaceOrAddNode(DataModelNames.PATR_GRAMMAR, _writer.WriteParameters(doc, grammar.Parameters), doc);
      ReplaceOrAddNode(DataModelNames.PATR_GRAMMAR, _writer.WriteLexicalRules(doc, grammar.LexicalRules), doc);
      SaveMasterLists(doc, grammar.MasterLists, _writer);
      
      EnforceDtdORder(doc);

      doc.Save(filePath);
      return true;
    }

    
    
    private static void SaveMasterLists(XmlDocument doc, MasterListsModel model, DataWriter writer)
    {
      ReplaceOrAddNode(DataModelNames.MASTER_LISTS, writer.WriteNonTerminalSymbols(doc, model.NonTerminals), doc);
      ReplaceOrAddNode(DataModelNames.MASTER_LISTS, writer.WriteTerminalSymbols(doc, model.Terminals), doc);
      ReplaceOrAddNode(DataModelNames.MASTER_LISTS, BuildBuiltInPrimitives(doc), doc);
      ReplaceOrAddNode(DataModelNames.MASTER_LISTS, writer.WriteFeatureSystem(doc, model.GrammarFeatureSystem), doc);
      ReplaceOrAddNode(DataModelNames.MASTER_LISTS, writer.WriteCollectionFeatures(doc, model.GrammarCollectionFeatures), doc);
    }

    /// <summary>
    /// enforeces the dtd node for for the top level noes
    /// </summary>
    private static void EnforceDtdORder(XmlDocument doc)
    {
      var patrGrammar = doc.SelectSingleNode("//patrGrammar");      
      var featureTemplates = patrGrammar.SelectSingleNode("featureTemplates");
      var constraintTemplates = patrGrammar.SelectSingleNode("constraintTemplates");
      var rules = patrGrammar.SelectSingleNode("rules");
      var parameters = patrGrammar.SelectSingleNode("parameters");
      var lexicalRules = patrGrammar.SelectSingleNode("lexicalRules");
      var masterLists = patrGrammar.SelectSingleNode("masterLists");
      
      patrGrammar.RemoveAll();
      XmlNode lastNode = null;
      
      InsertNextIfNotNull(patrGrammar, ref lastNode, featureTemplates);
      InsertNextIfNotNull(patrGrammar, ref lastNode, constraintTemplates);
      InsertNextIfNotNull(patrGrammar, ref lastNode, rules);
      InsertNextIfNotNull(patrGrammar, ref lastNode, parameters);
      InsertNextIfNotNull(patrGrammar, ref lastNode, lexicalRules);
      InsertNextIfNotNull(patrGrammar, ref lastNode, masterLists);
    }
    
    private static void InsertNextIfNotNull(XmlNode parentNode, ref XmlNode lastNode, XmlNode nextNode)
    {
      if (nextNode == null) return;
      if (lastNode == null)
      {
        parentNode.AppendChild(nextNode);
      } else
      {
        parentNode.InsertAfter(nextNode, lastNode);
      }

      lastNode = nextNode;
    }

    private static XmlNode BuildBuiltInPrimitives(XmlDocument doc)
    {
      var element = doc.CreateElement(DataModelNames.BUILT_IN_PRIMATIVES);
      element.InnerXml = "<cat id=\"pcpatrGrammarCat\" /><lex id=\"pcpatrGrammarLex\" /><gloss id=\"pcpatrGrammarGloss\" /><rootgloss id=\"pcpatrGrammarRootGloss\" /><none id=\"pcpatrGrammarNone\" />";

      return element;
    }

    public string LoadGrammar()
    {
      OperationInProgress = false;
      var result = LoadDocumentIfClosed().Bind(ExecuteIOOperations);
      _ioOperations = new List<Action<XmlDocument>>();
      if (result.HasException)
        return "Error loading grammar file (Please zip your grammar and send it to Fhardison@gmail.com)." +
          Environment.NewLine + "Error: " + result.Exception.ToString();

      return "Data Loaded sucessfully";
    }

    public string SaveGrammar(string filePath)
    {
      OperationInProgress = false;
      var result = LoadDocumentIfClosed().Bind(ExecuteIOOperations).Bind(EnforceGrammarNodeOrder).FMap(f =>
      {
        f.Save(filePath);
        return true;
      });

      if (result.HasException)
      {
        return "Error saving grammar file (no file written)." + Environment.NewLine + "Error: " + result.Exception.ToString();
      }

      return "Save sucessful";
    }



    /// <summary>
    /// LoadRules xml -> RulesRepository
    /// </summary>
    public void LoadRules(Action<RulesRepository> action)
    {
      _ioOperations.Add(f => action(_parser.ParseRulesRepository(f.SelectSingleNode("//rules"))));
    }

    /// <summary>
    /// LoadConstriantTemplates xml -> ConstraintTempaltesModel
    /// </summary>
    public void LoadConstriantTemplates(Action<ConstraintTemplatesModel> action)
    {
      _ioOperations.Add(f => action(_parser.ParseConstraintTemplates(f.SelectSingleNode("//constraintTemplates"))));
    }

    /// <summary>
    /// LoadFeatureTemplates xml -> IEnumerable of FeatureTemplateModel
    /// </summary>
    public void LoadFeatureTemplates(Action<IEnumerable<FeatureTemplateModel>> action)
    {
      _ioOperations.Add(f =>
      {
        var templates = f.SelectNodes("//featureTemplate");
        if (templates == null) return;
        action(templates.Cast<XmlNode>().Select(_parser.ParseFeatureTemplate).Where(result => result != null).ToList());
      });
    }

    /// <summary>
    /// LoadMasterLists xml -> MasterListsModel
    /// </summary>
    public void LoadMasterLists(Action<MasterListsModel> action)
    {
      _ioOperations.Add(f => action(_parser.ParseMasterLists(f.SelectSingleNode("//masterLists"))));
    }

    //LoadParameters xml -> ParametersModel
    //Purpose: loads parameters from xml data file;
    public void LoadParameters(Action<ParametersModel> action)
    {
      _ioOperations.Add(f => action(_parser.ParseParameters(f.SelectSingleNode("//parameters"))));
    }

    //LoadLexicalRules xml -> LexicalRulesModel
    public void LoadLexicalRules(Action<LexicalRulesRepository> action)
    {
      _ioOperations.Add(f => action(_parser.ParseLexicalRules(f.SelectSingleNode("//lexicalRules"))));
    }


    //SaveParameters ParametersModel -> xml
    public void SaveParameters(ParametersModel model)
    {
      _ioOperations.Add(doc => ReplaceOrAddNode(DataModelNames.PATR_GRAMMAR, _writer.WriteParameters(doc, model), doc));
    }

    //SaveMasterLists MasterListsModel -> xml
    public void SaveMasterLists(MasterListsModel model)
    {
      _ioOperations.Add(
        doc => ReplaceOrAddNode(DataModelNames.MASTER_LISTS, _writer.WriteNonTerminalSymbols(doc, model.NonTerminals), doc));
      _ioOperations.Add(
        doc => ReplaceOrAddNode(DataModelNames.MASTER_LISTS, _writer.WriteTerminalSymbols(doc, model.Terminals), doc));
      _ioOperations.Add(doc => ReplaceOrAddNode(DataModelNames.MASTER_LISTS, _writer.WriteFeatureSystem(doc, model.GrammarFeatureSystem), doc));
      _ioOperations.Add(doc => ReplaceOrAddNode(DataModelNames.MASTER_LISTS, _writer.WriteCollectionFeatures(doc, model.GrammarCollectionFeatures), doc));


    }
    public void SaveFeatures(FeatureSystem model) //TODO make this write all master lists
    {
      _ioOperations.Add(doc => ReplaceOrAddNode(DataModelNames.MASTER_LISTS, _writer.WriteFeatureSystem(doc, model), doc));
    }

    //SaveConstraintTemplates ConstraintTemplatesModel -> xml
    public void SaveConstraintTemplates(IEnumerable<ConstraintTemplateModel> templates)
    {
      _ioOperations.Add(doc => ReplaceOrAddNode(DataModelNames.PATR_GRAMMAR, _writer.WriteConstraintTemplates(doc, templates), doc));
    }

    //SaveRules RulesRepository -> xml
    public void SaveRules(IEnumerable<Rule> rules)
    {
      _ioOperations.Add(doc => ReplaceOrAddNode(DataModelNames.PATR_GRAMMAR, _writer.WriteRulesRepository(doc, rules), doc));
    }

    //SaveFeatureTemplates IEnumerable of FeatureTemplateModel -> xml
    public void SaveFeatureTemplates(IEnumerable<FeatureTemplateModel> list)
    {
      _ioOperations.Add(doc => ReplaceOrAddNode(DataModelNames.PATR_GRAMMAR, _writer.WriteFeatureTemplatesCollection(doc, list), doc));
    }

    //SaveLexicalRules LexicalRulesModel -> xml
    public void SaveLexicalRules(IEnumerable<LexicalRule> rules)
    {
      _ioOperations.Add(doc => ReplaceOrAddNode(DataModelNames.PATR_GRAMMAR, _writer.WriteLexicalRules(doc, rules), doc));
    }

    //SaveCollectionFeatures: CollectionFeatures -> xml
    public void SaveCollectionFeatures(CollectionFeatures features)
    {
      _ioOperations.Add(doc => ReplaceOrAddNode(DataModelNames.MASTER_LISTS, _writer.WriteCollectionFeatures(doc, features), doc));
    }



    private Exceptional<XmlDocument> LoadDocumentIfClosed()
    {
      if (OperationInProgress) return new Exceptional<XmlDocument>(new Exception("IO operation already in progress"));
      _doc = _docLoader();
      return _doc.ToExceptional();
    }

    private Exceptional<XmlDocument> ExecuteIOOperations(XmlDocument doc)
    {
      try
      {
        _ioOperations.ForEach(action => action(doc));
      }
      catch (Exception ex)
      {
        return new Exceptional<XmlDocument>(ex);
      }

      return doc.ToExceptional();
    }

    private static void ReplaceOrAddNode(string parentNodeName, XmlNode nodeToAdd, XmlDocument doc)
    {
      if (nodeToAdd == null) return;
      var target = doc.SelectSingleNode("//" + nodeToAdd.Name);
      var parentNode = doc.SelectSingleNode("//" + parentNodeName);

      if (target != null)
      {

        target.InnerXml = nodeToAdd.InnerXml;
      }
      else
      {
        parentNode.AppendChild(nodeToAdd);
      }
    }

    //EnforceGrammarNodeOrder: xml -> xml
    //purpose: enforce the required node order in the DTD for grammar xml files.
    private Exceptional<XmlDocument> EnforceGrammarNodeOrder(XmlDocument doc)
    {
      var grammar = doc.SelectSingleNode("//patrGrammar");
      if (grammar == null) return new Exceptional<XmlDocument>(new Exception("PCPATR grammr file mising required patrGrammar node"));
      var comments = grammar.SelectNodes("./comment");
      var featureTemplates = grammar.SelectSingleNode("//" + DataModelNames.FEATURE_TEMPLATES);
      var constraintTemplates = grammar.SelectSingleNode("//" + DataModelNames.CONSTRAINT_TEMPLATES);
      var rules = grammar.SelectSingleNode("//" + DataModelNames.RULES);
      var parameters = grammar.SelectSingleNode("//" + DataModelNames.PARAMETERS);
      var lexicalRules = grammar.SelectSingleNode("//" + DataModelNames.LEXICAL_RULES);
      var masterLists = grammar.SelectSingleNode("//" + DataModelNames.MASTER_LISTS);
      grammar.RemoveAll();

      Func<XmlNode, XmlNode, XmlNode> appendIfNotNull = (gram, node) =>
      {
        if (node == null) return gram;
        gram.AppendChild(node);
        return gram;
      };

      comments.Each(f => appendIfNotNull(grammar, f));
      var result = appendIfNotNull(grammar, featureTemplates).ToExceptional()
        .FMap(f => appendIfNotNull(f, constraintTemplates))
        .FMap(f => appendIfNotNull(f, rules))
      .FMap(f => appendIfNotNull(f, parameters))
      .FMap(f => appendIfNotNull(f, lexicalRules))
      .FMap(f => appendIfNotNull(f, masterLists));

      if (result.HasException)
      {
        return new Exceptional<XmlDocument>(result.Exception);
      }

      return doc.ToExceptional();
    }

    public class PcPatrGrammar
    {
      public XmlDocument GrammarDoc { get; set; }

      public IEnumerable<Rule> Rules { get; set; }

      public IEnumerable<ConstraintTemplateModel> ConstraintTemplates { get; set; }

      public IEnumerable<FeatureTemplateModel> FeatureTemplates { get; set; }

      public ParametersModel Parameters { get; set; }

      public IEnumerable<LexicalRule> LexicalRules { get; set; }

      public MasterListsModel MasterLists { get; set; }
    }

    private List<Action<XmlDocument>> _ioOperations;
    private readonly IO<XmlDocument> _docLoader;
    private readonly DataParser _parser;
    private readonly DataWriter _writer;
    private XmlDocument _doc;
  }
}