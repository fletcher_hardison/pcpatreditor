//  ***************************************************************
//  File name: NavigationManager.cs
//  Project: PCPATR.Services
//  Created by: 
//  Created on: 11,10,2012
// ***************************************************************

using Microsoft.Practices.Prism.Regions;


namespace PCPATR.Editor.MasterLists.Services
{
    public class NavigationManager : INavigationManager
    {
        private IRegionManager _regionManager;

        private ICargoBus _helper;

        public  NavigationManager(IRegionManager manager, ICargoBus helper)
        {
            _regionManager = manager;
            _helper = helper;
        }

        public  IRegionManager RegionManager
        {
            get { return _regionManager; }
        }

        public ICargoBus CargoBus
        {
            get { return _helper; }
        }

    }
}