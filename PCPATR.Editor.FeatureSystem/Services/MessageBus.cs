//  ***************************************************************
//  File name: MessageBus.cs
//  Project: PCPATR.Services
//  Created by: Brent Edwards (http://brentedwards.net/2010/04/13/roll-your-own-simple-message-bus-event-aggregator/) (13 April 2010)
//  Created on: 
// ***************************************************************

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using PCPATR.Common.Interfaces;


//Namespace changed to match required namespace for this project.

namespace PCPATR.Editor.MasterLists.Services
{
    /// <summary>
    /// Handles messages thier subscribers and publishers
    /// </summary>
    [DebuggerStepThrough]
    public class MessageBus : IMessageBus
    {
        private readonly Dictionary<Type, List<object>> _Subscribers = new Dictionary<Type, List<Object>>();

        #region IMessageBus Members

        public virtual void Subscribe<TMessage>(Action<TMessage> handler)
        {
            if (this._Subscribers.ContainsKey(typeof (TMessage))) {
                List<object> handlers = this._Subscribers[typeof (TMessage)];
                handlers.Add(handler);
            } else {
                var handlers = new List<Object>();
                handlers.Add(handler);
                this._Subscribers[typeof (TMessage)] = handlers;
            }
        }

        public virtual void Unsubscribe<TMessage>(Action<TMessage> handler)
        {
            if (this._Subscribers.ContainsKey(typeof (TMessage))) {
                List<object> handlers = this._Subscribers[typeof (TMessage)];
                handlers.Remove(handler);

                if (handlers.Count == 0) this._Subscribers.Remove(typeof (TMessage));
            }
        }

        public virtual void Publish<TMessage>(TMessage message)
        {
            if (this._Subscribers.ContainsKey(typeof (TMessage))) {
                List<object> handlers = this._Subscribers[typeof (TMessage)];
                foreach (Action<TMessage> handler in handlers) {
                    handler.Invoke(message);
                }
            }
        }

        public virtual void Publish(Object message)
        {
            Type messageType = message.GetType();
            if (this._Subscribers.ContainsKey(messageType)) {
                List<object> handlers = this._Subscribers[messageType];
                foreach (object handler in handlers) {
                    Type actionType = handler.GetType();

                    // ReSharper disable RedundantExplicitArrayCreation
                    MethodInfo invoke = actionType.GetMethod("Invoke", new Type[] {messageType});
                    invoke.Invoke(handler, new Object[] {message});
                    // ReSharper restore RedundantExplicitArrayCreation
                }
            }
        }

        public virtual void Publish<TMessage>(TMessage message, Action<TMessage> callback)
        {
            if (this._Subscribers.ContainsKey(typeof (TMessage))) {
                List<object> handlers = this._Subscribers[typeof (TMessage)];
                foreach (Action<TMessage> handler in handlers) {
                    handler.Invoke(message);
                }
            }
            callback(message);
        }

        public virtual void Publish(Object message, Action<Object> callback)
        {
            Type messageType = message.GetType();
            if (this._Subscribers.ContainsKey(messageType)) {
                List<object> handlers = this._Subscribers[messageType];
                foreach (object handler in handlers) {
                    Type actionType = handler.GetType();

                    // ReSharper disable RedundantExplicitArrayCreation
                    MethodInfo invoke = actionType.GetMethod("Invoke", new Type[] {messageType});

                    invoke.Invoke(handler, new Object[] {message});
                    // ReSharper restore RedundantExplicitArrayCreation
                }
                callback(message);
            }
        }

        #endregion
    }
}