namespace PCPATR.Editor.MasterLists.Services
{
    public interface ICargoBus
    {
        bool contains_parameter(int hash_code);

        void save_parameter<T>(int hash_code, T value);

        T retrive_parameter<T>(int hash_code, bool remove_code);

        string HashParamaterName { get; }
    }
}