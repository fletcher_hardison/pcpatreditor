//  ***************************************************************
//  File name: CargoBus.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 11,10,2012
// ***************************************************************

using System.Collections.Generic;
using System.Diagnostics;


namespace PCPATR.Editor.MasterLists.Services
{
    [DebuggerStepThrough]
    public class CargoBus : ICargoBus
    {
        private readonly Dictionary<int, object> _nav_object_dict = new Dictionary<int, object>();

        public bool contains_parameter(int hash_code)
        {
            return _nav_object_dict.ContainsKey(hash_code);
        }

        public void save_parameter<T>(int hash_code, T value)
        {
            _nav_object_dict.Add(hash_code,value);
        }

        public T retrive_parameter<T>(int hash_code, bool remove_code)
        {
            var result = _nav_object_dict[hash_code];
            if (remove_code) _nav_object_dict.Remove(hash_code);
            return (T) result;
        }

        public string HashParamaterName
        {
            get { return "hash"; }
        }
    }
}