using Microsoft.Practices.Prism.Regions;


namespace PCPATR.Editor.MasterLists.Services
{
    public interface INavigationManager {
        IRegionManager RegionManager { get; }

        ICargoBus CargoBus { get; }
    }
}