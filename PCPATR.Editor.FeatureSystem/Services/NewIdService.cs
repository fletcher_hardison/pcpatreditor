//  ***************************************************************
//  File name: NewIdService.cs
//  Project: PCPATR.Services
//  Created by: 
//  Created on: 09,01,2013
// ***************************************************************

using PCPATR.Common;

namespace PCPATR.Editor.MasterLists.Services
{
  public class NewIdService
  {
    public bool IsProposedIdVaild(string proposedId)
    {
      if (string.IsNullOrEmpty(proposedId)) return false;
      return !NameId.id_and_names.ContainsKey(proposedId);
    }

    public void RemoveId(string id)
    {
      if (NameId.id_and_names.ContainsKey(id))
      {
        NameId.id_and_names.Remove(id);
      }
    }

    public void AddId(string id, string name)
    {
      if (!NameId.id_and_names.ContainsKey(id))
      {
        NameId.id_and_names.Add(id,name);
      }
    }

    public bool ContainsId(string id)
    {
      if (string.IsNullOrEmpty(id)) return false;
      return NameId.id_and_names.ContainsKey(id);
    }
  }
}