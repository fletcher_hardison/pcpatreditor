﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.Models.MasterLists;

namespace PCPATR.Editor.MasterLists.Services
{
  public class CollectionFeaturesMapper
  {
    public CollectionFeaturesMapper(IUnityContainer container)
    {
      Mapper.AddProfile(new CollectionFeaturesToViewModelProfile(container));
      Mapper.AddProfile(new CollectionFeaturesToModelProfile(container));
      Mapper.AssertConfigurationIsValid();
    }


    public CollectionFeaturesRepositoryViewModel MapRepositoryToViewModel(CollectionFeatures model)
    {
      return Mapper.Map<CollectionFeaturesRepositoryViewModel>(model);
    }

    public CollectionFeatures MapRepositoryToModel(CollectionFeaturesRepositoryViewModel viewModel)
    {
      return Mapper.Map<CollectionFeatures>(viewModel);
    }

    public CollectionFeatureViewModel MapRepositoryToViewModel(CollectionFeature model)
    {
      return Mapper.Map<CollectionFeatureViewModel>(model);
    }

    public CollectionFeature MapRepositoryToModel(CollectionFeatureViewModel viewModel)
    {
      return Mapper.Map<CollectionFeature>(viewModel);
    }

  }

  public class CollectionFeaturesToViewModelProfile : Profile
  {
    public CollectionFeaturesToViewModelProfile(IUnityContainer container)
    {
      _collectionFeatureBuilder = r => container.Resolve<CollectionFeatureViewModel>();
      _repositoryBuilder = r => container.Resolve<CollectionFeaturesRepositoryViewModel>();
    }


    protected override void Configure()
    {
      CreateMap<CollectionFeatures, CollectionFeaturesRepositoryViewModel>().ConstructUsing(_repositoryBuilder)
        .ForMember(d => d.SelectedFeature, opt => opt.Ignore())
        .ForMember(d => d.AddCollectionFeature, opt => opt.Ignore())
        .ForMember(d => d.RemoveCollectionFeature, opt => opt.Ignore())
        .ForMember(d => d.DisplayName, opt => opt.Ignore());

      CreateMap<CollectionFeature, CollectionFeatureViewModel>().ConstructUsing(_collectionFeatureBuilder)
        .ForMember(d => d.SetId, opt => opt.Ignore())
        .ForMember(d => d.DisplayName, opt => opt.Ignore());

      CreateMap<List<string>, CommentsField>().ConvertUsing<CommentsFieldConverter>();
    }

    private readonly Func<ResolutionContext, CollectionFeatureViewModel> _collectionFeatureBuilder;
    private readonly Func<ResolutionContext, CollectionFeaturesRepositoryViewModel> _repositoryBuilder;
  }

  public class CollectionFeaturesToModelProfile : Profile
  {
    public CollectionFeaturesToModelProfile(IUnityContainer container)
    {
      _collectionFeatureBuilder = r => container.Resolve<CollectionFeature>();
      _collectionFeaturesBuilder = r => container.Resolve<CollectionFeatures>();
    }

    protected override void Configure()
    {
      CreateMap<CollectionFeaturesRepositoryViewModel, CollectionFeatures>().ConstructUsing(_collectionFeaturesBuilder);

      CreateMap<CollectionFeatureViewModel, CollectionFeature>().ConstructUsing(_collectionFeatureBuilder);

      CreateMap<CommentsField, List<string>>().ConvertUsing<CommentsFieldBackConverter>();
    }

    private readonly Func<ResolutionContext, CollectionFeatures> _collectionFeaturesBuilder;
    private readonly Func<ResolutionContext, CollectionFeature> _collectionFeatureBuilder;
  }
}
