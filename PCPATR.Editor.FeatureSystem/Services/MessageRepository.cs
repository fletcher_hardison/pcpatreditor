//  ***************************************************************
//  File name: MessageRepository.cs
//  Project: PCPATR.Services
//  Created by: 
//  Created on: 20,09,2012
// ***************************************************************

using System;
using System.Collections.Generic;
using PCPATR.Common.Interfaces;


namespace PCPATR.Editor.MasterLists.Services
{
    public class MessageRepository : IMessageRepository
    {
        private readonly IDictionary<Type, Delegate> _dialogs;


        public MessageRepository()
        {
            _dialogs = new Dictionary<Type, Delegate>();
        }

        public void RegisterMessage<T>( Func<T> messageCreator)
        {

            if (messageCreator == null) throw new ArgumentNullException("messageCreator");

            if (!_dialogs.ContainsKey(typeof(T)))
            {
                _dialogs.Add(typeof(T), messageCreator);
            }
        }

        public void UnregisterMessage<T>()
        {
            

            if (_dialogs.ContainsKey(typeof(T)))
            {
                _dialogs.Remove(typeof(T));
            }
        }

        public T GetMessage<T>()
        {
            

            var myDel = _dialogs[typeof(T)];
            /*The result is null becuase the action is not returning a value*/
            var result = (T)myDel.DynamicInvoke();
            return result;

        }
    }
}