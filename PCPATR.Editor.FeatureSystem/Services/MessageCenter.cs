//  ***************************************************************
//  File name: MessageCenter.cs
//  Project: PCPATR.Services
//  Created by: 
//  Created on: 11,10,2012
// ***************************************************************

using PCPATR.Common.Interfaces;


namespace PCPATR.Editor.MasterLists.Services
{
    public class MessageCenter : IMessageCenter
    {
        
         public MessageCenter(IMessageBus bus, IMessageRepository repo )
         {
             this.MessageBus = bus;
             this.MessageRepository = repo;
         }

         public IMessageBus MessageBus { get; set; }

         public IMessageRepository MessageRepository { get; set; }
    }
}