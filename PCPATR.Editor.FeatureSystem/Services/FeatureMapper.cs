using System;
using System.Diagnostics;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.ViewModels;

using PCPATR.Models.FeatureSystem;

namespace PCPATR.Editor.MasterLists.Services
{
  public class FeatureMapper
  {
    private readonly Func< FeatureViewModel> _featureViewModelBuilder;
    private readonly Func< FeatureSystemViewModel> _featureSystemViewModelBuilder;
    private readonly Func< Feature> _featureModelBuilder;
    private readonly Func< FeatureSystem> _featureSystemModelBuilder;

    public FeatureMapper(IUnityContainer container)
    {
      _featureViewModelBuilder = () => container.Resolve<FeatureViewModel>();
      _featureSystemViewModelBuilder = () => container.Resolve<FeatureSystemViewModel>();
      

      _featureModelBuilder = () => container.Resolve<Feature>();
      _featureSystemModelBuilder = () => container.Resolve<FeatureSystem>();

    }

    public FeatureViewModel MapToViewModel(Feature model)
    {
      return MapFeatureToFeatureViewModel(model, _featureViewModelBuilder);
    }

    public Feature MapToModel(FeatureViewModel model)
    {
      return MapFeatureViewModelToModel(model,_featureModelBuilder);
    }

    public FeatureSystemViewModel MapRepositoryToViewModel(FeatureSystem model)
    {
      return MapFeatureRepositoryToViewModel(model,_featureSystemViewModelBuilder,_featureViewModelBuilder);
    }

    public FeatureSystem MapRepositoryToModel(FeatureSystemViewModel viewModel)
    {
      return MapFeatureSystemViewModelToFeatureSystem(viewModel, _featureSystemModelBuilder, _featureModelBuilder);
    }

    public static FeatureSystemViewModel MapFeatureRepositoryToViewModel(FeatureSystem model, Func<FeatureSystemViewModel> constructor , Func<FeatureViewModel> featureConstructor )
    {
      var fsvm = constructor();
      fsvm.Features.Clear();

      foreach (var feature in model.Features)
      {
        fsvm.Features.Add(MapFeatureToFeatureViewModel(feature,featureConstructor));
      }

      return fsvm;
    }

    public static FeatureViewModel MapFeatureToFeatureViewModel(Feature feature, Func<FeatureViewModel> constructor)
    {
      var vm = constructor();
      vm.Name = feature.Name;
      vm.Id = feature.Id;
      foreach (var comment in feature.Comments)
      {
        vm.Comments.Add(comment);
      }


      vm.Description = feature.Description;
      vm.IsReference = feature.IsReference;
      vm.ReferenceTargetId = feature.TargetId;

      foreach (var child in feature.Children)
      {
        vm.Children.Add(MapFeatureToFeatureViewModel(child,constructor));
      }

      return vm;
    }

    public static FeatureSystem MapFeatureSystemViewModelToFeatureSystem(FeatureSystemViewModel viewModel,
                                                                         Func<FeatureSystem> constructor,
                                                                         Func<Feature> featureConstructor)
    {
      var model = constructor();
      model.Features.Clear();
      foreach (var feature in viewModel.Features)
      {
        model.Features.Add(MapFeatureViewModelToModel(feature,featureConstructor));
      }

      return model;
    }

    public static Feature MapFeatureViewModelToModel(FeatureViewModel feature, Func<Feature> constructor)
    {
      var model = constructor();
      model.Name = feature.Name;
      model.Id = feature.Id;
      foreach (var comment in feature.Comments)
      {
        model.Comments.Add(comment);
      }

      model.Description = feature.Description;
      model.IsReference = feature.IsReference;
      model.TargetId = feature.ReferenceTargetId;

      foreach (var child in feature.Children)
      {
        model.Children.Add(MapFeatureViewModelToModel(child,constructor));
      }

      return model;
    }
  }
}
