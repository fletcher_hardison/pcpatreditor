﻿//  ***************************************************************
//  File name: RuleService.cs
//  Project: PCPATR.Editor.MasterLists
//  Ceated by:
//  Created on: 24,02,2013
// ***************************************************************

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PCPATR.Models;

namespace PCPATR.Editor.MasterLists.Services
{
  public class ModelService<T> where T : IHasIdAndIdentifier
  {
    private readonly NewIdService _idService;
    private readonly Dictionary<string, T> _modelDictionary;

    public ModelService(NewIdService idService)
    {
      _idService = idService;
      _modelDictionary = new Dictionary<string, T>();
      ManagedIdIdentifierCollection = new List<string>();
    }


    public Dictionary<string, T> ModelDictionary
    {
      get { return _modelDictionary; }
    }

    public IList<string> ManagedIdIdentifierCollection { get; set; }

    public T GetModelFromIdentiferIdString(string str)
    {
      var id = ExtractIdFromIdentifierIdString(str);

      return GetModelFromId(id);
    }


    public T GetModelFromId(string id)
    {
      if (_modelDictionary.ContainsKey(id))
      {
        return _modelDictionary[id];
      }
      return default(T);
    }

    public string ExtractIdFromIdentifierIdString(string str)
    {
      if (string.IsNullOrEmpty(str)) return null;
      var index = str.IndexOf("(id:");
      if (index < 0) return str;
      var id = str.Substring(index + 4);
      id = id.Remove(id.Length - 1);

      if (string.IsNullOrEmpty(id))
      {
        id = str.Substring(0, index).Trim();
      }
      return id;
    }

    public string FormatIdentiferIdString(IHasIdAndIdentifier item)
    {
      if (string.IsNullOrEmpty(item.Id))
      {
        return item.Identifier;
      }

      return string.Format("{0} (id:{1})", item.Identifier, item.Id);
    }

    public IEnumerable<string> GetRulesIdList()
    {
      var result = new List<string>();
      foreach (var rule in _modelDictionary.Values)
      {
        result.Add(FormatIdentiferIdString(rule));
      }

      result.Sort((x, y) => string.Compare(x, y));

      return result;
    }

    public ModelService<T> LoadModels(IEnumerable<T> models)
    {
      _modelDictionary.Clear();
      ManagedIdIdentifierCollection.Clear();
      foreach (var model in models)
      {
        var idOrIdentfier = string.IsNullOrEmpty(model.Id) ? model.Identifier : model.Id;
        _modelDictionary.Add(idOrIdentfier, model);
        _idService.AddId(idOrIdentfier, model.Identifier);
        ManagedIdIdentifierCollection.Add(FormatIdentiferIdString(model));
      }

      return this;
    }

    public ModelService<T> RemoveRule(string id)
    {
      if (_modelDictionary.ContainsKey(id))
      {

        ManagedIdIdentifierCollection.Remove(FormatIdentiferIdString(_modelDictionary[id]));
        _modelDictionary.Remove(id);
        _idService.RemoveId(id);
      }

      return this;
    }

    public bool PersistModel(T model)
    {
      if (model != null)
      {
        var idOrIdentifier = string.IsNullOrEmpty(model.Id) ? model.Identifier : model.Id;
        if (_modelDictionary.ContainsKey(idOrIdentifier))
        {
          _modelDictionary.Remove(idOrIdentifier);
          UpdateElementInManagedIdIdentifierCollection(model);
          _modelDictionary.Add(idOrIdentifier, model);
        }
        else
        {
          _modelDictionary.Add(idOrIdentifier, model);
          _idService.AddId(idOrIdentifier, model.Identifier);
          ManagedIdIdentifierCollection.Add(FormatIdentiferIdString(model));
        }

        return true;
      }

      return false;
    }

    private ModelService<T> UpdateElementInManagedIdIdentifierCollection(T model)
    {
      var targetid = "(id:" + model.Id + ")";
      var index = -1;
      foreach (var str in ManagedIdIdentifierCollection)
      {
        if (str.Contains(targetid) | str.Contains(model.Id))
        {
          index = ManagedIdIdentifierCollection.IndexOf(str);
          ManagedIdIdentifierCollection.Remove(str);


          break;
        }
      }

      if (index < 0)
      {
        ManagedIdIdentifierCollection.Add(FormatIdentiferIdString(model));
      }
      else
      {
        ManagedIdIdentifierCollection.Insert(index, FormatIdentiferIdString(model));
      }
      
      return this;
    }

    

    public ModelService<T> SetManagedIdIdentifierCollection(IList<string> collectionToManage)
    {
      ManagedIdIdentifierCollection = collectionToManage;

      return this;
    }
  } 
}