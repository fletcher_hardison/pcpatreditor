using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using AutoMapper;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.Models.Parameters;

namespace PCPATR.Editor.MasterLists.Services
{
  public class ParametersMapper
  {
    public ParametersMapper(IUnityContainer container)
    {
      InitializeParametersMapper(container);
    }

    public static void InitializeParametersMapper(IUnityContainer container)
    {
      Mapper.AddProfile(new ParametersModelToViewModelProfile(container));
      Mapper.AddProfile(new ParametersViewModelToModelProfile(container));
      Mapper.AssertConfigurationIsValid();
    }

    public ParametersViewModel MapToViewModels(ParametersModel model)
    {
      return Mapper.Map<ParametersViewModel>(model);
    }

    public ParametersModel MapToModel(ParametersViewModel viewModel)
    {
      return Mapper.Map<ParametersModel>(viewModel);
    }
  }


  public class ParametersModelToViewModelProfile : Profile
  {
    public ParametersModelToViewModelProfile(IUnityContainer container)
    {
      _toggledValueCreator = r => container.Resolve<ToggledValueViewModel>();
      _parametersCreator = r => container.Resolve<ParametersViewModel>();
    }


    protected override void Configure()
    {
      CreateMap<ParametersModel, ParametersViewModel>().ConstructUsing(_parametersCreator)
                                                       .ForMember(vm => vm.SetStartSymbol, opt => opt.Ignore())
                                                       .ForMember(vm => vm.AddAttributeOrderItem, opt => opt.Ignore())
                                                       .ForMember(vm => vm.RemoveAttributeOrderItem, opt => opt.Ignore())
                                                       .ForMember(vm => vm.AddFeatureNameRef, opt => opt.Ignore())
                                                       .ForMember(vm => vm.MoveAttributeUp, opt => opt.Ignore())
                                                       .ForMember(vm => vm.MoveAttributeDown, opt => opt.Ignore())
                                                       .ForMember(vm => vm.SelectedUsedAttribute, opt => opt.Ignore())
                                                       .ForMember(vm => vm.AvailableAttributes, opt => opt.Ignore())
                                                       .ForMember(vm => vm.SelectedAvailableAttribute,
                                                                  opt => opt.Ignore())
                                                       .ForMember(vm => vm.DisplayName, opt => opt.Ignore());

      CreateMap<AttributeOrderItem, AttributeOrderViewModel>()
        .ForMember(vm => vm.DisplayName, opt => opt.Ignore());

      CreateMap<ToggledValue, ToggledValueViewModel>().ConstructUsing(_toggledValueCreator);

      CreateMap<IEnumerable<string>, CommentsField>().ConvertUsing<CommentsFieldConverter>();
    }


    private readonly Func<ResolutionContext, ToggledValueViewModel> _toggledValueCreator;
    private readonly Func<ResolutionContext, ParametersViewModel> _parametersCreator;
  }

  public class ParametersViewModelToModelProfile : Profile
  {
    public ParametersViewModelToModelProfile(IUnityContainer container)
    {
      _toggledValueCreator = r => container.Resolve<ToggledValue>();
      _parametersCreator = r => container.Resolve<ParametersModel>();
    }


    protected override void Configure()
    {
      CreateMap<ParametersViewModel, ParametersModel>().ConstructUsing(_parametersCreator);

      CreateMap<AttributeOrderViewModel, AttributeOrderItem>();

      CreateMap<ToggledValueViewModel, ToggledValue>().ConstructUsing(_toggledValueCreator);

      CreateMap<CommentsField, List<string>>().ConvertUsing<CommentsFieldBackConverter>();
    }


    private readonly Func<ResolutionContext, ToggledValue> _toggledValueCreator;
    private readonly Func<ResolutionContext, ParametersModel> _parametersCreator;
  }

  public class CommentsFieldConverter : ITypeConverter<IEnumerable<string>, CommentsField>
  {
    public CommentsField Convert(ResolutionContext context)
    {
      var comments = context.SourceValue as IEnumerable<string>;
      if (comments == null) return null;
      var field = new CommentsField();
      field.Comments = new ObservableCollection<string>(comments);

      return field;
    }
  }

  public class CommentsFieldBackConverter : ITypeConverter<CommentsField, List<string>>
  {
    public List<string> Convert(ResolutionContext context)
    {
      var comments = context.SourceValue as CommentsField;
      if (comments == null) return new List<string>();

      return new List<string>(comments.Comments);
    }
  }

  //public class ToggleFieldConverter : ITypeConverter<ToggledValue, ToggledValueViewModel>
  //{
  //  public ToggledValueViewModel Convert(ResolutionContext context)
  //  {
  //    var value = context.SourceValue as ToggledValue;
  //    if (value == null) return new ToggledValueViewModel { HasValue = false };
  //    if (!value.HasValue) return new ToggledValueViewModel { HasValue = false };
  //    var newValue = new ToggledValueViewModel { HasValue = true };

  //  }
  //}

}
