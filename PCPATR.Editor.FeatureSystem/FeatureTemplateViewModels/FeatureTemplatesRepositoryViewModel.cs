﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.BlockEditor;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.Models.FeatureTemplates;

namespace PCPATR.Editor.MasterLists.FeatureTemplateViewModels
{
  public class FeatureTemplatesRepositoryViewModel : ViewModelBase
  {
    public FeatureTemplatesRepositoryViewModel(ModelService<FeatureTemplateModel> modelSerice, BlockMapper blockMapper, IUnityContainer container, NewIdServiceViewModel idService)
    {
      _container = container;
      IdServiceViewModel = idService;
      _modelService = modelSerice;
      _blockMapper = blockMapper;
      _idIdentifiersList = new ObservableCollection<string>();
      _modelService.SetManagedIdIdentifierCollection(_idIdentifiersList);
      RemoveSelectedTemplate = new DelegateCommand<FeatureTemplateModel>(RemoveFeatureTemplate);
      AddTemplate = new DelegateCommand(AddNewFeatureTemplate);
      CancelAdd = new DelegateCommand(CancelAddMethod);
      SaveEdits = new DelegateCommand(TryPersistSelectedViewModelModel);
      BlockEditor = new BlockEditorViewModel();
    }

    public DelegateCommand SaveEdits { get; private set; }

    private ObservableCollection<string> _idIdentifiersList;
    public ObservableCollection<string> IdIdentifiersList
    {
      get { return _idIdentifiersList; }
      set
      {
        _idIdentifiersList = value;
        _modelService.SetManagedIdIdentifierCollection(_idIdentifiersList);
        RaisePropertyChanged("IdIdentifiersList");
      }
    }

    private string _selectedIdentifier;
    public string SelectedIdentifier
    {
      get { return _selectedIdentifier; }
      set
      {
        _selectedIdentifier = value;
        RaisePropertyChanged("SelectedIdentifier");
      }
    }

    private BlockEditorViewModel _blockEditor;
    public BlockEditorViewModel BlockEditor
    {
      get { return _blockEditor; }
      set
      {
        _blockEditor = value;
        RaisePropertyChanged("BlockEditor");
      }
    }

    public ModelService<FeatureTemplateModel> ModelService
    {
      get { return _modelService; }
    }

    public DelegateCommand AddTemplate { get; private set; }

    public DelegateCommand<FeatureTemplateModel> RemoveSelectedTemplate { get; private set; }

    private string _proposedIdentifier;
    public string ProporsedIdentifier
    {
      get { return _proposedIdentifier; }
      set
      {
        _proposedIdentifier = value;
        RaisePropertyChanged("ProposedIdentifier");
      }
    }

    public NewIdServiceViewModel IdServiceViewModel { get; private set; }
    
    public DelegateCommand CancelAdd { get; private set; }

    private void CancelAddMethod()
    {
      IdServiceViewModel.ProposedId = string.Empty;
      ProporsedIdentifier = string.Empty;
    }

    private void AddNewFeatureTemplate()
    {
      var result = _container.Resolve<FeatureTemplateModel>();
      result.Id = IdServiceViewModel.ProposedId;
      result.Identifier = _proposedIdentifier;

      _modelService.PersistModel(result);
      IdServiceViewModel.ProposedId = string.Empty;
      ProporsedIdentifier = string.Empty;
      //RaisePropertyChanged("IdIdentifiersList");
    }

    private void RemoveFeatureTemplate(FeatureTemplateModel doomed) //tis a far better thing...
    {
      if (string.IsNullOrEmpty(_selectedIdentifier)) return;
      _modelService.RemoveRule(_selectedIdentifier);

      // RaisePropertyChanged("IdIdentifiersList");      
    }

    public void LoadModels(IEnumerable<FeatureTemplateModel> models)
    {
      _modelService.LoadModels(models);
    }

    public void ShowSelectedIdentifier()
    {
      if (string.IsNullOrEmpty(_selectedIdentifier)) return;
      var initializationContext = new BlockInitializationContext();
      initializationContext.AddContextElement<FeatureTemplatesRepositoryViewModel>(this);
      initializationContext.AddContextElement<IUnityContainer>(_container);
      BlockEditor.Context = initializationContext;

      BlockEditor.Content = _blockMapper.MapToBlock(
        _modelService.GetModelFromIdentiferIdString(_selectedIdentifier), null, initializationContext) as ContainerBlock;
    }

    public void TryPersistSelectedViewModelModel()
    {
      if (BlockEditor == null) return;
      var model = _blockMapper.MapBlockToModelObject(_blockEditor.Content) as FeatureTemplateModel;
      _modelService.PersistModel(model);
    }

    public IEnumerable<FeatureTemplateModel> GetTemplateModels()
    {
      return _modelService.ModelDictionary.Values;
    }

    private readonly ModelService<FeatureTemplateModel> _modelService;
    private readonly BlockMapper _blockMapper;
    private readonly IUnityContainer _container;    
  }
}
