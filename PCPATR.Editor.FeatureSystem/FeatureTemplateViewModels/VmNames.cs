﻿namespace PCPATR.Editor.MasterLists.FeatureTemplateViewModels
{
  public static class VmNames
  {
    public const string TO_NONE_VALUE_PATH = "None Value Path";
    public const string TO_FEATURE_VALUE_REF = "Feature Value Ref";
    public const string REMOVE_ELEMENT = "Remove Element";
    public const string TO_SUBCAT_DISJUNCTION = "Subcat Disjunction";
    public const string TO_NONE_REF = "None Ref";
    public const string TO_TERMINAL_REF = "Terminal Reference";
    public const string TO_TEMPLATE_NAME_ID = "Name and Ids";
    public const string TO_COMPLEX_FEATURE_REF = "Complex Feature Reference";
    public const string TO_FEATURE_REF = "Feature Reference";
    public const string TO_TEMPLATE_FEATURE_DISJUNCTION = "Template Feature Disjunction";
    public const string TO_SUBCAT_REF = "Subcat Reference";
    public const string TO_CAT_VALUE = "Category Value";
    public const string TO_CAT_VALUE_PATH = TO_CAT_VALUE + PATH;
    public const string TO_LEX_VALUE = "Lexical Value";
    public const string TO_LEX_VALUE_PATH = TO_LEX_VALUE + PATH;
    public const string TO_GLOSS_VALUE = "Gloss Value";
    public const string TO_GLOSS_VALUE_PATH = TO_GLOSS_VALUE + PATH;
    public const string TO_ROOT_GLOSS_VALUE = "Root Gloss Value";
    public const string TO_ROOT_GLOSS_VALUE_PATH = TO_ROOT_GLOSS_VALUE + PATH;
    public const string TO_COLLECTION_FEATURE_REF = "Collection Feature Reference";
    public const string TO_FEATURE_TEMPLATE_REF = "Feature Template Reference";
    public const string TO_TEMPLATE_FEATURE_STRUCTURE = "Template Feature Structure";
    public const string TO_FIRST_REF = "Subcat First Reference";
    public const string TO_REST_REF = "Subcat Rest Reference";
    public const string TO_SUBCAT_FIRST_REST_REF_PAIR = "Subcat First, Rest Reference";
    public const string TO_NONE_VALUE = "None Value";
    public const string TO_TEMPLATE_FEATURE_PATH = "Template Feature Path";
    public const string TO_EMPTY = "Empty";

    private const string PATH = " Path";
  }
}