﻿//  ***************************************************************
//  File name: OutsideWorld.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 01,01,2013
// ***************************************************************

using System;
using System.IO;
using System.Xml;
using PCPATR.Common;

namespace PCPATR.Editor.MasterLists
{
  public class OutsideWorld
  {
    private static readonly string _appData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),"PC-PATR-EDITOR");
    public  static string DebugConfigPath = @"..\..\pcpatr_editor_configurations.txt";
    public static string ReleaseConfigPath = Path.Combine(_appData, "pcpatr_editor_configurations.txt");
    public static string DebugPathToInitFeature = @"Assets\InitFeature.htm";
    public static string ReleasePathToInitFeature = Path.Combine(_appData, "InitFeature.htm");
    public const string CURRENT_FILE_PATH = "current_file_path";
    public const string LAST_LEXICON = "last_lexicon";
    public const string LAST_DATA_FILE = "last_data_file";
    public const string DEBUG_PATH_TO_TRANSFORM = @"..\..\Assets\PcPatrGrammar.xsl";
    public static string ReleasePathToTransform = Path.Combine(_appData, @"PcPatrGrammar.xsl");
    public const string DEBUG_DTD_LOCATION = @"..\..\Assets\PcPatrGrammar.dtd";
    public static string ReleaseDtdLocation = Path.Combine(_appData,@"PcPatrGrammar.dtd");
    public static string ReleaseSchematronLocation = Path.Combine(_appData, @"PcPatrGrammar.sch");
    public static string NodeComparisonOutput = Path.Combine(_appData, @"NodeComparison.html");
    public static string FeatureComparisonOutput = Path.Combine(_appData, @"ParseComparison.html");

    public static string GetPathToShowFsTransForm()
    {
      return Path.Combine(_appData, @"ShowFS.xsl");
    }

    public static string GetPathToInitFeature()
    {
#if DEBUG
      return DebugPathToInitFeature;
#else
      return ReleasePathToInitFeature;
#endif
    }
    
    public static string GetPathToTransform()
    {
#if DEBUG
      return DEBUG_PATH_TO_TRANSFORM;
#else
      return ReleasePathToTransform;
#endif
    }

    public static string GetPathToConfigs()
    {
#if DEBUG
      return DebugConfigPath;
#else
      return ReleaseConfigPath;
#endif
    }

    public static string GetDtdLocation()
    {
     #if DEBUG
      return DEBUG_DTD_LOCATION;
#else
      return ReleaseDtdLocation;
#endif 
    }

    public OutsideWorld(ConfigReader configReader)
    {
      _configReader = configReader;

#if DEBUG
      _configReader.LoadConfiguration(DebugConfigPath);
#else      
      _configReader.LoadConfiguration(ReleaseConfigPath);
#endif

    }


    public IO<XmlDocument> LoadGrammarCreator()
    {
      return () =>
      {
        var doc = new XmlDocument();
        try
        {
          doc.Load(_configReader[CURRENT_FILE_PATH]);
        }
        catch (System.IO.FileNotFoundException)
        {
          return null;
        }

        return doc;
      };
    }


    private readonly ConfigReader _configReader;
  }
}