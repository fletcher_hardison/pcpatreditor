﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCPATR.Editor.MasterLists.Constraints.ViewModels
{
  public static class ConstraintViewModelNames
  {
    public const string TO_FEATURE_PATH = "Feature path";
    public const string TO_SUBCAT_PATH = "Subcat path";
    public const string TO_CATEGORY_REF = "Syntactic category ref";
    public const string TO_FEATURE_VALUE_REF = "Feature value ref";
    public const string TO_NONE_VALUE = "None value";
    public const string TO_BINARY_OPERATION = "Binary operation";
    public const string TO_UNARY_OPERATION = "Unary operation";
    public const string TO_LOGICAL_EXPRESSION = "Logical expression";
    public const string TO_LOGICAL_CONSTRAINT_REF = "Logical constraint ref";
    public const string TO_COLLECTION_FEATURE_REF = "Collection feature ref";
    public const string TO_LEX_VALUE_PATH = "Lexical Value";
    public const string TO_GLOSS_VALUE_PATH = "Gloss Value";
    public const string TO_ROOT_GLOSS_VALUE_PATH = "Root Gloss Value";
    public const string TO_CAT_VALUE = "Category Value";    
    public const string TO_LEX_VALUE = "Lexical Value";
    public const string TO_GLOSS_VALUE = "Gloss Value";
    public const string TO_ROOT_GLOSS_VALUE = "Root Gloss Value";
    public const string REMOVE_ELEMENT = "Remove Element";

    public const string TO_TEMPLATE_FEATURE_STRUCTURE = "Feature Structure";
    
	public const string TO_FEATURE_VALUE_DISJUNCTION = "Feature Value Disjunction";

    public const string UNIFICATION_CONSTRAINT = "Unification constraint";
    public const string PERCOLATION_OPERATION = "Percolation operation";
    public const string PRIORITY_UNION_CONSTRAINT = "Priority union operation";
    public const string LOGICAL_CONSTRAINT = "Logical constraint";
    public const string CONSTRAINT_DISJUNCTION = "Constraint disjunction";

    public const string EXISTANCE = "existence";
    public const string NEGATION = "negation";
    public const string AND = "and";
    public const string OR = "or";
    public const string CONDITIONAL = "conditional";
    public const string BICONDITIONAL = "biconditional";
    public const string VOID = "";

    public const string EXISTANCE_MAPPED = "IS";
    public const string NEGATION_MAPPED = "IS NOT";
    public const string AND_MAPPED = "AND";
    public const string OR_MAPPED = "OR";
    public const string CONDITIONAL_MAPPED = "IF";
    public const string BICONDITIONAL_MAPPED = "IF AND ONLY IF";
    public const string VOID_MAPPED = "type not found";

    public const string NONE_INDEXED_VARIABLE = "none";
  }
}
