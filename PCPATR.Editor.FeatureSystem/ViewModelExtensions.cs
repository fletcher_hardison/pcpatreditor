﻿//  ***************************************************************
//  File name: ViewModelExtensions.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 03,08,2012
// ***************************************************************

using System;
using System.Linq;


namespace PCPATR.Editor.MasterLists
{
    public static class ViewModelExtensions
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="needle"></param>
        /// <param name="haystack"></param>
        /// <returns></returns>
        /// <remarks> from http://stackoverflow.com/questions/541954/how-would-you-count-occurences-of-a-string-within-a-string-c post by ZombieSheep </remarks>
        public static int CountOccurances(this string haystack, string needle)
        {
            if (string.IsNullOrEmpty(haystack)) return 0;
            var count = haystack.Split(' ').Count(f => f.Contains(needle));
            return count;
        }

      public static void IfIsNotNull<T>(this T x, Action<T> action )
      {
        if (x == null) return;
        action(x);
      }

      public static U IfIsNotNull<T, U>(this T x, Func<T, U> func)
      {
        if (x == null) return default(U);
        return func(x);
      }
    }
}