﻿using System;
using PCPATR.Editor.MasterLists.PatrParser.ParserComponents;
using SIL.PcPatrBrowser;

namespace PCPATR.Editor.MasterLists.PatrParser
{
  public class LingTreeTreeControlNotifierService
  {
    private static LingTreeTreeControlNotifierService _instnace;

    public static LingTreeTreeControlNotifierService GetInstance()
    {
      if (_instnace == null)
      {
        _instnace = new LingTreeTreeControlNotifierService();
      }
      return _instnace;
    }

    private LingTreeTreeControlNotifierService()
    {

    }

    public void RegisterEndPoint(LingTreeTreeWrapperControl endpoint)
    {
      _endPoint = endpoint;
    }

    public void RegisterStartPoint(Action<string> callBack)
    {
      _callBack = callBack;
    }

    public void ShowParse(PcPatrSentence sentence, PcPatrParse parse)
    {
      if (_endPoint == null) return;
      _endPoint.Refresh();
      _endPoint.ShowParseTree(sentence, parse);
    }

    public void SendIdToStartPoint(string id)
    {
      if (_callBack == null) return;
      _callBack(id);
    }


    private LingTreeTreeWrapperControl _endPoint;

    private Action<string> _callBack;
  }
}