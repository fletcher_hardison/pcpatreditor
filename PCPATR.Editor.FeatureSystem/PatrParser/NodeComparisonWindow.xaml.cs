﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PCPATR.Editor.MasterLists.PatrParser
{
  /// <summary>
  /// Interaction logic for NodeComparisonWindow.xaml
  /// </summary>
  public partial class NodeComparisonWindow : Window
  {
    public NodeComparisonWindow(ParseNodeComparerViewModel viewModel)
    {
      InitializeComponent();
      _viewModel = viewModel;
      DataContext = _viewModel;
      Closing += NodeComparisonWindow_Closing;
    }



    void NodeComparisonWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      _viewModel.Reset();
      _viewModel.CancelIsComparingNodes();
    }


    private ParseNodeComparerViewModel _viewModel;

  }
}
