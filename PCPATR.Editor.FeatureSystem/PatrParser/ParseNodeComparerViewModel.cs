﻿//  ***************************************************************
//  File name: ParseNodeComparerViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 16,04,2013
// ***************************************************************

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Microsoft.Practices.Prism.Commands;

namespace PCPATR.Editor.MasterLists.PatrParser
{
  public class ParseNodeComparerViewModel : ViewModels.ViewModelBase
  {
    private readonly ObservableCollection<NodeToCompare> _nodes;
    private readonly XslCompiledTransform _transform;
    private readonly ParserControlViewModel _parserControl;
    public ParseNodeComparerViewModel(XslCompiledTransform transform, ParserControlViewModel parserControl)
    {
      _transform = transform;
      _nodes = new ObservableCollection<NodeToCompare>();
      _parserControl = parserControl;

      AddSelectedToList = new DelegateCommand(() => AddToListMethod());
      ResetList = new DelegateCommand(() => Reset());
      RemoveSelected = new DelegateCommand(() => RemoveNode());
      ShowComparison = new DelegateCommand(() => WriteNodesToHtml());
    }

    public DelegateCommand AddSelectedToList { get; private set; }
    public DelegateCommand RemoveSelected { get; private set; }
    public DelegateCommand ResetList { get; private set; }
    public DelegateCommand ShowComparison { get; private set; } 

    private ParseNodeComparerViewModel  AddToListMethod()
    {
      if (_parserControl.CurrentNodeToCompareData != null)
      {
        var parseNumber = _parserControl.CurrentNodeToCompareData.Item1;
        var xmlNode = _parserControl.CurrentNodeToCompareData.Item2;

        if (xmlNode != null)
        {
          AddNode(xmlNode, parseNumber);
        }
      }

      return this;
    }

    public ParseNodeComparerViewModel AddNode(XmlNode node, int parseNumber)
    {
      using (var swriter = new StringWriter())
      using (var writer = new XmlTextWriter(swriter))
      {
        var cat = "unknown";
        var catNode = node.SelectSingleNode(".//F[@name ='cat']/Str");
        if (catNode != null)
        {
          cat = catNode.InnerText;
        }

        XmlDocument doc1 = new XmlDocument();
        doc1.LoadXml(node.OuterXml);
        XPathNavigator nav = doc1.CreateNavigator();

        _transform.Transform(nav, null, writer, null);

        var nodeDoc = new XmlDocument();
        nodeDoc.LoadXml(swriter.ToString());
        var table = nodeDoc.SelectSingleNode("//body/table");
        if (table != null)
        {

          _nodes.Add(new NodeToCompare(cat, parseNumber, table));
        }
      }

      return this;
    }

    public ObservableCollection<NodeToCompare> Nodes { get { return _nodes; } }

    public ParseNodeComparerViewModel RemoveNode()
    {
      if (_selectedNode != null && _nodes.Contains(_selectedNode))
      {
        _nodes.Remove(_selectedNode);
        SelectedNode = null;
      }

      return this;
    }


    private NodeToCompare _selectedNode;
    public NodeToCompare SelectedNode 
    {
      get
      {
        return _selectedNode;
      } 
      set 
      { 
        _selectedNode = value; 
        RaisePropertyChanged("SelectedNode"); 
      }
    }

    public ParseNodeComparerViewModel Reset()
    {
      _nodes.Clear();
      return this;
    }

    public ParseNodeComparerViewModel WriteNodesToHtml()
    {
      var sb = new StringBuilder();
      sb.AppendLine("<html><meta></meta><body><table><tbody><tr>");

      foreach (var node in _nodes)
      {
        sb.Append(
          string.Format("<td><table><tbody><tr><td align=\"center\"><p>Parse {0}</p></td></tr><tr><td>{1}</td></tr></tbody></table><td>\n",
                        node.ParseNumber, node.Node.OuterXml));
      }

      sb.AppendLine("</tr></tbody></table></body></html>");
      
      File.WriteAllText(OutsideWorld.NodeComparisonOutput,sb.ToString());
      ParseAnalyser.ShowResultsInAnyWebBrowser(OutsideWorld.NodeComparisonOutput);

      return this;
    }


    public ParseNodeComparerViewModel CancelIsComparingNodes()
    {
      _parserControl.IsComparingNodes = false;
      return this;
    }
  }

  public class NodeToCompare
  {

    public NodeToCompare(string category, int parseNumber, XmlNode node)
    {
      Category = category;
      ParseNumber = parseNumber;
      Node = node;
    }

    public string Category { get; private set; }
    public int ParseNumber { get; private set; }
    public XmlNode Node { get; private set; }
  }
}