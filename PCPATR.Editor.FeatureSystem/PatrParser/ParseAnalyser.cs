/* ***************************************************************
// * File name: ParseAnalyser.cs
// * Project: PCPATR.Services
// * Created by: Fletcher Hardison
// * Created on: 21,04,2012
// * In the name of Christ with thanks to God
// */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Xsl;
using PCPATR.Common;
using PCPATR.Editor.MasterLists.PatrParser.DataModel;
using SIL.PcPatrBrowser;

namespace PCPATR.Editor.MasterLists.PatrParser
{
  public class ParseAnalyser
  {
    public ParseAnalyser(string pathToShowFs)
    {
      InputSentences = new List<string>();
      _formatter = BuildFormatter(pathToShowFs);
    }

    /// <summary>
    /// holds sentences sent to parser
    /// </summary>
    public List<string> InputSentences { get; set; }

    /// <summary>
    /// Loads the data sent to the parser
    /// </summary>
    /// <param name="pathToInputData">path to parser input sentences</param>
    public void LoadDataFile(string pathToInputData)
    {
      InputSentences.Clear();

      if (pathToInputData == null) throw new ArgumentNullException("pathToInputData");
      if (File.Exists(pathToInputData) == false)
        throw new
          ApplicationException(string.Format("File does not exist: {0}", pathToInputData));

      using (var reader = new StreamReader(pathToInputData))
      {
        string line;

        while ((line = reader.ReadLine()) != null)
        {
          if (line == string.Empty) continue;

          if (line.Contains("|"))
          {
            line = line.Remove(line.IndexOf("|"));
            if (line == String.Empty) continue;
          }

          InputSentences.Add(line);
        }
      }
    }

    /// <summary>
    /// Analyses the parser output file.
    /// </summary>
    /// <param name="pathToOutput">Path to parser output file.</param>
    /// <param name="faileresOn">indicates if parser was set to run with failures on</param>
    /// <returns>PatrAnalysisData object holing analysis results</returns>
    public PatrAnalysisData AnalysePatrOutput(string pathToOutput, bool faileresOn, char commentChar)
    {
      if (pathToOutput == null) throw new ArgumentNullException("pathToOutput");
      var analysisData = new PatrAnalysisData();
      if (!File.Exists(pathToOutput)) return analysisData;

      var contents = LoadFileIntoString(pathToOutput);
      //var placeInFile = MovePastFileHeader(contents);
      //Tuple<string, string> analysisTuple;
      //var currentSentenceIndex = 0;
      //while ((analysisTuple = GetNextSentenceAndComments(contents, ref placeInFile)) != null)
      //{
      //  var sentenceAnalysis = LoadSentenceAndCommentsIntoClass(analysisTuple.Item1, analysisData, faileresOn, ref currentSentenceIndex, InputSentences);
      //  analysisData.PatrSentences.Add(AnalyzePatrSentence(analysisTuple.Item2, sentenceAnalysis));
      //}

      //var otherMessages = CheckForOtherMessages(contents, (analysisData.PatrSentences.Count < 1));
      //if (otherMessages != null)
      //{
      //  analysisData.PatrSentences.Add(otherMessages);
      //}

      AnalyseByLooping(contents, analysisData, commentChar);

      return analysisData;
    }

    private void AnalyseByLooping(string contents, PatrAnalysisData analysis, char commentChar)
    {
      //var sentenceMarker = commentChar + "@@sentence";
      var reader = new StringReader(contents);
      var line = reader.ReadLine();
      if (line == null) return;
      if (line.Contains("Grammar file used"))
      {
        line = reader.ReadLine();
      }
      var analysisBuilder = new StringBuilder();
      PatrSentenceAnalysis sentence = null;
      while (line != null)
      {
        if (!(line.Contains(commentChar) || line.Contains("***") || line.Contains(ANALYSIS_OPEN_MARKER) || line.Contains(ANALYSIS_CLOSE_MARKER) || string.IsNullOrEmpty(line)))
        {
          if (sentence != null & !analysis.PatrSentences.Contains(sentence))
          {
            analysis.PatrSentences.Add(sentence);
          }

          sentence = new PatrSentenceAnalysis { Failed = true };
          sentence.Sentence = line.Trim(); //line.Replace(sentenceMarker, string.Empty).Trim();
        }
        else if (line.Contains("***"))
        {
          sentence.Comments.Add(line);
        }
        else if (line.Contains(ANALYSIS_OPEN_MARKER))
        {

          while (!line.Contains(ANALYSIS_CLOSE_MARKER))
          {
            analysisBuilder.AppendLine(line);
            line = reader.ReadLine();
          }
          analysisBuilder.AppendLine(line);
          analysis.PatrSentences.Add(AnalyzePatrSentence(analysisBuilder.ToString(), sentence));
          analysisBuilder.Clear(); //get ready for next sentence
        }

        line = reader.ReadLine();
      }

      if (!analysis.PatrSentences.Contains(sentence)) //make sure its not missing the last sentence
      {
        analysis.PatrSentences.Add(sentence);
      }
    }

    public static PatrSentenceAnalysis CheckForOtherMessages(string analysisData, bool noOtherDataFound)
    {
      var errorsHolder = new PatrSentenceAnalysis();
      errorsHolder.Failed = true;
      errorsHolder.Sentence = "Parser Messages";
      var lexicalErrors = CheckForLexiconErrors(analysisData);

      if (lexicalErrors != null)
      {
        var parse = new PatrParseAnalysis();
        var node = new PatrParseNode { Cat = "Parser Messages" };
        node.FailedFeatures = lexicalErrors.Select(f => WrapErrorinParseFeature("Lexical", f)).ToList();
        parse.Nodes.Add(node);
        errorsHolder.Parses.Add(parse);
      }

      if (noOtherDataFound)
      {
        if (errorsHolder.Parses.Count < 1)
        {
          errorsHolder.Parses.Add(CreateNoParseDataFoundNode());
        }
      }
      return errorsHolder;
    }

    private static PatrParseAnalysis CreateNoParseDataFoundNode()
    {
      var parse = new PatrParseAnalysis();
      var node = new PatrParseNode { Cat = "No data found!" };
      //Thank you to Amy Hardison for the humor of this next statement.
      var feature = new PatrParseFeature("EPIC ", "", "FAIL - no parse data could be extracted.",
                                         new List<PatrParseFeature>());
      node.FailedFeatures.Add(feature);
      parse.Nodes.Add(node);

      return parse;
    }

    private static PatrParseFeature WrapErrorinParseFeature(string errorType, string errorContent)
    {
      return new PatrParseFeature { Content = "FAIL:" + errorContent, Name = errorType };
    }

    public static IEnumerable<string> CheckForLexiconErrors(string analysisData)
    {
      var startOfAnalysisData = analysisData.IndexOf("in the lexicon");
      if (startOfAnalysisData < 0) return null;

      var lexicalMessage = analysisData.Substring(startOfAnalysisData);

      var reader = new StringReader(lexicalMessage);
      var result = new List<string>();
      string line;
      while (!string.IsNullOrEmpty((line = reader.ReadLine())))
      {
        if (!result.Contains(line))
        {
          result.Add(line);
        }
      }

      return result;
    }

    public static int MovePastFileHeader(string sDoc)
    {
      if (sDoc.IndexOf(ANALYSIS_OPEN_MARKER) == 0) return 0;

      return sDoc.IndexOf("\r\n") + 2; //header should be on first line
    }

    private const string ANALYSIS_CLOSE_MARKER = "</Analysis>";
    private const string ANALYSIS_OPEN_MARKER = "<Analysis";

    private static Tuple<string, string> GetNextSentenceAndComments(string sDoc, ref int startLoc)
    {

      if (sDoc.Length < startLoc | startLoc < 0)
      {
        startLoc = -1; //nothing there
        return null;
      }

      var workingDoc = sDoc.Substring(startLoc);
      var iAnalysisStart = workingDoc.IndexOf(ANALYSIS_OPEN_MARKER);
      if (iAnalysisStart == -1) return null;
      var iAnalysisEnd = workingDoc.IndexOf(ANALYSIS_CLOSE_MARKER) + 13;  // 13 is for skipping past the </Analysis> + nl
      var headerStart = iAnalysisStart - 1 < 0 ? iAnalysisStart : iAnalysisStart - 1;
      var header = workingDoc.Substring(0, headerStart);
      startLoc += iAnalysisEnd;
      var analysis = workingDoc.Substring(iAnalysisStart, Math.Max(iAnalysisEnd - iAnalysisStart, 1));

      return Tuple.Create(header, analysis);
    }



    public static PatrSentenceAnalysis LoadSentenceAndCommentsIntoClass(string header, PatrAnalysisData analysis_data, bool failuresOn, ref int current_sentence_index, List<string> inputSentences)
    {
      var sentence = new PatrSentenceAnalysis();
      if (current_sentence_index < inputSentences.Count)
      {
        sentence.Sentence = inputSentences[Math.Min(current_sentence_index, (inputSentences.Count - 1))];
        current_sentence_index += 1;
      }
      using (var reader = new StringReader(header))
      {
        var line = string.Empty;

        if (failuresOn)
        {
          ReadComments(reader, sentence);
        }
        else
        {
          while ((line = reader.ReadLine()) != null)
          {
            if ((string.IsNullOrEmpty(line))) continue;
            if (!(line.Contains("****") | line.Contains("|"))) continue;
            if (line.Contains("Not able to parse this sentence"))
            {
              line = HandleFailuresOffFailedSentence(analysis_data, ref current_sentence_index, inputSentences, line,
                                                     reader);

              if ((line.Contains("****") | line.Contains("|")))
              {
                sentence.Comments.Add(line);
              }
            }
            else
            {
              sentence.Comments.Add(line);
            }
          }
        }
      }

      return sentence;
    }

    private static void ReadComments(StringReader reader, PatrSentenceAnalysis sentence)
    {
      string line;
      while ((line = reader.ReadLine()) != null)
      {
        if ((string.IsNullOrEmpty(line) || line[0] == '|')) continue;
        if ((line.Contains("****") | line.Contains("|"))) sentence.Comments.Add(line);
      }
    }

    private static string HandleFailuresOffFailedSentence(PatrAnalysisData analysis_data, ref int current_sentence_index,
                                                          List<string> inputSentences, string line, StringReader reader)
    {
      var newSentence = new PatrSentenceAnalysis();
      newSentence.Sentence = inputSentences[Math.Min(current_sentence_index, (inputSentences.Count - 1))];
      current_sentence_index += 1;
      newSentence.Failed = true;
      newSentence.Comments.Add(line);
      while ((line = reader.ReadLine()) != null)
      {
        if (!line.Contains(("****"))) break;
        newSentence.Comments.Add(line);
      }
      analysis_data.PatrSentences.Add(newSentence);

      return line;
    }


    ///// <summary>
    ///// Get XML of next analysis element in document
    ///// </summary>
    ///// <param name="sDoc">Document text file</param>
    ///// <param name="iBegin">index of where to begin looking;  is set at end of the next analysis; if negative, it indicates nothing was found</param>
    ///// <returns>xml node containing the analysis element</returns>
    ///// From PcPatrDocument.cs by H. Andrew Black
    //protected XmlNode GetNextSentence(string sDoc, ref int iBegin)
    //{
    //  if (iBegin >= 0)
    //  {
    //    string sText = sDoc.Substring(iBegin);
    //    int iBeg = sText.IndexOf("<Analysis");
    //    int iEnd = sText.IndexOf("</Analysis>") + 13;  // 13 is for skipping past the </Analysis> + nl
    //    if (iBeg > 0)
    //    {
    //      iBegin += iEnd;
    //      string sXml = sText.Substring(iBeg, iEnd - iBeg);
    //      XmlDocument doc = new XmlDocument();
    //      doc.LoadXml(sXml);
    //      return doc.SelectSingleNode("/");
    //    }
    //    else
    //    {
    //      iBegin = -1; // nothing to find; make sure index is negative
    //    }
    //  }
    //  return null;
    //}


    private string LoadFileIntoString(string filePath)
    {

      return File.ReadAllText(filePath);
      //var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);

      //var buffer = new byte[1024];
      //var count = 1;
      //var sb = new StringBuilder();
      //while ((count = fileStream.Read(buffer, 0, 1024)) > 0)
      //{
      //  sb.Append(Encoding.Default.GetString(buffer, 0, count));
      //}

      //return sb.ToString();
    }

    /// <summary>
    /// Reads xAnalysis data from xml node into a new PatrSentenceAnalysis object
    /// </summary>
    /// <returns>new PatrSentenceAnalysis object with data loaded</returns>
    private static PatrSentenceAnalysis AnalyzePatrSentence(string sentenceString, PatrSentenceAnalysis analysis)
    {
      var sentence = LoadStringIntoXmlNode(sentenceString);

      var xmlAna = sentence.SelectSingleNode("./" + PatrNames.ANALYSIS);

      if (xmlAna != null)
      {
        ReadAnalysisAttributes(ref analysis, xmlAna);
        var isBush = CheckForBush(sentence, ref analysis);
        ReadPatrAnalysis(ref analysis, xmlAna, isBush);
      }

      return analysis;
    }

    private static bool CheckForBush(XmlNode sentence, ref PatrSentenceAnalysis analysis)
    {
      var bushTop = sentence.SelectSingleNode("./Analysis/Parse/Node/@cat");
      if (bushTop == null) return false;
      if (bushTop.Value == "?")
      {
        analysis.Failed = true;
        return true;
      }

      return false;
    }

    private static XmlNode LoadStringIntoXmlNode(string x)
    {
      var doc = new XmlDocument();
      try
      {
        doc.LoadXml(x);
      }
      catch
      {
        return null;
      }

      return doc.SelectSingleNode("/");
    }


    private static void ReadPatrAnalysis(ref PatrSentenceAnalysis sentence, XmlNode xAnalysis, bool isBush)
    {
      if (xAnalysis == null) throw new ArgumentNullException("xAnalysis");
      if (sentence == null) throw new ArgumentNullException("sentence");

      XmlNodeList parses = xAnalysis.SelectNodes(".//" + PatrNames.PARSE);
      if (parses == null) return;

      foreach (XmlNode parse in parses)
      {
        sentence.Parses.Add(AnalysePatrParse(parse, isBush, xAnalysis));
      }
    }

    private static PatrParseAnalysis AnalysePatrParse(XmlNode parse, bool isBush, XmlNode doc)
    {
      if (parse == null) throw new ArgumentException("parse");
      var analysis = new PatrParseAnalysis { Nodes = GetChildNodes(parse, doc) };

      //analysis.FailurePoints = GetParseFailurePoints(parse);


      if (isBush)
      {
        analysis.FailurePoints.Add(BushFailurePoint(parse));
      }

      return analysis;
    }



    private static List<PatrParseNode> GetChildNodes(XmlNode parse, XmlNode doc)
    {
      var xNodes = parse.SelectNodes(".//Node|.//Leaf");
      if (xNodes == null) return new List<PatrParseNode>();

      var nodes = FEs.Map(xNodes, f => LoadDataIntoPatrParseNode(f, doc));

      return new List<PatrParseNode>(nodes);
    }

    private static PatrParseNode BushFailurePoint(XmlNode parse) // no reflection on any presidents intended :-)
    {
      var failurePoint = new PatrParseNode();
      var node = parse.SelectSingleNode("./Node");
      if (node == null || node.Attributes == null)
      {
        failurePoint.Cat = "?";

        return failurePoint;
      }


      var cat = node.Attributes.GetNamedItem("cat");
      var id = node.Attributes.GetNamedItem("id");

      failurePoint.Cat = cat == null ? "?" : cat.Value;
      failurePoint.Id = id == null ? string.Empty : id.Value;

      return failurePoint;

    }

    private static List<PatrParseNode> GetParseFailurePoints(XmlNode xNode, XmlDocument doc)
    {
      var failures = new List<PatrParseNode>();

      var xFailNodes = xNode.SelectNodes(".//Node[@fail= 'true']");

      if (xFailNodes == null) return failures;

      foreach (XmlNode node in xFailNodes)
      {
        if (node.Attributes == null) continue;
        var failure = LoadDataIntoPatrParseNode(node, doc);

        GetFailedFeatures(ref failure, node, doc);

        failures.Add(failure);
      }

      return failures;
    }

    private static PatrParseNode LoadDataIntoPatrParseNode(XmlNode node, XmlNode doc)
    {
      var newNode = new PatrParseNode
      {
        Id = node.Attributes[PatrNames.ID] != null ? node.Attributes[PatrNames.ID].Value : null,
        RuleName = node.Attributes[PatrNames.RULE] != null ? node.Attributes[PatrNames.RULE].Value : null,
        All = node.Attributes[PatrNames.ALL] != null ? node.Attributes[PatrNames.ALL].Value : null,
        Cat = node.Attributes[PatrNames.CAT] != null ? node.Attributes[PatrNames.CAT].Value : null,
      };
      newNode.FeatureStructure = WalkFeatures(GetFeaturesNode(node), ref newNode, doc);
      newNode.FailedFeatures = GetFailedFeatures(newNode.FeatureStructure);

      if (node.Name == "Leaf")
      {
        newNode.LexicalFeatureStructure = WalkFeatures(GetLexicalFeaturesNode(node), ref newNode, doc);
      }
      return newNode;
    }

    private static XmlNode GetLexicalFeaturesNode(XmlNode node)
    {
      return node.SelectSingleNode("./Lexfs");
    }

    private static List<PatrParseFeature> GetFailedFeatures(PatrParseFeature structure)
    {
      var failedList = new List<PatrParseFeature>();
      if (structure.Failed)
      {
        failedList.Add(structure);
      }

      structure.ChildFeatures.ForEach(f => failedList.AddRange(GetFailedFeatures(f)));

      return failedList;
    }


    /// <summary>
    /// Reads failure point for which features failed.
    /// </summary>
    private static void GetFailedFeatures(ref PatrParseNode failurePoint, XmlNode xNode, XmlDocument doc)
    {
      if (failurePoint == null) throw new ArgumentNullException("failurePoint");
      if (xNode == null) throw new ArgumentNullException("xNode");

      failurePoint.FeatureStructure = WalkFeatures(GetFeaturesNode(xNode), ref failurePoint, doc);
    }

    private static PatrParseFeature WalkFeatures(XmlNode features, ref PatrParseNode failurePoint, XmlNode doc)
    {
      var failure = new PatrParseFeature { ChildFeatures = WalkChildFeatures(features, ref failurePoint, doc) };

      return failure;
    }

    private static List<PatrParseFeature> WalkChildFeatures(XmlNode features, ref PatrParseNode failurePoint, XmlNode doc)
    {
      var children = new List<PatrParseFeature>();
      for (var i = 0; i < features.ChildNodes.Count; i++)
      {
        var node = features.ChildNodes[i];
        var child = LoadFeatureWithBasicData(node);
        EvaluateFeatureValue(node, ref child, ref failurePoint, doc);
        if (child.Failed) failurePoint.FailedFeatures.Add(child);
        children.Add(child);
      }

      return children;
    }

    private static PatrParseFeature LoadFeatureWithBasicData(XmlNode feature)
    {
      var failure = new PatrParseFeature();

      if (feature.Attributes == null) return failure;
      var name = feature.Attributes.GetNamedItem("name");
      if (name != null)
      {
        failure.Name = name.Value;
      }
      var id = feature.Attributes.GetNamedItem("id");
      if (id != null)
      {
        failure.Id = id.Value;
      }

      return failure;
    }


    private static XmlNode GetFeaturesNode(XmlNode source)
    {
      return source.SelectSingleNode("./Fs");
    }

    private static void EvaluateFeatureValue(XmlNode feature, ref PatrParseFeature failure, ref PatrParseNode failurePoint, XmlNode doc)
    {
      if (feature == null) return;
      var childNode = feature.FirstChild;
      if (childNode == null)
      {
        var attFval = feature.Attributes.GetNamedItem("fVal");
        if (attFval == null) failure.Content = "Could not parse!";
        var referencedNode = doc.SelectSingleNode(string.Format("//*[@id='{0}']", attFval.Value));
        if (referencedNode == null) failure.Content = "Could not parse!";

        failure.ChildFeatures = WalkChildFeatures(referencedNode, ref failurePoint, doc);

        return;
      }

      switch (childNode.Name.ToUpper())
      {
        case "FS":
          var Fs = WalkFeatures(childNode, ref failurePoint, doc);
          failure.ChildFeatures.AddRange(Fs.ChildFeatures);
          break;
        case "STR":
          failure.Content = GetStrNodeContent(childNode);
          break;
        case "PLUS":
          failure.Content = "+";
          break;
        case "MINUS":
          failure.Content = "-";
          break;
        case "ANY":
          failure.Content = "Any";
          break;
        case "NONE":
          failure.Content = "None";
          break;
        case "SYM":
          failure.Content = "Symbol";
          break;
        case "NBR":
          if (childNode.Attributes == null)
          {

            var childAtt = childNode.Attributes.GetNamedItem("value");
            if (childAtt == null) failure.Content = "Number";

            else failure.Content = childAtt.Value;
          }
          else failure.Content = "Number";
          break;
      }
    }

    private static string GetStrNodeContent(XmlNode strNode)
    {
      return strNode.InnerText;
    }



    /// <summary>
    /// Reads the attributes on the Analysis node from a sentence parse result
    /// </summary>
    /// <param name="sentence">PatrSentenceAnalysis object to hold data</param>
    /// <param name="xAnalysis">Xml Analysis node holding data to be read</param>
    private static void ReadAnalysisAttributes(ref PatrSentenceAnalysis sentence, XmlNode xAnalysis)
    {
      var xCountAtt = xAnalysis.Attributes[PatrNames.COUNT];
      if (xCountAtt != null)
      {
        sentence.AnalysesCount = Convert.ToInt64(xCountAtt.Value);
      }

      var xFailAtt = xAnalysis.Attributes[PatrNames.FAIL];
      if (xFailAtt != null)
      {
        sentence.Failed = (xFailAtt.Value == "true");
      }
      else
      {
        sentence.Failed = false;
      }

    }

    public void CompareParsesForSentence(PcPatrSentence sentence)
    {
      CompareParses(sentence);
    }


    public static void RemoveNodesWhereAllIsTrue(XmlNode[] list)
    {
      foreach (var xmlNode in list)
      {
        var allIsTrueList = xmlNode.SelectNodes(".//Node[@all='true']");
        if (allIsTrueList == null) continue;
        foreach (XmlNode node in allIsTrueList)
        {
          var parentNode = node.ParentNode;
          if (parentNode != null)
          {
            parentNode.RemoveChild(node);
          }
        }
      }
    }

    //sentence analysis section
    public static void CompareParses(PcPatrSentence sentence)
    {
      var parses = GetParseNodesFromSentence(sentence);

      RemoveNodesWhereAllIsTrue(parses);


      var levelsList = GetNodeLevels(parses);

      compareLevels(levelsList);

      var levelsAfterComparison = GetNodeLevels(parses);
      var builder = new StringBuilder();
      DisplayLevel(levelsAfterComparison, builder, _formatter, sentence.NumberOfParses);

      System.IO.File.WriteAllText(OutsideWorld.FeatureComparisonOutput, builder.ToString());

      ShowResultsInAnyWebBrowser(OutsideWorld.FeatureComparisonOutput);
    }

    public static void ShowResultsInAnyWebBrowser(string filePath)
    {
      try
      {
        Process.Start(filePath);
      }
      catch
      {
        try
        {
          var ffStartInfo = new ProcessStartInfo("firefox.exe", filePath);
          Process.Start(ffStartInfo);
        }
        catch
        {
          try
          {
            var ieStartInfo = new ProcessStartInfo("IExpore.exe", filePath);
            Process.Start(ieStartInfo);
          }
          catch
          {
            try
            {
              LaunchWithBrowserControl(filePath);
            }
            catch
            {
              System.Windows.Forms.MessageBox.Show(
                "Could not start web browser to show results" + Environment.NewLine + "File location: " +
                OutsideWorld.FeatureComparisonOutput, "error");
            }
          }
        }
      }
    }

    private static void LaunchWithBrowserControl(string filePath)
    {

      var temp = new Window();

      var browser = new WebBrowser();

      browser.Navigate(filePath);

      temp.Content = browser;

      temp.Show();

    }

    private XslCompiledTransform _featureTransform;
    private static Func<XmlNode, string> _formatter;

    private Func<XmlNode, string> BuildFormatter(string filePath)
    {
      _featureTransform = new XslCompiledTransform();
      _featureTransform.Load(filePath);

      return node =>
      {
        var doc = new XmlDocument();
        doc.LoadXml(node.OuterXml);

        var builder = new StringBuilder();
        var textWriter = new StringWriter(builder);
        var writer = new XmlTextWriter(textWriter);

        _featureTransform.Transform(doc.CreateNavigator(), writer);

        var outDoc = new XmlDocument();
        outDoc.LoadXml(builder.ToString());

        var htmlTable = outDoc.SelectSingleNode("//body");
        if (htmlTable == null)
        {
          return outDoc.InnerXml;
        }
        else
        {
          return htmlTable.InnerXml;
        }
        //return builder.ToString();
      };
    }


    public static List<XmlNode[][]> GetNodeLevels(XmlNode[] parses)
    {
      var levelsList = new List<XmlNode[][]>();

      var xpath = "./Node|Leaf";
      var nodesFound = true;

      while (nodesFound)
      {

        var level = new XmlNode[parses.Length][];

        for (var i = 0; i < parses.Length; i += 1)
        {
          var nodesAtThisLevel = parses[i].SelectNodes(xpath);
          var levelNodeCount = nodesAtThisLevel.Count;

          nodesFound = levelNodeCount > 0 ? true : false;
          //keep looping while there are any nodes found.
          if (nodesFound)
          {
            var nodes = new XmlNode[levelNodeCount];

            for (var x = 0; x < levelNodeCount; x += 1)
            {
              nodes[x] = nodesAtThisLevel[x];
            }

            level[i] = nodes;
          }
          else
          {
            level[i] = null;
          }
        }

        levelsList.Add(level);
        xpath = xpath.Replace("|Leaf", string.Empty) + "/Node|Leaf";
      }

      return levelsList;
    }




    public static void DisplayLevel(IEnumerable<XmlNode[][]> levelsList, StringBuilder sb, Func<XmlNode, string> formatter, int parseCount)
    {
      var builders = new StringBuilder[parseCount];
      for (var i = 0; i < parseCount; i += 1)
      {
        builders[i] = new StringBuilder();
      }

      foreach (var level in levelsList)
      {
        if (level.Length < 1) continue;

        //foreach parse
        for (var x = 0; x < level.Length; x += 1)
        {
          var parse = level[x];
          var builder = builders[x];

          builder.AppendLine("<tr style=\"border-bottom: solid 1px black;\">");
          if (parse != null)
          {
            foreach (var node in parse)
            {
              builder.AppendLine("<td>");
              WriteNodeAndFeatureStructure(formatter, node, builder);
              builder.AppendLine("</td>");
            }
          }
          else
          {
            builder.AppendLine("<td></td>");
          }
          builder.AppendLine("</tr>");
        }
      }


      //start building main table
      sb.AppendLine("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/></head><body><table><tbody>");

      sb.AppendLine("<tr>");
      for (var i = 0; i < parseCount; i += 1)
      {
        sb.AppendLine("<th>Parse " + (i + 1) + "</th>");
      }
      sb.AppendLine("</tr>");


      sb.AppendLine("<tr>");

      const string TABLE_STYLE = "border:1px solid #000; border-top:0px; border-bottom:0px;";
      for (var i = 0; i < parseCount; i += 1)
      {
        var builder = builders[i];
        sb.AppendLine("<td><table style=\"" + TABLE_STYLE + "\"><tbody>");
        sb.Append(builder.ToString());
        sb.AppendLine("</tbody></table></td>");
      }
      sb.AppendLine("</tr>");
      sb.AppendLine("</tbody></table></body></html>");
    }

    private static void WriteNodeAndFeatureStructure(Func<XmlNode, string> formatter, XmlNode node, StringBuilder builder)
    {
      if (node == null) return;
      var cat = node.Attributes.GetNamedItem("cat");
      var ruleName = node.Attributes.GetNamedItem("rule");

      builder.AppendLine("<table><tbody>");
      if (cat != null)
      {

        builder.AppendLine("<tr>");
        builder.AppendLine("<td style=\"text-align:center; vertical-align:middle;\"><b>" + cat.Value + "</b>" + ((ruleName == null) ? string.Empty : " (" + ruleName.Value + ")") + "</td>");
        builder.AppendLine("</tr>");

        var nodeFs = node.SelectSingleNode("./Fs");

        builder.AppendLine("<tr>");
        if (nodeFs != null) builder.AppendLine("<td>" + formatter(nodeFs) + "</td>");
        else builder.AppendLine("<td>no features</td>");
        builder.AppendLine("</tr>");
      }
      else
      {
        builder.AppendLine("<tr><td>none</td></tr>");
        builder.AppendLine("<tr><td>none</td></tr>");
      }
      builder.AppendLine("</tbody></table>");

    }


    public static void compareLevels(IEnumerable<XmlNode[][]> levelsList)
    {
      foreach (var level in levelsList)
      {
        var nodeList =
          level.Aggregate((IEnumerable<XmlNode>)new List<XmlNode>(), (working, next) => (next == null) ? working : working.Concat(next));
        if (nodeList != null)
        {
          RemoveMatchingNodes(nodeList.ToArray());
        }
      }
    }


    public static XmlNode[] GetParseNodesFromSentence(PcPatrSentence sentence)
    {
      var parses = new XmlNode[sentence.NumberOfParses];
      for (var i = 0; i < sentence.Parses.Length; i += 1)
      {
        var parse = sentence.GoToParse(i + 1);
        parses[i] = parse.Node.CloneNode(true); // don't destroy the data
      }

      return parses;
    }

    public static XmlNodeList[] GetElementAcrossParses(string xpath, XmlNode[] parses)
    {
      var list = new List<XmlNodeList>();

      for (var i = 0; i < parses.Length; i += 1)
      {
        var node = parses[i].SelectNodes(xpath);
        list.Add(node);
      }

      return list.ToArray();
    }





    /// <summary>
    /// Unlinks nodes that are the same leaving the rest.
    /// </summary>
    public static void RemoveMatchingNodes(XmlNode[] list)
    {
      var listCount = list.Length;
      var previousNodes = new List<XmlNode>();
      var allNodesMatched = true;

      for (var i = 0; i < listCount; i += 1)
      {
        var nodesDidNotMatch = true;
        var currentNode = list[i];
        if (previousNodes.Count > 0)
        {
          foreach (var node in previousNodes)
          {
            //            switch (currentNode.Name) {
            //              case "Node":
            //                var currentCat = currentNode.Attributes.GetNamedItem("cat");
            //                var nodeCat = node.Attributes.GetNamedItem("cat");
            //                if (currentCat != null & nodeCat != null)
            //                {
            //                  if (currentCat.Value == nodeCat.Value)
            //                  {
            //                    SafeRemoveChildFromParent(currentNode);
            //                  }
            //                }
            //                break;
            //                
            //              case "F":
            //                var currentName = currentNode.Attributes.GetNamedItem("name");
            //                var nodeParent = node.ParentNode;
            //                if (nodeParent != null)
            //                {
            //                  var matchingFeature = nodeParent.SelectSingleNode("./F[@name='"+ currentName.Value + "']");
            //                  if (matchingFeature != null)
            //                  {
            //                    if (matchingFeature.InnerXml == currentNode.InnerXml)
            //                    {
            //                      SafeRemoveChildFromParent(currentNode);                      
            //                    }
            //                  }
            //                }
            //                break;
            //              default:
            //                if (node.InnerText == currentNode.InnerText)
            //                {
            //                  SafeRemoveChildFromParent(currentNode);
            //                  nodesDidNotMatch = false;
            //                  break;
            //                }
            //                else
            //                {
            //                  allNodesMatched = false;
            //
            //                }
            //                break;
            //            }

            if (node.InnerText == currentNode.InnerText)
            {
              SafeRemoveChildFromParent(currentNode);
              nodesDidNotMatch = false;
              break;
            }
            else
            {
              allNodesMatched = false;

            }
          }
        }

        if (nodesDidNotMatch)
        {
          previousNodes.Add(currentNode);
        }
      }

      if (allNodesMatched)
      {
        foreach (var previousNode in previousNodes)
        {
          SafeRemoveChildFromParent(previousNode);
        }
      }

      if (previousNodes.Count > 1)
      {
        var firstNode = previousNodes[0];
        var previousNodesArray = previousNodes.ToArray();
        if (firstNode != null)
        {
          switch (firstNode.Name)
          {
            case "Node":
              var featureStructures = GetFeatureStructures(previousNodesArray);
              CompareFeatureStructures(featureStructures);
              break;
          }
        }
        //        
        //        var childCount = getLowestChildCount(previousNodes);
        //        var childLists = getListsOfChildren(previousNodes.ToArray());
        //        for (var i = 0; i < childCount; i += 1)
        //        {
        //          var elementsAtIndex = getElementFromListsAtIndex(childLists, i);
        //
        //          RemoveMatchingNodes(elementsAtIndex.ToArray());
        //        }
      }
    }

    public static void CompareFeatureStructures(XmlNode[] structures)
    {
      var structuresLength = structures.Length;
      if (structuresLength < 2) return;

      var first = structures[0];
      var childFeatures = first.SelectNodes("./F");
      if (childFeatures != null)
      {
        foreach (XmlNode childNode in childFeatures)
        {
          for (int i = 1; i < structuresLength; i += 1)
          {
            var nextNode = structures[i];
            CompareFeatureStructureChildren(childNode, nextNode);
          }
        }
      }

    }


    public static void CompareFeatureStructureChildren(XmlNode childNode, XmlNode nextNode)
    {
      if (childNode.Name == "F" && childNode.SelectSingleNode("./Str") != null)
      {
        var nameAtt = childNode.Attributes.GetNamedItem("name");
        if (nameAtt == null) return;
        var matchingFeature = nextNode.SelectSingleNode("./F[@name='" + nameAtt.Value + "']");
        if (matchingFeature != null)
        {
          SafeRemoveChildFromParent(matchingFeature);
        }
      }
      else
      {
        foreach (XmlNode firstChild in childNode.ChildNodes) //handles Fs
        {
          var nameAtt = childNode.Attributes.GetNamedItem("name");
          if (nameAtt == null) continue;

          if (firstChild == null) continue;
          var matchingFeature = nextNode.SelectSingleNode("./F[@name='" + nameAtt.Value + "']");
          if (matchingFeature != null)
          {
            var matchingChild = matchingFeature.FirstChild;
            if (matchingChild != null & firstChild != null)
            {
              if (matchingChild.Name == firstChild.Name)
              {
                switch (matchingChild.Name)
                {
                  case "Fs":
                    CompareFeatureStructureChildren(firstChild.FirstChild, matchingChild);
                    break;
                  default:
                    if (matchingChild.InnerXml == firstChild.InnerXml)
                    {
                      SafeRemoveChildFromParent(matchingFeature);
                    }
                    break;
                }
              }
            }
          }
        }
      }
    }

    public static XmlNode[] GetFeatureStructures(XmlNode[] list)
    {
      var fs = new XmlNode[list.Length];

      for (var i = 0; i < list.Length; i += 1)
      {
        fs[i] = list[i].SelectSingleNode("./Fs");
      }

      return fs;
    }


    private static void SafeRemoveChildFromParent(XmlNode doomed)
    {
      var parent = doomed.ParentNode;
      if (parent != null)
      {
        parent.RemoveChild(doomed);
      }
    }

    private static IEnumerable<T> getElementFromListsAtIndex<T>(IEnumerable<IEnumerable<T>> listOfLists, int index)
    {
      var result = new List<T>();
      foreach (var list in listOfLists)
      {
        var element = list.ElementAt(index);
        result.Add(element);
      }

      return result;
    }

    private static XmlNode[][] getListsOfChildren(IEnumerable<XmlNode> input)
    {
      var list = input.ToArray();
      var result = new XmlNode[list.Length][];
      var i = 0;
      foreach (var node in list)
      {

        var childCount = node.ChildNodes.Count;
        var childnodes = new XmlNode[childCount];
        for (var x = 0; x < childCount; x += 1)
        {
          childnodes[x] = node.ChildNodes[x];
        }

        result[i] = childnodes;
        i += 1;
      }

      return result;
    }

    private static int getLowestChildCount(IEnumerable<XmlNode> list)
    {
      var count = 1000;
      foreach (var node in list)
      {
        var childCount = node.ChildNodes.Count;
        if (childCount < count)
        {
          count = childCount;
        }
      }

      return count;
    }

    private static int getHighestChildCount(IEnumerable<XmlNode> list)
    {
      var highest = 0;
      foreach (var node in list)
      {
        var childCount = node.ChildNodes.Count;
        if (childCount > highest)
        {
          highest = childCount;
        }
      }

      return highest;
    }

  }



}