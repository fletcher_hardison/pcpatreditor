using System;
using System.IO;
using System.Text;

namespace PCPATR.Editor.MasterLists.PatrParser
{
  public class PatrParserControl
  {
    public static string XmlOut = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "parserOutput.xml");

    /// <summary>
    /// Initializes a new instance of the PatrParserControl class.
    /// </summary>
    public PatrParserControl()
    {
      _parser = new PatrParser
                   {
                     CommentChar = '|',
                     CodePage = Encoding.UTF8.CodePage
                   };
    }


    /// <summary>
    /// Parses the file provided by the file path property.
    /// </summary>
    /// <remarks>prints the data on each line, any messages from the parser, then parse results</remarks>
    public string ParseFile(string filePath, string lexiconFile, string grammarFile, string logFile, ParseOptionsViewModel options, long debugLevel)
    {
      if (filePath == null)
        throw new ArgumentNullException("filePath");
     
      _parser.OpenLog(logFile);
      _parser.LoadGrammarFile(grammarFile);
      _parser.LoadLexiconFile(lexiconFile, 0); //0 here means not to append, but to remove all previous lexicon files then add this one.
      
      options.ReadyParser(_parser, debugLevel);


      if (File.Exists(filePath) == false)
        throw new ApplicationException(string.Format("File does not exist: {0}", filePath));
      
      _parser.ParseFile(filePath, XmlOut);

      if (!string.IsNullOrEmpty(logFile))
        _parser.CloseLog();

      _parser.Clear();

      return XmlOut;
    }

    private readonly PatrParser _parser;
  }
}