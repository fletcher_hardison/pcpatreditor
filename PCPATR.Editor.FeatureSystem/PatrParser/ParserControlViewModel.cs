﻿//  ***************************************************************
//  File name: ParserControlViewModel.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by:
//  Created on: 23,01,2013
// ***************************************************************

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Microsoft.Practices.Prism.Commands;
using PCPATR.Editor.MasterLists.PatrParser.DataModel;
using PCPATR.Editor.MasterLists.ViewModels;
using SIL.PcPatrBrowser;
using MessageBox = System.Windows.MessageBox;

namespace PCPATR.Editor.MasterLists.PatrParser
{
  /// <summary>
  /// Allow user to select files and sentences to execute parser
  /// </summary>
  public class ParserControlViewModel : ViewModelBase
  {

    public ParserControlViewModel(PatrParserControl parserControl, ParseAnalyser analyser, LingTreeTreeControlNotifierService notifierService, MainWindowViewModel mainWindow, ConfigReader configReader, ParseOptionsViewModel optionsViewModel)
    {

      _notifierService = notifierService;
      _notifierService.RegisterStartPoint(BuildFailurePointCallBack());
      _parserControl = parserControl;
      _analyser = analyser;
      _mainWindow = mainWindow;
      _configReader = configReader;
      _optionsViewModel = optionsViewModel;
      _parameters = _mainWindow.Parameters;
      m_fsTransform = new XslCompiledTransform();
      m_fsTransform.Load(OutsideWorld.GetPathToShowFsTransForm());
      _featurePathPage = OutsideWorld.GetPathToInitFeature();
      _parseComparer = new ParseNodeComparerViewModel(m_fsTransform, this);
      
      FileToParse = _configReader[OutsideWorld.LAST_DATA_FILE];
      LexiconFile = _configReader[OutsideWorld.LAST_LEXICON];
      WordNotInLexicon = new ObservableCollection<string>();

      SelectLexiconFile = new DelegateCommand(ChooseLexiconFile);
      SelectFileToParse = new DelegateCommand(ChooseFileToParse);
      RunParser = new DelegateCommand(RunParserOnSelectedFileCommandMethod);
      //SelectGrammarFile = new DelegateCommand(ChooseGrammarFile);
      LoadPreviousParseOutput = new DelegateCommand(ChoosePreviousParseDataToLoad);
      DebugGrammar = new DelegateCommand(RunParseInDebugModeOnSelectFile);
      OpenLexiconFileInTextEditor = new DelegateCommand(() => OpenFileInTextEditor(_lexiconFile));
      OpenDataFileInTextEditor = new DelegateCommand(() => OpenFileInTextEditor(_fileToParse));
      RunParserWithSelectedGrammar = new DelegateCommand(RunWithSelectedGrmFile);
      WriteConsoleOutputToFile = new DelegateCommand(WriteConsoleOutputToFileMethod);
      ClearRepl = new DelegateCommand(() => GrammarRepl = null);
      DisplayFeatureComparison = new DelegateCommand(ShowFeatureComparision);
      ShowNodeComparisonWindow = new DelegateCommand(ShowNodeComparisonWindowMethod);
    }



    
    public DelegateCommand OpenDataFileInTextEditor { get; private set; }
    public DelegateCommand OpenLexiconFileInTextEditor { get; private set; }
    public DelegateCommand SelectLexiconFile { get; private set; }
    public DelegateCommand SelectFileToParse { get; private set; }
    public DelegateCommand RunParser { get; private set; }
    public DelegateCommand DebugGrammar { get; private set; }
    public DelegateCommand RunParserWithSelectedGrammar { get; private set; }
    public DelegateCommand WriteConsoleOutputToFile { get; private set; }
    public DelegateCommand LoadPreviousParseOutput { get; private set; }
    public DelegateCommand ClearRepl {get; private set; }
    public DelegateCommand DisplayFeatureComparison { get; private set; }
    public DelegateCommand ShowNodeComparisonWindow { get; private set; }

    public string GrammarFile { get; set; }

    public PatrParseNode CurrentNode
    {
      get { return _selectedNode; }
      set
      {
        _selectedNode = value;
        RaisePropertyChanged("CurrentNode");
        RaisePropertyChanged("CurrentNodeFeatureStructure");
      }
    }
    
    private string _currentNodeRuleName;
    public string CurrentNodeRuleName    {
      get {return _currentNodeRuleName;}
      set { _currentNodeRuleName = value;
        RaisePropertyChanged("CurrentNodeRuleName");
      }
    }
    
    public string InitFeature
    {
      get { return _featurePathPage; }
    }

    public string FileToParse
    {
      get { return _fileToParse; }
      set
      {
        _fileToParse = value;
        _configReader[OutsideWorld.LAST_DATA_FILE] = value;
        SaveConfigurations(_configReader);
        RaisePropertyChanged("FileToParse");
      }
    }

    public string LexiconFile
    {
      get { return _lexiconFile; }
      set
      {
        _lexiconFile = value;
        _configReader[OutsideWorld.LAST_LEXICON] = value;
        SaveConfigurations(_configReader);
        RaisePropertyChanged("LexiconFile");
      }
    }

    public bool ParsingFinished
    {
      get { return _parsingFinished; }
      private set
      {
        _parsingFinished = value;
        RaisePropertyChanged("ParsingFinished");
      }
    }


    public ObservableCollection<PatrSentenceAnalysis> Sentences
    {
      get { return _analysisSentences; }
      set
      {
        _analysisSentences = value;
        RaisePropertyChanged("Sentences");
      }
    }

    public PatrSentenceAnalysis CurrentAnalysisSentence
    {
      get { return _selectedAnalysisSentence; }
      set
      {
        if (_selectedAnalysisSentence != null)
        {
          //remove handler from former values
          _selectedAnalysisSentence.SelectedParseChanged -= SelectedAnalysisSentenceSelectedParseChanged;
        }
        //set value
        _selectedAnalysisSentence = value;
        _currentPatrSentence = GetCurrentPatrSentence();
        //assign event handler
        _selectedAnalysisSentence.SelectedParseChanged += SelectedAnalysisSentenceSelectedParseChanged;

        RaisePropertyChanged("CurrentAnalysisSentnce");
        
        //raise event for LingTreeTreeControl.
        SelectedAnalysisSentenceSelectedParseChanged(this, new EventArgs());
      }
    }

    private PcPatrSentence _currentPatrSentence;
    private PcPatrParse _currentParse;

    public PatrParseAnalysis CurrentAnalysisParse
    {
      get
      {
        return _selectedAnalysisSentence.SelectedParse;
      }
    }

    

    public ParseOptionsViewModel ParseOptions
    {
      get { return _optionsViewModel; }
    }

    public ObservableCollection<string> WordNotInLexicon { get; set; }

    private string _grammarRepl;
    public string GrammarRepl
    {
      get {return _grammarRepl;}
      set
      {
        _grammarRepl = value;
        RaisePropertyChanged("GrammarRepl");
      }
    }
    
    
    private void WriteConsoleOutputToFileMethod()
    {
      var dialog = new SaveFileDialog();
      dialog.Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*";
      var result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        File.WriteAllText(dialog.FileName, ParserConsoleOutput);
      }
    }

    private void ChooseLexiconFile()
    {
      var result = SelectFile();
      _configReader[OutsideWorld.LAST_LEXICON] = result;
      _configReader.SaveConfigurations(OutsideWorld.GetPathToConfigs());
      if (result == null) return;
      LexiconFile = result;
    }

    private void ChooseFileToParse()
    {
      var result = SelectFile();
      _configReader[OutsideWorld.LAST_DATA_FILE] = result;
      _configReader.SaveConfigurations(OutsideWorld.GetPathToConfigs());
      if (result == null) return;
      FileToParse = result;
    }


    private void ChoosePreviousParseDataToLoad()
    {
      var result = SelectFile();
      if (result == null) return;
      GetAnalysisFromParseOutput(result, _optionsViewModel.FailuresPerSentence > 0);
    }

    private bool ShowWordsNotInLexicon()
    {
      if (String.IsNullOrEmpty(_lexiconFile) | String.IsNullOrEmpty(_fileToParse)) return false;
      var lexicon = PatrInputCheckerMethods.GetListOfWordsInLexicon(File.ReadAllText(_lexiconFile), _optionsViewModel.LexiconWordMarker);
      var wordsInFile = PatrInputCheckerMethods.GetListOfWordsInDataFile(File.ReadAllText(_fileToParse), _optionsViewModel.CommentCharacter);

      var wordsNotInLexiconFile = PatrInputCheckerMethods.ComputeWordsNotInLexicon(lexicon, wordsInFile);

      WordNotInLexicon.Clear();
      foreach (var word in wordsNotInLexiconFile)
      {
        WordNotInLexicon.Add(word);
      }

      if (WordNotInLexicon.Count > 0)
      {
        var dialog = new NotInLexiconFileDialog { DataContext = WordNotInLexicon };
        dialog.SaveToFile += DialogSaveToFile;
        var result = dialog.ShowDialog();

        if (result.HasValue && result == true)
        {
          return true;
        }

        return false;
      }

      return true;
    }

    private void DialogSaveToFile(object sender, EventArgs e)
    {
      var fileDialog = new SaveFileDialog();
      fileDialog.Filter = "Text files (*.txt)|*.txt";
      var result = fileDialog.ShowDialog();

      if (result == DialogResult.OK)
      {
        File.WriteAllText(fileDialog.FileName,FormatWordsForPrinting(WordNotInLexicon));
      }
    }

    private string FormatWordsForPrinting(IEnumerable<string> words)
    {
      var sb = new StringBuilder();

      foreach (var word in words)
      {
        sb.AppendLine(word);
      }

      return sb.ToString();
    }

    private void RunParserOnSelectedFileCommandMethod()
    {
      ParsingFinished = false;
      try
      {

        if (!ShowWordsNotInLexicon()) return;

        var outputPath = RunParserOnSelectedFile(_fileToParse, _lexiconFile, _mainWindow.PathToGrammarFile,
                                                 () => _mainWindow.SaveGrammarWithOutNotification(), _analyser,
                                                 _parserControl, _optionsViewModel,
                                                 _parameters.GetXsltArgsListFromParameters(), 0, path => GrammarFile = path);
        if (outputPath == NO_FILE_LOADED)
        {
          MessageBox.Show("No grammar loaded");
        }
        else
        {
          GetAnalysisFromParseOutput(outputPath, _optionsViewModel.FailuresPerSentence > 0);
        }
      }
      catch (Exception ex)
      {
        if (ex is ApplicationException)
        {
          MessageBox.Show("Error while running parser: " + ex.Message);
        }
        else
        {
          MessageBox.Show("Error occured while running parser: " + ex);
        }
      }

      ParsingFinished = true;
    }

    private void RunParseInDebugModeOnSelectFile()
    {
      ParsingFinished = false;
      try
      {
        var outputPath = RunParserOnSelectedFile(_fileToParse, _lexiconFile, _mainWindow.PathToGrammarFile,
                                                 () => _mainWindow.SaveGrammarWithOutNotification(), _analyser,
                                                 _parserControl, _optionsViewModel,
                                                 _parameters.GetXsltArgsListFromParameters(), 1, path => GrammarFile = path);
        if (outputPath == NO_FILE_LOADED)
        {
          MessageBox.Show("No grammar loaded");
        }
        else
        {
          GetAnalysisFromParseOutput(outputPath, _optionsViewModel.FailuresPerSentence > 0);
        }

      }
      catch (Exception ex)
      {
        if (ex is ApplicationException)
        {
          MessageBox.Show("Error while running parser: " + ex.Message);
        }
        else
        {
          MessageBox.Show("Error occured while running parser: " + ex);
        }
      }

      ParsingFinished = true;
    }

    private const string NO_FILE_LOADED = "No file loaded!";
    /// <summary>
    /// computes the parser output from the file to parse, lexicon file, xml grammar file and returns path to parser output.
    /// </summary>
    /// <returns></returns>
    public string RunParserOnSelectedFile(string fileToParse, string lexiconFile, string pathToXmlFile, Func<string> saveGrammar, ParseAnalyser analyser, PatrParserControl parserControl, ParseOptionsViewModel options, Dictionary<string, string> parameters, long debugLevel, Action<string> grammarPathSetter)
    {
      if (String.IsNullOrEmpty(fileToParse) | String.IsNullOrEmpty(lexiconFile)) return String.Empty;
      
      var saveResult = saveGrammar();
      if (saveResult == NO_FILE_LOADED) return NO_FILE_LOADED;
      if (saveResult != "Save sucessful") return string.Empty;
      
      var pathToGrm = GetGrmPathFromXmlPath(pathToXmlFile);
      
      grammarPathSetter(pathToGrm);

      IOHelperMethods.TransformGrammarXmlToGrmFile(pathToXmlFile, OutsideWorld.GetPathToTransform(), pathToGrm, false, parameters);

      

      return RunParserOnSelectedGrmFile(fileToParse, lexiconFile, pathToGrm, analyser, parserControl, options,
                                        parameters, debugLevel);
    }

    public static string StdOutFile = Path.Combine(Path.GetTempPath(), "ParserStdOutValue.txt");

    

    public string RunParserOnSelectedGrmFile(string fileToParse, string lexiconFile, string pathToGrmFile, ParseAnalyser analyser, PatrParserControl parserControl, ParseOptionsViewModel options, Dictionary<string, string> parameters, long debugLevel)
    {
      if (String.IsNullOrEmpty(fileToParse) | String.IsNullOrEmpty(lexiconFile)) return String.Empty;
      var tempFilePath = IOHelperMethods.GenerateTempCopyAndCleanFileToParse(fileToParse, _optionsViewModel.CommentCharacter);
      analyser.LoadDataFile(tempFilePath);
      
      if (!string.IsNullOrEmpty(_grammarRepl))
      {
        File.AppendAllText(pathToGrmFile,_grammarRepl);
      }
      
      try
      {
        if (options.RunWithPatr32)
        {
          ParserConsoleOutput =
            PatrProcessMethods.ExecuteParseOnProcess(PatrProcessMethods.GenerateTakFile(_optionsViewModel, this, PARSE_OUTPUT, fileToParse: tempFilePath), StdOutFile);
          // this won't crash the application and might give better feed back in the future.
        }
        else
        {
          parserControl.ParseFile(tempFilePath, lexiconFile, pathToGrmFile, PARSE_OUTPUT, options, debugLevel);

          ParserConsoleOutput = "Run with PcPatr32 is set to false. No ouput available.";
        }


      }
      // ReSharper disable RedundantCatchClause
      catch
      {
        throw;
      }
      // ReSharper restore RedundantCatchClause

      return PARSE_OUTPUT;
    }

    private string _parseConsoleOutput;
    public string ParserConsoleOutput
    {
      get { return _parseConsoleOutput; }
      set
      {
        _parseConsoleOutput = value;
        RaisePropertyChanged("ParserConsoleOutput");
      }
    }

    private void RunWithSelectedGrmFile()
    {
      var grmFile = SelectFile();

      if (String.IsNullOrEmpty(grmFile) || !grmFile.ToUpper().Contains(".GRM")) return;
      ParsingFinished = false;
      GrammarFile = grmFile;
      try
      {
        var resultPath = RunParserOnSelectedGrmFile(_fileToParse, _lexiconFile, grmFile, _analyser, _parserControl,
                                                    _optionsViewModel,
                                                    _parameters.GetXsltArgsListFromParameters(), 0);

        GetAnalysisFromParseOutput(resultPath, _optionsViewModel.FailuresPerSentence > 0);
      }
      catch (Exception ex)
      {
        if (ex is ApplicationException)
        {
          MessageBox.Show("Error while running parser: " + ex.Message);
        }
        else
        {
          MessageBox.Show("Error occured while running parser: " + ex);
        }
      }
      ParsingFinished = true;
    }

    private static string GetGrmPathFromXmlPath(string xmlPath)
    {
      var xmlExtensionIndex = xmlPath.IndexOf(".xml");


      if (xmlExtensionIndex > 0)
      {

        return xmlPath.Replace(".xml", ".grm");
      }
      else
      {
        var patrxmlExtensionIndex = xmlPath.ToUpper().IndexOf(".PATRXML");
        if (patrxmlExtensionIndex > 0)
        {
          return xmlPath.Replace(".patrxml", ".grm");
        }
      }


      return xmlPath + ".grm";
    }

    private void GetAnalysisFromParseOutput(string pathToOutput, bool failuresOn)
    {
      if (String.IsNullOrEmpty(pathToOutput)) return;

      _analysisModel = _analyser.AnalysePatrOutput(pathToOutput, failuresOn, _optionsViewModel.CommentCharacter);

      string grammarFile;
      _pcPatrDocument = new PcPatrDocument(pathToOutput, out grammarFile);
      Sentences = new ObservableCollection<PatrSentenceAnalysis>(_analysisModel.PatrSentences);
      RaisePropertyChanged("ParseDocument");
      RaisePropertyChanged("ParseAnalysis");
    }

    

    //public string CurrentNodeFeatureStructure
    //{
    //  get
    //  {
    //    if (_selectedNode == null) return string.Empty;
    //    if (_selectedNode.IsLeafNode)
    //    {
    //      return _selectedNode.FeatureStructure.FormattedRepresentation +
    //             Environment.NewLine + "Lexical Features" + Environment.NewLine +
    //             _selectedNode.LexicalFeatureStructure.FormattedRepresentation;
    //    }

    //    return _selectedNode.FeatureStructure.FormattedRepresentation;
    //  }
    //}


    private void SelectedAnalysisSentenceSelectedParseChanged(object sender, EventArgs e)
    {
      if (_currentPatrSentence == null) return;

      SetCurrentPcPatrParse(_currentPatrSentence);

      _notifierService.ShowParse(_currentPatrSentence, _currentParse);
      
      CreateEmptyFeatureStructureDoc(_featurePathPage);
      RaiseRequestingFeatureDisplay();
    }

    private void CreateEmptyFeatureStructureDoc(string filePath)
    {
      const string DEFAULT_CONTENT =
        "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/></head><body><p>Select a node to view feature structure</p></body></html>";
      File.WriteAllText(filePath,DEFAULT_CONTENT);
    }


    private PcPatrSentence GetCurrentPatrSentence()
    {
      var index = _analysisModel.PatrSentences.IndexOf(_selectedAnalysisSentence) + 1;
      if (_optionsViewModel.FailuresPerSentence < 1 & _selectedAnalysisSentence.Failed)
      {
        return null;
      }
      return _pcPatrDocument.GoToSentence(index);
      //RaisePropertyChanged("CurrentPatrSentence");
    }

    private PcPatrParse GetCurrentParseAnalysis()
    {
      var currentSentence = GetCurrentPatrSentence();
      if (currentSentence == null) return null;
      return currentSentence.GoToParse(_selectedAnalysisSentence.SelectedParseIndex);
    }

    private void SetCurrentPcPatrParse(PcPatrSentence currentSentence)
    {
      if (currentSentence == null) return;
      
      _currentParse = currentSentence.GoToParse(_selectedAnalysisSentence.SelectedParseIndex + 1); // 1 based list
    }

    public Tuple<int, XmlNode> CurrentNodeToCompareData { get; private set; }

    public void ShowFSInfo(string sId, string filePath)
    {
      var parse = GetCurrentParseAnalysis();
      XmlNode featStruct = GetFeatureStructureAtNode(sId, parse);
      if (featStruct == null)
        return; // avoid crash
      
      if (IsComparingNodes)
      {
        CurrentNodeToCompareData = Tuple.Create( _selectedAnalysisSentence.SelectedParseIndex + 1, featStruct);
      }

      XmlDocument doc1 = new XmlDocument();
      doc1.LoadXml(featStruct.OuterXml);
      XPathNavigator nav = doc1.CreateNavigator();

      XmlTextWriter writer = new XmlTextWriter(filePath, null);
      m_fsTransform.Transform(nav, null, writer, null);
      writer.Close();

      RaiseRequestingFeatureDisplay();
    }

    
    private bool _isComparingNodes;
    public bool IsComparingNodes
    {
      get { return _isComparingNodes; } 
      set { _isComparingNodes = value;
      if (!_isComparingNodes)
      {
        CurrentNodeToCompareData = null;
      }}
    }

    
    public XmlNode GetFeatureStructureAtNode(string sNodeId, PcPatrParse parse)
    {
      string sId = sNodeId;
      string sFeatureStructureElement = "Fs";
      if (sNodeId.EndsWith("gloss"))
      {
        sId = sNodeId.Substring(0, sNodeId.Length - 5);
        sFeatureStructureElement = "Lexfs";
      }
      if (sNodeId.EndsWith("lex"))
      {
        sId = sNodeId.Substring(0, sNodeId.Length - 3);
        sFeatureStructureElement = "Lexfs";
      }
      string sAtId = "[@id='" + sId + "']/" + sFeatureStructureElement;
      string sChosenNodeXPath = "//Node" + sAtId + " | //Leaf" + sAtId;
      XmlNode featStructBasic = parse.Node.SelectSingleNode(sChosenNodeXPath);
      XmlNode featStruct = FleshOutFVals(featStructBasic, parse);
      return featStruct;
    }

    private XmlNode FleshOutFVals(XmlNode basic, PcPatrParse parse)
    {
      if (basic == null)
        return null;
      XmlNodeList nodes = basic.SelectNodes("descendant::F/@fVal");
      if (nodes != null)
      {
        foreach (XmlNode node in nodes)
        {
          string sFVal = node.InnerText;
          XmlNode fleshed = GetFeatureStructureAtFs(sFVal, parse);
          if (fleshed != null)
          {
            // append fleshed to basic at right spot
            XmlNode f = node.SelectSingleNode("..");
            if (f != null) f.InnerXml = fleshed.OuterXml;
          }
        }
      }
      return basic;
    }


    private XmlNode GetFeatureStructureAtFs(string sFsId, PcPatrParse parse)
    {
      string sFs = "//Fs[@id='" + sFsId + "']";
      if (parse.Node == null)
        return null; // avoid crash
      XmlNode featStructBasic = parse.Node.SelectSingleNode(sFs);
      XmlNode featStruct = FleshOutFVals(featStructBasic, parse);
      return featStruct;
    }



    private Action<string> BuildFailurePointCallBack()
    {
      return id =>
      {
        if (_selectedAnalysisSentence == null || _selectedAnalysisSentence.SelectedParse == null) return;
        var matchingFailurePoint = _selectedAnalysisSentence.SelectedParse.FailurePoints.FirstOrDefault(f => f.Id == id);
        // if (matchingFailurePoint == null) return;
        _selectedAnalysisSentence.SelectedParse.SelectedFailurePoint = matchingFailurePoint;
        var matchingNode = _selectedAnalysisSentence.SelectedParse.Nodes.FirstOrDefault(f => f.Id == id);
        if (matchingNode != null)
        {
          CurrentNodeRuleName = matchingNode.RuleName;
        }
        CurrentNode = matchingNode;

        ShowFSInfo(id, _featurePathPage);
      };

    }


    private void ShowFeatureComparision()
    {
      if (_currentPatrSentence == null) return;
      _analyser.CompareParsesForSentence(_currentPatrSentence);
    }

    private void SaveConfigurations(ConfigReader reader)
    {
      #if DEBUG
      reader.SaveConfigurations(OutsideWorld.DebugConfigPath);
      #else
      reader.SaveConfigurations(OutsideWorld.ReleaseConfigPath);
      #endif
    }

    private void OpenFileInTextEditor(string filePath)
    {
      if (filePath == null) return;
      try
      {
        Process.Start(filePath);
      }
      catch
      {
        Process.Start("notepad.exe", filePath);
      }
    }

    private string SelectFile()
    {
      var dialog = new OpenFileDialog();
      var result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        return dialog.FileName;
      }

      return null;
    }

    private void RaiseRequestingFeatureDisplay()
    {
      if (RequestingFeatureDisplay != null)
      {
        RequestingFeatureDisplay(this, new EventArgs());
      }
    }

    private void ShowNodeComparisonWindowMethod()
    {
      IsComparingNodes = true;
      if (_parseComparer == null) return;
      var window = new NodeComparisonWindow(_parseComparer);
      window.Topmost = true;
      window.Show();
    }

    private string _fileToParse;
    private string _lexiconFile;
    //private string _grammarFile;
    private bool _parsingFinished;

    private readonly PatrParserControl _parserControl;
    private readonly ParseAnalyser _analyser;
    private PatrParseNode _selectedNode;
    private PatrAnalysisData _analysisModel;
    private PcPatrDocument _pcPatrDocument;
    private PatrSentenceAnalysis _selectedAnalysisSentence;
    private ObservableCollection<PatrSentenceAnalysis> _analysisSentences;
    private readonly LingTreeTreeControlNotifierService _notifierService;
    private readonly MainWindowViewModel _mainWindow;
    private readonly ConfigReader _configReader;
    private readonly ParseOptionsViewModel _optionsViewModel;
    private readonly ParametersViewModel _parameters;
    private readonly XslCompiledTransform m_fsTransform;
    private readonly string _featurePathPage;
    private readonly ParseNodeComparerViewModel _parseComparer;

    public EventHandler RequestingFeatureDisplay;

    public static string PARSE_OUTPUT = Path.Combine(Path.GetTempPath(), "ParseLog.log");
  }


}