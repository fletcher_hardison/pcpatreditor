﻿/*
 * Created by SharpDevelop.
 * User: TFHardison
 * Date: 3/16/2013
 * Time: 11:04 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace PCPATR.Editor.MasterLists.PatrParser
{
  /// <summary>
  /// Description of PatrInputCheckerMethods.
  /// </summary>
  public static class PatrInputCheckerMethods
  {
    public static IEnumerable<string> GetListOfWordsInLexicon(string fileContents, string wordMarker)
    {
      var wordMarkerLength = wordMarker.Length;

      fileContents = ConvertToUnixStyleLineEndings(fileContents);
      
      var contentsByLine = fileContents.Split('\n');           
      var wordRecords = contentsByLine.Where(f => f.IndexOf(wordMarker) == 0).Select(f => f.Substring(f.IndexOf(wordMarker) + wordMarkerLength).Trim());
      
      return wordRecords.Distinct();
    }
    
    private static string ConvertToUnixStyleLineEndings(string fileContents)
    {
      return fileContents.Replace("\r", string.Empty); //replace \r so file can be treated like it has unix line ending style.
    }

    public static IEnumerable<string> GetListOfWordsInDataFile(string fileContents, char commentChar)
    {
      fileContents = ConvertToUnixStyleLineEndings(fileContents);

      var list = fileContents
        .Replace("\\t", string.Empty)
        .Split('\n')
        .Where(f => f.IndexOf(commentChar) != 0)
        .Where(f=> f.IndexOf("\\id") < 0) // was f => f.IndexOf("\\t") < 0 && f.IndexOf("\\id") < 0
        .Select(f => f.Split(' ')).Aggregate<IEnumerable<string>>((working, nextList) => working.Concat(nextList));

      list = list.Where(f => !string.IsNullOrEmpty(f));
      return list.Distinct();
    }
    
    public static IEnumerable<string> ComputeWordsNotInLexicon(IEnumerable<string> lexicon, IEnumerable<string> wordInDataFile)
    {
      return wordInDataFile.Where(f => (! lexicon.Contains(f)));
    }
                                                 
  }
}
