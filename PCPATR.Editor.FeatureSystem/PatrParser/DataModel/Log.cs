using System.Collections.Generic;

namespace PCPATR.Editor.MasterLists.PatrParser.DataModel
{
    public class Log 
    {
        public bool FailuresExit { get; set; }
        public List<PatrSentenceAnalysis> Sentences { get; set; }
    }
}
