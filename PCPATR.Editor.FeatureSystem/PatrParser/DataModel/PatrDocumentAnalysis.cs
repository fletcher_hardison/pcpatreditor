using System.Collections.Generic;
using System.Xml;

namespace PCPATR.Editor.MasterLists.PatrParser.DataModel
{
  /// <summary>
  ///  Opens the data file and output parse file and create a sentence class for each one.
  /// </summary>
  /// <remarks>This sentence file includes the sentence and all parses</remarks>
  public class PatrDocumentAnalysisViewModel : SIL.PcPatrBrowser.PcPatrDocument
  {
    public List<PatrSentenceAnalysisViewModel> PatrSentences { get; set; }

    public int SentenceCount { get { return PatrSentences.Count; } }

    public string PathToParseResults { get; set; }
    public string PathToInputData { get; set; }
    private string _grammar;

    public PatrDocumentAnalysisViewModel(string logFile, string grammarFile, ParseAnalyser analyser)
      : base(logFile, out grammarFile)
    {
      _grammar = grammarFile;

    }


    public PatrSentenceAnalysisViewModel CurrentSentenceAnalysis
    {
      get { return PatrSentences[base.CurrentSentenceNumber]; }
    }
  }


  public class PatrSentenceAnalysisViewModel : SIL.PcPatrBrowser.PcPatrSentence
  {
    public int AnalysesCount { get; set; }
    public string Sentence { get; set; }

    /// <summary>
    /// Informs the user if the sentence failed.
    /// </summary>
    /// <remarks>equals true if parse failed</remarks>
    public bool Failed { get; set; }
    public List<PatrParseAnalysisViewModel> AnalysisParses { get; set; }

    public PatrSentenceAnalysisViewModel(XmlNode node)
      : base(node)
    {
      
    }


    public PatrParseAnalysisViewModel CurrentParseAnalysis
    {
      get { return AnalysisParses[base.CurrentParseNumber]; }
    }





  }

  public class PatrParseAnalysisViewModel : SIL.PcPatrBrowser.PcPatrParse
  {
    /// <summary>
    /// list of nodes that failed
    /// </summary>
    public List<PatrParseNode> FailurePoints { get; set; }
    public Node StartNode { get; set; }


    public PatrParseAnalysisViewModel(XmlNode node)
      : base(node)
    {

    }

  }


}

