// /* ***************************************************************
// * File name: PatrAnalysisData.cs
// * Project: PCPATR.Services
// * Created by: Fletcher Hardison
// * Created on: 04,05,2012
// * In the name of Christ with thanks to God 
// */

using System;
using System.Collections.Generic;
using System.Linq;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.PatrParser.DataModel
{
  /// <summary>
  /// Data model for analysis of Patr parser output document
  /// </summary>
  public class PatrAnalysisData
  {
    /// <summary>
    /// Initializes a new instance of the PatrAnalysisData class.
    /// </summary>
    public PatrAnalysisData()
    {
      PatrSentences = new List<PatrSentenceAnalysis>();
    }

    /// <summary>
    /// List of sentence analyses.
    /// </summary>
    public List<PatrSentenceAnalysis> PatrSentences { get; set; }

    /// <summary>
    /// count of sentences analyzed.
    /// </summary>
    public int SentenceCount { get { return PatrSentences.Count; } }
  }

  /// <summary>
  /// Data model for analysis of a sentence within patr parser output.
  /// </summary>
  public class PatrSentenceAnalysis : ViewModelBase
  {
    /// <summary>
    /// Initializes a new instance of the PatrSentenceAnalysis class.
    /// </summary>
    public PatrSentenceAnalysis()
    {
      Comments = new List<string>();
      Parses = new List<PatrParseAnalysis>();
    }



    /// <summary>
    /// numbers of analyses for this sentence found by the parser.
    /// </summary>
    public long AnalysesCount { get; set; }

    /// <summary>
    /// The sentence data that was sent to the parses.
    /// </summary>
    public string Sentence { get; set; }

    /// <summary>
    /// Informs the user if the sentence failed.
    /// </summary>
    /// <remarks>equals true if parse failed</remarks>
    public bool Failed { get; set; }

    public string Status { get { return Failed ? "Failed" : "Passed"; } }

    /// <summary>
    /// List of comments that parser made about the sentence.
    /// </summary>
    public List<string> Comments { get; set; }

    /// <summary>
    /// List of Analysis objects.
    /// </summary>
    public List<PatrParseAnalysis> Parses { get; set; }
    
    
    public PatrParseAnalysis SelectedParse
    {
      get { return _selectedParse; }
      set
      {
        _selectedParse = value;
        RaisePropertyChanged("SelectedParse");
        RaisePropertyChanged("SelectedParseIndex");
        RaiseSelectedParseChanged();
      }
    }

    public int SelectedParseIndex
    {
      get { return Parses.IndexOf(_selectedParse); }
    }

    public PatrParseNode SelectedNode
    {
      get { return _selectedNode; }
      set
      {
        _selectedNode = value;
        RaisePropertyChanged("SelectedNode");
      }
    }

    public void RaiseSelectedParseChanged()
    {
      if (SelectedParseChanged == null) return;
      SelectedParseChanged(this, new EventArgs());
    }

    private PatrParseAnalysis _selectedParse;
    private PatrParseNode _selectedNode;
    public event EventHandler SelectedParseChanged;
  }

  public class PatrParseAnalysis : ViewModelBase
  {
    private PatrParseNode _selectedFailurePoint;
    private List<PatrParseNode> _nodes;
    public PatrParseAnalysis()
    {
      // FailurePoints = new List<PatrParseNode>();
      _nodes = new List<PatrParseNode>();
    }

    public List<PatrParseNode> FailurePoints { get { return PatrParseAnalysisMethods.GetFailurePoints(Nodes); } }

    public Node StartNode { get; set; }

    public int FailureCount { get { return FailurePoints.Count; } }

    public PatrParseNode SelectedFailurePoint
    {
      get { return _selectedFailurePoint; }
      set
      {
        _selectedFailurePoint = value;
        RaisePropertyChanged("SelectedFailurePoint");
      }
    }

    public List<PatrParseNode> Nodes
    {
      get { return _nodes; }
      set
      {
        _nodes = value;
        RaisePropertyChanged("Nodes");
      }
    }



  }

  /// <summary>
  /// Holds data on nodes that caused parses to fail.
  /// </summary>
  public class PatrParseNode
  {
    /// <summary>
    /// Initializes a new instance of teh PatrPArseFailurePoint class.
    /// </summary>
    public PatrParseNode()
    {
      FailedFeatures = new List<PatrParseFeature>();
    }

    /// <summary>
    /// The Identifier attribute for the node with the failed feature 
    /// </summary>
    public string Id { get; set; }

    /// <summary>
    /// The name of the rule that built the node that contained the failure.
    /// </summary>
    public string RuleName { get; set; }

    /// <summary>
    /// category of the node containing the failure.
    /// </summary>
    public string Cat { get; set; }

    /// <summary>
    /// The value of the All attribute on the node that contained the failure
    /// </summary>
    public string All { get; set; }

    public bool Failed { get { return FailedFeatures.Count > 0; } }

    public bool IsLeafNode
    {
      get { return LexicalFeatureStructure != null; }
    }

    /// <summary>
    /// List of the names of the features that failed
    /// </summary>
    public PatrParseFeature FeatureStructure { get; set; }

    public List<PatrParseFeature> FailedFeatures { get; set; }

    public PatrParseFeature LexicalFeatureStructure { get; set; }
  }

  public class PatrParseFeature
  {
    public PatrParseFeature(string name, string id, string content, List<PatrParseFeature> childFeatures)
      : this()
    {
      Name = name;
      Id = id;
      Content = content;
      ChildFeatures = childFeatures;
    }

    public PatrParseFeature()
    {
      ChildFeatures = new List<PatrParseFeature>();
    }

    public string Name { get; set; }

    public string Id { get; set; }

    public string NameOrId { get { return string.IsNullOrEmpty(Name) ? Id : Name; } }

    public string Content { get; set; }

    public bool Failed { get { return string.IsNullOrEmpty(Content) ? false : Content.Contains("FAIL"); } }

    public string FormattedRepresentation
    {
      get
      {
        var result = PatrParseAnalysisMethods.FormatPatrParseFeature(this, 0);

        if (result.IndexOf(":") == 0)
        {
          result = result.Substring(1);
        }

        return result;
      }
    }

    public List<PatrParseFeature> ChildFeatures { get; set; }



    public bool HasChildFeatures { get { return ChildFeatures.Count > 0; } }
  }


  public static class PatrParseAnalysisMethods
  {
    public static string FormatPatrParseFeature(PatrParseFeature feature, int embeddingLevel)
    {

      if (!feature.HasChildFeatures)
      {

        return FormatFeature(feature, embeddingLevel, false) + Environment.NewLine;
      }

      var firstFeature = FormatFeature(feature, embeddingLevel, false) + "[" + Environment.NewLine;
      var childStrings = feature.ChildFeatures.Select(f => FormatPatrParseFeature(f, embeddingLevel + 1));
      var lastLine = new string('\t', embeddingLevel + 1) + "]" + Environment.NewLine;

      var result = (firstFeature + string.Join("", childStrings) + lastLine).Trim();



      return result;
    }

    private static string FormatFeature(PatrParseFeature feature, int embeddingLevel, bool isFirstChild)
    {
      if (isFirstChild)
      {

        return feature.NameOrId + " : " + feature.Content;
      }

      return new string('\t', embeddingLevel) + feature.NameOrId + " : " + feature.Content;
    }

    public static List<PatrParseNode> GetFailurePoints(IEnumerable<PatrParseNode> nodes)
    {
      return nodes.Where(f => f.Failed).ToList();
    }

    public static string WrapInTag(string tagName, string className, string value)
    {
      if (string.IsNullOrEmpty(className))
        return "<" + tagName + ">" + value + "</" + tagName + ">";

      return "<" + tagName + " class=\"" + className + "\">" + value + "</" + tagName + ">";
    }

    public static string GetFailedPatrParseFeatureHtmlRepresentation(PatrParseFeature feature)
    {
      return "<span class=\"failedFeature\" id=\"" + feature.Id + "\">" + feature.Id + ": " + feature.Content + "</span>";
    }
  }

}