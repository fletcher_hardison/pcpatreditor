using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
//using System.Windows.Forms;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using LingTree;
using SIL.PcPatrBrowser;


namespace PCPATR.Editor.MasterLists.PatrParser.ParserComponents
{
  public partial class LingTreeTreeWrapperControl : System.Windows.Forms.UserControl
  {
    /// <summary>
    /// initializes a new instance of the LingTreeTreeWrapperControl class.
    /// </summary>
    public LingTreeTreeWrapperControl()
    {
      InitializeComponent();
      _assembly = Assembly.GetExecutingAssembly();



      m_treeTransform = new XslCompiledTransform();
      //m_treeTransform.Load(Path.Combine(Application.StartupPath, @"PcPatrToLingTree.xsl"));
      var reader = XmlReader.Create(new StringReader(Properties.Resources.PcPatrToLingTree));

      m_treeTransform.Load(reader);

      InitLanguageInfo();
     
      m_tree = new LingTreeTree();
      m_tree.Parent = pnlTree;
      m_tree.m_LingTreeNodeClickedEvent += new LingTreeNodeClickedEventHandler(MTreeMLingTreeNodeClickedEvent);
      //this.Controls.Add(m_tree);
      pnlTree.Controls.Add(m_tree);
      pnlTree.Paint += pnlTree_Paint;
      
      _notifierService = LingTreeTreeControlNotifierService.GetInstance();
      _notifierService.RegisterEndPoint(this);
    }

    void pnlTree_Paint(object sender, PaintEventArgs e)
    {
      m_tree.OnPaint(sender,e);
    }

    void MTreeMLingTreeNodeClickedEvent(object sender, LingTreeNodeClickedEventArgs e)
    {
      var node = e.Node;
      _notifierService.SendIdToStartPoint(node.Id);
    }


    private Assembly _assembly;
    private XslCompiledTransform m_treeTransform;
    public LingTreeTree m_tree;
    private LanguageInfo m_language;
    private LingTreeTreeControlNotifierService _notifierService;
    /// <summary>
    /// Shows parse tree for given input sentence and parse
    /// </summary>
    /// <param name="sentence">Sentence to display</param>
    /// <param name="parse">Parse to display</param>
    public void ShowParseTree(PcPatrSentence sentence, PcPatrParse parse)
    {
      if (sentence.Parses.Length > 0 && parse != null) {
        XmlNode node = parse.Node;

        if (node != null) {
          XmlDocument treeDoc = MakeTreeDocFromNode(node);
          XmlNode xmlTree = treeDoc.SelectSingleNode("//TreeDescription/node");
          ShowTree(treeDoc, xmlTree, sentence);
        }
      } else {
        m_tree.Root = null;
        m_tree.Invalidate();
      }
    }

    /// <summary>
    ///  Initializes language info. hijacked for H. Andrew Black.
    /// </summary>
    private void InitLanguageInfo()
    {
      MyFontInfo nt = new MyFontInfo(new Font("Times New Roman", 14.0f), Color.Black);
      MyFontInfo lex = new MyFontInfo(new Font("Courier New", 12.0f), Color.Blue);
      MyFontInfo gloss = new MyFontInfo(new Font("Times New Roman", 12.0f), Color.Green);
      m_language = new LanguageInfo("Default Language", nt, lex, gloss, false, "-");

    }

    /// <summary>
    /// Displays a parse tree. Hijacked from H. Andrew Black
    /// </summary>
    /// <param name="treeDoc"></param>
    /// <param name="xmlTree"></param>
    /// <param name="sentence"></param>
    private void ShowTree(XmlDocument treeDoc, XmlNode xmlTree, PcPatrSentence sentence)
    {
      m_tree.Visible = true;
      m_tree.Enabled = true;
      //modifeed to default true
      m_tree.UseRightToLeft = false;
      m_tree.SetTreeParameters(treeDoc);
      m_tree.ParseXmlTreeDescription(xmlTree);
      m_tree.MessageText = "Parse " + sentence.CurrentParseNumber.ToString() + " of " +
                           sentence.NumberOfParses.ToString();
      //Graphics grfx = Graphics.FromHwnd(new System.Windows.Interop.WindowInteropHelper(this.Parent as Window).Handle);
      Graphics grfx = CreateGraphics();
      m_tree.CalculateCoordinates(grfx);
      m_tree.Draw(grfx, Color.Black);

      m_tree.Invalidate();
      pnlTree.Invalidate();

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    /// <remarks>from H. Andrew Black</remarks>
    private XmlDocument MakeTreeDocFromNode(XmlNode node)
    {
      XPathDocument xpath = MakeXPathDocFromNode(node);

      string sOutput = Path.GetTempFileName();
      StreamWriter result = new StreamWriter(sOutput);
      XsltArgumentList argList = new XsltArgumentList();
      argList.AddParam("sGlossFont", "", m_language.GlossInfo.Font.Name);
      argList.AddParam("sGlossFontSize", "", m_language.GlossInfo.Font.Size.ToString());
      argList.AddParam("sGlossFontColor", "", m_language.GlossInfo.Color.Name);
      argList.AddParam("sNTFont", "", m_language.NTInfo.Font.Name);
      argList.AddParam("sNTFontSize", "", m_language.NTInfo.Font.Size.ToString());
      argList.AddParam("sNTFontColor", "", m_language.NTInfo.Color.Name);
      argList.AddParam("sLexFont", "", m_language.LexInfo.Font.Name);
      argList.AddParam("sLexFontSize", "", m_language.LexInfo.Font.Size.ToString());
      argList.AddParam("sLexFontColor", "", m_language.LexInfo.Color.Name);
      m_treeTransform.Transform(xpath, argList, result);
      result.Close();

      XmlDocument treeDoc = new XmlDocument();
      treeDoc.Load(sOutput);
      File.Delete(sOutput);
      return treeDoc;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    /// <remarks>From H. Andrew Black</remarks>
    private XPathDocument MakeXPathDocFromNode(XmlNode node)
    {
      string sInput = Path.GetTempFileName();
      StreamWriter input = new StreamWriter(sInput);
      input.Write(node.OuterXml);
      input.Close();

      XPathDocument xpath = new XPathDocument(sInput);
      File.Delete(sInput);
      return xpath;
    }
  }


}
