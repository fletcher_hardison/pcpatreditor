using System.Windows.Forms;

namespace PCPATR.Editor.MasterLists.PatrParser.ParserComponents
{
  partial class LingTreeTreeWrapperControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlTree = new System.Windows.Forms.Panel();
      this.SuspendLayout();
      // 
      // pnlTree
      // 
      this.pnlTree.Location = new System.Drawing.Point(0, 0);
      this.pnlTree.Name = "pnlTree";
      //this.pnlTree.Size = new System.Drawing.Size(349, 333);
      //this.pnlTree.TabIndex = 0;
      this.pnlTree.Parent = this;
      this.pnlTree.Dock = DockStyle.Fill;
      this.pnlTree.AutoScroll = true;
      
      // 
      // LingTreeTreeWrapperControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnlTree);
      this.Name = "LingTreeTreeWrapperControl";
      this.Size = new System.Drawing.Size(353, 336);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlTree;
  }
}
