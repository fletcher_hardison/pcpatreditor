﻿/*
 * Created by SharpDevelop.
 * User: TFHardison
 * Date: 3/16/2013
 * Time: 11:27 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace PCPATR.Editor.MasterLists.PatrParser
{
  /// <summary>
  /// Interaction logic for NotInLexiconFileDialog.xaml
  /// </summary>
  public partial class NotInLexiconFileDialog : Window
  {
    public NotInLexiconFileDialog()
    {
      InitializeComponent();
    }

    void Button_Click(object sender, RoutedEventArgs e)
    {
      this.DialogResult = true;
      this.Close();
    }

    void Button_Click1(object sender, RoutedEventArgs e)
    {
      this.DialogResult = false;
      this.Close();
    }


    private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
    {
      if (SaveToFile != null)
      {
        SaveToFile(this, new EventArgs());
      } else
      {
        MessageBox.Show("Could not save list.", "Error!");
      }
    }

    public event EventHandler SaveToFile;
  }
}