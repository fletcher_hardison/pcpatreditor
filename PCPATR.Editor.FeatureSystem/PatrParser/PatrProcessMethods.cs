﻿//  ***************************************************************
//  File name: PatrProcessMethods.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 15,03,2013
// ***************************************************************

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
namespace PCPATR.Editor.MasterLists.PatrParser
{
  public static class PatrProcessMethods
  {

    public static string TAK_FILE = Path.Combine(Path.GetTempPath(), "parserCommands.TAK");

    /// <summary>
    ///  Builds TAK file for running pcpatr32
    /// </summary>
    public static string GenerateTakFile(ParseOptionsViewModel options, ParserControlViewModel control, string logFile, string fileToParse)
    {

      var sb = new StringBuilder();
      sb.AppendLine("set tree xml");

      sb.AppendLine("set ambiguities " + options.MaxAmbiguities);
      sb.AppendLine("set comment " + options.CommentCharacter);
      sb.AppendLine("set check-cycles " + ConvertBoolToOnOff(options.CheckCycles));
      sb.AppendLine("set final-punctuation " + options.SentenceFinalPunctuation);
      sb.AppendLine("set failures " + ConvertBoolToOnOff(options.FailuresPerSentence > 0));
      sb.AppendLine("set features full");
      sb.AppendLine("set gloss " + ConvertBoolToOnOff(options.ShowGloss));
      if (options.UseTimeLimitWhenParsing)
      {
        sb.AppendLine("set limit " + options.ParsingTimeLimitInSeconds);
      }
      sb.AppendLine("set promote-defaults " + ConvertBoolToOnOff(options.PromoteAtomicLexicalFeaturesToOrdinaryStatus));
      if (options.TrimEmptyFeatures)
      {
        sb.AppendLine("set trim-empty-feature " + ConvertBoolToOnOff(options.TrimEmptyFeatures));
      }
      sb.AppendLine("set unification " + ConvertBoolToOnOff(options.UnificationIsOn));
      sb.AppendLine("set top-down-filter " + ConvertBoolToOnOff(options.UseTopDownFiltering));
      
      sb.AppendLine("set marker category " + options.LexiconCategoryMarker);
      sb.AppendLine("set marker gloss " + options.LexiconGlossMarker);
      sb.AppendLine("set marker rootgloss " + options.LexiconRootGlossMarker);
      sb.AppendLine("set marker features " + options.LexiconFeaturesMarker);
      sb.AppendLine("set marker word " + options.LexiconWordMarker);
      sb.AppendLine("set marker record " + options.LexiconRecordMarker);

      sb.AppendLine("log " + logFile);
      sb.AppendLine("load grammar " + control.GrammarFile);
      sb.AppendLine("load lexicon " + control.LexiconFile);

      sb.AppendLine("file parse " + fileToParse);
      sb.AppendLine("close");   
      System.IO.File.WriteAllText(TAK_FILE, sb.ToString());

      return TAK_FILE;
    }

    private static string ConvertBoolToOnOff(bool value)
    {
      return (value ? "on" : "off");
    }

    /// <summary>
    /// Runs pcpatr32 quietly executing the specified tak file, clearning the parser, and returning output.
    /// </summary>
    /// <param name="takFile"></param>
    /// <returns></returns>
    public static string ExecuteParseOnProcess(string takFile, string stdOutFile)
    {
      File.Delete(ParserControlViewModel.StdOutFile);

      var p = new Process();
      p.StartInfo.UseShellExecute = false;
      p.StartInfo.CreateNoWindow = false;
      p.StartInfo.ErrorDialog = false;

      //p.StartInfo.RedirectStandardOutput = true;
      p.StartInfo.RedirectStandardInput = true;
      p.StartInfo.RedirectStandardError = true;
      p.EnableRaisingEvents = true;
      p.Exited += p_Exited;

      p.StartInfo.FileName = "Assets\\pcpatr32.exe";
      p.Start();

     // var ouputReader = TextReader.Synchronized(p.StandardOutput);
      var errorReader = TextReader.Synchronized(p.StandardError);
      var errorBuilder = new StringBuilder();
      var outputBuilder = new StringBuilder();
      //var outputWorker = new BackgroundWorker();
      //outputWorker.WorkerReportsProgress = true;
      //outputWorker.WorkerSupportsCancellation = true;
      //outputWorker.DoWork += (sender, eventArgs) =>
      //{
      //  while (!outputWorker.CancellationPending)
      //  {
      //    var count = 0;
      //    var outputBuffer = new char[1024];
      //    do
      //    {
      //      count = ouputReader.Read(outputBuffer, 0, 1024);
      //      outputBuilder.Append(outputBuffer, 0, count);
      //    } while (count > 0);
      //  }
      //};

      var errorWorker = new BackgroundWorker();
      errorWorker.WorkerReportsProgress = true;
      errorWorker.WorkerSupportsCancellation = true;
      errorWorker.DoWork += (sender, eventArgs) =>
      {
        while (!errorWorker.CancellationPending)
        {
          var count = 0;
          var errorBuffer = new char[1024];
          do
          {
            count = errorReader.Read(errorBuffer, 0, 1024);
            errorBuilder.Append(errorBuffer, 0, count);
          } while (count > 0);
        }
      };

      //outputWorker.RunWorkerAsync();
      errorWorker.RunWorkerAsync();

      //p.BeginOutputReadLine();
      //p.BeginErrorReadLine();
      p.StandardInput.WriteLine("take " + takFile);

      p.StandardInput.Flush();
      p.StandardInput.WriteLine("clear");

      p.StandardInput.WriteLine("quit");

      p.StandardInput.Flush();
      p.WaitForExit(180000);
     

      var results = outputBuilder.ToString() + Environment.NewLine + errorBuilder.ToString();
      File.WriteAllText(stdOutFile, results);

      return results;
    }


    static void p_Exited(object sender, EventArgs e)
    {
      Console.WriteLine("parser exited");
    }




  }
}