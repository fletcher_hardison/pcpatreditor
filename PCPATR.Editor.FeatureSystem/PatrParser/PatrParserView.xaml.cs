﻿using System;
namespace PCPATR.Editor.MasterLists.PatrParser
{
  /// <summary>
  /// Interaction logic for PatrParserView.xaml
  /// </summary>
  public partial class PatrParserView 
  {
    public PatrParserView(ParserControlViewModel viewModel)
    {
      InitializeComponent();
      DataContext = viewModel;
      FeatureBrowser.Navigate(viewModel.InitFeature);
      viewModel.RequestingFeatureDisplay += (sender, args) => FeatureBrowser.Refresh();
    }

    
  }
}
