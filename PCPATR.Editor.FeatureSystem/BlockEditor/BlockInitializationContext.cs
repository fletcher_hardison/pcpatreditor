﻿using System;
using System.Collections.Generic;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  public class BlockInitializationContext
  {
    private readonly Dictionary<Type, object> _context = new Dictionary<Type, object>();
    public Dictionary<Type, object> ContextDictionary { get { return _context; } }

    public bool AddContextElement<T>(object element)
    {
      if (!_context.ContainsKey(typeof (T)))
      {
        _context.Add(typeof(T),element);
      
        return true;
      }

      return false;
    }    

    public T GetElement<T>()
    {
      if (_context.ContainsKey(typeof (T)))
      {
        return (T)_context[typeof (T)];
      }

      return default(T);
    }

    public void RaiseSelectedBlockChanged(LinkedListNode<Block> selectedBlock)
    {
     if (SelectedBlockChanged != null)
     {
       SelectedBlockChanged(selectedBlock);
     }
    }

    public event SelectedBlockChangedHandler SelectedBlockChanged;
  }


  public delegate void SelectedBlockChangedHandler(LinkedListNode<Block> selectedBlock);
}