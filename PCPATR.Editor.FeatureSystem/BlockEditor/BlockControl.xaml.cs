using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  /// <summary>
  /// Interaction logic for BlockControl.xaml
  /// </summary>
  public partial class BlockControl : UserControl
  {
    public BlockControl()
    {
      InitializeComponent();
    }

    //TODO figure out why i seem to be having trouble with middle container menu display
    private void UIElement_OnMouseDown(object sender, MouseButtonEventArgs e)
    {
      var block = DataContext as Block;
      if (block == null) return;

      block.SelectBlock();

    }    

    private void BlockContentPresenter_OnMouseDown(object sender, MouseButtonEventArgs e)
    {
      var block = DataContext as Block;
      if (block == null) return;
    //  if (block.IsBlockSelected)
    //  {
        block.SelectBlock();
        block.TryExecuteEditAction();
    //  }
    }

    private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
    {
      var block = DataContext as Block;
      if (block == null) return;
      if (block.IsBlockSelected)
      {
        block.TryExecuteEditAction();
      }
    }
  }
}
