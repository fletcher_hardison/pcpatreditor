﻿//  ***************************************************************
//  File name: BlockMapper.cs
//  Project: PCPATR.Editor.MasterLists
//  Created by: 
//  Created on: 05,03,2013
// ***************************************************************

using System;
using System.Collections.ObjectModel;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.Constraints.ViewModels;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  public class BlockMapper
  {
    private readonly ObjectToBlockMapper _objectToBlockMapper;
    private readonly BlockToObjectMapper _blockToObjectMapper;
    private readonly IUnityContainer _container;
    public BlockMapper(IUnityContainer container)
    {
      _container = container;
      _blockToObjectMapper = _container.Resolve<BlockToObjectMapper>();
      _objectToBlockMapper = _container.Resolve<ObjectToBlockMapper>();
    }

    public Block MapToBlock(object model, Block parent, BlockInitializationContext initContext)
    {
      return _objectToBlockMapper.MapObjectToBlock(model, parent, initContext);
    }

    public object MapBlockToModelObject(Block block)
    {
      return _blockToObjectMapper.MapBlockToObject(block);
    }

  }
}