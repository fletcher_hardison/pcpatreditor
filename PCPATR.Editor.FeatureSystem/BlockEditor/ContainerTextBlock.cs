﻿/*
 * Created by SharpDevelop.
 * User: TFHardison
 * Date: 4/3/2013
 * Time: 11:55 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.ObjectModel;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  /// <summary>
  /// Description of ContainerTextBlock.
  /// </summary>
  public class ContainerTextBlock : Block
  {
    public ContainerTextBlock()
    {
    }
    
    private ObservableCollection<string> _elements;
    public ObservableCollection<string> Elements
    {
      get {return _elements; }
      set { _elements = value;
        RaisePropertyChanged("Elements");
      }
    }
    
    private string _selectedElement;
    public string SelectedElement
    {
      get { return _selectedElement; }
      set { _selectedElement = value;
        RaisePropertyChanged("SelectedElement");
      }
    }
    
    public void AddElement(string element)
    {
      
    }
    
    public string GrammarRule { get; set; }
    
    
    
  }
}
