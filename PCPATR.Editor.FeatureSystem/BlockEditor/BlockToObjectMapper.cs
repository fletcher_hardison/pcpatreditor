﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using PCPATR.Common;
using PCPATR.DataAccess.Parser;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Models.MasterLists;
using PCPATR.Models.Rules;
using PCPATR.Models.Rules.Constraints;
using PCPATR.Models.Rules.PSRElements;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  public class BlockToObjectMapper
  {
    private IUnityContainer _container;
    private BlockGrammar _grammar;

    public BlockToObjectMapper(IUnityContainer container)
    {
      _container = container;
      _grammar = BlockGrammar.GetInstance();
    }

    public object MapBlockToObject(Block block)
    {
      switch (block.TypeName)
      {
        case BlockGrammarLoadMethods.Names.UNIFICATION_CONSTRAINT:
          return MapBlockToConstraint(block, _container.Resolve<UnificationConstraintModel>());
        case BlockGrammarLoadMethods.Names.PRIORITY_UNION_OPERATION:
          return MapBlockToConstraint(block, _container.Resolve<PriorityUnionModel>());
        case BlockGrammarLoadMethods.Names.PERCOLATION_OPERATION:
          return MapBlockToConstraint(block, _container.Resolve<PercolationOperationModel>());
        case BlockGrammarLoadMethods.Names.LOGICAL_CONSTRAINT:
          return MapBlockToConstraint(block, _container.Resolve<LogicalConstraintModel>());
        case BlockGrammarLoadMethods.Names.CONSTRAINT_DISJUNCTION:
          return MapBlockToConstraintDisjunction(block);
        case BlockGrammarLoadMethods.Names.CONSTRAINTS:
          return MapBlockToConstraints(block);
        case BlockGrammarLoadMethods.Names.COMMENT:
          return block.Content;
        case BlockGrammarLoadMethods.Names.CAT_VALUE:
          return MapCatValueBlockToObject(block);
        case BlockGrammarLoadMethods.Names.LEX_VALUE:
          return MapLexGlossRootGlossValueBlockToObject(block, _container.Resolve<LexValue>());
        case BlockGrammarLoadMethods.Names.GLOSS_VALUE:
          return MapLexGlossRootGlossValueBlockToObject(block, _container.Resolve<GlossValue>());
        case BlockGrammarLoadMethods.Names.ROOT_GLOSS_VALUE:
          return MapLexGlossRootGlossValueBlockToObject(block, _container.Resolve<RootGlossValue>());
        case BlockGrammarLoadMethods.Names.NONE_VALUE:
          return MapNoneValueBlockToObject();
        case BlockGrammarLoadMethods.Names.CAT_VALUE_PATH:
          return MapCatValuePathToObject(block);
        case BlockGrammarLoadMethods.Names.LEX_VALUE_PATH:
          return MapLexGlossrotGlossValuePathToObject(block, _container.Resolve<LexValuePath>());
        case BlockGrammarLoadMethods.Names.GLOSS_VALUE_PATH:
          return MapLexGlossrotGlossValuePathToObject(block, _container.Resolve<GlossValuePath>());
        case BlockGrammarLoadMethods.Names.ROOT_GLOSS_VALUE_PATH:
          return MapLexGlossrotGlossValuePathToObject(block, _container.Resolve<RootGlossValuePath>());
        case BlockGrammarLoadMethods.Names.NONE_VALUE_PATH:
          return MapNoneValuePathToObject(block);
        case BlockGrammarLoadMethods.Names.NONE_REF:
          return MapNoneRefToObject();
        case BlockGrammarLoadMethods.Names.FEATURE_PATH:
          return MapFeaturePathToObject(block);
        case BlockGrammarLoadMethods.Names.EMBEDDED_FEATURE_PATH:
          return MapEmbeddedFeaturePathToObject(block);
        case BlockGrammarLoadMethods.Names.SUBCAT_PATH:
          return MapSubcatPathToObject(block);
        case BlockGrammarLoadMethods.Names.CATEGORY_VALUE_REF:
          return MapCategoryValueRefToObject(block);
        case BlockGrammarLoadMethods.Names.FEATURE_VALUE_REF:
          return MapFeatureValueRefToObject(block);
        case BlockGrammarLoadMethods.Names.FEATURE_VALUE_DISJUNCTION:
          return MapFeatureValueDisjunctionToObject(block);
        case BlockGrammarLoadMethods.Names.FEATURE_TEMPLATE_REF:
          return MapFeatureTemplateRefToObject(block);
        case BlockGrammarLoadMethods.Names.LOGICAL_CONSTRAINT_REF:
          return MapLogicalConstraintRefToObject(block);
        case BlockGrammarLoadMethods.Names.FEATURE:
          return MapFeatureToObject(block);
        case BlockGrammarLoadMethods.Names.COLLECTION_FEATURE_REF:
          return MapCollectionFeatureRefToObject(block);
        case BlockGrammarLoadMethods.Names.COMPLEX_FEATURE_REF:
          return MapComplexFeatureRefToObject(block);
        case BlockGrammarLoadMethods.Names.TEMPLATE_FEATURE_DISJUNCTION:
          return MapTemplateFeatureDisjunctionToObject(block);
        case BlockGrammarLoadMethods.Names.FEATURE_REF:
          return MapFeatureRefToObject(block);
        case BlockGrammarLoadMethods.Names.UNARY_EXPRESSION:
          return MapUnaryOperationToObject(block);
        case BlockGrammarLoadMethods.Names.BINARY_EXPRESSION:
          return MapBinaryOperationToObject(block);
        case BlockGrammarLoadMethods.Names.LOGICAL_EXPRESSION:
          return MapLogicalExpressionToObject(block);
        case BlockGrammarLoadMethods.Names.SUBCAT_REF:
          return MapSubcatRefToObject(block);
        case BlockGrammarLoadMethods.Names.SUBCAT_FIRST:
          
        case BlockGrammarLoadMethods.Names.SUBCAT_REST:
        case BlockGrammarLoadMethods.Names.SUBCAT_FIRST_REST_PAIR:
          return MapSubcatFirstRestPairToObject(block);
        case BlockGrammarLoadMethods.Names.SUBCAT_DISJUNCTION:
          return MapSubcatDisjunctionToObject(block);
        case BlockGrammarLoadMethods.Names.FEATURE_TEMPLATE:
          return MapFeatureTemplateToObject(block);
        case BlockGrammarLoadMethods.Names.FEATURE_TEMPLATE_NAME_ID:
          return MapFeatureTemplateNameIdToObject(block);
        case BlockGrammarLoadMethods.Names.TEMPLATE_FEATURE_PATH:
          return MapTemplateFeaturePathToObject(block);
        case BlockGrammarLoadMethods.Names.TEMPLATE_FEATURE_STRUCTURE:
          return MapTemplateFeatureStructureToObject(block);
        case BlockGrammarLoadMethods.Names.TEMPLATE_FEATURE_PATH_DISJUNCTION:
          return MapTemplateFeaturePathDisjunctionToObject(block);
        case BlockGrammarLoadMethods.Names.CONSTRAINT_TEMPLATE:
          return MapConstraintTemplateToObject(block);
        case BlockGrammarLoadMethods.Names.LEXICAL_RULE:
          return MapLexicalRuleToObject(block);
        case BlockGrammarLoadMethods.Names.PHRASE_STRUCTURE_RULE:
          return MapPhraseStructureRuleToObject(block);
        case BlockGrammarLoadMethods.Names.SYMBOL_REF:
          return MapSymbolRefToObject(block);
        case BlockGrammarLoadMethods.Names.OPTIONS_SYMBOLS:
          return MapOptionalSymbolsToObject(block);
        case BlockGrammarLoadMethods.Names.DISJUNCTION:
          return MapDisjunctionToObject(block);
        case BlockGrammarLoadMethods.Names.DISJUNCTIVE_SYMBOLS:
          return MapDisjunctiveSymbolsToObject(block);     
        default:
          throw new InvalidOperationException("Program cannot map block back to data objec:" + block.TypeName);
      }
    }

    private object MapTemplateFeatureStructureToObject(Block block)
    {
      var con = block as ContainerBlock;
      if (con == null) return null;

      var structure = _container.Resolve<FeatureTemplateFeatureStructure>();

      var elem = con.Children.First;

      while (elem != null)
      {
        structure.Elements.Add(MapBlockToObject(elem.Value));
        elem = elem.Next;
      }

      return structure;

    }

    private object MapDisjunctionToObject(Block block)
    {
      var con = block as ContainerBlock;
      if (con == null) return null;

      var disjunction = _container.Resolve<Disjunction>();

      var next = con.Children.First;
      
      while (next != null)
      {
        switch (next.Value.TypeName)
        {
          case BlockGrammarLoadMethods.Names.DISJUNCTIVE_SYMBOLS:
            disjunction.DisjunctiveSymbols = MapDisjunctiveSymbolsToObject(next.Value) as DisjunctiveSymbols;
            break;
          case BlockGrammarLoadMethods.Names.OPTIONS_SYMBOLS:
            disjunction.LeftElements.Add(MapOptionalSymbolsToObject(next.Value) as PsrElement);
            break;
          default:
            disjunction.LeftElements.Add(MapSymbolRefToObject(next.Value) as PsrElement);
            break;
        }

        next = next.Next;
      }

      return disjunction;
    }

    private object MapDisjunctiveSymbolsToObject(Block value)
    {
      var con = value as ContainerBlock;
      if (con == null) return null;

      var ds = _container.Resolve<DisjunctiveSymbols>();

      var next = con.Children.First;
      
      while (next != null)
      {
        switch (next.Value.TypeName)
        {
          case BlockGrammarLoadMethods.Names.DISJUNCTIVE_SYMBOLS:
            ds.EmbeddedDisjuctiveSymbols = MapDisjunctiveSymbolsToObject(next.Value) as DisjunctiveSymbols;            
            break;
          case BlockGrammarLoadMethods.Names.OPTIONS_SYMBOLS:
            ds.LeftElements.Add(MapOptionalSymbolsToObject(next.Value) as PsrElement);
            break;
          default:
            ds.LeftElements.Add(MapSymbolRefToObject(next.Value) as PsrElement);
            break;
        }

        next = next.Next;
      }

      return ds;
    }

    private object MapOptionalSymbolsToObject(Block block)
    {
      var con = block as ContainerBlock;
      if (con == null) return null;
      var optionals = _container.Resolve<OptionalSymbols>();

      var next = con.Children.First;
      while (next != null)
      {
        optionals.Elements.Add(MapBlockToObject(next.Value) as PsrElement);
        next = next.Next;
      }

      return optionals;
    }

    private object MapPhraseStructureRuleToObject(Block block)
    {
      var con = block as ContainerBlock;
      if (con == null) return null;
      var psr = _container.Resolve<PhraseStructureRule>();
      var lhs = con.Children.First;
      if (lhs != null)
      {
        psr.LeftHandSide = MapSymbolRefToObject(lhs.Value) as SymbolRef;

        var rhs = lhs.Next;

        if (rhs != null)
        {
          var rhsBlock = rhs.Value as ContainerBlock;

          if (rhsBlock != null)
          {
            var child = rhsBlock.Children.First;
            while (child != null)
            {
              psr.RightHandSide.Add(MapBlockToObject(child.Value) as PsrElement);
              child = child.Next;
            }
          }
        }
      }

      return psr;
    }

    private object MapSymbolRefToObject(Block value)
    {
     // var con = value as ContainerBlock;
      if (value == null) return null;
      var symbolRef = _container.Resolve<SymbolRef>();
      //var symbolValueNode = con.Children.First;
//      if (symbolValueNode != null)
//      {
        //symbolRef.Symbol.IdRef = symbolValueNode.Value.Content as string;
        symbolRef.Symbol.IdRef = value.Content as string;
        var id = value.Attributes["Id"] as BlockAttributeId;
        if (id != null)
        {
          symbolRef.Id = id.MyValue;
        }
     // }

      return symbolRef;
    }

    private object MapLexicalRuleToObject(Block block)
    {
      var con = block as ContainerBlock;
      if (con == null) return null;
      var rule = _container.Resolve<LexicalRule>();
      var nameNode = con.Children.First;
      if (nameNode != null)
      {
        rule.Name = nameNode.Value.Content as string;
        
        var next = nameNode.Next;
        while(next != null)
        {
          rule.LexicalRuleDefinitions.Add(MapLexicalRuleDefinitionToObject(next.Value));
          next = next.Next;
        }
      }

      return rule;
    }
    
    private static void RecurseAndExecute<T>(LinkedListNode<T> node, Action<T> action)
    {
      var next = node.Next;
      while (next != null)
      {
        action(next.Value);
        next = next.Next;
      }
    }

    private LexicalRuleDefinition MapLexicalRuleDefinitionToObject(Block value)
    {
      var con = value as ContainerBlock;
      if (con == null) return null;
      var lexDef = _container.Resolve<LexicalRuleDefinition>();
      var leftPath = con.Children.First;

      if (leftPath != null)
      {
        lexDef.Left = MapLexicalRuleFeaturePathToObject(leftPath.Value);

        var rightPath = leftPath.Next;

        if (rightPath != null)
        {
          if (rightPath.Value.TypeName == BlockGrammarLoadMethods.Names.LEXICAL_RULE_FEATURE_PATH)
          {
            lexDef.RightElement = MapLexicalRuleFeaturePathToObject(rightPath.Value);
          } else
          {
            lexDef.RightElement = MapFeatureValueRefToObject(rightPath.Value) as FeatureValueRefModel;
          }
        }        
      }

      return lexDef;
    }

    private LexicalRuleFeaturePath MapLexicalRuleFeaturePathToObject(Block block)
    {
      var con = block as ContainerBlock;
      if (con == null) return null;

      var path = _container.Resolve<LexicalRuleFeaturePath>();

      var startNode = con.Children.First;
      if (startNode != null)
      {
        path.Start = startNode.Value.Content as string;

        var endNode = startNode.Next;
        if (endNode != null)
        {
          path.End = endNode.Value.Content as string;
        }
      }

      return path;
    }

    private object MapConstraintTemplateToObject(Block block)
    {
      var con = block as ContainerBlock;
      if (con == null) return null;
      var template = _container.Resolve<ConstraintTemplateModel>();
      var nameNode = con.Children.First;
      template.Name = nameNode.Value.Content as string;

      var idNode = nameNode.Next;
      if (idNode != null)
      {
        template.Id = idNode.Value.Content as string;


        var logicalExpressionNode = idNode.Next;
        if (logicalExpressionNode != null)
        {
          template.LogicalExpression =
            MapLogicalExpressionToObject(logicalExpressionNode.Value) as LogicalExpressionModel;

          var commentNode = logicalExpressionNode.Next;
          while (commentNode != null)
          {
            template.Comments.Add(commentNode.Value.Content as string);
            commentNode = commentNode.Next;
          }
        }        
      }

      return template;
    }

    private object MapFeatureTemplateToObject(Block block)
    {

      var con = block as ContainerBlock;
      if (con == null) return null;
      var featureTemplate = _container.Resolve<FeatureTemplateModel>();
      featureTemplate.Enabled = (con.Attributes[BlockAttributes.ENABLED] as BlockAttributeBoolean).MyValue;
      featureTemplate.UseWhenDebugging = (con.Attributes[BlockAttributes.USE_WHEN_DEBUGGING] as BlockAttributeBoolean).MyValue;

      if (con[0].TypeName == BlockGrammarLoadMethods.Names.TERMINAL_REF)
      {
        featureTemplate.Left = MapTerminalRefToObject(con[0]) as FeatureTemplateIdBase;
      } 
      else
      {
        featureTemplate.Left = (FeatureTemplateIdBase) MapFeatureTemplateNameIdToObject(con[0]);
      }

      foreach (var childBlock in con.Children.Skip(1))
      {
        featureTemplate.RightValues.Add(MapBlockToObject(childBlock));
      }

      return featureTemplate;
    }

    private object MapFeatureTemplateNameIdToObject(Block block)
    {
      var con = block as ContainerBlock;
      if (con == null) return null;
      var featureTemplateName= _container.Resolve<FeatureTemplateNameModel>();
      featureTemplateName.Name = con[0].Content as string;
      if (con.Children.Count > 1)
      {
        featureTemplateName.Id = con[1].Content as string;
      }

      return featureTemplateName;
    }

    private object MapTerminalRefToObject(Block block)
    {
      var terminalRef = _container.Resolve<TerminalRefModel>();
      terminalRef.Id = block.Content as string;

      return terminalRef;
    }

    private object MapTemplateFeaturePathToObject(Block block)
    {
      var con = block as ContainerBlock;
      if (con == null) return null;
      
      var templateFeaturePath = _container.Resolve<TemplateFeaturePath>();
      templateFeaturePath.Start = con[0].Content as string;
      templateFeaturePath.End = con[1].Content as string;
      
      if (con.Children.Count > 2){
        templateFeaturePath.Value = MapBlockToObject(con[3]);
      }
      if (con.Attributes.ContainsKey(BlockAttributes.IS_DEFAULT))
      {
        var isDefaultAtt = con.Attributes[BlockAttributes.IS_DEFAULT] as BlockAttributeBoolean;
        if (isDefaultAtt != null)
        {
          templateFeaturePath.IsDefault = isDefaultAtt.MyValue;
        }
      }
      
           
      return templateFeaturePath;
    }
    
    private object MapTemplateFeaturePathDisjunctionToObject(Block block)
    {
      var con = block as ContainerBlock;
      if (con == null) return null;
      var disjunction = _container.Resolve<FeatureTemplateFeaturePathDisjunction>();
      
      disjunction.Start = con[0].Content as string;
      disjunction.End = con[1].Content as string;
      
      if (con.Children.Count > 2){
        disjunction.LeftPath = MapFeatureValueRefToObject(con[2]) as FeatureValueRefModel;
      }
      
      if (con.Children.Count > 3){
        foreach (var valRef in con.Children.Skip(3)) {
          disjunction.RightPaths.Add(MapFeatureValueRefToObject(valRef) as FeatureValueRefModel);
        }
      }
      
      return disjunction;
    }
    
    
    private object MapFeaturePathToObject(Block block)
    {
      var conBlock = block as ContainerBlock;
      if (conBlock == null) return null;
      var path = _container.Resolve<FeaturePathModel>();
      var node = conBlock.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_NODE, 0);
      var start = conBlock.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_START, 1);
      var end = conBlock.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_END, 2);

      if (node != null)
      {
        path.node = new NameIdRef(node.Content as string);
      }
      if (start != null)
      {
        path.feature_start = new NameIdRef(start.Content as string);
      }
      if (end != null)
      {
        path.feature_end = new NameIdRef(end.Content as string);
      }

      if (conBlock.Children.Count == 4)
      {
        path.EmbeddedFeaturePath = MapBlockToObject(conBlock[3]);
      }

      return path;      
    }

    private object MapEmbeddedFeaturePathToObject(Block block)
    {
      var conBlock = block as ContainerBlock;
      if (conBlock == null) return null;
      var path = _container.Resolve<FeaturePathModel>();
      var start = conBlock.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_START, 0);
      var end = conBlock.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_END, 1);
      var embeddedPath =
        conBlock.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.EMBEDDED_FEATURE_PATH, 2);

      if (start != null)
      {
        path.feature_start = new NameIdRef(start.Content as string);
      }
      if (end != null)
      {
        path.feature_end = new NameIdRef(end.Content as string);
      }
      if (embeddedPath != null)
      {
        path.EmbeddedFeaturePath = MapEmbeddedFeaturePathToObject(embeddedPath);
      }

      return path;
    }

    private object MapSubcatPathToObject(Block x)
    {
      var block = x as ContainerBlock;
      if (block == null) return null;
      var path = _container.Resolve<SubcatPathModel>();
      var node = block.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_NODE, 0);
      var start = block.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_START, 1);
      var end = block.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_END, 2);
      var subcatEnd = block.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.SUBCAT_END, 3);
      var afterStart =
        block.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_AFTER_START, 4);
      var afterEnd = block.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_AFTER_END, 5);

      if (node != null)
      {
        path.node = new NameIdRef(node.Content as string);
      }
      if (start != null)
      {
        path.feature_start = new NameIdRef(start.Content as string);
      }
      if (end != null)
      {
        path.feature_end = new NameIdRef(end.Content as string);
      }
      if (subcatEnd != null)
      {
        var subcatEndContent = (subcatEnd.Content as string).Replace("Subcat ","");
        if (subcatEndContent == "None")
        {
          path.subcat_end = SubcatEndValues.NoneValue;
        }
        else
        {
          path.subcat_end = subcatEndContent == "First" ? SubcatEndValues.First : SubcatEndValues.Rest;
        }
      }
      if (afterStart != null)
      {
        path.feature_after_start = new NameIdRef(afterStart.Content as string);
      }
      if (afterEnd != null)
      {
        path.feature_after_end = new NameIdRef(afterEnd.Content as string);
      }

      return path;
    }

    private object MapNoneRefToObject()
    {
      return _container.Resolve<NoneRef>();
    }

    private object MapNoneValuePathToObject(Block block)
    {
      var none = _container.Resolve<NoneValuePath>();
      none.Value = block.Content as string;

      return none;
    }

    private object MapNoneValueBlockToObject()
    {
      return _container.Resolve<NoneValueModel>();
    }

    private List<ConstraintModelBase> MapBlockToConstraints(Block x)
    {
      var conBlock = x as ContainerBlock;
      var list = new List<ConstraintModelBase>();
      foreach (var child in conBlock.Children)
      {
        list.Add(MapBlockToObject(child) as ConstraintModelBase);
      }

      return list;
    }    

    private object MapBlockToConstraintDisjunction(Block x)
    {
      var cdis = x as ContainerBlock;
      if (cdis == null) return null;
      var next = cdis.Children.First;
      var model = _container.Resolve<ConstraintDisjunctionModel>();
      model.LeftConstraints = MapBlockToConstraints(next.Value);

      next = next.Next;
      while (next != null)
      {
        model.RightConstraints.Add(MapBlockToConstraints(next.Value));
        next = next.Next;
      }

      return model;
    }


    private object MapBlockToConstraint(Block x, ConstraintModelBase uni)
    {
      var block = x as ContainerBlock;

      uni.enabled = (x.Attributes[BlockAttributes.ENABLED] as BlockAttributeBoolean).MyValue;
      uni.use_when_debugging = (x.Attributes[BlockAttributes.USE_WHEN_DEBUGGING] as BlockAttributeBoolean).MyValue;
      var firstChild = block.Children.First;

      uni.left = MapBlockToObject(firstChild.Value);
      uni.right = MapBlockToObject(firstChild.Next.Value);

      return uni;
    }

    private object MapLexGlossRootGlossValueBlockToObject(Block block, FeatureTemplateTerminalValueModelBase obj)
    {
      obj.Value = block.Content as string;

      return obj;
    }

    private object MapLexGlossrotGlossValuePathToObject(Block block, FeatureTemplateTerminalValuePathModelBase obj)
    {
      obj.Value = block.Content as string;

      return obj;
    }

    private object MapCatValueBlockToObject(Block block)
    {
      var obj = _container.Resolve<CatValue>();
      obj.Symbol.IdRef = block.Content as string;

      return obj;
    }

    private object MapCatValuePathToObject(Block block)
    {
      var obj = _container.Resolve<CatValuePath>();
      obj.Symbol.IdRef = block.Content as string;

      return obj;
    }

    private object MapCategoryValueRefToObject(Block block)
    {
      var catValRef = _container.Resolve<CategoryValueRefModel>();
      catValRef.value = new NameIdRef(block.Content as string);

      return catValRef;
    }

    private object MapFeatureValueRefToObject(Block block)
    {
      var valRef = _container.Resolve<FeatureValueRefModel>();
      valRef.value = new NameIdRef(block.Content as string);
      valRef.IsDefault = (block.Attributes[BlockAttributes.IS_DEFAULT] as BlockAttributeBoolean).MyValue;
     
      return valRef;
    }

    private object MapFeatureValueDisjunctionToObject(Block block)
    {
      var con = block as ContainerBlock;
      var valDis = _container.Resolve<FeatureValueDisjunction>();

      valDis.LeftElement =
        MapFeatureValueRefToObject(
          con.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_VALUE_REF, 0)) as FeatureValueRefModel;

      var rightElements = con.Children.Where(f => f.IndexInRule == 1);

      foreach (var rightElement in rightElements)
      {
        valDis.RightElements.Add(MapFeatureValueRefToObject(rightElement) as FeatureValueRefModel);
      }

      return valDis;
    }

    private object MapFeatureTemplateRefToObject(Block block)
    {
      var templateRef = _container.Resolve<FeatureFeatureTemplateRef>();
      templateRef.Id = new NameIdRef(block.Content as string);

      return templateRef;
    }

    private object MapLogicalConstraintRefToObject(Block block)
    {
      var constrRef = _container.Resolve<LogicalConstraintRefModel>();
      constrRef.constraint = new NameIdRef(block.Content as string);     

      return constrRef;
    }

    private object MapFeatureToObject(Block block)
    {
      var con = block as ContainerBlock;
      var feature = _container.Resolve<LogicalFeatureModel>();
      
      var start = con.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_START, 0);
      var end = con.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE_END, 1);
      var indexVar = con.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.INDEX_VARIABLE, 2);
      var embedded = con.GetFirstChildWithMatchingTypeNameAndIndexInRule(BlockGrammarLoadMethods.Names.FEATURE, 3);

      feature.feature_start = new NameIdRef(start.Content as string);
      feature.feature_end = new NameIdRef(end.Content as string);

      if (indexVar != null)
      {
        feature.indexed_variable = (indexVar.Content as string).Replace("[","").Replace("]","");
      }

      if (embedded != null)
      {
        feature.embedded_feature = MapFeatureToObject(embedded) as LogicalFeatureModel;
      }

      return feature;
    }

    private object MapCollectionFeatureRefToObject(Block block)
    {
      var con = block as ContainerBlock;
      var colFeat = _container.Resolve<CollectionFeatureRef>();
      colFeat.IsDefault = (con.Attributes[BlockAttributes.IS_DEFAULT] as BlockAttributeBoolean).MyValue;

      colFeat.CollectionFeature =  con[0].Content as string;

      colFeat.EmbeddedPath = MapBlockToObject(con[1]);

      return colFeat;
    }

    private object MapComplexFeatureRefToObject(Block block)
    {
      var con = block as ContainerBlock;
      var complexRef = _container.Resolve<ComplexFeatureRef>();
      complexRef.Id = con[0].Content as string;

      var values = con.Children.Where(f => f.IndexInRule == 1);

      foreach (var val in values)
      {
        complexRef.Values.Add(MapBlockToObject(val));
      }

      return complexRef;
    }

    private object MapTemplateFeatureDisjunctionToObject(Block block)
    {
      var con = block as ContainerBlock;
      var disjunction = _container.Resolve<FeatureTemplateFeatureDisjunction>();

      disjunction.LeftTerm = MapBlockToObject(con[0]);

      var values = con.Children.Where(f => f.IndexInRule == 1);

      foreach (var val in values)
      {
        disjunction.RightTerms.Add(MapBlockToObject(val));
      }

      return disjunction;
    }
    
    private object MapFeatureRefToObject(Block block)
    {
      var con = block as ContainerBlock;
      
      
      var featureRef = new FeatureRef( con[0].Content as string, con[1].Content as string);

      featureRef.IsDefault = (con.Attributes[BlockAttributes.IS_DEFAULT] as BlockAttributeBoolean).MyValue;
      var embedded = con.Children.FirstOrDefault(f => f.IndexInRule == 2);

      if (embedded != null)
      {
        featureRef.Child = MapBlockToObject(embedded);
      }

      return featureRef;
    }

    private object MapUnaryOperationToObject(Block block)
    {
      var con = block as ContainerBlock;
      var exp = _container.Resolve<UnaryOperationModel>();
      exp.OperationType = LogicalOperationNameMapper.map_back(con[0].Content as string);
      exp.Factor1 = MapBlockToObject(con[1]);

      return exp;
    }

    private object MapBinaryOperationToObject(Block block)
    {
      var con = block as ContainerBlock;
      var binOp = _container.Resolve<BinaryOperationModel>();
      var factor1 = con.Children.FirstOrDefault(f => f.IndexInRule == 0);
      var opType = con.Children.FirstOrDefault(f => f.IndexInRule == 1);
      var factor2 = con.Children.FirstOrDefault(f => f.IndexInRule == 2);

      if (factor1 != null)
      {
        binOp.Factor1 = MapBlockToObject(factor1);
      }
      if (opType != null)
      {
        binOp.OperationType = LogicalOperationNameMapper.map_back(opType.Content as string);
      }
      if (factor2 != null)
      {
        binOp.Factor2 = MapBlockToObject(factor2);
      }

      return binOp;
    }
    
    private object MapLogicalExpressionToObject(Block block)
    {
      var con = block as ContainerBlock;
      var exp = _container.Resolve<LogicalExpressionModel>();
      exp.operation = MapBlockToObject(con[0]) as LogicalOperationModelBase;

      return exp;
    }

    private object MapSubcatRefToObject(Block block)
    {
      var con = block as ContainerBlock;
      var subcatRef = _container.Resolve<SubcatRef>();

      subcatRef.Value = MapBlockToObject(con[0]);

      return subcatRef;
    }

    
    
    private object MapSubcatFirstRestPairToObject(Block block)
    {
      var con = block as ContainerBlock;
      var firstRest = _container.Resolve<SubcatFirstRest>();
      firstRest.First = MapBlockToObject((con[0] as ContainerBlock).Children.First.Value);
      firstRest.Rest = MapBlockToObject((con[1] as ContainerBlock).Children.First.Value);

      return firstRest;
    }
    
    private object MapSubcatDisjunctionToObject(Block block)
    {
      var con = block as ContainerBlock;
      var disjunction = _container.Resolve<SubcatDisjunction>();
      disjunction.FirstFactor = MapBlockToObject(con[0]);

      var values = con.Children.Where(f => f.IndexInRule == 1);

      foreach (var val in values)
      {
        disjunction.OtherFactors.Add(MapBlockToObject(val));
      }

      return disjunction;
    }
  }
}