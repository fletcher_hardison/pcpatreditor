﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Microsoft.Practices.Unity;
using PCPATR.DataAccess.Parser;
using PCPATR.Editor.MasterLists.FeatureTemplateViewModels;
using PCPATR.Editor.MasterLists.Rules.ViewModels;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.Editor.MasterLists.Views;
using PCPATR.Editor.MasterLists.Views.Dialogs;
using PCPATR.Models.Rules.Constraints;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  public static class BlockGrammarActionLoaderMethods
  {


    public static void LoadLexicalReferenceActions(BlockGrammar grammar, IUnityContainer container)
    {
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_SYMBOL_REFERENCE, ReferenceSyntaticCategory);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_LEFT_HAND_SIDE_SYMBOL, ReferenceNonTerminalSymbol);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_SYMBOL_REF_ID, SetPsrId);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.FEATURE_NODE_EDIT_ACTION, SymbolInPsrSelectorAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.FEATURE_START_EDIT_ACTION, FeatureStartEditAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.FEATURE_END_EDIT_ACTION, FeatureEndEditAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SUBCAT_END_EDIT_ACTION, SubcatEndEditAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.FEATURE_AFTER_START_ACTION, FeatureStartEditAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.FEATURE_AFTER_END_ACTION, FeatureAfterEndAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_LOGICAL_EXPRESSION_TYPE, SetLogicalExpressionType);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.INSERT_LEXICAL_VALUE,
                                       InsertLexGlossRootGlossNoneValueAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.INSERT_GLOSS_VALUE,
                                       InsertLexGlossRootGlossNoneValueAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.INSERT_ROOT_GLOSS_VALUE,
                                       InsertLexGlossRootGlossNoneValueAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.INSERT_NONE_VALUE_PATH_CONTENT,
                                       InsertLexGlossRootGlossNoneValueAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_INDEX_VARIABLE, SetIndexVariable);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.REFERNECE_SYNTATIC_CATEGORY,
                                       ReferenceSyntaticCategory);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.REFERENCE_ATOMIC_FEATURE, ReferenceAtomicFeature);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.REFERENCE_LOGICAL_CONSTRAINT_TEMPLATE,
                                       ReferenceLogicalConstraintTemplate);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.REFERNCE_COLLECTION_FEATURE, ReferenceCollectionFeature);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.REFERENCE_FEATURE_TEMPLATE, RefernceFeatureTemplate);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.REFERENCE_COMPLEX_FEATURE_ID, ReferenceComplexFeatureId);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_FEATURE_TEMPLATE_NAME,InsertLexGlossRootGlossNoneValueAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_CONSTRAINT_TEMPLATE_NAME,
                                       InsertLexGlossRootGlossNoneValueAction);

      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_FEATURE_TEMPLATE_ID, SetId);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_TERMINAL_SYMBOL, ReferenceTerminalSymbol);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_CONSTRAINT_TEMPLATE_ID, SetId);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_COMMENT, InsertLexGlossRootGlossNoneValueAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_LEXICAL_RULE_NAME, SetId);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_TEMPLATE_FEATURE_PATH_START, TemplateFeatureStartEditAction);
      grammar.EditActionDictionary.Add(BlockGrammarLoadMethods.Actions.SET_TEMPLATE_FEATURE_PATH_END, TemplateFeatureEndEditAction);

    }

    private static readonly Action<Block> SymbolInPsrSelectorAction = block =>
    {
      var rule = block.InitializationContext.GetElement<RuleViewModel>();
      var idList = new List<string>();
      BlockPsrHelperMethods.ExtractSymbolRefIds(rule.PsrEditor.Content, idList);
      
      var listbox = new ListBox { MaxHeight = 200, ItemsSource = idList };      
      block.EditContent = listbox;

      block.OkAction = b => b.Content = listbox.SelectedItem;
        
    };

    

    private static readonly Action<Block> FeatureStartEditAction = block =>
    {
      var treeView = block.InitializationContext.GetElement<IUnityContainer>().Resolve<FeatureSystemTreeView>();
      treeView.RefershFeatures();
      block.EditContent = treeView;

      block.OkAction = b => b.Content = treeView.Selected.Id;
    };

    private static readonly Action<Block> TemplateFeatureStartEditAction = block =>
    {
      var container = block.InitializationContext.GetElement<IUnityContainer>(); 
      var treeView = container.Resolve<FeatureSystemTreeView>();
      var collectionFeaturesWrapper = GetCollectionFeaturesAndWrapInFeatureViewModel(container);

      treeView.features.Add(collectionFeaturesWrapper);
      
      block.EditContent = treeView;

      block.OkAction = b => b.Content = treeView.Selected.Id;
    };

    private static readonly Action<Block> TemplateFeatureEndEditAction = block =>
    {
      var container = block.InitializationContext.GetElement<IUnityContainer>();
      var builtInPrimitives = container.Resolve<BuiltInPrimitivesViewModel>();
      var treeView = container.Resolve<FeatureSystemTreeView>();
      var featureStartBlock = block.FindSibling(block, false,
                                                f => f.TypeName == BlockGrammarLoadMethods.Names.FEATURE_START);

      if (featureStartBlock != null)
      {
        var target = featureStartBlock.Content as string;
        treeView.FilterFeatures(f => f.Id == target);

      }

      treeView.features.Add(GetCollectionFeaturesAndWrapInFeatureViewModel(container));
      treeView.features.Add(ConvertBuildInPrimivesToFeatureViewModels(builtInPrimitives, container));

      block.EditContent = treeView;

      block.OkAction = b => b.Content = treeView.Selected.Id;
    };

    private static FeatureViewModel GetCollectionFeaturesAndWrapInFeatureViewModel(IUnityContainer container)
    {
      var masterLists = container.Resolve<MasterListsViewModel>();
      var collectionFeatures = masterLists.CollectionFeatures.Features;

      var collectionFeaturesWrapper = container.Resolve<FeatureViewModel>();
      collectionFeaturesWrapper.Name = "Collection Features";
      collectionFeaturesWrapper.Id = "collectionFeatures";


      foreach (var feature in collectionFeatures)
      {
        var output = container.Resolve<FeatureViewModel>();
        output.Name = feature.Name;
        output.Id = feature.Id;
        collectionFeaturesWrapper.Children.Add(output);
      }
      return collectionFeaturesWrapper;
    }


    private static FeatureViewModel ConvertBuildInPrimivesToFeatureViewModels(
      BuiltInPrimitivesViewModel bip, IUnityContainer container)
    {
      var output = container.Resolve<FeatureViewModel>();
      output.Name = "Built In Primitives";
      output.Id = "builtInPrimitivies";
      foreach (var primitive in bip.BuiltInPrimitivesWithOutNone)
      {
        var feature = container.Resolve<FeatureViewModel>();
        feature.Id = primitive.IdRef;
        feature.Name = primitive.Name;
        output.Children.Add(feature);
      }

      return output;
    }

    private static readonly Action<Block> FeatureEndEditAction = block =>
    {
      var container = block.InitializationContext.GetElement<IUnityContainer>();
      var builtInPrimitives = container.Resolve<BuiltInPrimitivesViewModel>();
      var treeView = container.Resolve<FeatureSystemTreeView>();
      var featureStartBlock = block.FindSibling(block, false, f => f.TypeName == BlockGrammarLoadMethods.Names.FEATURE_START);

      if (featureStartBlock != null)
      {
        var target = featureStartBlock.Content as string;
        treeView.FilterFeatures(f => f.Id == target);
        treeView.features.Add(ConvertBuildInPrimivesToFeatureViewModels(builtInPrimitives, container));
      }

      block.EditContent = treeView;

      block.OkAction = b => b.Content = treeView.Selected.Id;
    };

    private static readonly Action<Block> FeatureAfterEndAction = block =>
    {
      var container = block.InitializationContext.GetElement<IUnityContainer>();

      var treeView = container.Resolve<FeatureSystemTreeView>();
      var featureStartBlock = block.FindSibling(block, false, f => f.TypeName == BlockGrammarLoadMethods.Names.FEATURE_AFTER_START);
      var builtInPrimitives = container.Resolve<BuiltInPrimitivesViewModel>();
      if (featureStartBlock != null)
      {
        var target = featureStartBlock.Content as string;
        treeView.FilterFeatures(f => f.Id == target);
        treeView.features.Add(ConvertBuildInPrimivesToFeatureViewModels(builtInPrimitives,container));
      }

      block.EditContent = treeView;

      block.OkAction = b => b.Content = treeView.Selected.Id;
    };
    private static readonly Action<Block> SubcatEndEditAction = block =>
    {
      var combo = new ComboBox();

      combo.Items.Add("Subcat " + SubcatEndValues.First.ToString());
      combo.Items.Add("Subcat " + SubcatEndValues.Rest.ToString());
      combo.Items.Add("Subcat " + SubcatEndValues.NoneValue.ToString());

      block.EditContent = combo;

      block.OkAction = b => b.Content = combo.SelectedItem as string;
    };

    private static readonly Action<Block> SetLogicalExpressionType = block =>
    {
      ComboBox combo = new ComboBox();
      if (block.Parent.TypeName == BlockGrammarLoadMethods.Names.BINARY_EXPRESSION)
      {
        combo.Items.Add(LogicalOperationNameMapper.AND_MAPPED);
        combo.Items.Add(LogicalOperationNameMapper.OR_MAPPED);
        combo.Items.Add(LogicalOperationNameMapper.CONDITIONAL_MAPPED);
        combo.Items.Add(LogicalOperationNameMapper.BICONDITIONAL_MAPPED);
      }
      else
      {
        combo.Items.Add(LogicalOperationNameMapper.EXISTANCE_MAPPED);
        combo.Items.Add(LogicalOperationNameMapper.NEGATION_MAPPED);
      }

      block.EditContent = combo;

      block.OkAction = b => b.Content = combo.SelectedItem as string;
    };

    private static readonly Action<Block> InsertLexGlossRootGlossNoneValueAction = block =>
    {
      var inputBox = new TextBox();

      block.EditContent = inputBox;

      block.OkAction = b => b.Content = inputBox.Text;
    };

    private static readonly Action<Block> SetIndexVariable = block =>
    {
      var combo = new ComboBox();
      combo.Items.Add("[1]");
      combo.Items.Add("[2]");
      combo.Items.Add("[3]");
      combo.Items.Add("[4]");
      combo.Items.Add("[5]");
      combo.Items.Add("[6]");
      combo.Items.Add("[7]");
      combo.Items.Add("[8]");
      combo.Items.Add("[9]");
      combo.Items.Add("[none]");

      block.EditContent = combo;

      block.OkAction = b => b.Content = combo.SelectedItem as string;
    };


    private static readonly Action<Block> ReferenceSyntaticCategory = block =>
    {
      var masterLists = block.InitializationContext.GetElement<IUnityContainer>().Resolve<MasterListsViewModel>();

      var symbols =
        masterLists.Symbols.NonTerminalSymbols.Select(f => f.Id)
                   .Concat(masterLists.Symbols.TerminalSymbols.Select(f => f.Id));
      var listbox = new ListBox { ItemsSource = symbols, MaxHeight = 200 };

      block.EditContent = listbox;

      block.OkAction = b => b.Content = listbox.SelectedItem as string;
    };

    private static readonly Action<Block> ReferenceTerminalSymbol = block =>
    {
      var container = block.InitializationContext.GetElement<IUnityContainer>();
      var masterLists = container.Resolve<MasterListsViewModel>();

      var symbols = masterLists.Symbols.TerminalSymbols.Select(f => f.Id);
      var listbox = new ListBox { ItemsSource = symbols, MaxHeight = 200 };

      block.EditContent = listbox;

      block.OkAction = b => b.Content = listbox.SelectedItem as string;
    };

    private static readonly Action<Block> ReferenceNonTerminalSymbol = block =>
    {
      var container = block.InitializationContext.GetElement<IUnityContainer>();
      var masterLists = container.Resolve<MasterListsViewModel>();

      var symbols = masterLists.Symbols.NonTerminalSymbols.Select(f => f.Id);
      var listbox = new ListBox { ItemsSource = symbols, MaxHeight = 200 };

      block.EditContent = listbox;

      block.OkAction = b => b.Content = listbox.SelectedItem as string;
    };


    private static readonly Action<Block> ReferenceAtomicFeature = block =>
    {
      var container = block.InitializationContext.GetElement<IUnityContainer>();
      var control = container.Resolve<FeatureValueControl>();
      var featureSystem = container.Resolve<FeatureSystemViewModel>();
      control.ClosedFeatures = featureSystem.SearchFeatures(FeatureHelperMethods.IsClosedFeature);

      block.EditContent = control;

      block.OkAction = b => { 
        if (control.SelectedFeature != null)
        {
          b.Content = control.SelectedFeature.Id;
        }
      };
    };

    private static readonly Action<Block> ReferenceLogicalConstraintTemplate = block =>
    {
      var constraintTemplates =
        block.InitializationContext.GetElement<IUnityContainer>().Resolve<ConstraintTemplatesViewModel>();

      var listbox = new ListBox { ItemsSource = constraintTemplates.GetTemplateModels().Select(f => f.Id), MaxHeight = 200 };

      block.EditContent = listbox;

      block.OkAction = b => b.Content = listbox.SelectedItem;
    };

    private static readonly Action<Block> ReferenceCollectionFeature = block =>
    {
      var collectionFeatures = block.InitializationContext.GetElement<IUnityContainer>().Resolve<MasterListsViewModel>().CollectionFeatures.Features;
      var listbox = new ListBox { ItemsSource = collectionFeatures.Select(f => f.Id), MaxHeight = 200 };
      
      block.EditContent = listbox;
      
      block.OkAction = b => b.Content = listbox.SelectedItem as string;
    };


    private static readonly Action<Block> RefernceFeatureTemplate = block =>
    {
      var featureTemplates =
        block.InitializationContext.GetElement<IUnityContainer>()
             .Resolve<FeatureTemplatesRepositoryViewModel>()
             .GetTemplateModels();

      var listbox = new ListBox {ItemsSource = featureTemplates.Where(f => !string.IsNullOrEmpty(f.Left.Id)), MaxHeight = 200};

      block.EditContent = listbox;

      block.OkAction = b => b.Content = listbox.SelectedItem;
    };

    private static readonly Action<Block> ReferenceComplexFeatureId = block =>
    {
      var featureSystem = block.InitializationContext.GetElement<IUnityContainer>().Resolve<FeatureSystemViewModel>();
      var complex = featureSystem.SearchFeatures(FeatureHelperMethods.IsComplexFeature);

      var listbox = new ListBox { ItemsSource = complex.Select(f => f.Id), MaxHeight = 200 };

      block.EditContent = listbox;

      block.OkAction = b => b.Content = listbox.SelectedItem;
    };

    private static readonly Action<Block> SetPsrId = block =>
    {
       var idService = block.InitializationContext.GetElement<IUnityContainer>().Resolve<AddNewIdWithValidationControl>();
       var rule = block.InitializationContext.GetElement<RuleViewModel>();

       block.EditContent = idService;

       block.OkAction = b =>
       {
         if (idService.IsValid)
         {
           var oldId = b.Content as string;
           b.Content = idService.PropsedId;
           var symbols = BlockHelperMethods.SearchBlocks(rule.ConstraintsEditor.Content,
                                                         f => f.TypeName == BlockGrammarLoadMethods.Names.FEATURE_NODE);
           foreach (var symbol in symbols)
           {
             if ((symbol.Content as string) == oldId)
             {
               symbol.Content = b.Content;
             }
           }
         }
       };
    };

    public static readonly Action<Block> SetId = block =>
    {
      var idService = block.InitializationContext.GetElement<IUnityContainer>().Resolve<AddNewIdWithValidationControl>();

      block.EditContent = idService;

      block.OkAction = b =>
      {
        if (idService.IsValid)
        {
          b.Content = idService.PropsedId;          
        }
      };
    };

  }
}