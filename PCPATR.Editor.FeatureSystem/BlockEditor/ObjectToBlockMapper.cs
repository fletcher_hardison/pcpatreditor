﻿using System;
using System.Collections.Generic;
using PCPATR.DataAccess.Parser;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Models.Rules;
using PCPATR.Models.Rules.Constraints;
using PCPATR.Models.Rules.PSRElements;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  public class ObjectToBlockMapper
  {
    private BlockGrammar _grammar = BlockGrammar.GetInstance();

    private Dictionary<Type, Func<object, Block, BlockInitializationContext, Block>> mappingDictionary =
      new Dictionary<Type, Func<object, Block, BlockInitializationContext, Block>>();

    public ObjectToBlockMapper()
    {
      SetupMappingDictionary();
    }

    private void SetupMappingDictionary()
    {
      mappingDictionary.Add(typeof (UnificationConstraintModel),
                            (model, parent, initContext) =>
                            ToConstraintBlock((ConstraintModelBase) model,
                                              BlockGrammarLoadMethods.Names.UNIFICATION_CONSTRAINT,
                                              parent, initContext));

      mappingDictionary.Add(typeof (ConstraintDisjunctionModel),
                            (model, parent, initContext) =>
                            ToConstraintDisjunction((ConstraintDisjunctionModel) model,
                                                    parent, initContext));

      mappingDictionary.Add(typeof (PercolationOperationModel),
                            (model, parent, initContext) =>
                            ToConstraintBlock((ConstraintModelBase) model,
                                              BlockGrammarLoadMethods.Names.PERCOLATION_OPERATION,
                                              parent, initContext));

      mappingDictionary.Add(typeof (PriorityUnionModel),
                            (model, parent, initContext) =>
                            ToConstraintBlock((ConstraintModelBase) model,
                                              BlockGrammarLoadMethods.Names.PRIORITY_UNION_OPERATION,
                                              parent, initContext));

      mappingDictionary.Add(typeof (LogicalConstraintModel),
                            (model, parent, initContext) =>
                            ToConstraintBlock((ConstraintModelBase) model,
                                              BlockGrammarLoadMethods.Names.LOGICAL_CONSTRAINT, parent,
                                              initContext));

      mappingDictionary.Add(typeof (FeaturePathModel),
                            (model, parent, initContext) =>
                            MapFeaturePathToBlock((FeaturePathModel) model, parent, initContext));

      mappingDictionary.Add(typeof (EmbeddedFeaturePath),
                            (model, parent, initContext) =>
                            MapEmbeddedFeaturePathToBlock((EmbeddedFeaturePath) model, parent, initContext));

      mappingDictionary.Add(typeof (SubcatPathModel),
                            (model, parent, initContext) =>
                            MapSubcatPathToBlock((SubcatPathModel) model, parent, initContext));

      mappingDictionary.Add(typeof (CatValue),
                            (model, parent, initContext) => MapCatValueToBlock((CatValue) model, parent, initContext));

      mappingDictionary.Add(typeof (LexValue),
                            (model, parent, initContext) =>
                            MapLexGlossRootGlossValueToBlock((FeatureTemplateTerminalValueModelBase) model,
                                                             BlockGrammarLoadMethods.Names.LEX_VALUE, parent,
                                                             initContext));

      mappingDictionary.Add(typeof (GlossValue),
                            (model, parent, initContext) =>
                            MapLexGlossRootGlossValueToBlock((FeatureTemplateTerminalValueModelBase) model,
                                                             BlockGrammarLoadMethods.Names.GLOSS_VALUE, parent,
                                                             initContext));

      mappingDictionary.Add(typeof (RootGlossValue),
                            (model, parent, initContext) =>
                            MapLexGlossRootGlossValueToBlock((FeatureTemplateTerminalValueModelBase) model,
                                                             BlockGrammarLoadMethods.Names.ROOT_GLOSS_VALUE, parent,
                                                             initContext));

      mappingDictionary.Add(typeof (CatValuePath),
                            (model, parent, initContext) =>
                            MapCatValuePathToBlock((CatValuePath) model, parent, initContext));

      mappingDictionary.Add(typeof (LexValuePath),
                            (model, parent, initContext) =>
                            MapLexGlossRootGlossPathToBlock((FeatureTemplateTerminalValuePathModelBase) model,
                                                            BlockGrammarLoadMethods.Names.LEX_VALUE_PATH, parent,
                                                            initContext));

      mappingDictionary.Add(typeof (GlossValuePath),
                            (model, parent, initContext) =>
                            MapLexGlossRootGlossPathToBlock((FeatureTemplateTerminalValuePathModelBase) model,
                                                            BlockGrammarLoadMethods.Names.GLOSS_VALUE_PATH, parent,
                                                            initContext));

      mappingDictionary.Add(typeof (RootGlossValuePath),
                            (model, parent, initContext) =>
                            MapLexGlossRootGlossPathToBlock((FeatureTemplateTerminalValuePathModelBase) model,
                                                            BlockGrammarLoadMethods.Names.ROOT_GLOSS_VALUE_PATH, parent,
                                                            initContext));

      mappingDictionary.Add(typeof (CategoryValueRefModel),
                            (model, parent, initContext) =>
                            MapCategoryValueRefToBlock((CategoryValueRefModel) model, parent, initContext));

      mappingDictionary.Add(typeof (NoneValueModel),
                            (model, parent, initContext) => MapNoneValueToBlock(parent, initContext));
      mappingDictionary.Add(typeof (NoneRef), (model, parent, initContext) => MapNoneRefToBlock(parent, initContext));

      mappingDictionary.Add(typeof (NoneValuePath),
                            (model, parent, initContext) =>
                            MapNoneValuePathToBlock((NoneValuePath) model, parent, initContext));

      mappingDictionary.Add(typeof (List<ConstraintModelBase>),
                            (model, parent, initContext) =>
                            MapConstraintsToBlock((List<ConstraintModelBase>) model, parent, initContext));

      mappingDictionary.Add(typeof (LogicalFeatureModel),
                            (model, parent, initContext) =>
                            MapFeatureToBlock((LogicalFeatureModel) model, parent, initContext));

      mappingDictionary.Add(typeof (ComplexFeatureRef),
                            (model, parent, initContext) =>
                            MapComplexFeatureRefToBlock((ComplexFeatureRef) model, parent, initContext));

      mappingDictionary.Add(typeof (CollectionFeatureRef),
                            (model, parent, initContext) =>
                            MapCollectionFeatureRefToBlock((CollectionFeatureRef) model, parent, initContext));

      mappingDictionary.Add(typeof (FeatureTemplateFeatureStructure),
                            (model, parent, initContext) =>
                            MapTemplateFeatureStructureToBlock((FeatureTemplateFeatureStructure) model, parent,
                                                               initContext));

      mappingDictionary.Add(typeof (FeatureTemplateFeatureDisjunction),
                            (model, parent, initContext) =>
                            MapTemplateFeatureDisjunctionToBlock((FeatureTemplateFeatureDisjunction) model, parent,
                                                                 initContext));

      mappingDictionary.Add(typeof (FeatureRef),
                            (model, parent, initContext) =>
                            MapFeatureRefToBlock((FeatureRef) model, parent, initContext));

      mappingDictionary.Add(typeof (FeatureFeatureTemplateRef),
                            (model, parent, initContext) =>
                            MapFeatureTemplateRefToBlock((FeatureFeatureTemplateRef) model, parent, initContext));

      mappingDictionary.Add(typeof (UnaryOperationModel),
                            (model, parent, initContext) =>
                            MapUnaryExpressionToBlock((UnaryOperationModel) model, parent, initContext));

      mappingDictionary.Add(typeof (BinaryOperationModel),
                            (model, parent, initContext) =>
                            MapBinaryExpressionToBlock((BinaryOperationModel) model, parent, initContext));

      mappingDictionary.Add(
        typeof (SubcatRef), (model, parent, initContext) => MapSubcatRefToBlock((SubcatRef) model, parent, initContext));

      mappingDictionary.Add(typeof (SubcatFirstRest),
                            (model, parent, initContext) =>
                            MapSubcatFirstRestToBlock((SubcatFirstRest) model, parent, initContext));

      mappingDictionary.Add(typeof (SubcatDisjunction),
                            (model, parent, initContext) =>
                            MapSubcatDisjunctionToBlock((SubcatDisjunction) model, parent, initContext));

      mappingDictionary.Add(typeof (FeatureValueDisjunction),
                            (model, parent, initContext) =>
                            MapFeatureValueDisjunctionToBlock((FeatureValueDisjunction) model, parent, initContext));

      mappingDictionary.Add(typeof (FeatureValueRefModel),
                            (model, parent, initContext) =>
                            MapFeatureValueRefToBlock((FeatureValueRefModel) model, parent, initContext));

      mappingDictionary.Add(typeof (LogicalExpressionModel),
                            (model, parent, initContext) =>
                            MapLogicalExpressionToBlock((LogicalExpressionModel) model, parent, initContext));

      mappingDictionary.Add(typeof (LogicalConstraintRefModel),
                            (model, parent, initContext) =>
                            MapLogicalConstraintRefToBlock((LogicalConstraintRefModel) model, parent, initContext));

      mappingDictionary.Add(typeof (FeatureTemplateModel),
                            (model, parent, initContext) =>
                            MapFeatureTemplateToBlock((FeatureTemplateModel) model, parent, initContext));

      mappingDictionary.Add(typeof (TemplateFeaturePath),
                            (model, parent, initContext) =>
                            MapTemplateFeaturePathToBlock((TemplateFeaturePath) model, parent, initContext));

      mappingDictionary.Add(typeof (FeatureTemplateFeaturePathDisjunction),
                            (model, parent, initContext) =>
                            MapTemplateFeaturePathDisjunctionToBlock((FeatureTemplateFeaturePathDisjunction) model,
                                                                     parent, initContext));

      mappingDictionary.Add(typeof (ConstraintTemplateModel),
                            (model, parent, initContext) =>
                            MapConstraintTemplateToBlock((ConstraintTemplateModel) model, parent, initContext));
      mappingDictionary.Add(typeof (LexicalRule), (model, parent, initContext) =>
                                                  MapLexicalRuleToBlock((LexicalRule) model, parent, initContext));
      mappingDictionary.Add(typeof (PhraseStructureRule),
                            (model, parent, initContext) =>
                            MapPhraseStructureRuleToBlock((PhraseStructureRule) model, parent, initContext));
      mappingDictionary.Add(typeof (SymbolRef),
                            (model, parent, initContext) => MapSymbolRefToBlock((SymbolRef) model, parent, initContext));
      mappingDictionary.Add(typeof (OptionalSymbols),
                            (model, parent, initContext) =>
                            MapOptionalSymbolsToBlock((OptionalSymbols) model, parent, initContext));
      mappingDictionary.Add(typeof (Disjunction),
                            (model, parent, initContext) =>
                            MapDisjunctionToBlock((Disjunction) model, parent, initContext));
      mappingDictionary.Add(typeof (DisjunctiveSymbols),
                            (model, parent, initContext) =>
                            MapDisjunctiveSymbolsToBlock((DisjunctiveSymbols) model, parent, initContext));
    }

    private Block MapOptionalSymbolsToBlock(OptionalSymbols model, Block parent, BlockInitializationContext init_context)
    {
      var opt = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.OPTIONS_SYMBOLS, init_context) as ContainerBlock;
      opt.Parent = parent;

      opt.Children.Clear();

      foreach (var element in model.Elements)
      {
        opt.Children.AddLast(MapObjectToBlock(element, opt, init_context));
      }

      return opt;
    }

    private Block MapDisjunctionToBlock(Disjunction model, Block parent, BlockInitializationContext initContext)
    {
      var disjunction =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.DISJUNCTION, initContext) as ContainerBlock;
      disjunction.Parent = parent;
      disjunction.Children.Clear();
      foreach (var leftElement in model.LeftElements)
      {
        var le = MapObjectToBlock(leftElement, disjunction, initContext);
        le.IndexInRule = 0;
        disjunction.Children.AddLast(le);
      }

      var ds = MapDisjunctiveSymbolsToBlock(model.DisjunctiveSymbols, disjunction, initContext);
      ds.IndexInRule = 1;
      disjunction.Children.AddLast(ds);

      return disjunction;
    }

    private Block MapDisjunctiveSymbolsToBlock(DisjunctiveSymbols disjunctiveSymbols, Block parent,
                                               BlockInitializationContext initContext)
    {
      var ds = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.DISJUNCTIVE_SYMBOLS, initContext) as ContainerBlock;
      ds.Parent = parent;

      ds.Children.Clear();
      foreach (var leftElement in disjunctiveSymbols.LeftElements)
      {
        var leftBlock = MapObjectToBlock(leftElement, ds, initContext);
        leftBlock.IndexInRule = 0;
        ds.Children.AddLast(leftBlock);
      }


      if (disjunctiveSymbols.EmbeddedDisjuctiveSymbols != null)
      {
        var embedded = MapDisjunctiveSymbolsToBlock(disjunctiveSymbols.EmbeddedDisjuctiveSymbols, ds, initContext);
        embedded.IndexInRule = 1;
        ds.Children.AddLast(embedded);
      }

      return ds;
    }

    private Block MapSymbolRefToBlock(SymbolRef model, Block parent, BlockInitializationContext init_context)
    {
      var symbolRef = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.SYMBOL_REF, init_context); //as ContainerBlock;
      symbolRef.Parent = parent;
      var idAtt = symbolRef.Attributes["Id"] as BlockAttributeId;
      idAtt.MyValue = model.Id;
      
      symbolRef.Content = model.Symbol.IdRef;
      
//      var contentBlock = symbolRef.Children.First;
//      if (contentBlock != null)
//      {
//        contentBlock.Value.Content = model.Symbol.IdRef;
//
//        //var id = contentBlock.Next;
//        //if (id != null) id.Value.Content = model.Id;
//      }

      return symbolRef;
    }

    private Block MapPhraseStructureRuleToBlock(PhraseStructureRule model, Block parent,
                                                BlockInitializationContext init_context)
    {
      var psr =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.PHRASE_STRUCTURE_RULE, init_context) as ContainerBlock;
      var lhsNode = psr.Children.First;

      if (lhsNode != null)
      {
        var lhsBlock = lhsNode.Value;;
        lhsBlock.Content = model.LeftHandSide.Symbol.IdRef;
        
        var lhsId = lhsBlock.Attributes["Id"] as BlockAttributeId;
        lhsId.MyValue = model.LeftHandSide.Id;
        
//        var lhsSymbol = lhsBlock.Children.First;
//        if (lhsSymbol != null)
//        {
//          lhsSymbol.Value.Content = model.LeftHandSide.Symbol.IdRef;
//          var lhsId = lhsSymbol.Next;
//          if (lhsId != null) lhsId.Value.Content = model.LeftHandSide.Id;
//        }

        var rhsNode = lhsNode.Next;
        if (rhsNode != null)
        {
          var rhsBlock = rhsNode.Value as ContainerBlock;
          if (rhsBlock != null)
          {
            foreach (var psrElement in model.RightHandSide)
            {
              var rhElement = MapObjectToBlock(psrElement, rhsBlock, init_context);
              rhElement.IndexInRule = 0;
              rhsBlock.Children.AddLast(rhElement);
            }
          }
        }
      }


      return psr;
    }


    public Block MapObjectToBlock(object model, Block parent, BlockInitializationContext initContext)
    {
      if (model == null) return null;
      var t = model.GetType();
      try
      {
        var mappingFunction = mappingDictionary[t];

        return mappingFunction(model, parent, initContext);
      }
      catch (KeyNotFoundException ex)
      {
        throw new InvalidOperationException("program cannot map the given kind of constraint to Block: " + t, ex);
      }
    }

    private Block MapConstraintTemplateToBlock(ConstraintTemplateModel model, Block parent,
                                               BlockInitializationContext initContext)
    {
      var block =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.CONSTRAINT_TEMPLATE, initContext) as ContainerBlock;
      if (block == null) return null;
      block.Parent = parent;

      var nameBlock = CreateBlockWithContentAndWrap(BlockGrammarLoadMethods.Names.CONSTRAINT_TEMPLATE_NAME, model.Name,
                                                    block, initContext, 0, false);
      var idBlock = CreateBlockWithContentAndWrap(BlockGrammarLoadMethods.Names.CONSTRAINT_TEMPLATE_ID, model.Id, block,
                                                  initContext, 1, false);

      block.Children.AddFirst(nameBlock);
      block.Children.AddAfter(nameBlock, idBlock);

      var expression = MapLogicalExpressionToBlock(model.LogicalExpression, block, initContext);
      expression.IndexInRule = 2;

      var expressionBlock = new LinkedListNode<Block>(expression);
      block.Children.AddAfter(idBlock, expressionBlock);

      foreach (var comment in model.Comments)
      {
        var commentBlock = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.COMMENT, initContext);
        commentBlock.IndexInRule = 3;
        commentBlock.Content = comment;
        commentBlock.Parent = block;
        block.Children.AddLast(commentBlock);
      }

      return block;
    }

    private LinkedListNode<Block> CreateBlockWithContentAndWrap(string typeName, object content, Block parent,
                                                                BlockInitializationContext context, int indexInRule,
                                                                bool removable)
    {
      var block = _grammar.ParseSelection(typeName, context, removable);
      block.Parent = parent;
      block.Content = content;
      block.IndexInRule = indexInRule;
      return new LinkedListNode<Block>(block);
    }

    private Block MapFeatureTemplateToBlock(FeatureTemplateModel model, Block parent,
                                            BlockInitializationContext init_content)
    {
      var block =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_TEMPLATE, init_content) as ContainerBlock;
      block.Parent = parent;


      var templateName = model.Left as FeatureTemplateNameModel;

      if (templateName != null)
      {
        var nameBlock =
          _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_TEMPLATE_NAME_ID, init_content, false, 0, block)
          as
          ContainerBlock;
        nameBlock[0].Content = templateName.Name; //FEATURE_TEMPLATE_NAME block

        if (!string.IsNullOrEmpty(templateName.Id))
        {
          var idBlock = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_TEMPLATE_ID, init_content, true, 1,
                                                nameBlock);
          idBlock.Content = templateName.Id;
          nameBlock.Children.AddLast(idBlock);
        }

        block.Children.AddLast(nameBlock);
      } else
      {
        var terminalRefBlock = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.TERMINAL_REF, init_content, false,
                                                       0, block);
        terminalRefBlock.Content = model.Left.Id;

        block.Children.AddLast(terminalRefBlock);
      }

      foreach (var rightValue in model.RightValues)
      {
        var rightBlock = MapObjectToBlock(rightValue, block, init_content);
        rightBlock.IndexInRule = 1;
        block.Children.AddLast(rightBlock);
      }


      return block;
    }


    private Block MapTemplateFeaturePathToBlock(TemplateFeaturePath model, Block parent,
                                                BlockInitializationContext context)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.TEMPLATE_FEATURE_PATH, context);
      block.Parent = parent;
      var start = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_START, context);
      start.Parent = block;
      start.Content = model.Start;
      block.AppendChild(start, 0);

      var end = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_END, context);
      end.Parent = block;
      end.Content = model.End;
      block.AppendChild(end, 1);

      if (model.Value != null) block.AppendChild(MapObjectToBlock(model.Value, block, context), 2);
      
      if (block.Attributes.ContainsKey(BlockAttributes.IS_DEFAULT))
      {
        var isDefaultAtt = block.Attributes[BlockAttributes.IS_DEFAULT] as BlockAttributeBoolean;
        if (isDefaultAtt != null)
        {
          isDefaultAtt.MyValue = model.IsDefault;
        }
      }
      

      return block;
    }

    private Block MapTemplateFeaturePathDisjunctionToBlock(FeatureTemplateFeaturePathDisjunction model, Block parent,
                                                           BlockInitializationContext context)
    {
      var block =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.TEMPLATE_FEATURE_PATH_DISJUNCTION, context) as
        ContainerBlock;
      if (block == null) return null;
      block.Parent = parent;
      block[0].Content = model.Start;
      block[1].Content = model.End;

      block.AppendChild(MapFeatureValueRefToBlock(model.LeftPath, block, context), 2);

      foreach (var valRef in model.RightPaths)
      {
        block.AppendChild(MapFeatureValueRefToBlock(valRef, block, context), 3);
      }

      return block;
    }

    private Block MapConstraintsToBlock(IEnumerable<ConstraintModelBase> constraints, Block parent,
                                        BlockInitializationContext context)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.CONSTRAINTS, context) as ContainerBlock;
      if (block == null) return null;

      foreach (var constr in constraints)
      {
        block.AppendChild(MapObjectToBlock(constr, block, context), 0);
      }

      block.Parent = parent;

      return block;
    }

    private Block ToConstraintDisjunction(ConstraintDisjunctionModel x, Block parent,
                                          BlockInitializationContext initContext)
    {
      var cdis =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.CONSTRAINT_DISJUNCTION, initContext) as ContainerBlock;
      cdis.Parent = parent;
      var left = cdis.Children.First;
      var firstRight = left.Next;
      var rightConstraintsCount = x.RightConstraints.Count;

      left.Value = MapConstraintsToBlock(x.LeftConstraints, cdis, initContext);

      firstRight.Value = MapConstraintsToBlock(x.RightConstraints[0], cdis, initContext);
      if (rightConstraintsCount > 1)
      {
        for (var i = 1; i < rightConstraintsCount; i += 1)
        {
          var next = new LinkedListNode<Block>(MapConstraintsToBlock(x.RightConstraints[i], cdis, initContext));
          cdis.Children.AddAfter(firstRight, next);
          firstRight = next;
        }
      }

      return cdis;
    }

    private Block ToConstraintBlock(ConstraintModelBase x, string blockTypeName, Block parent,
                                    BlockInitializationContext initContext)
    {
      var uniBlock = _grammar.ParseSelection(blockTypeName, initContext) as ContainerBlock;
      if (uniBlock == null) return null;
      uniBlock.Parent = parent;
      var first = uniBlock.Children.First;

      if (blockTypeName == BlockGrammarLoadMethods.Names.CONSTRAINT_DISJUNCTION)
      {
        var constrDis = x as ConstraintDisjunctionModel;
        if (constrDis == null) return null;

        first.Value = MapObjectToBlock(constrDis.LeftConstraints, uniBlock, initContext);

        foreach (var list in constrDis.RightConstraints)
        {
          uniBlock.AppendChild(MapObjectToBlock(list, uniBlock, initContext), 1);
        }
      } else
      {
        first.Value = MapObjectToBlock(x.left, uniBlock, initContext);
        var left = MapObjectToBlock(x.right, uniBlock, initContext);
        left.IndexInRule = 1;
        first.Next.Value = left;
      }
      foreach (var comment in x.comments)
      {
        var commentBlock = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.COMMENT, initContext);
        commentBlock.Content = comment;
        uniBlock.AppendChild(commentBlock, 1);
      }

      SetConstraintAttributes(uniBlock, x.use_when_debugging, x.enabled);

      return uniBlock;
    }

    private void SetConstraintAttributes(ContainerBlock constraint, bool useWhenDebugging, bool isEnabled)
    {
      var debuggingAdd = constraint.Attributes[BlockAttributes.USE_WHEN_DEBUGGING] as BlockAttributeBoolean;
      var enabled = constraint.Attributes[BlockAttributes.ENABLED] as BlockAttributeBoolean;
      if (debuggingAdd != null) debuggingAdd.MyValue = useWhenDebugging;
      if (enabled != null) enabled.MyValue = isEnabled;
    }

    private Block MapFeaturePathToBlock(FeaturePathModel x, Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_PATH, initContext) as ContainerBlock;
      if (block == null) return null;

      block.Parent = parent;
      block[0].Content = x.node.IdRef;
      block[1].Content = x.feature_start.IdRef;

      if (x.feature_end != null)
      {
        var featureEnd = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_END, initContext);
        featureEnd.Parent = block;
        featureEnd.Content = x.feature_end.IdRef;
        block.AppendChild(featureEnd, 2);
      }

      if (x.EmbeddedFeaturePath != null)
      {
        var embededPath = MapObjectToBlock(x.EmbeddedFeaturePath, block, initContext);
        block.AppendChild(embededPath, 3);
      }

      return block;
    }

    private Block MapEmbeddedFeaturePathToBlock(EmbeddedFeaturePath model, Block parent,
                                                BlockInitializationContext initContext)
    {
      var block =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.EMBEDDED_FEATURE_PATH, initContext) as ContainerBlock;
      if (block == null) return null;

      block.Parent = parent;
      block[0].Content = model.feature_start.IdRef;
      if (model.featrure_end != null) block[1].Content = model.featrure_end.IdRef;

      if (model.ChildPath != null) block.AppendChild(MapEmbeddedFeaturePathToBlock(model.ChildPath, block, initContext), 2);

      return block;
    }

    private Block MapSubcatPathToBlock(SubcatPathModel model, Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.SUBCAT_PATH, initContext) as ContainerBlock;
      if (block == null) return null;

      block.Parent = parent;
      block[0].Content = model.node.IdRef;
      if (model.feature_start != null)
      {
        var start = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_START, initContext);
        start.Content = model.feature_start.IdRef;
        start.Parent = block;
        block.AppendChild(start, 1);
      }

      if (model.feature_end != null)
      {
        var featEnd = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_END, initContext);
        featEnd.Content = model.feature_end.IdRef;
        featEnd.Parent = block;
        block.AppendChild(featEnd, 2);
      }
      if (model.subcat_end != SubcatEndValues.NoneValue)
      {
        var subcatEnd = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.SUBCAT_END, initContext);
        subcatEnd.Content = model.subcat_end == SubcatEndValues.First ? "First" : "Rest";
        subcatEnd.Parent = block;
        block.AppendChild(subcatEnd, 3);
      }
      if (model.feature_after_start != null)
      {
        var afterStart = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_AFTER_START, initContext);
        afterStart.Content = model.feature_after_start.IdRef;
        afterStart.Parent = block;
        block.AppendChild(afterStart, 4);
      }
      if (model.feature_after_end != null)
      {
        var afterEnd = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_AFTER_END, initContext);
        afterEnd.Content = model.feature_after_end.IdRef;
        afterEnd.Parent = block;
        block.AppendChild(afterEnd, 5);
      }

      return block;
    }

    private Block MapLexGlossRootGlossValueToBlock(FeatureTemplateTerminalValueModelBase valueModel, string typeName,
                                                   Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(typeName, initContext);
      block.Content = valueModel.Value;
      block.Parent = parent;
      return block;
    }

    private Block MapLexGlossRootGlossPathToBlock(FeatureTemplateTerminalValuePathModelBase valueModel, string typeName,
                                                  Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(typeName, initContext);
      block.Content = valueModel.Value;
      block.Parent = parent;
      return block;
    }

    private Block MapCatValueToBlock(CatValue cat, Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.CAT_VALUE, initContext);
      block.Content = cat.Symbol.IdRef;
      block.Parent = parent;
      return block;
    }

    private Block MapCatValuePathToBlock(CatValuePath cat, Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.CAT_VALUE_PATH, initContext);
      block.Content = cat.Symbol.IdRef;
      block.Parent = parent;

      return block;
    }

    private Block MapCategoryValueRefToBlock(CategoryValueRefModel refModel, Block parent,
                                             BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.CATEGORY_VALUE_REF, initContext);
      block.Content = refModel.value.IdRef;
      block.Parent = parent;
      return block;
    }

    private Block MapNoneValueToBlock(Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.NONE_VALUE, initContext);
      block.Parent = parent;

      return block;
    }

    private Block MapNoneRefToBlock(Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.NONE_REF, initContext);
      block.Parent = parent;

      return block;
    }

    private Block MapNoneValuePathToBlock(NoneValuePath model, Block parent, BlockInitializationContext initContext)
    {
      var none = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.NONE_VALUE_PATH, initContext);
      none.Content = model.Value;
      none.Parent = parent;
      return none;
    }

    private Block MapFeatureToBlock(LogicalFeatureModel feature, Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE, initContext) as ContainerBlock;
      if (block == null) return null;

      block.Parent = parent;
      block[0].Content = feature.feature_start.IdRef;
      block[1].Content = feature.feature_end.IdRef;

      if (!string.IsNullOrEmpty(feature.indexed_variable))
      {
        var indexVar = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.INDEX_VARIABLE, initContext);
        indexVar.Content = "[" + feature.indexed_variable + "]";
        indexVar.Parent = block;
        block.AppendChild(indexVar, 2);
      }

      if (feature.embedded_feature != null) block.AppendChild(MapFeatureToBlock(feature.embedded_feature, block, initContext), 3);

      return block;
    }


    private Block MapCollectionFeatureRefToBlock(CollectionFeatureRef model, Block parent,
                                                 BlockInitializationContext initContext)
    {
      var block =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.COLLECTION_FEATURE_REF, initContext) as ContainerBlock;
      if (block == null) return null;

      block.Parent = parent;
      block[0].Content = model.CollectionFeature;

      if (model.EmbeddedPath != null) block.AppendChild(MapObjectToBlock(model.EmbeddedPath, block, initContext), 1);

      return block;
    }

    private Block MapComplexFeatureRefToBlock(ComplexFeatureRef featureRef, Block parent,
                                              BlockInitializationContext initContext)
    {
      var block =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.COMPLEX_FEATURE_REF, initContext) as ContainerBlock;
      if (block == null) return null;

      block[0].Content = featureRef.Id;
      block.Parent = parent;

      foreach (var element in featureRef.Values)
      {
        block.AppendChild(MapObjectToBlock(element, block, initContext), 1);
      }

      return block;
    }

    private Block MapTemplateFeatureStructureToBlock(FeatureTemplateFeatureStructure structure, Block parent,
                                                     BlockInitializationContext initContext)
    {
      var block =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.TEMPLATE_FEATURE_STRUCTURE, initContext) as ContainerBlock;
      if (block == null) return null;

      block.Parent = parent;
      block.Children.Clear(); //remove default child added by BlockGrammar.

      foreach (var element in structure.Elements)
      {
        block.AppendChild(MapObjectToBlock(element, block, initContext), 0);
      }

      return block;
    }

    private Block MapTemplateFeatureDisjunctionToBlock(FeatureTemplateFeatureDisjunction disjunction, Block parent,
                                                       BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.TEMPLATE_FEATURE_DISJUNCTION, initContext);
      block.Parent = parent;
      block.AppendChild(MapObjectToBlock(disjunction.LeftTerm, block, initContext), 0);

      foreach (var rightElement in disjunction.RightTerms)
      {
        block.AppendChild(MapObjectToBlock(rightElement, block, initContext), 1);
      }

      return block;
    }

    private Block MapFeatureRefToBlock(FeatureRef featureRef, Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_REF, initContext) as ContainerBlock;
      if (block == null) return null;

      block.Parent = parent;
      block[0].Content = featureRef.Start;
      block[1].Content = featureRef.End;

      if (featureRef.Child != null) block.AppendChild(MapObjectToBlock(featureRef.Child, block, initContext), 2);

      return block;
    }

    private Block MapFeatureTemplateRefToBlock(FeatureFeatureTemplateRef model, Block parent,
                                               BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_TEMPLATE_REF, initContext);
      block.Parent = parent;
      block.Content = model.Id.IdRef;

      return block;
    }

    private Block MapLogicalExpressionToBlock(LogicalExpressionModel expr, Block parent,
                                              BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.LOGICAL_EXPRESSION, initContext);
      block.Parent = parent;
      block.AppendChild(MapObjectToBlock(expr.operation, block, initContext), 0);

      return block;
    }

    private Block MapUnaryExpressionToBlock(UnaryOperationModel model, Block parent,
                                            BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.UNARY_EXPRESSION, initContext) as ContainerBlock;
      if (block == null) return null;

      block.Parent = parent;
      block[0].Content = LogicalOperationNameMapper.map(model.OperationType);

      block.AppendChild(MapObjectToBlock(model.Factor1, block, initContext), 1);

      return block;
    }

    private Block MapBinaryExpressionToBlock(BinaryOperationModel model, Block parent,
                                             BlockInitializationContext initContext)
    {
      var block =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.BINARY_EXPRESSION, initContext) as ContainerBlock;
      if (block == null) return null;

      block.Parent = parent;
      block.AppendChild(MapObjectToBlock(model.Factor1, block, initContext), 0);
      block[1].Content = LogicalOperationNameMapper.map(model.OperationType);
      block.AppendChild(MapObjectToBlock(model.Factor2, block, initContext), 2);

      return block;
    }

    private Block MapSubcatFirstRestToBlock(SubcatFirstRest firstRest, Block parent,
                                            BlockInitializationContext initContext)
    {
      var block =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.SUBCAT_FIRST_REST_PAIR, initContext) as ContainerBlock;
      if (block == null) return null;

      block.Parent = parent;
      block[0].Clear();
      block[0].AppendChild(MapObjectToBlock(firstRest.First, block, initContext), 0);
      block[1].Clear();
      block[1].AppendChild(MapObjectToBlock(firstRest.Rest, block, initContext), 1);

      return block;
    }

    private Block MapSubcatRefToBlock(SubcatRef subcatRef, Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.SUBCAT_REF, initContext);
      block.Parent = parent;
      block.AppendChild(MapObjectToBlock(subcatRef.Value, block, initContext), 0);

      return block;
    }

    private Block MapSubcatDisjunctionToBlock(SubcatDisjunction disjunction, Block parent,
                                              BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.SUBCAT_DISJUNCTION, initContext);
      block.Parent = parent;
      block.AppendChild(MapObjectToBlock(disjunction.FirstFactor, block, initContext), 0);

      foreach (var factor in disjunction.OtherFactors)
      {
        block.AppendChild(MapObjectToBlock(factor, block, initContext), 1);
      }

      return block;
    }

    private Block MapFeatureValueDisjunctionToBlock(FeatureValueDisjunction disjunction, Block parent,
                                                    BlockInitializationContext initContext)
    {
      var block =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_VALUE_DISJUNCTION, initContext) as ContainerBlock;
      if (block == null) return null;

      block.Parent = parent;
      block[0].Content = disjunction.LeftElement.value.IdRef;
      block.Children.Remove(block.Children.First.Next);
      foreach (var refModel in disjunction.RightElements)
      {
        block.AppendChild(MapFeatureValueRefToBlock(refModel, block, initContext), 1);
      }

      return block;
    }

    private Block MapFeatureValueRefToBlock(FeatureValueRefModel ref_model, Block parent,
                                            BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.FEATURE_VALUE_REF, initContext);
      block.Parent = parent;
      block.Content = ref_model.value.IdRef;
      (block.Attributes[BlockAttributes.IS_DEFAULT] as BlockAttributeBoolean).MyValue = ref_model.IsDefault;
      return block;
    }

    private Block MapLogicalConstraintRefToBlock(LogicalConstraintRefModel constraintRef, Block parent,
                                                 BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.LOGICAL_CONSTRAINT_REF, initContext);
      block.Parent = parent;
      block.Content = constraintRef.constraint.IdRef;

      return block;
    }

    private Block MapLexicalRuleToBlock(LexicalRule model, Block parent, BlockInitializationContext initContext)
    {
      var block = _grammar.ParseSelection(BlockGrammarLoadMethods.Names.LEXICAL_RULE, initContext) as ContainerBlock;
      if (block == null) return null;
      block.Parent = parent;

      var nameNode = block.Children.First;
      if (nameNode != null)
      {
        nameNode.Value.Content = model.Name;

        var previousNode = nameNode;
        foreach (var definition in model.LexicalRuleDefinitions)
        {
          var defBlock = MapLexicalRuleDefinitionToBlock(definition, block, initContext);
          defBlock.IndexInRule = 1;
          var defNode = new LinkedListNode<Block>(defBlock);
          block.Children.AddAfter(previousNode, defNode);
          previousNode = defNode;
        }
      }

      return block;
    }

    private Block MapLexicalRuleDefinitionToBlock(LexicalRuleDefinition model, Block parent,
                                                  BlockInitializationContext initContext)
    {
      var definition =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.LEXICAL_RULE_DEFINITION, initContext) as ContainerBlock;
      // ReSharper disable PossibleNullReferenceException
      var firstPath = definition.Children.First; //won't ever be null unless someone changes the BlockGrammar
      // ReSharper restore PossibleNullReferenceException
      if (firstPath != null)
      {
        var firstPathBlock = MapLexicalRuleFeaturePathToBlock(model.Left, definition, initContext);
        firstPathBlock.Removable = false;
        firstPathBlock.IndexInRule = 0;
        firstPath.Value = firstPathBlock;

        var rightPath = firstPath.Next;
        if (rightPath != null)
        {
          Block rightPathBlock;
          var lexicalFeaturePath = model.RightElement as LexicalRuleFeaturePath;
          if (lexicalFeaturePath != null) rightPathBlock = MapLexicalRuleFeaturePathToBlock(lexicalFeaturePath, definition, initContext);
          else
          {
            rightPathBlock = MapFeatureValueRefToBlock(model.RightElement as FeatureValueRefModel, definition,
                                                       initContext);
          }

          rightPathBlock.Removable = false;
          rightPathBlock.IndexInRule = 1;
          rightPath.Value = rightPathBlock;
        }
      }

      return definition;
    }

    private Block MapLexicalRuleFeaturePathToBlock(LexicalRuleFeaturePath featurePath, Block parent,
                                                   BlockInitializationContext context)
    {
      var block =
        _grammar.ParseSelection(BlockGrammarLoadMethods.Names.LEXICAL_RULE_FEATURE_PATH, context) as ContainerBlock;
      block.Parent = parent;
      var featureStartNode = block.Children.First;
      if (featureStartNode != null)
      {
        featureStartNode.Value.Content = featurePath.Start;

        var featureEnd = featureStartNode.Next;
        if (featureEnd != null) featureEnd.Value.Content = featurePath.End;
      }

      return block;
    }
  }
}