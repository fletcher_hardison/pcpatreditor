using System;
using System.Collections.Generic;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using PCPATR.Editor.MasterLists.ViewModels;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  public class Block : ViewModelBase
  {
    private static readonly BlockGrammar _blockGrammar = BlockGrammar.GetInstance();

    public Block()
    {
      Removable = true;
      OkCommand = new DelegateCommand(TryExecuteOkAction);
      CancelCommand = new DelegateCommand(TryExecuteCancelAction);
    }

    public Block(string openingSymbol, string closingSymbol, bool removable)
      : this()
    {
      _openingSymbol = openingSymbol;
      _closingSymbol = closingSymbol;
      Removable = removable;
      BorderThickness = 0;
    }

    public int BorderThickness { get; set; }

    public Action<Block> RemoveAction { get; set; }

    public BlockInitializationContext InitializationContext { get; set; }

    private bool _showTypeName;
    public bool ShowTypeName
    {
      get { return _showTypeName; }
      set
      {
        _showTypeName = value;
        RaisePropertyChanged("ShowTypeName");
      }
    }

    private string _typeName;
    public string TypeName
    {
      get { return _typeName; }
      set
      {
        _typeName = value;
        RaisePropertyChanged("TypeName");
      }
    }

    private Dictionary<string, BlockAttribute> _attributes;
    public Dictionary<string, BlockAttribute> Attributes
    {
      get { return _attributes; }
      set
      {
        _attributes = value;
        RaisePropertyChanged("Attributes");
      }
    }

    public bool HasAttributesAndIsSelected { get { return (_attributes != null && _attributes.Count > 0 && _isBlockSelected); } }

    public int IndexInRule { get; set; }

    private object _content;
    public object Content
    {
      get { return _content; }
      set
      {
        _content = value;
        RaisePropertyChanged("Content");
      }
    }


    private Block _parent;
    public Block Parent
    {
      get { return _parent; }
      set
      {
        _parent = value;
        RaisePropertyChanged("Parent");
      }
    }

    private List<string> _insertList = new List<string>();
    public List<string> InsertList
    {
      get { return _insertList; }
      set
      {
        _insertList = value;
        RaisePropertyChanged("InsertList");
      }
    }

    private List<string> _insertNextList = new List<string>();
    public List<string> InsertNextList
    {
      get { return _insertNextList; }
      set
      {
        _insertNextList = value;
        RaisePropertyChanged("InsertNextList");
      }
    }

    private List<string> _insertBeforeList = new List<string>();
    public List<string> InsertBeforeList
    {
      get { return _insertBeforeList; }
      set
      {
        _insertBeforeList = value;
        RaisePropertyChanged("InsertBeforeList");
      }
    }

    public void SelectBlock()
    {
      IsBlockSelected = true;
      if (Parent != null)
      {
        Parent.SelectMe(this);
      } else
      {
        InitializationContext.RaiseSelectedBlockChanged(new LinkedListNode<Block>(this));
      }
    }

    private bool _isBlockSelected;
    public virtual bool IsBlockSelected
    {
      get { return _isBlockSelected; }
      set
      {
        if (value)
        {
          BlockHelperMethods.DeselectAllButMe(this);
        }
        if (Parent == null)
        {
          _isBlockSelected = false;
        }
        else
        {
          _isBlockSelected = value;
        }

        //CheckMenuVisibilities(value);
        RaisePropertyChanged("IsBlockSelected");
        RaisePropertyChanged("HasAttributesAndIsSelected");
      }
    }

    public Action<Block> EditAction { get; set; }

    public Action<Block> OkAction { get; set; }

    public Action<Block> CancelAction { get; set; }

    public DelegateCommand OkCommand { get; private set; }

    public DelegateCommand CancelCommand { get; private set; }

    public void TryExecuteRemoveAction()
    {
      if (RemoveAction != null)
      {
        RemoveAction(this);
      }
    }

    public void TryExecuteEditAction()
    {
      if (EditAction != null)
      {
        EditAction(this);
        IsEditContentVisibile = true;
      }
    }

    private void TryExecuteOkAction()
    {
      if (OkAction != null)
      {
        OkAction(this);
      }
      IsEditContentVisibile = false;
    }

    private void TryExecuteCancelAction()
    {
      if (CancelAction != null)
      {
        CancelAction(this);
      }
      IsEditContentVisibile = false;
    }

    //privides a place for the content needed to edit this block's content.
    private object _editContent;
    public object EditContent
    {
      get { return _editContent; }
      set
      {
        _editContent = value;
        RaisePropertyChanged("EditContent");
      }
    }

    // determines whether the EditContent is visible or not.
    private bool _isEditContentVisibile;
    public bool IsEditContentVisibile
    {
      get { return _isEditContentVisibile; }
      set
      {
        _isEditContentVisibile = value;
        RaisePropertyChanged("IsEditContentVisibile");
      }
    }

    public string EditActionName { get; set; }

    private string _openingSymbol = string.Empty;
    public string OpeningSymbol
    {
      get { return _openingSymbol; }
      set
      {
        _openingSymbol = value;
        RaisePropertyChanged("OpeningSymbol");
      }
    }

    private string _closingSymbol = string.Empty;
    public string ClosingSymbol
    {
      get { return _closingSymbol; }
      set
      {
        _closingSymbol = value;
        RaisePropertyChanged("ClosingSymbol");
      }
    }

    public bool Removable { get; set; }
    
    public bool CancelRemove { get; set; }


    public Block ParseInsertList(string selection)
    {

      IsBlockSelected = false;
      if (selection == "(edit)")
      {
        TryExecuteEditAction();

        return null;
      }
      else
      {
        return _blockGrammar.ParseSelection(selection, InitializationContext);
      }
    }




    protected void SelectParent()
    {
      if (Parent != null)
      {
        IsBlockSelected = false;
        Parent.IsBlockSelected = true;
      }

    }

    public virtual void SetInsertLists(Block child)
    {

    }

    protected virtual void InsertHere(Block replaceMe, Block me, bool starPresent)
    {

    }

    protected virtual void SelectMe(Block me)
    {

    }
    protected virtual void InsertBefore(Block beforeMe, Block me)
    {

    }

    protected virtual void InsertAfter(Block afterMe, Block me)
    {

    }

    protected virtual bool CheckForStarInRule(int myIndexInRule, string targetNameInRule)
    {
      return false;
    }

    public virtual void RemoveElement(Block me)
    {

    }

    public virtual Block FindSibling(Block me, bool moveLeftToRight, Predicate<Block> pred)
    {
      return Parent.FindSibling(me, moveLeftToRight, pred);
    }

    /// <summary>
    /// Sets the content of this Block
    /// </summary>
    /// <param name="content"></param>
    /// <param name="index"></param>
    public virtual void AppendChild(object content, int index)
    {
      Content = content;
    }

    public virtual void Clear()
    {
      Content = null;
    }

    public virtual void TryHandleKeyDown(object sender, KeyEventArgs e)
    {
      if (!IsBlockSelected)
      {
        e.Handled = false;
      }
      else
      {
        HandleKeyDown(sender, e);
      }
    }

    public void HandleKeyDown(object sender, KeyEventArgs e)
    {
      BlockHelperMethods.HandleBlockKeyDown(this, sender, e);
    }

    public override string ToString()
    {
      return TypeName;
    }
  }

  public class BlockAttribute : ViewModelBase
  {
    public string AttributeType { get; set; }
  }

  public class BlockAttributeId : BlockAttribute
  {
    private readonly NewIdServiceViewModel _idService;
    public BlockAttributeId(NewIdServiceViewModel idService)
    {
      _idService = idService;
      OkCommand = new DelegateCommand(OkCommandMethod);
      CancelCommand = new DelegateCommand(CancelCommandMethod);
    }

    private string _myValue;
    public string MyValue {
      get {return _myValue; }
      set {
        _myValue = value;
        RaisePropertyChanged("MyValue");
      }
    }
    
    public string ProposedValue
    {
      get { return _idService.ProposedId; }
      set
      {
        _idService.ProposedId = value;
        RaisePropertyChanged("ProposedValue");
        RaisePropertyChanged("IsValid");
      }
    }

    public bool IsValid
    {
      get { return _idService.IsValid; }

    }
    
    public DelegateCommand OkCommand{ get; private set;}
    
    public DelegateCommand CancelCommand { get; private set; }
    
    private void OkCommandMethod()
    {
      MyValue = ProposedValue;
      ProposedValue = string.Empty;
    }
    
    private void CancelCommandMethod()
    {
      ProposedValue = string.Empty;
    }
  }

  public class BlockAttributeBoolean : BlockAttribute
  {
    private bool _myValue;
    public bool MyValue
    {
      get { return _myValue; }
      set
      {
        _myValue = value;
        RaisePropertyChanged("MyValue");
      }
    }
  }
  
  //  public class BlockAttributeId : BlockAttribute
  //  {
  //    public BlockAttributeId(Action<BlockAttributeId> idSetter)
  //    {
  //      _setIdAction = idSetter;
  //      SetId = new DelegateCommand(TryExecuteAction);
  //    }
//
  //    private readonly Action<BlockAttributeId> _setIdAction;
//
  //    private string _myValue;
  //    public string MyValue {
  //      get {return _myValue; }
  //      set {
  //        _myValue = value;
  //        RaisePropertyChanged("MyValue");
  //      }
  //    }
//
  //    public DelegateCommand SetId {get; private set; }
//
  //    private void TryExecuteAction()
  //    {
  //      if (_setIdAction != null){
  //        _setIdAction(this);
  //      }
  //    }
  //  }
}