﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  public class ObservableLinkedList<T> : INotifyCollectionChanged, IEnumerable<T>
  {
    private readonly LinkedList<T> _internalList;

    [DebuggerStepThrough]
    public ObservableLinkedList()
    {
      _internalList = new LinkedList<T>();
    }

    [DebuggerStepThrough]
    public void AddFirst(LinkedListNode<T> node)
    {
      _internalList.AddFirst(node);
      RaiseCollectionChanged();
    }

    [DebuggerStepThrough]
    public void AddFirst(T value)
    {
      _internalList.AddFirst(value);
      RaiseCollectionChanged();
    }

    [DebuggerStepThrough]
    public void AddLast(LinkedListNode<T> node)
    {
      _internalList.AddLast(node);
      RaiseCollectionChanged();
    }

    [DebuggerStepThrough]
    public void AddLast(T value)
    {
      _internalList.AddLast(value);
      RaiseCollectionChanged();
    }

    [DebuggerStepThrough]
    public void AddAfter(LinkedListNode<T> node, T value)
    {
      _internalList.AddAfter(node, value);
      RaiseCollectionChanged();

    }

    [DebuggerStepThrough]
    public void AddAfter(LinkedListNode<T> node, LinkedListNode<T> value)
    {
      _internalList.AddAfter(node, value);
      RaiseCollectionChanged();

    }

    [DebuggerStepThrough]
    public void AddBefore(LinkedListNode<T> node, LinkedListNode<T> value)
    {
      _internalList.AddBefore(node, value);
      RaiseCollectionChanged();

    }

    [DebuggerStepThrough]
    public void AddBefore(LinkedListNode<T> node, T value)
    {
      _internalList.AddBefore(node, value);
      RaiseCollectionChanged();

    }

    [DebuggerStepThrough]
    public LinkedListNode<T> Find(T target)
    {
      return _internalList.Find(target);
    }

    [DebuggerStepThrough]
    public LinkedListNode<T> FindLast(T target)
    {
      return _internalList.FindLast(target);
    }

    [DebuggerStepThrough]
    public void Remove(T target)
    {
      _internalList.Remove(target);
      RaiseCollectionChanged();

    }

    [DebuggerStepThrough]
    public void Remove(LinkedListNode<T> target)
    {
      _internalList.Remove(target);
      RaiseCollectionChanged();
    }

    [DebuggerStepThrough]
    public void Replace(T target, T newValue)
    {
      var findResult = _internalList.Find(target);
      if (findResult != null)
      {
        findResult.Value = newValue;
      }

      RaiseCollectionChanged();
    }

    [DebuggerStepThrough]
    public void Clear()
    {
      _internalList.Clear();
      RaiseCollectionChanged();
    }


    public int Count
    {
      get { return _internalList.Count; }
    }


    public LinkedListNode<T> First
    {
      get { return _internalList.First; }
    }

    public LinkedListNode<T> Last
    {
      get { return _internalList.Last; }
    }
    
    [DebuggerStepThrough]
    public void RaiseCollectionChanged()
    {
      if (CollectionChanged != null)
      {
        CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
      }
    }






    public IEnumerator<T> GetEnumerator()
    {
      return _internalList.GetEnumerator();
    }

    public event NotifyCollectionChangedEventHandler CollectionChanged;

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
  }
}
