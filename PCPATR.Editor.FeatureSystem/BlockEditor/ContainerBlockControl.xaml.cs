using System.Windows.Controls;
using System.Windows.Input;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  /// <summary>
  /// Interaction logic for ContainerBlockControl.xaml
  /// </summary>
  public partial class ContainerBlockControl : UserControl
  {
    public ContainerBlockControl()
    {
      InitializeComponent();
    }

    private void ContainerBlockControl_OnMouseDown(object sender, MouseButtonEventArgs e)
    {
      var block = DataContext as ContainerBlock;
      if (block == null) return;

      block.SelectBlock();
    }
    
    void LboxChildList_KeyDown(object sender, KeyEventArgs e)
    {
      e.Handled = false;
      var block = DataContext as ContainerBlock;
      if (block == null) return;
      BlockHelperMethods.HandleKeyDownByFindingSelectedBlock(block, sender, e);
    }
  }
}
