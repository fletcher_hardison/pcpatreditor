using System;
using System.Collections.Generic;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  public class BlockGrammar
  {
    private static BlockGrammar _instance;
    public static BlockGrammar GetInstance()
    {
      if (_instance == null)
      {
        _instance = new BlockGrammar();
      }

      return _instance;
    }

    private BlockGrammar() { }

    private  readonly  Dictionary<string,Action<Block>> _editActionDictionary = new Dictionary<string, Action<Block>>();
    public Dictionary<string, Action<Block>> EditActionDictionary { get { return _editActionDictionary; } }

    private readonly Dictionary<string, Func<BlockInitializationContext,Block>> _grammarRules = new Dictionary<string, Func<BlockInitializationContext, Block>>();

    public Dictionary<string, Func<BlockInitializationContext,Block>> GrammarRules { get { return _grammarRules; } }

    public bool AddRule(string rule, Func<BlockInitializationContext,Block> initializer)
    {
      //var rightHandSide = GetRuleLeftHandSide(rule);
      if (_grammarRules.ContainsKey(rule))
      {
        return false;
      }

      _grammarRules.Add(rule, initializer);

      return true;
    }

    public string GetRuleLeftHandSide(string rule)
    {
      var index = rule.IndexOf("->");

      return rule.Substring(0, index).Trim();
    }

    public bool AddEditAction(string name, Action<Block> action)
    {
      if (_editActionDictionary.ContainsKey(name))
      {
        return false;
      }

      _editActionDictionary.Add(name, action);

      return true;
    }

    public Block ParseSelection(string selection, BlockInitializationContext context, bool removable)
    {
      return ParseSelection(selection, context, removable, null);
    }

    public Block ParseSelection(string selection, BlockInitializationContext context, bool removable, Action<Block> initializationMethod)
    {
      var cleanSelection = selection.Replace("*", string.Empty);
      if (GrammarRules.ContainsKey(cleanSelection))
      {
        var initializer = _grammarRules[cleanSelection];
        var result = initializer(context);
        result.Removable = removable;

        if (initializationMethod != null)
        {
          initializationMethod(result);
        }

        InjectEditAction(result);

        return result;
      }
      else
      {
        return null;
      }
    }

    private static readonly Func<int, Action<Block>> _injectIndex = i => f => f.IndexInRule = i;

    public Block ParseSelection(string selection, BlockInitializationContext context, bool removable, int indexInRule)
    {
      var block = ParseSelection(selection, context, removable, _injectIndex(indexInRule));      

      return block;
    }

    private static readonly Func<int, Block, Action<Block>>  _injectIndexAndParent = (index, parent) => b =>
    {
      b.IndexInRule = index;
      b.Parent = parent;
    };

    public Block ParseSelection(string selection, BlockInitializationContext context, bool removable, int indexInRule,
                                 Block parent)
    {
      return ParseSelection(selection, context, removable, _injectIndexAndParent(indexInRule, parent));
    }


    public Block ParseSelection(string selection, BlockInitializationContext context)
    {
      return ParseSelection(selection, context, true);
    }

    
    private void InjectEditAction(Block target)
    {
      if (target.EditAction == null & !string.IsNullOrEmpty(target.EditActionName))
      {
        if (EditActionDictionary.ContainsKey(target.EditActionName))
        {
          target.EditAction = _editActionDictionary[target.EditActionName];
        }
      }
    }
  }
}
