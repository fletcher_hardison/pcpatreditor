using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  public class ContainerBlock : Block
  {
    public ContainerBlock()
      : this(null, null, true, null)
    {

    }

    public ContainerBlock(string openingSymbol, string closingSymbol, bool removable, object listSeperator)
      : base(openingSymbol, closingSymbol, removable)
    {
      _listSeperator = listSeperator;
      AddInitialElementCommand = new DelegateCommand<string>(AddInitialElementMethod);
      SetInitialRuleList = new DelegateCommand(SetAddInitialElementListMethod);
    }

    public DelegateCommand<string> AddInitialElementCommand { get; private set; }

    public DelegateCommand SetInitialRuleList { get; private set; }

    private readonly object _listSeperator;
    public object ListSeperator { get { return _listSeperator; } }

    private ObservableLinkedList<Block> _children = new ObservableLinkedList<Block>();
    public ObservableLinkedList<Block> Children
    {
      get { return _children; }
      set
      {
        _children = value;
        RaisePropertyChanged("Children");
      }
    }

    private void InsertFirstChild(Block child)
    {
      if (Children.Count > 0) throw new InvalidOperationException("Cannot run InsertFirstChild when container block has children");
      child.IndexInRule = 0;
      child.Parent = this;
      Children.AddFirst(child);
      RaisePropertyChanged("HasChildren");
    }

    public bool HasChildren
    {
      get { return _children.Count > 0; }
    }

    public Block this[int index]
    {
      get { return _children.ElementAt(index); }
      set
      {
        int i = 0;

        LinkedListNode<Block> child = Children.First;        
        while (i < index + 1 & child != null)
        {
          child = child.Next;
          i += 1;
        }

        if (child == null)
        {
          _children.AddLast(value);
        }
        else
        {
          _children.AddAfter(child, value);
        }

      }
    }

    private string _grammarRule;
    public string GrammarRule
    {
      get { return _grammarRule; }
      set
      {
        _grammarRule = value;
        _ruleLists = BlockHelperMethods.ParseGrammarRule(_grammarRule);
      }
    }

    private bool _isVertical;
    public bool IsVertical
    {
      get { return _isVertical; }
      set
      {
        _isVertical = value;
        RaisePropertyChanged("IsVertical");
      }
    }
    private LinkedListNode<Block> _selectedNode;
    public LinkedListNode<Block> SelectedNode
    {
      get { return _selectedNode; }
      set
      {
        _selectedNode = value;
        RaisePropertyChanged("SelectedNode");
        InitializationContext.RaiseSelectedBlockChanged(_selectedNode);
        _selectedNode.Value.IsBlockSelected = true;
      }
    }

    private Block _selectedBlock;
    public Block SelectedBlock
    {
      get { return _selectedBlock; }
      set
      {
        if (_selectedBlock != null)
        {
          _selectedBlock.IsBlockSelected = false;
        }

        _selectedBlock = value;        
        SelectedNode = _children.Find(_selectedBlock);
        // SetInsertLists(_selectedBlock);
        //_selectedBlock.IsBlockSelected = true;
        RaisePropertyChanged("SelectedBlock");
      }
    }

    private List<List<string>> _ruleLists;
    public List<List<string>> RuleLists
    {
      get { return _ruleLists; }
      set { _ruleLists = value; }
    }

    public void RaiseChildrenChange()
    {
      Children.RaiseCollectionChanged();
      RaisePropertyChanged("Children");
    }

    public void DeselectChildren()
    {
      foreach (var child in Children)
      {
        child.IsBlockSelected = false;
      }
    }

    public Block GetFirstChildWithMatchingTypeNameAndIndexInRule(string typeName, int indexInRule)
    {
      foreach (var child in Children)
      {
        if (child.TypeName == typeName && child.IndexInRule == indexInRule)
        {
          return child;
        }
      }

      return null;
    }

    /// <summary>
    /// Allows Child to tell this to select it.
    /// </summary>
    /// <param name="me">child to select</param>
    protected override void SelectMe(Block me)
    {
      if (Children.Contains(me))
      {
        SelectedBlock = me;
      }
    }

    /// <summary>
    /// Adds a block to Children at with the specified index in this rule's rule list.
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="index"></param>
    public override void AppendChild(object obj, int index)
    {
      var child = obj as Block;
      if (child == null) return;
      child.Parent = this;
      child.IndexInRule = index;
      
      var meNode = Children.First;
      
      while (meNode != null)
      {
        if (meNode.Value.IndexInRule > index)
        {
          break;
        }
        meNode = meNode.Next;
      }

      if (meNode != null)
      {
        _children.AddBefore(meNode, child);
      }
      else
      {
        _children.AddLast(child);
      }
    }


    public override void RemoveElement(Block me)
    {
      if (_children.Count < 2) return; // why?
      me.Parent = null;
      me.IsBlockSelected = false;
      Children.Remove(me);
    }

    public void SelectChild(Block child)
    {
      IsBlockSelected = false;
      child.IsBlockSelected = true;
    }


    public override void Clear()
    {
      Children.Clear();
    }

    public override Block FindSibling(Block me, bool moveLeftToRight, Predicate<Block> pred)
    {
      var indexOfMe = Children.Find(me);

      if (moveLeftToRight)
      {
        var next = indexOfMe;
        while ((next = next.Next) != null)
        {
          if (pred(next.Value))
          {
            return next.Value;
          }
        }
      }
      else
      {
        var previous = indexOfMe;
        while ((previous = previous.Previous) != null)
        {
          if (pred(previous.Value))
          {
            return previous.Value;
          }
        }
      }

      return null;
    }

    public override void TryHandleKeyDown(object sender, KeyEventArgs e)
    {
      BlockHelperMethods.HandleKeyDownByFindingSelectedBlock(this, sender, e);
    }

    private void SetAddInitialElementListMethod()
    {
      if (InsertList.Count < 1)
      {
        SetInsertLists(this);
      }
    }

    private void AddInitialElementMethod(string elementToAdd)
    {
      if (string.IsNullOrEmpty(elementToAdd)) return;
      InsertFirstChild(ParseInsertList(elementToAdd));
    }



  }
}