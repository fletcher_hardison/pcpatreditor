﻿/*
 * Created by SharpDevelop.
 * User: TFHardison
 * Date: 3/11/2013
 * Time: 8:47 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  /// <summary>
  /// Interaction logic for BlockEditorControl.xaml
  /// </summary>
  public partial class BlockEditorControl : UserControl
  {
    public BlockEditorControl()
    {
      InitializeComponent();
    }

    private void BlockEditorControl_OnKeyDown(object sender, KeyEventArgs e)
    {
      var viewModel = DataContext as BlockEditorViewModel;
      if (viewModel == null) return;
      var con = viewModel.SelectedElement as ContainerBlock;
      if (con == null)
      {
        BlockHelperMethods.HandleBlockKeyDown(viewModel.SelectedElement, sender, e);
      } else
      {
        BlockHelperMethods.HandleContainerKeyDown(con, sender, e);
      }
    }
  }
}