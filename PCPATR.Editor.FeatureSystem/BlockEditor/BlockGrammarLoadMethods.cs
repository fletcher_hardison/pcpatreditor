﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Forms;
using Microsoft.Practices.Unity;
using PCPATR.Editor.MasterLists.Properties;
using PCPATR.Editor.MasterLists.Services;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  public static class BlockPsrHelperMethods
  {
    public static void ExtractSymbolRefIds(ContainerBlock startPoint, IList<string> list)
    {
      if (startPoint == null) return;

      var next = startPoint.Children.First;

      while (next != null)
      {
        if (next.Value.TypeName == BlockGrammarLoadMethods.Names.SYMBOL_REF)
        {
          list.Add(ExtracSymbolRefIdFromSymbolRef(next.Value));
        } else if (next.Value.TypeName == BlockGrammarLoadMethods.Names.LEFT_HAND_SIDE)
        {
          list.Add(ExtracSymbolRefIdFromLeftHandSide(next.Value));
        }

        ExtractSymbolRefIds(next.Value as ContainerBlock, list);
        next = next.Next;
      }
    }

    private static string ExtracSymbolRefIdFromLeftHandSide(Block value)
    {
      var con = value as ContainerBlock;
      if (con == null) return null;
      var idAtt = con.Children.Last;
      if (idAtt == null) return null;

      return idAtt.Value.Content as string;
    }

    private static string ExtracSymbolRefIdFromSymbolRef(Block symbolRef)
    {
      var idAtt = symbolRef.Attributes["Id"] as BlockAttributeId;
      if (idAtt == null) return null;

      return idAtt.MyValue;
    }
  }

  public static class BlockGrammarLoadMethods
  {
    public static Action<Block> RemoveIdfromIdService = block =>
    {
      var idService = block.InitializationContext.GetElement<IUnityContainer>().Resolve<NewIdService>();
      idService.RemoveId(block.Content as string);
    };



    public static class Names
    {
      public static string LEFT_HAND_SIDE_SYMBOL = "Left Hand Side Symbol";
      public const string PHRASE_STRUCTURE_RULE = "Phrase Structure Rule";
      public const string SYMBOL_REF_ID = "Symbol Ref Id";
      public const string LEFT_HAND_SIDE = "Left Hand Side";
      public const string RIGHT_HAND_SIDE = "Right Hand Side";
      public const string SYMBOL_REF = "Symbol Ref";
      public const string OPTIONS_SYMBOLS = "Optional Symbols";
      public const string DISJUNCTION = "Disjunction";
      public const string DISJUNCTIVE_SYMBOLS = "Disjunctive Symbols";


      public const string SYMBOL_REF_VALUE = "Symbol Ref Value";



      public const string LEXICAL_RULE_DEFINITION = "Lexical Rule Definition";
      public const string LEXICAL_RULE = "Lexical Rule";
      public const string LEXICAL_RULE_NAME = "Lexical Rule Name";
      public const string LEXICAL_RULE_FEATURE_PATH = "Lexical Rule Feature Path";
      public const string CONSTRAINT_TEMPLATE_NAME = "Constraint Template Name";
      public const string CONSTRAINT_TEMPLATE_ID = "Constraint Template Id";
      public const string FEATURE_TEMPLATE_ID = "Feature Template Id";
      public const string FEATURE_TEMPLATE_NAME_ID = "Feature Template Name Id";
      public const string TEMPLATE_FEATURE_PATH_DISJUNCTION = "Template Feature Path Disjunction";
      public const string TEMPLATE_FEATURE_PATH = "Template Feature Path";
      public const string TEMPLATE_FEATURE_PATH_START = "Template Feature Path Start";
      public const string TEMPLATE_FEATURE_PATH_END = "Template Feature Path End";
      public const string FEATURE_TEMPLATE = "Feature Template";
      public const string FEATURE_TEMPLATE_NAME = "Feature Template Name";
      public const string TERMINAL_REF = "Terminal Reference";


      public const string NONE_VALUE_PATH = "None Value Path";
      public const string LOGICAL_EXPRESSION_TYPE = "Logical Expression Type";
      public const string CONSTRAINTS = "Constraints";

      public const string COMMENT = "Comment";
      public const string EMPTY = "[]";

      public const string UNIFICATION_CONSTRAINT = "Unification Constraint";
      public const string PERCOLATION_OPERATION = "Percolation Operation";
      public const string LOGICAL_CONSTRAINT = "Logical Constraint";
      public const string CONSTRAINT_DISJUNCTION = "Constraint Disjunction";
      public const string PRIORITY_UNION_OPERATION = "Priority Union Operation";

      public const string ROOT_GLOSS_VALUE_PATH = "Root Gloss Value Path";
      public const string GLOSS_VALUE_PATH = "Gloss Value Path";
      public const string LEX_VALUE_PATH = "Lexical Value Path";
      public const string CAT_VALUE_PATH = "Category Value Path";
      public const string CATEGORY_VALUE_REF = "Category Reference in Psr";
      public const string NONE_VALUE = "None Value";

      public const string FEATURE = "Feature";
      public const string COLLECTION_FEATURE_REF = "Collection Feature Reference";
      public const string CAT_VALUE = "Category Value";
      public const string LEX_VALUE = "Lexical Value";
      public const string GLOSS_VALUE = "Gloss Value";
      public const string ROOT_GLOSS_VALUE = "Root Gloss Value";
      public const string INDEX_VARIABLE = "Index Variable";

      public const string COMPLEX_FEATURE_REF = "Complex Feature Reference";

      public const string COLLECTION_FEATURE_ID_REF = "Collection Feature Id Reference";
      public const string COMPLEX_FEATURE_ID_REF = "Complex Feature Id Reference";

      public const string TEMPLATE_FEATURE_DISJUNCTION = "Template Feature Disjunction";
      public const string NONE_REF = "none";
      public const string FEATURE_REF = "Feature Reference";
      public const string FEATURE_TEMPLATE_REF = "Feature Template Reference";
      public const string UNARY_EXPRESSION = "Unary Expression";
      public const string BINARY_EXPRESSION = "Binary Expression";
      public const string FACTOR = "Factor";


      public const string FEATURE_PATH = "Feature Path";
      public const string EMBEDDED_FEATURE_PATH = "Embedded Feature Path";
      public const string SUBCAT_PATH = "Subcat Path";

      public const string SUBCAT_REST = "Rest";
      public const string SUBCAT_FIRST = "First";
      public const string SUBCAT_REF = "SubcatRef";
      public const string SUBCAT_FIRST_REST_PAIR = "SubcatFirstRestPair";
      public const string SUBCAT_DISJUNCTION = "SubcatDisjunction";

      public const string FEATURE_START = "Path Start";
      public const string FEATURE_END = "Path End";
      public const string FEATURE_NODE = "Node";
      public const string SUBCAT_END = "Subcat";
      public const string FEATURE_AFTER_START = "Feature After Start";
      public const string FEATURE_AFTER_END = "Feature After End";

      public const string FEATURE_VALUE_DISJUNCTION = "Feature Value Disjunction";
      public const string FEATURE_VALUE_REF = "Feature Value Ref";

      public const string TEMPLATE_FEATURE_STRUCTURE = "Feature Structure";
      public const string LOGICAL_EXPRESSION = "Logical_Expression";
      public const string LOGICAL_CONSTRAINT_REF = "Logical Constraint Refernce";

      public const string CONSTRAINT_TEMPLATE = "Constraint Template";
    }

    public const string OR = " | ";
    public const string ARROW = " -> ";
    public const string COMMA = ", ";
    public const string OPEN_ANGLE = "<";
    public const string CLOSE_ANGLE = ">";
    public const string STAR = "*";

    private static Block SetParent(Block block, Block parent)
    {
      block.Parent = parent;

      return block;
    }

    private static string ALL_THREE = BuildRuleOrList(Names.SYMBOL_REF + STAR, Names.OPTIONS_SYMBOLS + STAR, Names.DISJUNCTION + STAR);

    private static Action<Block> RemoveSymbolRefAction = block =>
    {
      var result =
        System.Windows.Forms.MessageBox.Show(
          "Removing this element will break any references to or its content it in unification constraints. Continue?",
          "Warning", MessageBoxButtons.YesNo);
      if (result == DialogResult.No)
      {
        block.CancelRemove = true;
      }
    };

    public static void ConfigurePhraseStructureRuleAndElements(BlockGrammar grammar, IUnityContainer container)
    {

      grammar.AddRule(Names.SYMBOL_REF_VALUE, con =>
      {
        var refVal = new Block("val:", "", true)
        {
          TypeName = Names.SYMBOL_REF_VALUE,
          Content = "(symbol reference)",
          InitializationContext = con,
          EditActionName = Actions.SET_SYMBOL_REFERENCE,          
        };        
        return refVal;
      });

      grammar.AddRule(Names.SYMBOL_REF_ID, con =>
      {
        var refVal = new Block("id:", "", true)
        {
          TypeName = Names.SYMBOL_REF_ID,
          Content = "(symbol ref id)",
          InitializationContext = con,
          EditActionName = Actions.SET_SYMBOL_REF_ID,
          RemoveAction = RemoveIdfromIdService
        };

        return refVal;
      });

      AddBlockRule(grammar, Names.LEFT_HAND_SIDE_SYMBOL, "(left hand side symbol)", Actions.SET_LEFT_HAND_SIDE_SYMBOL);

      grammar.AddRule(Names.PHRASE_STRUCTURE_RULE, con =>
      {
        var psr = new ContainerBlock
        {
          TypeName = Names.PHRASE_STRUCTURE_RULE,
          InitializationContext = con,
          
          GrammarRule = Names.PHRASE_STRUCTURE_RULE + ARROW + Names.LEFT_HAND_SIDE + COMMA + Names.RIGHT_HAND_SIDE,
        };

        var lhs = new LinkedListNode<Block>(grammar.ParseSelection(Names.LEFT_HAND_SIDE, con, false, 0, psr));
        var rhs = grammar.ParseSelection(Names.RIGHT_HAND_SIDE, con, false, 1, psr);
        psr.Children.AddFirst(lhs);
        psr.Children.AddAfter(lhs, rhs);

        return psr;
      });

      grammar.AddRule(Names.LEFT_HAND_SIDE, con =>
      {
//        var lhs = new ContainerBlock
//        {
//          TypeName = Names.LEFT_HAND_SIDE,
//          IsVertical = true,
//          ClosingSymbol = "->",          
//          InitializationContext = con,
//          GrammarRule = Names.LEFT_HAND_SIDE + ARROW + Names.LEFT_HAND_SIDE_SYMBOL + COMMA + Names.SYMBOL_REF_ID
//        };
//
//        var symbolValue = new LinkedListNode<Block>(grammar.ParseSelection(Names.LEFT_HAND_SIDE_SYMBOL, con, false, 0, lhs));
//        var refId = grammar.ParseSelection(Names.SYMBOL_REF_ID, con, false, 1, lhs);
//
//        lhs.Children.AddFirst(symbolValue);
//        lhs.Children.AddAfter(symbolValue, refId);

        
        var symbolRef = new Block
        {
          TypeName = Names.SYMBOL_REF,
          InitializationContext = con,
          EditActionName = Actions.SET_LEFT_HAND_SIDE_SYMBOL,
          RemoveAction = RemoveSymbolRefAction,
          ClosingSymbol = "->"
          //GrammarRule = Names.SYMBOL_REF + ARROW + Names.SYMBOL_REF_VALUE + COMMA + Names.SYMBOL_REF_ID,          
        };
        
        symbolRef.Attributes = new Dictionary<string, BlockAttribute>{ {"Id", container.Resolve<BlockAttributeId>()}};
        
        symbolRef.Content = "(non_terminal ref)";
        //var valueBlock = new LinkedListNode<Block>(SetParent(grammar.ParseSelection(Names.SYMBOL_REF_VALUE, con, false, 0), symbolRef));
        //var idBlock = SetParent(grammar.ParseSelection(Names.SYMBOL_REF_ID, con, false, 1), symbolRef);

        //symbolRef.Children.AddFirst(valueBlock);
       // symbolRef.Children.AddAfter(valueBlock, idBlock);

        return symbolRef;

        //return lhs;
      });

      grammar.AddRule(Names.RIGHT_HAND_SIDE, con =>
      {
        var rhs = new ContainerBlock
        {
          TypeName = Names.RIGHT_HAND_SIDE,
          InitializationContext = con,
          GrammarRule = Names.RIGHT_HAND_SIDE + ARROW + ALL_THREE + OR + Names.COMMENT + STAR
        };

        return rhs;
      });

      grammar.AddRule(Names.SYMBOL_REF, con =>
      {
        var symbolRef = new Block
        {
          TypeName = Names.SYMBOL_REF,
          InitializationContext = con,
          EditActionName = Actions.SET_SYMBOL_REFERENCE,
          RemoveAction = RemoveSymbolRefAction
          //GrammarRule = Names.SYMBOL_REF + ARROW + Names.SYMBOL_REF_VALUE + COMMA + Names.SYMBOL_REF_ID,          
        };
        
        symbolRef.Attributes = new Dictionary<string, BlockAttribute>{ {"Id", container.Resolve<BlockAttributeId>()}};
        
        symbolRef.Content = "(symbol ref)";
        //var valueBlock = new LinkedListNode<Block>(SetParent(grammar.ParseSelection(Names.SYMBOL_REF_VALUE, con, false, 0), symbolRef));
        //var idBlock = SetParent(grammar.ParseSelection(Names.SYMBOL_REF_ID, con, false, 1), symbolRef);

        //symbolRef.Children.AddFirst(valueBlock);
       // symbolRef.Children.AddAfter(valueBlock, idBlock);

        return symbolRef;
      });



      grammar.AddRule(Names.OPTIONS_SYMBOLS, con =>
      {
        var optionals = new ContainerBlock
        {
          TypeName = Names.OPTIONS_SYMBOLS,
          InitializationContext = con,
          BorderThickness = 2,
          RemoveAction = RemoveSymbolRefAction,
          ShowTypeName = true,
          GrammarRule = Names.OPTIONS_SYMBOLS + ARROW + ALL_THREE
        };

        optionals.Children.AddFirst(grammar.ParseSelection(Names.SYMBOL_REF, con, true, 0, optionals));

        return optionals;
      });

      grammar.AddRule(Names.DISJUNCTION, con =>
      {
        var disjunction = new ContainerBlock
        {
          TypeName = Names.DISJUNCTION,
          InitializationContext = con,
          BorderThickness = 2,
          RemoveAction = RemoveSymbolRefAction,
          ShowTypeName = true,
          GrammarRule = Names.DISJUNCTION + ARROW + Names.SYMBOL_REF + STAR + OR + Names.OPTIONS_SYMBOLS + STAR + COMMA + Names.DISJUNCTIVE_SYMBOLS
        };

        disjunction.Children.AddFirst(grammar.ParseSelection(Names.SYMBOL_REF, con, true, 0, disjunction));

        return disjunction;
      });

      grammar.AddRule(Names.DISJUNCTIVE_SYMBOLS, con =>
      {
        var ds = new ContainerBlock("OR", string.Empty, true, string.Empty)
        {
          TypeName = Names.DISJUNCTIVE_SYMBOLS,
          InitializationContext = con,
          ShowTypeName = true,
          RemoveAction = RemoveSymbolRefAction,
          GrammarRule =
            Names.DISJUNCTIVE_SYMBOLS + ARROW + Names.SYMBOL_REF + STAR + OR + Names.OPTIONS_SYMBOLS + STAR +
            COMMA + Names.DISJUNCTIVE_SYMBOLS
        };

        ds.Children.AddFirst(grammar.ParseSelection(Names.SYMBOL_REF, con, true, 0, ds));

        return ds;
      });
    }

    public static void ConfigureLexicalRulesAndElements(BlockGrammar grammar)
    {
      grammar.AddRule(Names.LEXICAL_RULE, con =>
      {
        var rule = new ContainerBlock()
      {
        TypeName = Names.LEXICAL_RULE,
        InitializationContext = con,
        IsVertical = true,
        GrammarRule = Names.LEXICAL_RULE + ARROW + Names.LEXICAL_RULE_NAME + COMMA + Names.LEXICAL_RULE_DEFINITION + STAR
      };

        var nameBlock = grammar.ParseSelection(Names.LEXICAL_RULE_NAME, con, false, 0);
        nameBlock.Parent = rule;

        rule.Children.AddFirst(nameBlock);

        return rule;
      });


      AddBlockRule(grammar, Names.LEXICAL_RULE_NAME, "(lexical rule name)", Actions.SET_LEXICAL_RULE_NAME, RemoveIdfromIdService,"name:",null);

      grammar.AddRule(Names.LEXICAL_RULE_DEFINITION, context =>
      {
        var def = new ContainerBlock
        {
          TypeName = Names.LEXICAL_RULE_DEFINITION,
          InitializationContext = context,
          GrammarRule =
            Names.LEXICAL_RULE_DEFINITION + ARROW + Names.LEXICAL_RULE_FEATURE_PATH + COMMA +
            Names.LEXICAL_RULE_FEATURE_PATH + OR + Names.FEATURE_VALUE_REF + COMMA + Names.COMMENT
        };

        var firstPath = grammar.ParseSelection(Names.LEXICAL_RULE_FEATURE_PATH, context, false, 0);
        firstPath.Parent = def;
        var firstNode = new LinkedListNode<Block>(firstPath);

        var nextPath = grammar.ParseSelection(Names.LEXICAL_RULE_FEATURE_PATH, context, false, 1);
        nextPath.Parent = def;


        def.Children.AddFirst(firstNode);
        def.Children.AddAfter(firstNode, nextPath);

        return def;
      });

      AddBlockRule(grammar, Names.TEMPLATE_FEATURE_PATH_START, "(feature start)", Actions.SET_TEMPLATE_FEATURE_PATH_START);
      AddBlockRule(grammar, Names.TEMPLATE_FEATURE_PATH_END, "(feature end)", Actions.SET_TEMPLATE_FEATURE_PATH_END);

      grammar.AddRule(Names.LEXICAL_RULE_FEATURE_PATH, con =>
      {
        var path = new ContainerBlock
      {
        TypeName = Names.LEXICAL_RULE_FEATURE_PATH,
        InitializationContext = con,
        GrammarRule = Names.LEXICAL_RULE_FEATURE_PATH + ARROW + Names.TEMPLATE_FEATURE_PATH_START + COMMA + Names.TEMPLATE_FEATURE_PATH_END
      };

        var startNode = new LinkedListNode<Block>(grammar.ParseSelection(Names.TEMPLATE_FEATURE_PATH_START , con, false, 0, path));

        var featureEnd = grammar.ParseSelection(Names.TEMPLATE_FEATURE_PATH_END, con, false, 1, path);


        path.Children.AddFirst(startNode);
        path.Children.AddAfter(startNode, featureEnd);

        return path;
      });
    }

    public static void ConfigureConstraintTemplatesAndElements(BlockGrammar grammar)
    {
      grammar.AddRule(Names.CONSTRAINT_TEMPLATE, con =>
      {
        var template = new ContainerBlock
        {
          TypeName = Names.CONSTRAINT_TEMPLATE,
          IsVertical = true,
          GrammarRule = Names.CONSTRAINT_TEMPLATE + ARROW + Names.CONSTRAINT_TEMPLATE_NAME + COMMA + Names.CONSTRAINT_TEMPLATE_ID + COMMA + Names.LOGICAL_EXPRESSION + COMMA + Names.COMMENT + STAR,
          InitializationContext = con,
        };

        return template;
      });

      AddBlockRule(grammar, Names.CONSTRAINT_TEMPLATE_NAME, "(constraint template name)", Actions.SET_CONSTRAINT_TEMPLATE_NAME,null,"name:",null);
      AddBlockRule(grammar, Names.CONSTRAINT_TEMPLATE_ID, "(constraint template id)", Actions.SET_CONSTRAINT_TEMPLATE_ID, RemoveIdfromIdService,"id:", null);
    }

    public static void ConfigureFeatureStructureAndElements(BlockGrammar grammar)
    {
      AddBlockRule(grammar, Names.EMPTY, "[ ]", string.Empty);

      grammar.AddRule(Names.TEMPLATE_FEATURE_STRUCTURE, con =>
      {
        var fs = new ContainerBlock
        {
          TypeName = Names.TEMPLATE_FEATURE_STRUCTURE,
          IsVertical = true,
          InitializationContext = con,
          GrammarRule = Names.TEMPLATE_FEATURE_STRUCTURE + ARROW + BuildRuleOrList(Names.COMPLEX_FEATURE_REF + STAR, Names.FEATURE_REF + STAR, Names.FEATURE + STAR, Names.TEMPLATE_FEATURE_DISJUNCTION + STAR, Names.SUBCAT_REF + STAR, Names.CAT_VALUE + STAR, Names.LEX_VALUE + STAR, Names.GLOSS_VALUE + STAR, Names.ROOT_GLOSS_VALUE + STAR)
        };

        fs.AppendChild(grammar.ParseSelection(Names.FEATURE_REF, con, true), 0);

        return fs;
      });

      AddBlockRule(grammar, Names.COMPLEX_FEATURE_ID_REF, "(complex feature reference)", Actions.REFERENCE_COMPLEX_FEATURE_ID);

      grammar.AddRule(Names.COMPLEX_FEATURE_REF, con =>
      {
        var complex = new ContainerBlock
        {
          TypeName = Names.COMPLEX_FEATURE_REF,
          InitializationContext = con,
          GrammarRule =
            Names.COMPLEX_FEATURE_REF + ARROW + Names.COMPLEX_FEATURE_ID_REF + COMMA +
            BuildRuleOrList(Names.COMPLEX_FEATURE_REF + STAR, Names.FEATURE_REF + STAR, Names.COLLECTION_FEATURE_REF + STAR,
                            Names.TEMPLATE_FEATURE_DISJUNCTION + STAR, Names.SUBCAT_REF + STAR, Names.CAT_VALUE + STAR, Names.COMMENT + STAR),
        };
        complex.AppendChild(grammar.ParseSelection(Names.COMPLEX_FEATURE_ID_REF, con, false), 0);

        return complex;
      });

      grammar.AddRule(Names.TEMPLATE_FEATURE_DISJUNCTION, con =>
      {
        var tfd = new ContainerBlock
      {
        TypeName = Names.TEMPLATE_FEATURE_DISJUNCTION,
        ShowTypeName = true,
        InitializationContext = con,
        GrammarRule = Names.TEMPLATE_FEATURE_DISJUNCTION + ARROW + BuildRuleOrList(Names.TEMPLATE_FEATURE_STRUCTURE, Names.FEATURE_TEMPLATE_REF, Names.COLLECTION_FEATURE_REF, Names.NONE_VALUE, Names.FEATURE_VALUE_REF, Names.LEX_VALUE + STAR) + COMMA + BuildRuleOrList(Names.TEMPLATE_FEATURE_STRUCTURE + STAR, Names.FEATURE_TEMPLATE_REF + STAR, Names.COLLECTION_FEATURE_REF + STAR, Names.NONE_VALUE + STAR, Names.FEATURE_VALUE_REF + STAR, Names.LEX_VALUE + STAR)
      };

        return tfd;
      });

      grammar.AddRule(Names.SUBCAT_REF, con =>
      {
        var subcat = new ContainerBlock
      {
        TypeName = Names.SUBCAT_REF,
       
        GrammarRule = Names.SUBCAT_REF + ARROW + Names.NONE_REF + OR + Names.SUBCAT_FIRST_REST_PAIR + OR + Names.SUBCAT_DISJUNCTION,
        InitializationContext = con,
        Attributes = new Dictionary<string, BlockAttribute> { { BlockAttributes.IS_DEFAULT, new BlockAttributeBoolean { MyValue = false, AttributeType = BlockAttributes.IS_DEFAULT } } }

      };

        return subcat;
      });

      grammar.AddRule(Names.SUBCAT_FIRST_REST_PAIR, con =>
      {
        var firstRest = new ContainerBlock
        {
          TypeName = Names.SUBCAT_FIRST_REST_PAIR,
          InitializationContext = con,
          IsVertical  = true,
          GrammarRule = Names.SUBCAT_FIRST_REST_PAIR + ARROW + Names.SUBCAT_FIRST + COMMA + Names.SUBCAT_REST,
        };

        firstRest.AppendChild(grammar.ParseSelection(Names.SUBCAT_FIRST, con, false), 0);
        firstRest.AppendChild(grammar.ParseSelection(Names.SUBCAT_REST, con, false), 1);

        return firstRest;
      });
      grammar.AddRule(Names.SUBCAT_FIRST, con =>
      {
        var first = new ContainerBlock
      {
        TypeName = Names.SUBCAT_FIRST,
         ShowTypeName = true,
        InitializationContext = con,
        GrammarRule = Names.SUBCAT_FIRST + ARROW + BuildRuleOrList(Names.COMPLEX_FEATURE_REF, Names.FEATURE_REF, Names.TEMPLATE_FEATURE_DISJUNCTION, Names.NONE_REF, Names.FEATURE_TEMPLATE_REF, Names.TEMPLATE_FEATURE_STRUCTURE, Names.SUBCAT_DISJUNCTION)
      };

        return first;
      });

      grammar.AddRule(Names.SUBCAT_REST, con =>
      {
        var rest = new ContainerBlock
      {
        TypeName = Names.SUBCAT_REST,
         ShowTypeName = true,
        InitializationContext = con,
        GrammarRule = Names.SUBCAT_REST + ARROW + BuildRuleOrList(Names.COMPLEX_FEATURE_REF, Names.FEATURE_REF, Names.TEMPLATE_FEATURE_DISJUNCTION, Names.NONE_REF, Names.FEATURE_TEMPLATE_REF, Names.TEMPLATE_FEATURE_STRUCTURE, Names.SUBCAT_DISJUNCTION)
      };

        return rest;
      });

      AddBlockRule(grammar, Names.NONE_REF, "none", null);

      grammar.AddRule(Names.SUBCAT_DISJUNCTION, con =>
      {
        var disjunction = new ContainerBlock
      {
        TypeName = Names.SUBCAT_DISJUNCTION,
        InitializationContext = con,
        GrammarRule = Names.SUBCAT_DISJUNCTION + ARROW + Names.NONE_REF + OR + Names.SUBCAT_FIRST_REST_PAIR + COMMA + Names.NONE_REF + STAR + OR + Names.SUBCAT_FIRST_REST_PAIR + STAR
      };

        return disjunction;
      });
    }



    public static void ConfigureRuleConstraintsPathElements(BlockGrammar grammar)
    {
      grammar.AddRule(Names.FEATURE_NODE, context =>

        new Block
        {
          TypeName = Names.FEATURE_NODE,
          EditActionName = Actions.FEATURE_NODE_EDIT_ACTION,

          Content = "(symbol ref)",
          InitializationContext = context
        });

      grammar.AddRule(Names.FEATURE_START, con =>
                                       new Block
                                       {
                                         TypeName = Names.FEATURE_START,
                                         EditActionName = Actions.FEATURE_START_EDIT_ACTION,
                                         Content = "(feature)",
                                         InitializationContext = con
                                       });      

      grammar.AddRule(Names.FEATURE_END, con => new Block
      {
        TypeName = Names.FEATURE_END,
        EditActionName = Actions.FEATURE_END_EDIT_ACTION,

        Content = "(feature)",
        InitializationContext = con
      });

      grammar.AddRule(Names.FEATURE_PATH, con =>
        {
          var featurePath = new ContainerBlock(OPEN_ANGLE, CLOSE_ANGLE, true, string.Empty)
          {
            TypeName = Names.FEATURE_PATH,
            InitializationContext = con,
            GrammarRule = Names.FEATURE_PATH + ARROW + Names.FEATURE_NODE + COMMA + Names.FEATURE_START + COMMA + Names.FEATURE_END + COMMA + Names.EMBEDDED_FEATURE_PATH + OR + Names.CAT_VALUE_PATH + OR + Names.LEX_VALUE_PATH + OR + Names.GLOSS_VALUE_PATH + OR + Names.ROOT_GLOSS_VALUE_PATH
          };
          featurePath.AppendChild(grammar.ParseSelection(Names.FEATURE_NODE, con), 0);
          featurePath.AppendChild(grammar.ParseSelection(Names.FEATURE_START, con), 1);

          return featurePath;
        });

      grammar.AddRule(Names.EMBEDDED_FEATURE_PATH, con =>
      {
        var embedded = new ContainerBlock
        {
          TypeName = Names.EMBEDDED_FEATURE_PATH,
          InitializationContext = con,
          GrammarRule =
            Names.EMBEDDED_FEATURE_PATH + ARROW + Names.FEATURE_START + COMMA + Names.FEATURE_END + COMMA + Names.EMBEDDED_FEATURE_PATH
        };
        embedded.AppendChild(grammar.ParseSelection(Names.FEATURE_START, con, false), 0);
        embedded.AppendChild(grammar.ParseSelection(Names.FEATURE_END, con, false), 1);

        return embedded;
      });

      grammar.AddRule(Names.SUBCAT_END, con =>
      {
        var subEnd = new Block
        {
          TypeName = Names.SUBCAT_END,
          EditActionName = Actions.SUBCAT_END_EDIT_ACTION,
          Content = "(none)",
          InitializationContext = con
        };

        return subEnd;
      });

      grammar.AddRule(Names.FEATURE_AFTER_START, con =>
      {
        var fas = new Block
        {
          TypeName = Names.FEATURE_AFTER_START,
          EditActionName = Actions.FEATURE_AFTER_START_ACTION,
          Content = "(after start)",
          InitializationContext = con,
        };

        return fas;
      });

      grammar.AddRule(Names.FEATURE_AFTER_END, con =>
      {
        var fae = new Block
        {
          TypeName = Names.FEATURE_AFTER_END,
          EditActionName = Actions.FEATURE_AFTER_END_ACTION,
          Content = "(after end)",
          InitializationContext = con,
        };

        return fae;
      });

      grammar.AddRule(Names.SUBCAT_PATH, con =>
      {
        var subcatPath = new ContainerBlock
        {
          TypeName = Names.SUBCAT_PATH,
          InitializationContext = con,
          GrammarRule =
            Names.SUBCAT_PATH + ARROW + Names.FEATURE_NODE + COMMA + Names.FEATURE_START + COMMA + Names.FEATURE_END + COMMA + Names.SUBCAT_END +
            COMMA + Names.FEATURE_AFTER_START + COMMA + Names.FEATURE_AFTER_END,

        };
        subcatPath.AppendChild(grammar.ParseSelection(Names.FEATURE_NODE, con, false), 0);

        return subcatPath;
      });
    }




    public static void ConfigureFeatureElements(BlockGrammar grammar)
    {
      grammar.AddRule(Names.FEATURE_VALUE_REF, con =>
      {
        var fvr = new Block
        {
          TypeName = Names.FEATURE_VALUE_REF,
          Content = "(atomic feature reference)",
          EditActionName = Actions.REFERENCE_ATOMIC_FEATURE,
          InitializationContext = con,
          Attributes = new Dictionary<string, BlockAttribute> { { BlockAttributes.IS_DEFAULT, new BlockAttributeBoolean { MyValue = false, AttributeType = BlockAttributes.IS_DEFAULT } } }
        };

        return fvr;
      });

      grammar.AddRule(Names.FEATURE_VALUE_DISJUNCTION, con =>
      {
        var disjunction = new ContainerBlock("{", "}", true, "/")
        {
          TypeName = Names.FEATURE_VALUE_DISJUNCTION,
          InitializationContext = con,
          GrammarRule =
            Names.FEATURE_VALUE_DISJUNCTION + ARROW + Names.FEATURE_VALUE_REF + COMMA + Names.FEATURE_VALUE_REF + STAR
        };

        disjunction.AppendChild(grammar.ParseSelection(Names.FEATURE_VALUE_REF, con, false), 0);
        disjunction.AppendChild(grammar.ParseSelection(Names.FEATURE_VALUE_REF, con, false), 1);

        return disjunction;
      });

      grammar.AddRule(Names.LOGICAL_CONSTRAINT_REF, con =>
      {
        var logConstraintREf = new Block
        {
          TypeName = Names.LOGICAL_CONSTRAINT_REF,
          Content = "(logical constraint template reference)",
          InitializationContext = con,
          EditActionName = Actions.REFERENCE_LOGICAL_CONSTRAINT_TEMPLATE,          
        };

        return logConstraintREf;
      });

      AddBlockRule(grammar, Names.COLLECTION_FEATURE_ID_REF, "(collection feature reference)", Actions.REFERNCE_COLLECTION_FEATURE);

      grammar.AddRule(Names.COLLECTION_FEATURE_REF, con =>
      {
        var colRef = new ContainerBlock
        {
          TypeName = Names.COLLECTION_FEATURE_REF,
          InitializationContext = con,
          GrammarRule =
           Names.COLLECTION_FEATURE_REF + ARROW + Names.COLLECTION_FEATURE_ID_REF + COMMA +
            BuildRuleOrList(Names.NONE_REF, Names.CAT_VALUE, Names.LEX_VALUE, Names.GLOSS_VALUE, Names.ROOT_GLOSS_VALUE, Names.TEMPLATE_FEATURE_STRUCTURE,
                            Names.FEATURE_REF, Names.FEATURE_TEMPLATE_REF),
          Attributes = new Dictionary<string, BlockAttribute> { { BlockAttributes.IS_DEFAULT, new BlockAttributeBoolean { MyValue = false, AttributeType = BlockAttributes.IS_DEFAULT } } }
        };
        colRef.AppendChild(grammar.ParseSelection(Names.COLLECTION_FEATURE_ID_REF, con, false), 0);

        return colRef;
      });

      grammar.AddRule(Names.FEATURE_REF, con =>
      {
        var feature_ref = new ContainerBlock
        {
          TypeName = Names.FEATURE_REF,
          InitializationContext = con,
          GrammarRule = Names.FEATURE_REF + ARROW + Names.FEATURE_START + COMMA + Names.FEATURE_END + COMMA + BuildRuleOrList(Names.FEATURE_REF, Names.COLLECTION_FEATURE_REF, Names.TEMPLATE_FEATURE_DISJUNCTION, Names.FEATURE_TEMPLATE_REF),
          Attributes = new Dictionary<string, BlockAttribute> { { BlockAttributes.IS_DEFAULT, new BlockAttributeBoolean { MyValue = false, AttributeType = BlockAttributes.IS_DEFAULT } } }
        };

        feature_ref.AppendChild(grammar.ParseSelection(Names.FEATURE_START, con), 0);
        feature_ref.AppendChild(grammar.ParseSelection(Names.FEATURE_END, con), 1);

        return feature_ref;
      });

      AddBlockRule(grammar, Names.NONE_REF, "none", string.Empty);
      AddBlockRule(grammar, Names.FEATURE_TEMPLATE_REF, "(feature template reference)", Actions.REFERENCE_FEATURE_TEMPLATE);
    }

    public static string FactorList = BuildRuleOrList(Names.FEATURE, 
                                                      Names.COLLECTION_FEATURE_REF,
                                                      Names.COMPLEX_FEATURE_REF,
                                                      Names.LOGICAL_EXPRESSION, 
                                                      Names.CAT_VALUE, 
                                                      Names.LEX_VALUE,
                                                      Names.GLOSS_VALUE,
                                                      Names.ROOT_GLOSS_VALUE);



    public static void ConfigureLogicalConstraintTemplateElements(BlockGrammar grammar)
    {
      grammar.AddRule(Names.LOGICAL_EXPRESSION, con =>
      {
        var logExp = new ContainerBlock
        {
          TypeName = Names.LOGICAL_EXPRESSION,
          InitializationContext = con,
          GrammarRule = Names.LOGICAL_EXPRESSION + ARROW + Names.UNARY_EXPRESSION + OR + Names.BINARY_EXPRESSION
        };


        return logExp;
      });

      grammar.AddRule(Names.UNARY_EXPRESSION, con =>
      {
        var unExp = new ContainerBlock
        {
          TypeName = Names.UNARY_EXPRESSION,
          InitializationContext = con,
          GrammarRule = Names.UNARY_EXPRESSION + ARROW + Names.LOGICAL_EXPRESSION_TYPE + COMMA + FactorList
        };

        unExp.AppendChild(grammar.ParseSelection(Names.LOGICAL_EXPRESSION_TYPE, con, false), 0);
        return unExp;
      });



      grammar.AddRule(Names.BINARY_EXPRESSION, con =>
      {
        var binExp = new ContainerBlock
        {
          TypeName = Names.BINARY_EXPRESSION,
          InitializationContext = con,
          GrammarRule = Names.BINARY_EXPRESSION + ARROW + FactorList + COMMA + FactorList
        };

        binExp.AppendChild(grammar.ParseSelection(Names.LOGICAL_EXPRESSION_TYPE, con, false), 1);

        return binExp;
      });

      AddBlockRule(grammar, Names.LOGICAL_EXPRESSION_TYPE, "(expression type)", Actions.SET_LOGICAL_EXPRESSION_TYPE);
      AddBlockRule(grammar, Names.INDEX_VARIABLE, "[none]", Actions.SET_INDEX_VARIABLE);

      grammar.AddRule(Names.FACTOR, con =>
      {
        return new ContainerBlock
        {
          TypeName = Names.FACTOR,
          InitializationContext = con,
          GrammarRule = Names.FACTOR + ARROW +
            BuildRuleOrList(Names.FEATURE, Names.COLLECTION_FEATURE_REF, Names.LOGICAL_EXPRESSION, Names.CAT_VALUE, Names.LEX_VALUE, Names.GLOSS_VALUE,
                           Names.ROOT_GLOSS_VALUE)
        };
      });

      grammar.AddRule(Names.FEATURE, con =>
      {
        var feature = new ContainerBlock
        {
          TypeName = Names.FEATURE,
          InitializationContext = con,
          GrammarRule =
            Names.FEATURE + ARROW + BuildRuleCommaList(Names.FEATURE_START, Names.FEATURE_END, Names.INDEX_VARIABLE) +
            COMMA + Names.FEATURE
        };

        feature.AppendChild(grammar.ParseSelection(Names.FEATURE_START, con), 0);
        feature.AppendChild(grammar.ParseSelection(Names.FEATURE_END, con), 1);

        return feature;
      });
    }



    public static void ConfigureLexicalReferenceElements(BlockGrammar grammar)
    {
      grammar.AddRule(Names.CATEGORY_VALUE_REF, con =>
      {
        var cvr = new Block
        {
          TypeName = Names.CATEGORY_VALUE_REF,
          Content = "(syntatic catgory refernce in Psr)",
          EditActionName =Actions.FEATURE_NODE_EDIT_ACTION,
          InitializationContext = con
        };

        return cvr;
      });
      AddBlockRule(grammar, Names.CAT_VALUE, "(syntatic catgory reference)", Actions.REFERNECE_SYNTATIC_CATEGORY);
      AddBlockRule(grammar, Names.CAT_VALUE_PATH, "(syntatic category reference)", Actions.REFERNECE_SYNTATIC_CATEGORY);
      AddBlockRule(grammar, Names.LEX_VALUE, "(lexical value)", Actions.INSERT_LEXICAL_VALUE);
      AddBlockRule(grammar, Names.LEX_VALUE_PATH, "(lexical value)", Actions.INSERT_LEXICAL_VALUE);
      AddBlockRule(grammar, Names.GLOSS_VALUE, "(gloss value)", Actions.INSERT_GLOSS_VALUE);
      AddBlockRule(grammar, Names.GLOSS_VALUE_PATH, "(gloss value)", Actions.INSERT_GLOSS_VALUE);
      AddBlockRule(grammar, Names.ROOT_GLOSS_VALUE, "(root gloss value)", Actions.INSERT_ROOT_GLOSS_VALUE);
      AddBlockRule(grammar, Names.ROOT_GLOSS_VALUE_PATH, "(root gloss value)", Actions.INSERT_ROOT_GLOSS_VALUE);
      AddBlockRule(grammar, Names.NONE_VALUE, "none", string.Empty);
      AddBlockRule(grammar, Names.NONE_VALUE_PATH, "none", Actions.INSERT_NONE_VALUE_PATH_CONTENT);
    }

    public static Dictionary<string, BlockAttribute> CreateConstraintControlAttributeDictionary()
    {
      return new Dictionary<string, BlockAttribute>
      {
        {BlockAttributes.USE_WHEN_DEBUGGING, new BlockAttributeBoolean {MyValue = false, AttributeType = BlockAttributes.USE_WHEN_DEBUGGING}},
        {BlockAttributes.ENABLED, new BlockAttributeBoolean {MyValue = true, AttributeType = BlockAttributes.ENABLED}}
      };
    }

    public static void ConfigureRuleConstraints(BlockGrammar grammar)
    {
      const string UNIFICATION_RHS = Names.FEATURE_PATH + OR + Names.SUBCAT_PATH + OR + Names.FEATURE_VALUE_REF + 
                                     OR + Names.CATEGORY_VALUE_REF +
                                     OR + Names.LEX_VALUE_PATH + OR + Names.GLOSS_VALUE_PATH + OR + Names.ROOT_GLOSS_VALUE_PATH + OR +
                                     Names.NONE_VALUE + OR + Names.TEMPLATE_FEATURE_STRUCTURE;
      const string UNIFICATION_LHS = Names.FEATURE_PATH + OR + Names.SUBCAT_PATH;
      const string CONSTRAINTS_COMPONENTS =
        Names.PERCOLATION_OPERATION + STAR + OR + Names.UNIFICATION_CONSTRAINT + STAR + OR + Names.PRIORITY_UNION_OPERATION +
        STAR + OR + Names.LOGICAL_CONSTRAINT + STAR + OR + Names.CONSTRAINT_DISJUNCTION + STAR + OR + Names.COMMENT + STAR;

      grammar.AddRule(Names.CONSTRAINTS, con =>

        new ContainerBlock
        {
          TypeName = Names.CONSTRAINTS,
          IsVertical = true,
          ShowTypeName = true,
          InitializationContext = con,
          GrammarRule = Names.CONSTRAINTS + ARROW + CONSTRAINTS_COMPONENTS
        });

      grammar.AddRule(Names.UNIFICATION_CONSTRAINT, con =>
      {
        var unification = new ContainerBlock
        {
          TypeName = Names.UNIFICATION_CONSTRAINT,
          InitializationContext = con,
          ShowTypeName = true,
          GrammarRule = Names.UNIFICATION_CONSTRAINT + ARROW + UNIFICATION_LHS + COMMA + UNIFICATION_RHS,
          Attributes = CreateConstraintControlAttributeDictionary()
        };

        unification.AppendChild(grammar.ParseSelection(Names.FEATURE_PATH, con, false), 0);
        unification.AppendChild(grammar.ParseSelection(Names.FEATURE_PATH, con, false), 1);

        return unification;
      });

      grammar.AddRule(Names.PERCOLATION_OPERATION, con =>
     {
       var percolation = new ContainerBlock
       {
         TypeName = Names.PERCOLATION_OPERATION,
         InitializationContext = con,
         ShowTypeName = true,
         GrammarRule = Names.PERCOLATION_OPERATION + ARROW + UNIFICATION_LHS + COMMA + UNIFICATION_RHS,
         Attributes = CreateConstraintControlAttributeDictionary()
       };

       percolation.AppendChild(grammar.ParseSelection(Names.FEATURE_PATH, con, false), 0);
       percolation.AppendChild(grammar.ParseSelection(Names.FEATURE_PATH, con, false), 1);

       return percolation;
     });

      grammar.AddRule(Names.PRIORITY_UNION_OPERATION, con =>
      {
        var puo = new ContainerBlock
        {
          TypeName = Names.PRIORITY_UNION_OPERATION,
          InitializationContext = con,
          ShowTypeName = true,
          GrammarRule = Names.PRIORITY_UNION_OPERATION + ARROW + Names.FEATURE_PATH + OR + Names.SUBCAT_PATH + COMMA + Names.FEATURE_PATH + OR + Names.FEATURE_VALUE_REF + OR + Names.CATEGORY_VALUE_REF + OR + Names.FEATURE_VALUE_DISJUNCTION + OR + Names.NONE_VALUE + OR + Names.TEMPLATE_FEATURE_STRUCTURE + COMMA + Names.COMMENT + STAR,
          Attributes = CreateConstraintControlAttributeDictionary()
        };

        puo.AppendChild(grammar.ParseSelection(Names.FEATURE_PATH, con, false), 0);
        puo.AppendChild(grammar.ParseSelection(Names.FEATURE_PATH, con, false), 1);

        return puo;
      });

      grammar.AddRule(Names.LOGICAL_CONSTRAINT, con =>
      {
        var lc = new ContainerBlock
        {
          TypeName = Names.LOGICAL_CONSTRAINT,
          InitializationContext = con,
          ShowTypeName = true,
          GrammarRule = Names.LOGICAL_CONSTRAINT + ARROW + Names.FEATURE_PATH + COMMA + Names.LOGICAL_EXPRESSION + OR + Names.LOGICAL_CONSTRAINT_REF + COMMA + Names.COMMENT,
          Attributes = CreateConstraintControlAttributeDictionary()
        };

        lc.AppendChild(grammar.ParseSelection(Names.FEATURE_PATH, con, false), 0);
        lc.AppendChild(grammar.ParseSelection(Names.LOGICAL_EXPRESSION, con, false), 1);

        return lc;
      });

      grammar.AddRule(Names.CONSTRAINT_DISJUNCTION, con =>
      {
        var cd = new ContainerBlock("{", "}", true, "/")
        {
          TypeName = Names.CONSTRAINT_DISJUNCTION,
          InitializationContext = con,
          ShowTypeName = true,
          IsVertical = true,
          GrammarRule = Names.CONSTRAINT_DISJUNCTION + ARROW + Names.CONSTRAINTS + COMMA + Names.CONSTRAINTS + STAR,
          Attributes = CreateConstraintControlAttributeDictionary()
        };

        cd.AppendChild(grammar.ParseSelection(Names.CONSTRAINTS, con, false), 0);
        cd.AppendChild(grammar.ParseSelection(Names.CONSTRAINTS, con, false), 1);

        return cd;
      });
    }

    public static void ConfigureFeatureTemplates(BlockGrammar grammar)
    {
      grammar.AddRule(Names.FEATURE_TEMPLATE, con =>
      {
        var ft = new ContainerBlock("[", "]", true, null)
        {
          TypeName = Names.FEATURE_TEMPLATE,
          InitializationContext = con,
          ShowTypeName = true,
          IsVertical = true,
          GrammarRule = Names.FEATURE_TEMPLATE + ARROW + BuildRuleOrList(Names.FEATURE_TEMPLATE_NAME_ID, Names.TERMINAL_REF) + COMMA +
                        BuildRuleOrList(Names.TEMPLATE_FEATURE_STRUCTURE + STAR,
                                        Names.TEMPLATE_FEATURE_DISJUNCTION + STAR, Names.TEMPLATE_FEATURE_PATH + STAR,
                                        Names.TEMPLATE_FEATURE_PATH_DISJUNCTION + STAR,
                                        Names.FEATURE_TEMPLATE_REF + STAR, Names.CAT_VALUE_PATH + STAR,
                                        Names.LEX_VALUE_PATH + STAR, Names.GLOSS_VALUE_PATH + STAR,
                                        Names.ROOT_GLOSS_VALUE + STAR, Names.EMPTY),
          Attributes = CreateConstraintControlAttributeDictionary()

        };

        return ft;
      });

      grammar.AddRule(Names.FEATURE_TEMPLATE_NAME_ID, con =>
      {
        var tn = new ContainerBlock
        {
          TypeName = Names.FEATURE_TEMPLATE_NAME_ID,
          InitializationContext = con,
          GrammarRule = Names.FEATURE_TEMPLATE_NAME_ID + ARROW + Names.FEATURE_TEMPLATE_NAME + COMMA + Names.FEATURE_TEMPLATE_ID
        };
        tn.AppendChild(grammar.ParseSelection(Names.FEATURE_TEMPLATE_NAME, con, false), 0);

        return tn;
      });

      AddBlockRule(grammar, Names.FEATURE_TEMPLATE_NAME, "(feature template name)", Actions.SET_FEATURE_TEMPLATE_NAME,null, "name:", null);
      AddBlockRule(grammar, Names.FEATURE_TEMPLATE_ID, "(feature template id)", Actions.SET_FEATURE_TEMPLATE_ID, RemoveIdfromIdService, "id:", null);
      
      grammar.AddRule(Names.TEMPLATE_FEATURE_PATH, con =>
      {
        var tfp = new ContainerBlock
        {
          TypeName = Names.TEMPLATE_FEATURE_PATH,
          GrammarRule =
            Names.TEMPLATE_FEATURE_PATH + ARROW + Names.FEATURE_START + COMMA + Names.FEATURE_END + COMMA +
            BuildRuleOrList(Names.TEMPLATE_FEATURE_PATH, Names.CAT_VALUE_PATH, Names.NONE_VALUE_PATH,
                            Names.GLOSS_VALUE_PATH, Names.LEX_VALUE_PATH, Names.ROOT_GLOSS_VALUE_PATH),
          InitializationContext = con,
          Attributes = new Dictionary<string, BlockAttribute> { { BlockAttributes.IS_DEFAULT, new BlockAttributeBoolean { MyValue = false, AttributeType = BlockAttributes.IS_DEFAULT } } }
        };

        return tfp;
      });

      grammar.AddRule(Names.TEMPLATE_FEATURE_PATH_DISJUNCTION, con =>
      {
        var disjunction = new ContainerBlock("{", "}", true, "/")
        {
          TypeName = Names.TEMPLATE_FEATURE_PATH_DISJUNCTION,
          GrammarRule =
            Names.TEMPLATE_FEATURE_PATH_DISJUNCTION + ARROW + Names.FEATURE_START + COMMA + Names.FEATURE_END + COMMA +
            Names.FEATURE_VALUE_REF +
            COMMA +
            Names.FEATURE_VALUE_REF + STAR,
          InitializationContext = con
        };
        disjunction.AppendChild(grammar.ParseSelection(Names.FEATURE_START, con), 0);
        disjunction.AppendChild(grammar.ParseSelection(Names.FEATURE_END, con), 1);

        return disjunction;
      });

      AddBlockRule(grammar, Names.TERMINAL_REF, "(terminal symbol ref)", Actions.SET_TERMINAL_SYMBOL);
      AddBlockRule(grammar, Names.COMMENT, "(comment)", Actions.SET_COMMENT);
    }

    private static void AddBlockRule(BlockGrammar grammar, string typeName, string content, string editActionName)
    {
      grammar.AddRule(typeName, con =>
      {
        return new Block
        {
          TypeName = typeName,
          Content = content,
          EditActionName = editActionName,
          InitializationContext = con
        };
      });
    }

    private static void AddBlockRule(BlockGrammar grammar, string typeName, string content, string editActionName, Action<Block> removeAction, string startSymbol, string closeSymbol)
    {
      grammar.AddRule(typeName, con =>
      {
        return new Block
        {
          TypeName = typeName,
          Content = content,
          EditActionName = editActionName,
          ClosingSymbol = closeSymbol,
          OpeningSymbol = startSymbol,
          InitializationContext = con,          
          RemoveAction = removeAction
        };
      });
    }

    private static void AddBlockRule(BlockGrammar grammar, string typeName, string content, string editActionName, Action<Block> removeAction)
    {
      grammar.AddRule(typeName, con =>
      {
        return new Block
        {
          TypeName = typeName,
          Content = content,
          EditActionName = editActionName,
          InitializationContext = con,
          RemoveAction = removeAction
        };
      });
    }

    public static string BuildRuleOrList(params string[] list)
    {
      var result = string.Empty;
      foreach (var str in list)
      {
        result += str + OR;
      }

      return result.Substring(0, result.Length - OR.Length);
    }

    public static string BuildRuleCommaList(params string[] list)
    {
      var result = string.Empty;
      foreach (var str in list)
      {
        result += str + COMMA;
      }

      return result.Substring(0, result.Length - COMMA.Length);
    }


    public static class Actions
    {
      public const string SET_TEMPLATE_FEATURE_PATH_START = "Set Template Feature Path Start";
      public const string SET_TEMPLATE_FEATURE_PATH_END = "Set Template Feature Path End";
      public const string SET_LEFT_HAND_SIDE_SYMBOL = "Set Left Hand Side Symbol";
      public const string SET_SYMBOL_REF_ID = "Set Symbol Ref Id";
      public const string SET_SYMBOL_REFERENCE = "Set Symbol Reference";
      public const string SET_LEXICAL_RULE_NAME = "Set Lexical Rule Name";
      public const string SET_COMMENT = "Set Comment";
      public const string SET_CONSTRAINT_TEMPLATE_ID = "Set Constraint Template Id";
      public const string SET_CONSTRAINT_TEMPLATE_NAME = "Set Constraint Template Name";
      public const string SET_TERMINAL_SYMBOL = "Choose Terminal Symbol";
      public const string SET_FEATURE_TEMPLATE_ID = "Set Feature Template Id";
      public const string SET_FEATURE_TEMPLATE_NAME = "Set Feature Template Name";
      public const string INSERT_NONE_VALUE_PATH_CONTENT = "Set None Value Path Content";
      public const string REFERENCE_ATOMIC_FEATURE = "Reference Atomic Feature Action";
      public const string REFERENCE_LOGICAL_CONSTRAINT_TEMPLATE = "Reference Logical Constraint Template Action";
      public const string SET_INDEX_VARIABLE = "Set Index Variable";
      public const string REFERNCE_COLLECTION_FEATURE = "Reference Collection Feature";
      public const string SET_LOGICAL_EXPRESSION_TYPE = "Set Logical Expresion Type";
      public const string REFERNECE_SYNTATIC_CATEGORY = "Refernce Syntatic Category Action";
      public const string INSERT_LEXICAL_VALUE = "Insert Lexical Value";
      public const string INSERT_GLOSS_VALUE = "Insert Gloss Value";
      public const string INSERT_ROOT_GLOSS_VALUE = "Insert Root Gloss Value";
      public const string REFERENCE_FEATURE = "Reference Feature";
      public const string REFERENCE_FEATURE_TEMPLATE = "Reference Feature Template";
      public const string CHOOSE_UNARY_EXPRESSION_TYPE = "Choose Unary Expression Type Action";
      public const string CHOOSE_BINARY_EXPRESSION_TYPE = "Choose Binary Expression Type Action";
      public const string LOGICAL_EXPRESSION_TYPE = "Logical Expression Type";
      public const string REFERENCE_COMPLEX_FEATURE_ID = "Reference Complex Feature Id";
      public const string FEATURE_START_EDIT_ACTION = "FeatureStartEditAction";
      public const string FEATURE_AFTER_END_ACTION = "Feature After End Action";
      public const string FEATURE_AFTER_START_ACTION = "Feature After Start Action";
      public const string SUBCAT_END_EDIT_ACTION = "Subcat End Edit Action";
      public const string FEATURE_NODE_EDIT_ACTION = "FeatureNodeEditAction";
      public const string FEATURE_END_EDIT_ACTION = "FeatureEndEditAction";


    }
  }


  public static class BlockAttributes
  {
    public const string SYMBOL_REF_ID = "Symbol Ref Id";
    public const string USE_WHEN_DEBUGGING = "Use When Debugging";
    public const string ENABLED = "Enabled";
    public const string IS_DEFAULT = "Is Default";
  }
}
