using System;
using System.Collections.Generic;
using System.Windows.Input;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  public static class BlockHelperMethods
  {
    public static void HandleContainerKeyDown(ContainerBlock me, object sender, KeyEventArgs e)
    {
      switch (e.Key)
      {
        case Key.Down:
          me.SelectChild(me.Children.First.Value);          
          break;
        default:
          HandleBlockKeyDown(me, sender, e);
          break;
      }
    }

    public static void HandleKeyDownByFindingSelectedBlock(Block me, object sender, KeyEventArgs e)
    {
      var block = FindSelectedBlockFromTop(me);
      var con = block as ContainerBlock;
      if (con != null)
      {
        HandleContainerKeyDown(con, sender, e);
      }
      else
      {
        HandleBlockKeyDown(block, sender, e);
      }
    }

    //public static void TryHandleContainerKeyDown(ContainerBlock me, object sender, KeyEventArgs e)
    //{
    //  if (me.IsBlockSelected)
    //  {
    //    HandleContainerKeyDown(me, sender, e);
    //  }
    //  else
    //  {
    //    foreach (var child in me.Children)
    //    {
    //      child.TryHandleKeyDown(sender, e);
    //      if (e.Handled) return;
    //    }
    //  }
    //}



    public static void HandleBlockKeyDown(Block me, object sender, KeyEventArgs e)
    {
      if (me == null)
      {
        Console.WriteLine("Block was null when handling key down");
        return;
      }

      if (me.Parent != null)
      {
        switch (e.Key)
        {
          case Key.Up:
            DeselectAllButMe(me.Parent);
            me.Parent.IsBlockSelected = true;
            e.Handled = true;
            break;
          case Key.Left:
            me.IsBlockSelected = false;
            SelectSibling(me, false);
            break;
          case Key.Right:
            me.IsBlockSelected = false;
            SelectSibling(me, true);
            break;
          case Key.Delete:
            if (me.Removable)
            {
              me.TryExecuteRemoveAction();
              if (!me.CancelRemove)
              {
                me.Parent.RemoveElement(me);
              }
              me.CancelRemove = false;
            }
            break;
        }
      }

      //e.Handled = true;
    }


    private static void SelectSibling(Block me, bool moveRight)
    {
      var parent = me.Parent as ContainerBlock;
      if (parent == null) return;
      var nextNode = parent.Children.Find(me);
      if (nextNode != null)
      {
        if (moveRight)
        {
          if (nextNode.Next != null)
          {
            parent.SelectedBlock = nextNode.Next.Value;
          }
        }
        else
        {
          if (nextNode.Previous != null)
          {
            parent.SelectedBlock = nextNode.Previous.Value;
          }
        }
      }
      else
      {
        Console.WriteLine("Next node was null");
      }

    }

    public static List<List<string>> ParseGrammarRule(string rule)
    {
      var outerList = new List<List<string>>();

      if (string.IsNullOrEmpty(rule))
      {
        return outerList;
      }

      var rhs = rule.Substring(rule.IndexOf("->") + 2);
      var ruleRhsList = rhs.Split(',');

      foreach (var s in ruleRhsList)
      {
        var childList = new List<string>();
        if (s.Contains("|"))
        {
          var options = s.Split('|');
          foreach (var option in options)
          {

            childList.Add(option.Trim());
          }
        }
        else
        {
          childList.Add(s.Trim());
        }
        outerList.Add(childList);
      }

      return outerList;
    }

    /// <summary>
    /// Returns the index of the first string in the rule that contains the target string
    /// </summary>
    /// <param name="list"></param>
    /// <param name="tagert"></param>
    /// <returns></returns>
    public static int SearchList(IEnumerable<string> list, string tagert)
    {
      var index = -1;
      foreach (var str in list)
      {
        index += 1;
        if (str.Contains(tagert)) { return index; }
      }

      return -1;
    }


    public static List<string> CleanList(IEnumerable<string> list, string toRemove)
    {
      var result = new List<string>();

      foreach (var str in list)
      {
        result.Add(str.Replace(toRemove, string.Empty));
      }

      return result;
    }

    public static ContainerBlock IsEmptyContainerBlock(Block b)
    {
      var container = b as ContainerBlock;
      if (container == null) return null;
      if (container.Children.Count < 1) return container;

      return null;
    }

    /// <summary>
    /// Checks the child list for a child with an index in rule higher than the specified index
    /// </summary>
    /// <param name="block">ContainerBlock to check</param>
    /// <param name="index"></param>
    /// <returns>child's index if it has a higher index in rule, -1 otherwise</returns>
    public static LinkedListNode<Block> CheckContainerForChildWithHigherRuleIndex(ContainerBlock block, int index)
    {
      var child = block.Children.First;

      do
      {
        if (child.Value.IndexInRule > index) return child;
      } while ((child = child.Next) != null);

      return null;
    }

    public static Block FindSelectedBlockFromTop(Block me)
    {
      var firstParent = FindFirstParent(me);

      return FindSelectedBlock(firstParent);
    }

    public static Block FindSelectedBlock(Block me)
    {
      var con = me as ContainerBlock;
      if (con != null)
      {
        if (con.IsBlockSelected) return con;
        foreach (var child in con.Children)
        {
          if (child.IsBlockSelected) return child;
          var gChild = FindSelectedBlock(child);
          if (gChild != null) return gChild;
        }
      }
      else
      {
        if (me.IsBlockSelected) return me;
      }

      return null;
    }

    public static void DeselectAllButMe(Block me)
    {
      var firstParent = FindFirstParent(me) as ContainerBlock;
      if (firstParent == null) return;
      Console.WriteLine("[INFO] deselecting all but a " + me.TypeName);

      foreach (var child in firstParent.Children)
      {
        DeselectBlockAndChildrenExceptTarget(child, me);
      }
    }

    private static void DeselectBlockAndChildrenExceptTarget(Block block, Block target)
    {
      var container = block as ContainerBlock;
      if (block != target)
      {
        block.IsBlockSelected = false;
      }
      if (container == null) return;
      foreach (var child in container.Children)
      {
        DeselectBlockAndChildrenExceptTarget(child, target);
      }
    }

    public static void SetInsertHereList(ContainerBlock parent, Block child, IList<string> outputList)
    {
      outputList.Clear();
      if (parent == null) return;

      foreach (var str in parent.RuleLists[child.IndexInRule])
      {
        outputList.Add(str);
      }
    }

    public static void SetInsertNextList(ContainerBlock parent, LinkedListNode<Block> child, IList<string> outputList, IList<string> insertHereList)
    {
      outputList.Clear();
      if (parent == null) return;
      var childIndex = child.Value.IndexInRule;
      var nextSibling = child.Next;
      if (nextSibling == null)
      {
        if (childIndex + 1 < parent.RuleLists.Count)
        {
          foreach (var str in parent.RuleLists[childIndex + 1])
          {
            outputList.Add(str);
          }
        }
      }
      else if (nextSibling.Value.IndexInRule == childIndex)
      {
        var parentRulelist = parent.RuleLists[childIndex];
        if (parentRulelist.Contains(child.Value.TypeName + "*"))//check for star
        {
          foreach (var str in insertHereList)
          {
            outputList.Add(str);
          }
        }
      }
      else if (nextSibling.Value.IndexInRule > childIndex)
      {
        var nextList = parent.RuleLists[childIndex + 1];
        if (nextList.Contains(nextSibling.Value.TypeName + "*")) //check for star
        {
          foreach (var str in nextList)
          {
            outputList.Add(str);
          }
        }
      }
    }

    public static void SetInsertBeforeList(ContainerBlock parentBlock, LinkedListNode<Block> find, IList<string> insertBeforeList, IList<string> insertHereList)
    {
      insertBeforeList.Clear();
      if (parentBlock == null) return;
      var me = find.Value;
      var previousNode = find.Previous;
      var childIndex = me.IndexInRule;
      if (previousNode == null)
      {
        if (childIndex > 0)
        {
          foreach (var str in RemoveEditAndCleanList(parentBlock.RuleLists[childIndex - 1]))
          {
            insertBeforeList.Add(str);
          }
        }
      }
      else
      {
        var previousSibling = previousNode.Value;
        if (previousSibling.IndexInRule == childIndex)
        {
          var parentRulelist = parentBlock.RuleLists[childIndex];
          if (parentRulelist.Contains(me.TypeName + "*")) //check for stars
          {
            foreach (var str in insertHereList)
            {
              insertBeforeList.Add(str);
            }
          }
        }
        else if (previousSibling.IndexInRule < childIndex)
        {
          var previousList = parentBlock.RuleLists[childIndex - 1];
          if (previousList.Contains(previousSibling.TypeName + "*")) // check for stars
          {
            foreach (var str in previousList)
            {
              insertBeforeList.Add(str);
            }
          }
        }
      }
    }

    private static int GetIndexWithPossibleStar(IList<string> list, string typeName)
    {
      if (string.IsNullOrEmpty(typeName)) return -1;
      if (list.Count < 1) return -1;
      var indexWithOutStar = list.IndexOf(typeName);
      if (indexWithOutStar > -1)
      {
        return indexWithOutStar;
      }
      else
      {
        return list.IndexOf(typeName + "*");
      }
    }

    private static List<string> RemoveEditAndCleanList(List<string> list)
    {
      const string EDIT = "(edit)";
      const string STAR = "*";

      if (list.Contains(EDIT))
      {
        list.Remove(EDIT);
      }

      return CleanList(list, STAR);
    }

    public static Block FindFirstParent(Block me)
    {
      if (me.Parent != null)
      {
        return FindFirstParent(me.Parent);
      }
      else
      {
        return me;
      }
    }

    public static LinkedListNode<Block> FindBlockForFirst(ContainerBlock parent, Predicate<Block> pred)
    {
      LinkedListNode<Block> next = parent.Children.First;
      if (next == null) return null;
      do
      {
        if (pred(next.Value)) return next;
        next = next.Next; //psuedo recursive step
      } while (next != null);

      return null;
    }

    public static IEnumerable<Block> SearchBlocks(ContainerBlock searchSpace, Predicate<Block> pred)
    {
      var result = new List<Block>();
      foreach (var child in searchSpace.Children)
      {
        var con = child as ContainerBlock;
        if (pred(child))
        {
          result.Add(child);
        }

        if (con != null)
        {
          result.AddRange(SearchBlocks(con, pred));
        }

      }

      return result;
    }
  }
}