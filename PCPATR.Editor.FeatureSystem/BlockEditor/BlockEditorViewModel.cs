﻿using System.Collections.ObjectModel;
using System.Text;
using Microsoft.Practices.Prism.Commands;
using PCPATR.Editor.MasterLists.ViewModels;
using System.Collections.Generic;

namespace PCPATR.Editor.MasterLists.BlockEditor
{
  /// <summary>
  /// Description of BlockEditorViewModel.
  /// </summary>
  public class BlockEditorViewModel : ViewModelBase
  {
    private static readonly BlockGrammar _blockGrammar = BlockGrammar.GetInstance();

    public BlockEditorViewModel()
    {
      InsertBefore = new DelegateCommand<string>(InsertBeforeMethod);
      InsertHere = new DelegateCommand<string>(InsertHereMethod);
      InsertAfter = new DelegateCommand<string>(InsertAfterMethod);
      SetInitialRuleList = new DelegateCommand(SetupInitialListMethod);
      EditSelected = new DelegateCommand(ExecuteSelectedBlockEditAction);
      AddInitialElementOnlyButton = new DelegateCommand<string>(AddInitialElementToEmptyContainer);
    }

    private void HandleSelectedBlockChanged(LinkedListNode<Block> newSelection)
    {
      SelectedNode = newSelection;
      SelectedElement = newSelection.Value;
      BlockHelperMethods.DeselectAllButMe(_selectedBlock);
      
      _selectedBlockAttributes.Clear();
      if (_selectedBlock.Attributes == null) return;
      foreach (var att in _selectedBlock.Attributes.Values){
        _selectedBlockAttributes.Add(att);
      }
    }

    public DelegateCommand<string> InsertHere { get; private set; }

    public DelegateCommand<string> InsertAfter { get; private set; }

    public DelegateCommand<string> InsertBefore { get; private set; }

    public DelegateCommand<string> AddInitialElementOnlyButton { get; private set; }

    public DelegateCommand SetInitialRuleList { get; private set; }

    public DelegateCommand EditSelected { get; private set; }
    
    private ObservableCollection<string> _initialList = new ObservableCollection<string>();
    public ObservableCollection<string> InitialList
    {
      get { return _initialList; }
      set
      {
        _initialList = value;
        RaisePropertyChanged("InitialList");
      }
    }

    private BlockInitializationContext _context;
    public BlockInitializationContext Context
    {
      get { return _context; }
      set
      {
        _context = value;
        value.SelectedBlockChanged += HandleSelectedBlockChanged;
      }
    }

    private ContainerBlock _content;
    public ContainerBlock Content
    {
      get { return _content; }
      set
      {
        _content = value;
        RaisePropertyChanged("Content");
      }
    }

    private LinkedListNode<Block> _selectedNode;
    public LinkedListNode<Block> SelectedNode
    {
      get { return _selectedNode; }
      set
      {
        _selectedNode = value;
        RaisePropertyChanged("SelectedNode");
      }
    }

    public bool CanEditContent
    {
      get { return (_selectedBlock != null && _selectedBlock.GetType() == typeof (Block)); }
    }

    private Block _selectedBlock;
    public Block SelectedElement
    {
      get { return _selectedBlock; }
      set
      {
        _selectedBlock = value;
        RaisePropertyChanged("SelectedElement");
        _parentBlock = _selectedBlock.Parent as ContainerBlock;
        SetSelectedElementDisplayInfo(_selectedBlock);

        SetLists(_parentBlock, _selectedBlock, _insertList, _insertAfter, _insertBefore);
        RaisePropertyChanged("InsertHereList");
        RaisePropertyChanged("InsertAfterList");
        RaisePropertyChanged("InsertBeforeList");

        SelectedBlockHasChildren = DoesBlockHaveChildren(_selectedBlock);
        RaisePropertyChanged("CanEditContent");
      }
    }

    private ObservableCollection<BlockAttribute> _selectedBlockAttributes = new ObservableCollection<BlockAttribute>();
    public ObservableCollection<BlockAttribute> SelectedBlockAttributes
    {
      get {return _selectedBlockAttributes; }
      set {
        _selectedBlockAttributes  = value;
        RaisePropertyChanged("SelectedBlockAttributes");
      }
    }
    
    private void ExecuteSelectedBlockEditAction()
    {
      if (_selectedBlock == null) return;

      _selectedBlock.TryExecuteEditAction();
    }

    private bool DoesBlockHaveChildren(Block block)
    {
      var con = block as ContainerBlock;
      if (con == null) return false;
      
      return (con.Children.Count > 0);
    }

    private bool _selectedBlockHasChildren;
    public bool SelectedBlockHasChildren
    {
      get { return _selectedBlockHasChildren; }
      set
      {
        _selectedBlockHasChildren = value;
        RaisePropertyChanged("SelectedBlockHasChildren");
      }
    }


    private ObservableCollection<string> _insertList = new ObservableCollection<string>();
    public ObservableCollection<string> InsertHereList
    {
      get { return _insertList; }
      set
      {
        _insertList = value;
        RaisePropertyChanged("InsertHereList");
      }
    }

    private ObservableCollection<string> _insertBefore = new ObservableCollection<string>();
    public ObservableCollection<string> InsertBeforeList
    {
      get { return _insertBefore; }
      set
      {
        _insertBefore = value;
        RaisePropertyChanged("InsertBeforeList");
      }
    }

    private ObservableCollection<string> _insertAfter = new ObservableCollection<string>();
    public ObservableCollection<string> InsertAfterList
    {
      get { return _insertAfter; }
      set
      {
        _insertAfter = value;
        RaisePropertyChanged("InsertAfterList");
      }
    }

    private string _selectedElementParentTypeName;
    public string SelectedElementParentTypeName
    {
      get { return _selectedElementParentTypeName; }
      private set
      {
        _selectedElementParentTypeName = value;
        RaisePropertyChanged("SelectedElementParentTypeName");
      }
    }

    private string _selectedElementTypeName;
    public string SelectedElementTypeName
    {
      get { return _selectedElementTypeName; }
      private set
      {
        _selectedElementTypeName = value;
        RaisePropertyChanged("SelectedElementTypeName");
      }
    }

    private ContainerBlock _parentBlock;


    private void SetLists(ContainerBlock parent, Block child, ObservableCollection<string> insertList, ObservableCollection<string> insertAfterList, ObservableCollection<string> insertBeforeList)
    {

      if (parent == null) return;
      BlockHelperMethods.SetInsertHereList(parent, child, insertList);
      BlockHelperMethods.SetInsertNextList(parent, parent.Children.Find(child), insertAfterList, insertList);
      BlockHelperMethods.SetInsertBeforeList(parent, parent.Children.Find(child), insertBeforeList, insertList);

    }

    private void InsertHereMethod(string typeName)
    {
      if (string.IsNullOrEmpty(typeName)) return;
      if (_selectedBlock == null) return;
      
      var block = _blockGrammar.ParseSelection(typeName, Context);
      block.Parent = _parentBlock;
      block.IndexInRule = _selectedBlock.IndexInRule;

      var grammarList = _parentBlock.RuleLists[_selectedBlock.IndexInRule];
      if (grammarList.Contains(_selectedBlock.TypeName + "*"))
      {
        _parentBlock.Children.AddAfter(_selectedNode, block);
      }
      else
      {
        _parentBlock.Children.Replace(_selectedBlock, block);
        SelectedElement = block;
        RaisePropertyChanged("SelectedNode");
      }
    }

    private void InsertBeforeMethod(string typeName)
    {
      if (string.IsNullOrEmpty(typeName)) return;
      if (_selectedBlock == null) return;
      
      var block = _blockGrammar.ParseSelection(typeName, Context);
      block.Parent = _parentBlock;
      if (_selectedNode.Previous != null && _selectedNode.Previous.Value.IndexInRule == _selectedBlock.IndexInRule)
      {
        block.IndexInRule = _selectedBlock.IndexInRule;
      }
      else
      {
        block.IndexInRule = _selectedBlock.IndexInRule - 1;
      }
      _parentBlock.Children.AddBefore(_selectedNode, block);
    }

    private void InsertAfterMethod(string typeName)
    {
      if (string.IsNullOrEmpty(typeName)) return;
      if (_selectedBlock == null) return;
      
      if (_parentBlock == null) return;
      var block = _blockGrammar.ParseSelection(typeName, Context);
      
      block.Parent = _parentBlock;
      if (_selectedNode.Next != null && _selectedNode.Next.Value.IndexInRule == _selectedBlock.IndexInRule)
      {
        block.IndexInRule = _selectedBlock.IndexInRule;
      }
      else
      {
        block.IndexInRule = _selectedBlock.IndexInRule + 1;
      }

      _parentBlock.Children.AddAfter(_selectedNode, block);
    }

    private void SetSelectedElementDisplayInfo(Block selectedElement)
    {
      SelectedElementTypeName = FormatSelectedItemsPath(selectedElement);
    }

    private static string FormatSelectedItemsPath(Block selectedElement)
    {
      var resultString = selectedElement.TypeName;

      var parent = selectedElement.Parent;
      while (parent != null)
      {
        resultString = parent.TypeName + " > " + resultString;
        parent = parent.Parent;
      }

      return resultString;
    }

    private void SetupInitialListMethod()
    {
      var con = _selectedBlock as ContainerBlock;
      if (con == null) return;

      _initialList.Clear();
      foreach (var str in con.RuleLists[0])
      {
        _initialList.Add(str);
      }
    }

    private void AddInitialElementToEmptyContainer(string typeName)
    {
      if (string.IsNullOrEmpty(typeName)) return;
      var containerBlock = _selectedBlock as ContainerBlock;
      if (containerBlock == null) return;

      var block = _blockGrammar.ParseSelection(typeName, Context);
      block.Parent = containerBlock;
      containerBlock.Children.AddFirst(block);
      SelectedBlockHasChildren = true;
    }
  }
}
