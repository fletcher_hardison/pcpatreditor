﻿using PCPATR.Common;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.Models.FeatureSystem;
using PCPATR.Models.MasterLists;
using System.Collections.Generic;

namespace PCPATR.Editor.MasterLists
{

  /// <summary>
  /// NonTerminalSymbolSelector: IEnumerable of NameIdRef -> NameIdRef;
  /// lets user select a non terminal symbol and return a NameIdRef refering to it.
  /// </summary>
  public delegate NameIdRef NonTerminalSymbolSelector();

  public delegate NameId NameIdListSelector(IEnumerable<NameId> list);

  public delegate NameIdRef NameIdRefListSelector(IEnumerable<NameIdRef> list);

  public delegate NameIdRef FeatureSelector();

  public delegate string ValidNewIdGetter();

  public delegate Symbol EditSymbol(Symbol viewmodel);

  public delegate bool ConfirmationGetter(string message);

  public delegate Symbol SymbolCreator();

  public delegate FeatureViewModel FeatureCreator();
  
  public delegate FeatureViewModel FeatureCreatorWithArg(Feature model);

  public delegate CollectionFeatureViewModel CollectionFeatureCreator();

  public delegate string NewFilePicker();
}
