﻿using System;
using System.Collections.Generic;
using System.Xml;



namespace PCPATR.Models.Parameters
{
    public class AttributeOrder : List<AttributeOrderItem>
    {
        
        #region Properties

        

        public List<string> Comments { get; set; }
        #endregion

//Constructor
        

        public AttributeOrder()
        {
            Comments = new List<string>();
        }

    }
}