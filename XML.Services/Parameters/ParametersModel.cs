using System;
using System.Collections.Generic;
using PCPATR.Common;


namespace PCPATR.Models.Parameters
{
  public class ParametersModel
  {
    public ParametersModel()
    {
      Comments = new List<string>();
      StartSymbolComments = new List<string>();
      AttributeOrderComments = new List<string>();
      AttributeOrder = new List<AttributeOrderItem>();
      CategoryFeature = new ToggledValue();
      LexicalFeature = new ToggledValue();
      GlossFeature = new ToggledValue();
      RootGlossFeature = new ToggledValue();
    }


    public NameIdRef StartSymbol { get; set; }

    public ToggledValue CategoryFeature { get; set; }

    public ToggledValue LexicalFeature { get; set; }

    public ToggledValue GlossFeature { get; set; }

    public ToggledValue RootGlossFeature { get; set; }

    public List<AttributeOrderItem> AttributeOrder { get; set; }
    public List<string> StartSymbolComments { get; set; }
    public List<string> AttributeOrderComments { get; set; }
    public List<string> Comments { get; set; }
  }

  public static class ParameterFieldNames
  {
    public const string CAT = "Category Feature";
    public const string LEX = "Lexical Feature";
    public const string GLOSS = "Gloss Feature";
    public const string ROOT_GLOSS = "Root Gloss Feature";
  }


  public class ToggledValue
  {
    public ToggledValue()
    {
      Comments = new List<string>();
    }


    public List<string> Comments { get; set; }

    public string Value { get; set; }

    public string DisplayName { get; set; }

    public bool HasValue { get; set; }
  }

}