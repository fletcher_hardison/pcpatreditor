using System.Collections.Generic;
using PCPATR.Common;


namespace PCPATR.Models.Parameters
{
  public class AttributeOrderItem
  {
    public string Name { get; set; }
    public NameIdRef IdRef { get; set; }
  }

  public static class ParameterAttributeOrderNames
  {
    public const string CAT = "Cat";
    public const string LEX = "Lex";
    public const string GLOSS = "Gloss";
    public const string ROOT_GLOSS = "Root Gloss";
    public const string NONE = "None";
    public const string SUBCAT = "Subcat";
    public const string FIRST = "First";
    public const string REST = "Rest";
    public const string FEATURE_NAME_REF = "Feature Name Ref";
  }

}