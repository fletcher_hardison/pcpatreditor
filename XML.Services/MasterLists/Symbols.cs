﻿using System.Collections.Generic;
using System;
using PCPATR.Common;
using PCPATR.Common.Interfaces;


namespace PCPATR.Models.MasterLists
{
    public class Symbol: IHasDescription,IHasId, ICloneable, ISymbol
    {
        public List<string> Comments { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Symbol()
        { Comments = new List<string>(); }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
