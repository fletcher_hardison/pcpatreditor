using System.Collections.Generic;
using PCPATR.Models.FeatureSystem;

namespace PCPATR.Models.MasterLists
{
    /// <summary>
    /// Implements the master lists. Terminal and non terminal nodes, Built In primatives, feature system, etc.
    /// </summary>
    public class MasterListsModel
    {
        //Events

        //Fields
        private List<Symbol> _terminals = new List<Symbol>();
        private List<Symbol> _nonTerminals = new List<Symbol>();

        
        private FeatureSystem.FeatureSystem _featureSystem = new FeatureSystem.FeatureSystem();
        private BuiltInPrimitives _builtInPrimitives = new BuiltInPrimitives();
        private CollectionFeatures _collectionFeatures = new CollectionFeatures();

        //Constructor

        //Properties


        public List<Symbol> Terminals
        {
            get{return _terminals;}
            set { _terminals = value; }

        }

        public List<Symbol> NonTerminals
        {
            get { return _nonTerminals; }
            set { _nonTerminals = value; }
        }

        public FeatureSystem.FeatureSystem GrammarFeatureSystem
        {
            get { return _featureSystem; }
            set { _featureSystem = value; }
        }

        public BuiltInPrimitives GrammarBuildInPrimatives
        {
            get { return _builtInPrimitives; }
            set { _builtInPrimitives = value; }
        }

        public CollectionFeatures GrammarCollectionFeatures
        {
            get { return _collectionFeatures; }
            set { _collectionFeatures = value; }
        }


       

       
    }
}