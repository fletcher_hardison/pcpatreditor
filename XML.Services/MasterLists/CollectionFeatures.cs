﻿using System;
using System.Collections.Generic;
using PCPATR.Common;
using PCPATR.Common.Interfaces;



namespace PCPATR.Models.MasterLists
{
  public class CollectionFeature : IHasId, IHasDescription
  {
    public CollectionFeature()
    {
      Comments = new List<string>();
      _nameId = new NameId("", "");
    }

    public string Id
    {
      get { return _nameId.id; }
      set { _nameId.id = value; }
    }

    public string Name
    {
      get { return _nameId.name; }
      set { _nameId.name = value; }
    }

    public NameId Identifier
    {
      get { return _nameId; }
      set { _nameId = value; }
    }

    public string Description { get; set; }
    public List<string> Comments { get; set; }

    private NameId _nameId;
  }

  public class CollectionFeatures
  {
    public List<CollectionFeature> Features { get; set; }

    //Constructor
    public CollectionFeatures()
    {
      Features = new List<CollectionFeature>();
    }
  }

}