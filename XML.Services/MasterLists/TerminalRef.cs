namespace PCPATR.Models.MasterLists
{
    public class TerminalRefModel : FeatureTemplates.FeatureTemplateIdBase
    {
      public string DisplayName { get { return base.Identifier; } }
    }
}