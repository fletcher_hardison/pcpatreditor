﻿using PCPATR.Common;

namespace PCPATR.Models.MasterLists
{
    public class BuiltInPrimitives
    {
        /*
         * Built in primatives are parts of the PC-PATR grammar that are required by every grammar. 
         * The use is allowed to specify what thier IDs will be named. 
         */
      public BuiltInPrimitives()
      {
        CatId = new NameId("Cat", "pcpatrGrammarCat");
        CatId.try_register();
        LexId = new NameId("Lex", "pcpatrGrammarLex");
        LexId.try_register();
        GlossId = new NameId("Gloss", "pcpatrGrammarGloss");
        GlossId.try_register();
        RootGlossId = new NameId("RootGloss", "pcpatrGrammarRootGloss");
        RootGlossId.try_register();
        NoneId = new NameId("None", "pcpatrGrammarNone");
        NoneId.try_register();

      }

        #region Properties

        public NameId CatId { get; private set; }

        public NameId LexId { get; private set; }

        public NameId GlossId { get; private set; }

        public NameId RootGlossId { get; private set; }

        public NameId NoneId { get; private set; }

        #endregion

       
       
    }
}