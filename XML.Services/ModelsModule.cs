﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using PCPATR.Common.Interfaces;
using PCPATR.Models.FeatureSystem;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Models.MasterLists;
using PCPATR.Models.Parameters;
using PCPATR.Models.Rules;
using PCPATR.Models.Rules.PSRElements;

namespace PCPATR.Models
{
  public class ModelsModule : IModule
  {
    private readonly IUnityContainer _container;

    public ModelsModule(IUnityContainer container)
    {
      _container = container;
    }

    public void Initialize()
    {
      RegisterModelesWithIoc();
    }

    public void RegisterFeatureSystemModelsWithIoc()
    {
      _container.RegisterType<Feature>();
      _container.RegisterType<FeatureDescription>();
      _container.RegisterType<FeatureSystem.FeatureSystem>(new ContainerControlledLifetimeManager());
    }

    private void RegisterConstraintModelWithIoC()
    {      
      _container.RegisterType<ParametersModel>();
      _container.RegisterType<ToggledValue>();
    }

    private void RegisterTemplateModelsWithIoC()
    {
      _container.RegisterType<FeatureTemplateModel>();
      _container.RegisterType<FeatureTemplateModelBase>();
      _container.RegisterType<FeatureTemplateFeatureStructure>();

      _container.RegisterType<ConstraintTemplatesModel>();
    }

    private void RegisterRuleModelsWithIoc()
    {

      _container.RegisterType<Disjunction>();
      _container.RegisterType<DisjunctiveSymbols>();
      _container.RegisterType<OptionalSymbols>();
      _container.RegisterType<PhraseStructureRule>();
      _container.RegisterType<PsrElement>();
      _container.RegisterType<PsrSymbol>();
      _container.RegisterType<SymbolRef>();

      //Phrase structure rules
      _container.RegisterType<Rule>();
      _container.RegisterType<RulesRepository>();

      //Lexical rules
      _container.RegisterType<LexicalRulesRepository>();
      _container.RegisterType<LexicalRule>();
    }

    private void RegisterMasterListModelsWithIoc()
    {
      _container.RegisterType<CollectionFeatures>();
      _container.RegisterType<CollectionFeature>();
      _container.RegisterType<MasterListsModel>(new ContainerControlledLifetimeManager());
      _container.RegisterType<ISymbol, Symbol>();
    }

    private void RegisterModelesWithIoc()
    {

      this.RegisterFeatureSystemModelsWithIoc();

      this.RegisterMasterListModelsWithIoc();

      this.RegisterConstraintModelWithIoC();

      this.RegisterRuleModelsWithIoc();

      this.RegisterTemplateModelsWithIoC();

    }

  }
}
