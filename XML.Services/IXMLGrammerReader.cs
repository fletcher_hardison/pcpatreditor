﻿namespace PCPATR.Models
{
    internal interface IXmlGrammerReader
    {
        void ReadXml();
        void WriteXml();
    }
}