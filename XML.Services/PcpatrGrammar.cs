﻿using System;
using System.Xml;
using System.Collections.Generic;
using PCPATR.Models.MasterLists;
using PCPATR.Models.Rules;

using PCPATR.Models.FeatureTemplates;

namespace PCPATR.Models
{
    public class PcpatrGrammar
    {
        private string _pathToGrammar;
        
        public List<string> Comments { get; set; }
        public IEnumerable<FeatureTemplateModel> FeatureTemplates { get; set; }
        public ConstraintTemplatesModel ConstraintTemplates { get; set; }
        public RulesRepository Rules { get; set; }
        public Parameters.ParametersModel Parameters { get; set; }
        public LexicalRulesRepository LexicalRules { get; set; }
        public MasterListsModel MasterLists { get; set; }


        public  PcpatrGrammar()
        {
            Comments = new List<string>();
        }

        public PcpatrGrammar(String grammarFilePath)
        {
            _pathToGrammar = grammarFilePath;
          
        }

        
    }
}