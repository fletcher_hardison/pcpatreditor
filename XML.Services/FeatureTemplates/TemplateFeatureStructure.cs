﻿using System.Collections.Generic;
using PCPATR.Models.Rules.Constraints;
using PCPATR.Common;

namespace PCPATR.Models.FeatureTemplates
{
  public class FeatureTemplateFeatureStructure : FeatureTemplateModelBase
  {
    public List<object> Elements { get; set; }

    public FeatureTemplateFeatureStructure()
    {
      Elements = new List<object>();
    }
  }


  public class FeatureTemplateModelBase
  {
    public bool IsDefault { get; set; }
  }

  
  public class ComplexFeatureRef : FeatureTemplateModelBase
  {
   public ComplexFeatureRef()
    {
      Values = new List<object>();
      Comments = new List<string>();
    }


    public List<object> Values
    {
      get;
      set;
    }

    public string Id { get; set; }

    public List<string> Comments { get; set; }
  }


  public class SubcatRef : FeatureTemplateModelBase
  {
    public object Value { get; set; }
  }


  public class FeatureTemplateFeatureDisjunction : FeatureTemplateModelBase
  {
    public object LeftTerm { get; set; }
    public List<object> RightTerms { get; set; }

    public FeatureTemplateFeatureDisjunction()
    {
      RightTerms = new List<object>();
    }
  }


  public class FeatureRef : FeatureTemplateModelBase, IHasStartAndEndFields
  {
    public FeatureRef() {}

    public FeatureRef(string startId, string endId) {
      Start = startId;  
      End = endId;
    }


    public object Child { get; set; }
    public string Start { get; set; }
    public string End { get; set; }
  }


  public class CollectionFeatureRef : FeatureTemplateModelBase
  {
    public object EmbeddedPath { get; set; }
    public string CollectionFeature { get; set; }
  }


  public class CatValue : FeatureTemplateModelBase
  {
    public CatValue() {
      Symbol = new NameIdRef("newCatValue");
    }

    public NameIdRef Symbol { get; set; }
  }


  public class CatValuePath : FeatureTemplateTerminalValueModelBase
  {
    public CatValuePath() {
      Symbol = new NameIdRef("newCatValuePath");
    }

    public NameIdRef Symbol { get; set; }
  }

  public abstract class FeatureTemplateTerminalValueModelBase : FeatureTemplateModelBase
  {
    public string Value { get; set; }
  }

  public abstract class FeatureTemplateTerminalValuePathModelBase : FeatureTemplateModelBase
  {
    public string Value { get; set; }
  }

  public class LexValue : FeatureTemplateTerminalValueModelBase
  {
  }


  public class LexValuePath : FeatureTemplateTerminalValuePathModelBase
  {
  }


  public class GlossValue : FeatureTemplateTerminalValueModelBase
  {
  }


  public class GlossValuePath : FeatureTemplateTerminalValuePathModelBase
  {
  }


  public class RootGlossValue : FeatureTemplateTerminalValueModelBase
  {
  }


  public class RootGlossValuePath : FeatureTemplateTerminalValuePathModelBase
  {
  }


  public class NoneRef : FeatureTemplateModelBase
  {
  }

  public class NoneValuePath : FeatureTemplateModelBase
  {
    public NoneValuePath(string value) {
      Value = value;
    }


    public string Value { get; set; }
  }


  public class SubcatDisjunction : FeatureTemplateModelBase
  {
    public object FirstFactor { get; set; }

    public List<object> OtherFactors { get; set; }

    public SubcatDisjunction()
    {
      OtherFactors = new List<object>();
    }
  }


  public class SubcatFirstRest : FeatureTemplateModelBase
  {
    public object First { get; set; }
    public object Rest { get; set; }
  }


  public class FeatureFeatureTemplateRef : FeatureTemplateModelBase
  {
    public NameIdRef Id { get; set; }
  }


  public class TemplateFeaturePath : FeatureTemplateModelBase, IHasStartAndEndFields
  {
    public string Start { get; set; }
    public string End { get; set; }
    public object Value { get; set; }
  }


  public class FeatureTemplateFeaturePathDisjunction : FeatureTemplateModelBase, IHasStartAndEndFields
  {
    public string Start { get; set; }
    public string End { get; set; }
    public FeatureValueRefModel LeftPath { get; set; }
    public List<FeatureValueRefModel> RightPaths { get; set; }
    public TemplateFeaturePath TemplatePath {get; set; }
    public FeatureTemplateFeaturePathDisjunction()
    {
      RightPaths = new List<FeatureValueRefModel>();
    }
  }


  public class EmptyElement : FeatureTemplateModelBase
  {
  }


  public interface IHasStartAndEndFields
  {
    string Start { get; set; }
    string End { get; set; }
  }
}