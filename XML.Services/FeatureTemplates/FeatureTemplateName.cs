using PCPATR.Common.Interfaces;


namespace PCPATR.Models.FeatureTemplates
{
  public class FeatureTemplateNameModel : FeatureTemplateIdBase, IHasId
  {
    public string Name
    {
      get { return base.Identifier; }
      set
      {
        base.Identifier = value;
      }
    }

    public string DisplayName { get { return string.IsNullOrEmpty(base.Identifier) ? base.Id : base.Identifier; } }

  }

}