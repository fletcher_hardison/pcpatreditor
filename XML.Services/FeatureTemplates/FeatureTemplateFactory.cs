﻿using PCPATR.Common;
using PCPATR.Models.MasterLists;

namespace PCPATR.Models.FeatureTemplates
{
  public class FeatureTemplateFactory
  {
    public FeatureTemplateModel BuildFeatureTemplateModel()
    {
      var m = new FeatureTemplateModel {Left = BuildFeatureTemplateNameModel()};
      m.RightValues.Add(BuildComplexFeatureRef());
      m.Enabled = true;
      m.UseWhenDebugging = true;
      
      return m;
    }

    public FeatureTemplateIdBase BuildFeatureTemplateNameModel()
    {
      var m = new FeatureTemplateNameModel {Name = "New Feature Template", Id = "ftNewFeatureTemplate"};

      return m;
    }

    public TerminalRefModel BuildTerminalRefModel()
    {
      return new TerminalRefModel { Id = "ctrNewTerminalRef" };
    }

    public FeatureTemplateFeatureStructure BuildTemplateFeatureStructure()
    {
      var m = new FeatureTemplateFeatureStructure();
      m.Elements.Add(BuildComplexFeatureRef());

      return m;
    }


    public ComplexFeatureRef BuildComplexFeatureRef()
    {
      var m = new ComplexFeatureRef();
      m.Values.Add(BuildFeatureRef());
      m.Id = "cfrNewComplexFeatureRef";

      return m;
    }

    public FeatureRef BuildFeatureRef()
    {
      var m = new FeatureRef {Start = "frNewStart", End = "frNewEnd"};

      return m;
    }

    public FeatureTemplateFeatureDisjunction BuildTemplateFeatureDisjunction()
    {
      var m = new FeatureTemplateFeatureDisjunction {LeftTerm = BuildTemplateFeatureStructure()};
      m.RightTerms.Add(BuildTemplateFeatureStructure());

      return m;
    }

    public FeatureFeatureTemplateRef BuildFeatureTemplateRef()
    {
      var m = new FeatureFeatureTemplateRef {Id = new NameIdRef("ftNewFeatureTemplateRef")};

      return m;
    }

    public CollectionFeatureRef BuildCollectionFeatureRef()
    {
      var m = new CollectionFeatureRef
      {
        CollectionFeature = "ftNewcollectionFeatureRef"
      };

      return m;
    }


  }

}
