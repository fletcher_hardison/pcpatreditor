namespace PCPATR.Models.FeatureTemplates
{
    public abstract class Path
    {
        public string FeatureStart { get; set; }
        public string FeatureEnd { get; set; }
    }
}