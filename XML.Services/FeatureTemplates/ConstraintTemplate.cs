using System.Collections.Generic;
using PCPATR.Common.Interfaces;

using PCPATR.Models.Rules.Constraints;

namespace PCPATR.Models.FeatureTemplates
{
  public class ConstraintTemplateModel : IHasId, IHasConstraintControl, IHasIdAndIdentifier
  {
    public ConstraintTemplateModel()
    {
      Comments = new List<string>();
    }

    public string Identifier
    {
      get { return Name; } 
      set { Name = value; }
    }

    public string Id { get; set; }

    public string Name { get; set; }

    public List<string> Comments { get; set; }
    public bool enabled { get; set; }
    public bool use_when_debugging { get; set; }

    public LogicalExpressionModel LogicalExpression { get; set; }
  }
}