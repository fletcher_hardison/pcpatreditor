using System.Collections.Generic;

namespace PCPATR.Models.FeatureTemplates
{
  public class FeatureTemplateModel : FeatureTemplateModelBase, IHasIdAndIdentifier
  {
    public FeatureTemplateModel()
    {
      RightValues = new List<object>();
      Comments = new List<string>();
      Left = new FeatureTemplateNameModel();
    }

    public FeatureTemplateIdBase Left { get; set; }

    public string Identifier
    {
      get { return Left.Identifier; }
      set { Left.Identifier = value; }
    }

    public string Id
    {
      get { return Left.Id; }
      set { Left.Id = value; }
    }

    public List<object> RightValues { get; set; }

    public List<string> Comments { get; set; }

    public bool Enabled { get; set; }

    public bool UseWhenDebugging { get; set; }
  }
}