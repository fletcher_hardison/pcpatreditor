using PCPATR.Common;

namespace PCPATR.Models.FeatureTemplates
{
  public class FeatureTemplateIdBase
  {
    protected FeatureTemplateIdBase()
    {
      
    }

    public string Id { get; set; }


    public string Identifier
    {
      get;
      set;
    }
  }
}