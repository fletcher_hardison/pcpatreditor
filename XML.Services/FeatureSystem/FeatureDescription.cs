using System.Collections.Generic;
using PCPATR.Common;
using PCPATR.Common.Interfaces;


namespace PCPATR.Models.FeatureSystem
{/*
 * DEPRECIATED CLASS 18 May 2012
 */
    public abstract class FeatureDescElements{}
    /*
 * DEPRECIATED CLASS 18 May 2012
 */
    /// <summary>
    /// Both Complex and Closed feature descriptions share certain elements
    /// </summary>
    public abstract class FeatureDescription : FeatureDescElements , IHasId, IHasDescription
    {
        
        public List<string> Comments { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
        public string Description { get; set; }

        
    }
}