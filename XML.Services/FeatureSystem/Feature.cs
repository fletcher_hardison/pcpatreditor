﻿//  ***************************************************************
//  File name: LogicalFeature.cs
//  Project: PCPATR.DAL
//  Created by: 
//  Created on: 18,05,2012
// ***************************************************************

using System.Collections.Generic;
using PCPATR.Common;
using PCPATR.Common.Interfaces;


namespace PCPATR.Models.FeatureSystem
{
  public class Feature : IHasDescription, IHasId
  {
    private bool _isComplex;


    public Feature()
    {
      Comments = new List<string>();
      Children = new List<Feature>();
    }
    /// <summary>
    /// gets or sets whether or not this feature is a reference to another feature.
    /// (is a ComplexFeatureRef or not)
    /// </summary>
    public bool IsReference { get; set; }
    /// <summary>
    /// Gets or sets whether this feature is a complex feature.
    /// </summary>
    public bool IsComplex
    {
      get
      {
        if (Children.Count > 0)
        {
          var atLeatOnChild = false;
          foreach (var child in Children)
          {
            if (child.Children.Count > 0) atLeatOnChild = true;
          }
          _isComplex = atLeatOnChild;
        }
        else _isComplex = false;

        return _isComplex;
      }
      set { _isComplex = value; }
    }

    /// <summary>
    /// Gets or sets the collection of comments associated with this feature.
    /// </summary>
    public List<string> Comments { get; set; }

    /// <summary>
    /// Gets or sets the name of this feature.        
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the unique identifier associated with this feature.
    /// </summary>
    public string Id
    {
      get;
      set;
    }


    /// <summary>
    /// gets or sets the Description property of this feature.
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Gets or sets the collection of child features of this feature.
    /// </summary>
    public List<Feature> Children { get; set; }

    /// <summary>
    /// Gets or sets the Identifier of the feature this feature refers to.
    /// Should only be set this is a ComplexFeaturereference.
    /// </summary>
    public string TargetId { get; set; }

  }

  public class ComplexFeatureRef : Feature
  {
    public new bool IsReference
    {
      get { return true; }
    }

    public bool? IsDefault { get; set; }

    public new string TargetId { get; set; }

  }

  public class ValueDef : Feature
  {
  }
}