using System.Collections.Generic;
using System.Xml;

namespace PCPATR.Models.FeatureSystem
{
    /// <summary>
    /// The data stucture for the feature system. includes methods to parse the xml and return that information.
    /// </summary>
    public class FeatureSystem
    {
      public FeatureSystem()
      {
        Features = new List<Feature>();
      }
        //2012-04-05, TFH, changed to value from FeatureDescription to IFeatureDefinition
        /*2012-05-18, TFH changed to feature. Collapsing closed and complex feautres into a single class with an 
        attribute to tell the difference
         */

        public XmlNode FeaturesXmlNode { get; set; }
      public List<Feature> Features { get; set; }
    }
}