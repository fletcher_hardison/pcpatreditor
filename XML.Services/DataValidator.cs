﻿//  ***************************************************************
//  File name: DataValidator.cs
//  Project: PCPATR.DAL
//  Created by: 
//  Created on: 30,05,2012
// ***************************************************************
using System.Collections.Generic;
using System.Windows.Forms;
namespace PCPATR.Models
{
    public class DataValidator
    {
        public List<string> GrammarIds= new List<string>();

        public string TempId { get; set; }

        public  string ValidateId(string id)
        {
            //if (TempId != null) {
            //    GrammarIds.Add(TempId);
            //}

            id = id.Replace(" ", "");

            if (GrammarIds.Contains(id))
            {
                var x =1 ;
                while (GrammarIds.Contains(id + x.ToString())) {
                    x += 1;
                }
                id += x.ToString();

                MessageBox.Show("Identifier was not unique so it was changed to " + id);
            }
            TempId = id;
            return id;
        }

        /// <summary>
        /// Causes the temporary id to be saved to the id list
        /// </summary>
        public void PersistId()
        {
            if (TempId != null & !GrammarIds.Contains(TempId)) {

                GrammarIds.Add(TempId);
            }
        }

        /// <summary>
        /// Removes the Identifier from the id list
        /// </summary>
        /// <param name="id"></param>
        public void FeatureDeleted(string id)
        {
            if (id == null) return;

            if (GrammarIds.Contains(id)) {
                GrammarIds.Remove(id);
            }
        }

        

    }
}