﻿/*
 * Created by SharpDevelop.
 * User: TFHardison
 * Date: 3/10/2013
 * Time: 8:31 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace PCPATR.Models
{
  /// <summary>
  /// Description of IHasIdAndIdentifier.
  /// </summary>
  public interface IHasIdAndIdentifier
  {
    string Identifier {get;set;}
    
    string Id {get;set;}
  }
}
