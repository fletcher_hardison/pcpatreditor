//  ***************************************************************
//  File name: Extensions.cs
//  Project: PCPATR.Editor
//  Created by: 
//  Created on: 27,06,2012
// ***************************************************************

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace PCPATR.DAL
{
    public static class Extensions
    {
         public  static ObservableCollection<T> ToObservabableCollection<T> (this List<T> list )
         {
             return new ObservableCollection<T>(list);
         }

        public static void AddRange<T>(this ObservableCollection<T> col, IEnumerable<T> range )
        {
            var cast = new List<T>(range);

            foreach (var elem in cast) {
                col.Add(elem);
            }
        }

        public  static List<T> ToList<T> (this IList<T> observable )
        {
            return Enumerable.ToList(observable);
        }

        public static List<U> ConvertList<T,U> (this IEnumerable<T> list, Func<T,U> action )
        {
            var result = new List<U>();

            foreach (var item in list) {
                result.Add(action(item));    
            }
            return result;
        }

        /// <summary>
        /// Iterates a collection and finds all that match a certain predicate
        /// </summary>
        /// <typeparam name="T">List type to search</typeparam>
        /// <param name="list">list to search</param>
        /// <param name="predicate">predicate to search by</param>
        /// <returns>null if search had no matches, otherwise returns matches</returns>
        public static List<T> FindAll<T>(this IList<T> list, Predicate<T> predicate  )
        {
            var result = new List<T>();

            foreach (var elem in list) {
                if (predicate(elem)) result.Add(elem);
            }

            return !result.Any() ? null : result;
        }

    }
}