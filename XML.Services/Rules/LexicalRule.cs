using System.Collections.Generic;
using PCPATR.Common;
using PCPATR.Common.Interfaces;


namespace PCPATR.Models.Rules
{
    public class LexicalRule : IHasName, IHasIdAndIdentifier
    {
        public LexicalRule()
        {
            Comments = new List<string>();
            LexicalRuleDefinitions = new List<LexicalRuleDefinition>();
        }

        public List<string> Comments { get; set; }

        public List<LexicalRuleDefinition> LexicalRuleDefinitions { get; set; }

        #region IHasName Members

        public string Name { get; set; }

        #endregion

      public string Identifier { get; set; }

      public string Id { get { return Name; } set { Name = value; } }
    }
}