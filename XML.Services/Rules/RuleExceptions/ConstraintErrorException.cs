﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCPATR.Models.Rules.RuleExceptions
{
    internal class ConstraintErrorException : Exception
    {
        public ConstraintErrorException(string message) : base(message)
        {
        }
    }
}