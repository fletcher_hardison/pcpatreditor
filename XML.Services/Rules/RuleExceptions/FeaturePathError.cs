using System;

namespace PCPATR.Models.Rules.RuleExceptions
{
    internal class FeaturePathError : Exception
    {
        public FeaturePathError(string errorMessage) : base(errorMessage)
        {
        }
    }
}