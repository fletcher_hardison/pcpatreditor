using System;

namespace PCPATR.Models.Rules.RuleExceptions
{
    internal class RuleNotValidException : Exception
    {
        // private string _message;

        //public string Message
        //{
        //    get { return _message; }
        //    set { _message = value; }
        //}
        public string XmlString;

        public  RuleNotValidException(string myMessage) :base(myMessage){}

        public RuleNotValidException(string myMessage, string xmlDefinition): this(myMessage)
        {
            XmlString = xmlDefinition;
        }
    }
}