﻿using System.Collections.Generic;
using PCPATR.Common.Interfaces;
using PCPATR.Models.Rules.Constraints;

using PCPATR.Models.Rules.PSRElements;

namespace PCPATR.Models.Rules
{
  /// <summary>
  /// Model of a grammar rule (phrase structure rule) including constraints
  /// </summary>
  public class Rule : IHasId, IHasConstraintControl, IHasIdAndIdentifier
  {
    public Rule()
    {
      Comments = new List<string>();
      ConstraintsComments = new List<string>();
      Constraints = new List<ConstraintModelBase>();
    }

    public string Id { get; set; }

    public string Identifier { get; set; }

    public List<string> Comments { get; set; }

    public bool enabled
    {
      get; 

     set ;
  }

    public bool use_when_debugging { get; set; }

    public PhraseStructureRule PhraseStructureRule { get; set; }

    public List<ConstraintModelBase> Constraints { get; set; }

    public List<string> ConstraintsComments { get; set; }
  }
}