
namespace PCPATR.Models.Rules
{
  public interface ILexicalRuleRightElement
  {
    
  }

    public class LexicalRuleDefinition
    {
        public LexicalRuleFeaturePath Left { get; set; }

        //uses interface because it can containe a FeatureValueRefModel which is also used in the Constraint system 
        //and so cannot be with a common base class
        public ILexicalRuleRightElement RightElement { get; set; }
    }
}