using System.Collections.Generic;
using PCPATR.Common;

using PCPATR.Models.FeatureTemplates;

namespace PCPATR.Models.Rules
{
    public class LexicalRulesRepository
    {
      public LexicalRulesRepository()
      {
        Rules = new List<LexicalRule>();
      }


      public List<LexicalRule> Rules { get; set; }
    }

    public class LexicalRuleFeaturePath : ILexicalRuleRightElement, IHasStartAndEndFields
    {
      public string Start { get; set; }
      public string End { get; set; }
    }
}