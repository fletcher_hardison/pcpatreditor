﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PCPATR.Models.Rules.Constraints
{
  public class ConstraintModels { }

  [DebuggerStepThrough]
  public class ConstraintModelBase : UseWhenDebuggingEnabledModelBase
  {
    public ConstraintModelBase() {
      comments = new List<string>();
    }

    public virtual object left { get; set; }

    public virtual object right { get; set; }

    public List<string> comments { get; set; }
  }


  public class LogicalConstraintModel : ConstraintModelBase
  {
    public override object left {
      get { return _left; }
      set { _left = (FeaturePathModel)value; }
    }

    public override object right {
      get { return _right; }
      set { _right = value; }
    }

    private FeaturePathModel _left;

    private object _right;
  }

  public class UnificationConstraintModel : ConstraintModelBase
  {
  }

  public class PriorityUnionModel : ConstraintModelBase
  {
    public override object left {
      get { return _left; }
      set { _left = (FeaturePathModel)value; }
    }

    private FeaturePathModel _left;
  }

  public class PercolationOperationModel : ConstraintModelBase
  {
    public bool validate(object x) {
      Type t = x.GetType();
      return (t == typeof(FeaturePathModel) || t == typeof(SubcatPathModel));
    }
  }

  public class ConstraintDisjunctionModel : ConstraintModelBase
  {
    public ConstraintDisjunctionModel() {
      _leftConstraints = new List<ConstraintModelBase>();
      _rightConstraints = new List<List<ConstraintModelBase>>();
    }

    public List<ConstraintModelBase> LeftConstraints {
      get { return _leftConstraints; }
      set { _leftConstraints = value; }
    }

    public new List<List<ConstraintModelBase>> RightConstraints {
      get { return _rightConstraints; }
      set { _rightConstraints = value; }
    }

    private List<ConstraintModelBase> _leftConstraints;

    private List<List<ConstraintModelBase>> _rightConstraints;
  }

  public class FeatureValueDisjunction
  {
    public FeatureValueDisjunction() {
      RightElements = new List<FeatureValueRefModel>();
    }

    public FeatureValueRefModel LeftElement { get; set; }

    public List<FeatureValueRefModel> RightElements { get; set; }
  }
}