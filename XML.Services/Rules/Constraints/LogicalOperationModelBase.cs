namespace PCPATR.Models.Rules.Constraints
{
  public class LogicalOperationModelBase
  {
    public object Factor1 { get; set; }

    public string OperationType { get; set; }
  }
}