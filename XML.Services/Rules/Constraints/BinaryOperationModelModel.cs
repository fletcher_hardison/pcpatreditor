namespace PCPATR.Models.Rules.Constraints
{
  public class BinaryOperationModel : LogicalOperationModelBase
  {
    public object Factor2 { get; set; }
  }
}