using PCPATR.Common;
using PCPATR.Common.Interfaces;

using System.Collections.Generic;

namespace PCPATR.Models.Rules.Constraints
{

  

  public class FeaturePathModel
  {
    public bool is_valid
    {
      get { return node.HasValue; }
    }

    public NameIdRef node { get; set; }

    public NameIdRef feature_start { get; set; }

    public NameIdRef feature_end { get; set; }

    public object EmbeddedFeaturePath { get; set; }
  }

  public class EmbeddedFeaturePath
  {
    public EmbeddedFeaturePath() {}

    public EmbeddedFeaturePath(string start, string end) {
      feature_start = new NameIdRef(start);
      if (!string.IsNullOrEmpty(end)) {
        featrure_end = new NameIdRef(end);
      }
    }


    public NameIdRef feature_start { get; set; }
    public NameIdRef featrure_end { get; set; }

    public EmbeddedFeaturePath ChildPath { get; set; }
  }


  public class SubcatPathModel : FeaturePathModel
  {      
    public SubcatEndValues subcat_end { get; set; }

    public NameIdRef feature_after_start { get; set; }

    public NameIdRef feature_after_end { get; set; }
  }


  public enum SubcatEndValues
  {
    NoneValue,
    First,
    Rest      
  }

  public class NameIdOnlyModelBase 
  {
    public bool has_value
    {
      get { return value.HasValue; }
    }

    public NameIdRef value { get; set; }
  }

  public class CategoryValueRefModel : NameIdOnlyModelBase {}


  public class FeatureValueRefModel : NameIdOnlyModelBase, ILexicalRuleRightElement
  {
    public bool IsDefault { get; set; }
  }


  public class NoneValueModel 
  {
    public string value
    {
      get { return "none"; }
    }
  }


  public class UseWhenDebuggingEnabledModelBase : IHasConstraintControl
  {
    public bool use_when_debugging { get; set; }

    public bool enabled { get; set; }
  }


  public class LogicalConstraintRefModel :  IHasConstraintControl
  {
    public NameIdRef constraint { get; set; }

    public bool use_when_debugging { get; set; }

    public bool enabled { get; set; }
  }


  

  

  public class LogicalFeatureModel 
  {
    public static List<string> index_variable_values = new List<string> { "none", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

    public LogicalFeatureModel embedded_feature { get; set; }

    public NameIdRef feature_start { get; set; }

    public NameIdRef feature_end { get; set; }

    public string indexed_variable { get; set; }
  }

  public class LogicalExpressionModel 
  {
    public LogicalOperationModelBase operation { get; set; }
  }
}