using System.ComponentModel;
using PCPATR.Common;


namespace PCPATR.Models.Rules.PSRElements
{
  public class SymbolRef : PsrSymbol
  {
    public SymbolRef()
    {
      Symbol = new NameIdRef("");
      _nameId = new NameId("", "");
    }

   
    /// <summary>
    /// This references a symbol in the master lists
    /// </summary>
    public NameIdRef Symbol { get; set; }

    /// <summary>
    /// The Unique id of this Symbol Ref
    /// </summary>
    public string Id { get { return _nameId.id; } set { _nameId.id = value; } }

    public string Name {get { return _nameId.name; } set { _nameId.name = value; }}
    

    private readonly NameId _nameId;
  }
}