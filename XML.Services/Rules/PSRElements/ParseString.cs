//  ***************************************************************
//  File name: ParseString.cs
//  Project: PCPATR.Services
//  Created by: 
//  Created on: 14,09,2012
// ***************************************************************

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;


namespace PCPATR.Models.Rules.PSRElements
{
    public class ParseString
    {
        public ParseString()
        {
            this._data = new ParseStringData
                         {IdsInValue = new Dictionary<string, string>(), Value = new ObservableCollection<string>()};
        }

        #region IParseString Members

        public ObservableCollection<string> Value
        {
            get { return this._data.Value; }
            set { this._data.Value = value; }
        }


        public Dictionary<string, string> IdsInValue
        {
            get { return this._data.IdsInValue; }
            set
            {
                if (this._data.IdsInValue == value) return;
                this._data.IdsInValue = value;
                this.RaisePropertyChanged("IdsInValue");
            }
        }

        public string SelectedElement
        {
            get { return this._selectedElement; }
            set
            {
                if (this._selectedElement == value) return;
                this._selectedElement = value;
                this.RaisePropertyChanged("SelectedElement");
            }
        }

        public  string FlattenedValue
        {
            get
            {
                if (this._data.Value.Any()) {
                return _data.Value.Aggregate((list, next) => list + " " + next).Trim();
            }
                return string.Empty;
            }
        }



        public void InsertSymbol(string toInsert, int cursorLocation, string idOfSymbol)
        {
            if (string.IsNullOrEmpty(toInsert)) return;

            
            var count_already_present = this._data.Value.Count(s => s.Contains(toInsert));
             count_already_present += 1;
            var insert_formatted = toInsert + "_" + count_already_present.ToString();
            this._data.Value.Insert(cursorLocation + 1, insert_formatted);
            this.IdsInValue.Add(insert_formatted , idOfSymbol + "_" + count_already_present.ToString());
          
            this.RaisePropertyChanged("Name");
        }

        public void append_counted_element_and_id(string element, string id)
        {
            if (string.IsNullOrEmpty(element) | string.IsNullOrEmpty(id)) return;

            var count_already_present = this._data.Value.Count(s => s.Contains(element));
            count_already_present += 1;
            var insert_formatted = element + "_" + count_already_present.ToString();

            this._data.Value.Add(insert_formatted);
            
            this._data.IdsInValue.Add(insert_formatted,id);
        }

        public void InsertNonSymbol(string toInsert, int cursorLocation)
        {
            if (string.IsNullOrEmpty(toInsert)) return;

            this._data.Value.Insert(cursorLocation + 1, toInsert);
        }

        public string FindBounds(IList<string> source_string, int startLoc)
        {
            return source_string[startLoc];
        }

        public void RemoveSelected(int cursorIndex)
        {
            string to_remove = this._data.Value[cursorIndex];
            this._data.Value.RemoveAt(cursorIndex);
            if (this._data.IdsInValue.ContainsKey(to_remove)) this._data.IdsInValue.Remove(to_remove);
        }

        public void RemoveContainer(int cursorIndex, string startChar, string endChar)
        {
            int start_index = this.find_container_part(startChar, cursorIndex, this._data.Value);
            int end_index = this.find_container_part(endChar, cursorIndex, this._data.Value);

            if (start_index != -1 & end_index != -1) {
                for (int i = start_index; i < end_index + 1; i++) {
                    this._data.Value.RemoveAt(start_index);
                }
            }

            this.RaisePropertyChanged("Name");
        }

        public string find_container_representation(int cursor_index, string start_marker, string end_marker)
        {
            int start_index = this.find_container_part(start_marker, cursor_index, this._data.Value);
            int count_end_to_find = this.count_target_starts_to_first_close(start_marker, start_index, end_marker,
                                                                            this._data.Value);
            int end_index = this.find_container_part(end_marker, cursor_index, count_end_to_find, this._data.Value);
            string result = string.Empty;

            if (start_index != -1 & end_index != -1) {
                for (int i = start_index; i < end_index + 1; i++) {
                    result += this._data.Value[i] + ' ';
                }
            } else result = "could not find state representation";
            return result.Trim();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Remove(string toRemove)
        {
            string symbol = toRemove.Trim();
            if (this.IdsInValue.ContainsKey(symbol)) this.RemoveSymbol(symbol);
            else this.Value.Remove(symbol);

            //this.NormalizeString();
            this.RaisePropertyChanged("Name");
        }

        public void BeginEdit()
        {
            this._isInEditMode = true;
            this._copy = this._data;
        }

        public void EndEdit()
        {
            if (!this._isInEditMode) return;
            this._copy = new ParseStringData();
            this._isInEditMode = false;
        }

        public void CancelEdit()
        {
            if (!this._isInEditMode) return;
            this._data = this._copy;
            this._copy = new ParseStringData();
            this._isInEditMode = false;
        }

        public bool IsInEditMode
        {
            get { return this._isInEditMode; }
        }

        public void ReplaceSymbol(string to_remove, string replacement, string replacement_id)
        {
            if (this.IdsInValue.ContainsKey(to_remove)) this.IdsInValue.Remove(to_remove);

            int replacement_index = this._data.Value.IndexOf(to_remove);

            this.Value.RemoveAt(replacement_index);
            this._data.Value.Insert(replacement_index, replacement);

            this.IdsInValue.Add(replacement, replacement_id);
            //this.NormalizeString();
            this.RaisePropertyChanged("Name");
        }

        #endregion

        public void NormalizeString()
        {
            // this._value = this._normalizer(this._value);
        }

        private int find_container_part(string target_char, int container_start, IEnumerable<string> source)
        {
            var source_list = new List<string>(source);

            IEnumerable<string> match_list = from x in source_list
                                             where x.Equals(target_char)
                                             select x;

            foreach (string s in match_list) {
                if ((source_list.IndexOf(s) >= container_start)) return source_list.IndexOf(s);
            }
            return -1;
        }

        private int count_target_starts_to_first_close(string target_char, int first_target, string close_marker,
                                                       IEnumerable<string> source)
        {
            var source_list = new List<string>(source);
            int first_close_loc = this.find_container_part(close_marker, first_target, source);

            IEnumerable<string> match_list = from x in source_list
                                             where x.Equals(target_char)
                                             select x;
            int start_count = 0;

            foreach (string s in match_list) {
                int index = source_list.IndexOf(s);
                if (index >= first_target & index < first_close_loc) start_count += 1;
            }

            return (start_count == 0) ? 1 : start_count; //should return at least one.
        }

        private int find_container_part(string target_char, int container_start, int number_to_find,
                                        IEnumerable<string> source)
        {
            var source_list = new List<string>(source);

            IEnumerable<string> match_list = from x in source_list
                                             where x.Equals(target_char)
                                             select x;
            int number_found = 0;
            int last_index_found = 0;
            foreach (string s in match_list) {
                int index = source_list.FindIndex(last_index_found, x => x.Equals(s));
                last_index_found = index + 1;
                if ((index >= container_start)) {
                    number_found += 1;
                    if (number_found == number_to_find) return index;
                }
            }
            return -1;
        }

        public void Replace(string to_remove, string replace_with)
        {
            string symbol = to_remove.Trim();
            if (this.IdsInValue.ContainsKey(' ' + symbol + ' ')) this.RemoveSymbol(symbol);
            else {
                int replacement_index = this._data.Value.IndexOf(symbol);
                this._data.Value.RemoveAt(replacement_index);
                this._data.Value.Insert(replacement_index, replace_with);
            }

            this.RaisePropertyChanged("Name");
        }


        public void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged == null) return;

            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool ShouldLookRight(string input, int startLoc)
        {
            if (startLoc + 1 > input.Length) return false;
            return (input[startLoc] == ' ') & (startLoc > (input.Length/2));
        }

        public void RemoveSymbol(string toRemove)
        {
            string symbol = toRemove.Trim();
            if (this.IdsInValue.ContainsKey(symbol)) {
                this.IdsInValue.Remove(symbol);
                this._data.Value.Remove(toRemove);
                this.NormalizeString();
                this.RaisePropertyChanged("Name");
            }
        }

        private ParseStringData _copy;

        private ParseStringData _data;

        private bool _isInEditMode;

        private string _selectedElement;

        #region Nested type: ParseStringData

        private struct ParseStringData
        {
            public Dictionary<string, string> IdsInValue { get; set; }

            public ObservableCollection<string> Value { get; set; }
        }

        #endregion
    }
}