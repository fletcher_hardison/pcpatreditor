using System.Collections.Generic;

namespace PCPATR.Models.Rules.PSRElements
{
  public class DisjunctiveSymbols
  {
    public DisjunctiveSymbols()
    {
      LeftElements = new List<PsrElement>();

    }


    public List<PsrElement> LeftElements { get; set; }

    public DisjunctiveSymbols EmbeddedDisjuctiveSymbols { get; set; }
  }
}