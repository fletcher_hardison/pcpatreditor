﻿using System;
using System.Collections.Generic;
using System.Linq;

using TFH.Basics.RubyLike;

namespace PCPATR.Models.Rules.PSRElements
{
  public class PhraseStructureRule
  {
    public PhraseStructureRule()
    {

      RightHandSide = new List<PsrElement>();
      LeftHandSide = new SymbolRef();
      Comments = new List<string>();
      RightHandComments = new List<string>();
      RightHandIds = new Dictionary<string, string>();
    }
    

    public SymbolRef LeftHandSide { get; set; }

    public List<PsrElement> RightHandSide { get; set; }

    public List<string> Comments { get; set; }
    public List<string> RightHandComments { get; set; }
    public Dictionary<string, string> RightHandIds { get; set; }
  }
}