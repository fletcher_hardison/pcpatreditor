using System.Xml;
using PCPATR.Models.Rules.RuleExceptions;


namespace PCPATR.Models.Rules.PSRElements
{
    /// <summary>
    /// Abstract class that allows all of the elements of the right hand of a phrase 
    /// structure rule to be added to the same collection
    /// </summary>
    public abstract class PsrElement
    {
        
    }

    public abstract class PsrSymbol : PsrElement
    {

    }
}