using System;
using System.Collections.Generic;
using System.Xml;


namespace PCPATR.Models.Rules.PSRElements
{
  public class OptionalSymbols : PsrSymbol
  {
    public OptionalSymbols()
    {
      _optionalSymbols = new List<PsrElement>();
    }


    public List<PsrElement> Elements
    {
      get { return _optionalSymbols; }
      set { _optionalSymbols = value; }
    }


    public void AddSymbol(PsrElement myElem)
    {
      _optionalSymbols.Add(myElem);
    }


    private List<PsrElement> _optionalSymbols;
  }
}