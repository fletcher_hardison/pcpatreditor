using System.Collections.Generic;

namespace PCPATR.Models.Rules.PSRElements
{
  public class Disjunction : PsrElement
  {
    public Disjunction()
    {
      _disjunctElements = new List<PsrElement>();
      //_disjunctiveSymbolsAdded = false;
    }


    /// <summary>
    /// The seciond option in the disjunction
    /// </summary>
    public DisjunctiveSymbols DisjunctiveSymbols { get; set; }

    /// <summary>
    /// The first option in the disjunction.
    /// </summary>
    public List<PsrElement> LeftElements
    {
      get { return _disjunctElements; }
    }


    public void AddLeft(PsrElement myRef)
    {
      _disjunctElements.Add(myRef);
    }
    public void AddLeft(OptionalSymbols myOptions)
    {
      _disjunctElements.Add(myOptions);
    }


    private readonly List<PsrElement> _disjunctElements;
  }
}