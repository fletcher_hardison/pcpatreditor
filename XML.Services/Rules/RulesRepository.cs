﻿using System.Collections.Generic;
using System.Xml;
using PCPATR.Models.Rules.RuleExceptions;


namespace PCPATR.Models.Rules
{
    /// <summary>
    /// the collection of Phrase Structure rules found in the grammar
    /// </summary>
    public class RulesRepository : List<Rule> 
    {
      public RulesRepository()
      {
        RuleParseErrors = new List<string>();
      }

      public List<string> RuleParseErrors { get; set; }
    }
}