namespace PCPATR.Models
{
  public static class DataModelNames
  {
    public const string PATR_GRAMMAR = "patrGrammar";
    
    public const string FIRST = "first";
    public const string REST = "rest";
    public const string SUBCAT = "subcat";
    public const string RULE = "rule";
    public const string CAT = "cat";
    public const string LEX = "lex";
    public const string GLOSS = "gloss";
    public const string ROOT_GLOSS = "rootgloss";
    public const string NONE = "none";
    public const string LEX_REF = "lexRef";
    public const string CAT_REF = "catRef";
    public const string GLOSS_REF = "glossRef";
    public const string ROOT_GLOSS_REF = "rootFlossRef";
    public const string FEATURE_NAME_REF = "featureNameRef";

    public const string COMPLEX = "complex";
    //Grammar sections
    public const string MASTER_LISTS = "masterLists";

    public const string RULES = "rules";
    public const string PARAMETERS = "parameters";






    public const string TERMINAL = "terminal";
    public const string NONTERMINAL = "nonTerminal";
    public const string TEMPLATE_FEATURE_REF = "templateFeatureRef";

    public const string IS_DEFAULT = "isDefault";
    //MasterList names
    public const string NONTERMINALS = "nonTerminals";
    public const string TERMINALS = "terminals";
    public const string BUILT_IN_PRIMATIVES = "builtInPrimitives";
    public const string FEATURE_SYSTEM = "featureSystem";

    //Parameters
    public const string CATEGORY_FEATURE = "categoryFeature";
    public const string LEXICAL_FEATURE = "lexicalFeature";
    public const string GLOSS_FEATURE = "glossFeature";
    public const string ROOT_GLOSS_FEATURE = "rootGlossFeature";
    public const string ATTRIBUTE_ORDER = "attributeOrder";
    public const string START_SYMBOL = "startSymbol";

    //Collection Feaures
    public const string COLLECTION_FEATURES = "collectionFeatures";
    public const string COLLECTION_FEATURE = "collectionFeature";


    //LogicalFeature system
    public const string COMPLEX_FEATURE_DESC = "complexFeatureDefinition";
    public const string CLOSED_FEATURE_DESC = "closedFeatureDefinition";
    public const string NAME = "name";
    public const string COMPLEX_FEATURE_DESC_REF = "complexFeatureDefinitionRef";
    public const string COMPLEX_FEATURE_REFERENCED = "complex";
    public const string FEATURE_VALUE_DEFINITION = "featureValueDefinition";
    public const string DESCRIPTION = "description";

    //constraints
    public const string PERCOLATION_OPERATION = "percolationOperation";
    public const string PRIORITY_UNION_OPERATION = "priorityUnionOperation";
    public const string LOGICAL_CONSTRAINT = "logicalConstraint";
    public const string UNIFICATION_CONSTRAINT = "unificationConstraint";
    public const string CONSTRAINT_DISJUNCTION = "constraintDisjunction";

    // feature and subcat definitions
    public const string FEATURE_PATH = "featurePath";
    public const string EMBEDDED_FEATURE_PATH = "embeddedFeaturePath";
    public const string SUBCAT_PATH = "subcatPath";
    public const string COMMENT = "comment";
    public const string NODE = "node";
    public const string FEATURE_START = "featureStart";
    public const string FEATURE_END = "featureEnd";
    public const string SUBCAT_END = "subcatEnd";
    public const string FEATURE_AFTER_START = "featureAfterStart";
    public const string FEATURE_AFTER_END = "featureAfterEnd";
    public const string SYMBOL = "symbol";
    public const string VALUE = "value";
    // priority union operations

    public const string CATEGORY_VALUE_REF = "categoryValueRef";
    public const string FEATURE_VALUE_REF = "featureValueRef";
	public const string FEATURE_VALUE_DISJUNCTION = "featureValueDisjunction";
    public const string NONE_VALUE = "noneValue";
    // logical constriant
    public const string LOGICAL_EXPRESSION = "logicalExpression";
    public const string LOGICAL_CONSTRAINT_REF = "logicalConstraintRef";
    public const string UNARY_OPERATION = "unaryOperation";
    public const string BINARY_OPERATION = "binaryOperation";
    public const string OPERATION = "operation";

    public const string CONSTRAINT = "constraint";

    // Unary Operation types
    public const string EXISTENCE = "existence";
    public const string NEGATION = "negation";

    // Binary Operation Types
    public const string AND = "and";
    public const string OR = "or";
    public const string CONDITIONAL = "conditional";
    public const string BICONDITIONAL = "biconditional";

    // factor definitions
    public const string FACTOR = "factor";
    public const string FEATURE = "feature";
    public const string INDEXED_VARIABLE = "indexedVariable";

    public const string USE_WHEN_DEBUGGING = "useWhenDebugging";
    public const string ENABLED = "enabled";

    //Rule definitions
    public const string ID = "id";
    public const string IDENTIFIER = "identifier";
    public const string LEFT_HAND_SIDE = "leftHandSide";
    public const string RIGHT_HAND_SIDE = "rightHandSide";

    public const string SYMBOL_REF = "symbolRef";
    public const string OPTIONAL_SYMBOLS = "optionalSymbols";
    public const string DISJUNCTIVE_SYMBOLS = "disjunctiveSymbols";
    public const string DISJUNCTION = "disjunction";
    public const string PHRASE_STRUCTURE_RULE = "phraseStructureRule";
    public const string CONSTRAINTS = "constraints";

    //Lexical rules
    public const string LEXICAL_RULES = "lexicalRules";
    public const string LEXICAL_RULE = "lexicalRule";
    public const string LEXICAL_RULE_DEFINITION = "lexicalRuleDefinition";
    public const string LEXICAL_RULE_FEATURE_PATH = "lexicalRuleFeaturePath";

    //Constraint Templates
    public const string CONSTRAINT_TEMPLATES = "constraintTemplates";
    public const string CONSTRAINT_TEMPLATE = "constraintTemplate";

    //LogicalFeature Templates
    public const string FEATURE_TEMPLATES = "featureTemplates";
    public const string FEATURE_TEMPLATE = "featureTemplate";
    public const string FEATURE_TEMPLATE_DEFINITION = "featureTemplateDefinition";
    public const string FEATURE_TEMPLATE_NAME = "featureTemplateName";
    public const string TERMINAL_REF = "terminalRef";
    //all these things need to be a collection
    public const string TEMPLATE_FEATURE_STRUCTURE = "templateFeatureStructure";
    public const string TEMPLATE_FEATURE_DISJUNCTION = "templateFeatureDisjunction";
    public const string TEMPLATE_FEATURE_PATH = "templateFeaturePath";
    public const string TEMPLATE_FEATURE_PATH_DISJUNCTION = "templateFeaturePathDisjunction";
    public const string FEATURE_TEMPLATE_REF = "featureTemplateRef";
    public const string EMPTY = "empty";

    //TEMPLATE FEATURE STRUCTURE
    public const string FEATURE_REF = "featureRef";
    public const string SUBCAT_REF = "subcatRef";
    public const string CAT_VALUE = "catValue";
    public const string LEX_VALUE = "lexValue";
    public const string GLOSS_VALUE = "glossValue";
    public const string ROOT_GLOSS_VALUE = "rootGlossValue";
    public const string CAT_VALUE_PATH = "catValuePath";
    public const string LEX_VALUE_PATH = "lexValuePath";
    public const string GLOSS_VALUE_PATH = "glossValuePath";
    public const string NONE_VALUE_PATH = "noneValuePath";
    public const string ROOT_GLOSS_VALUE_PATH = "rootGlossValuePath";
    public const string COLLECTION_FEATURE_REF = "collectionFeatureRef";
    public const string NONE_REF = "noneRef";


    //Subcat Ref 
    public const string COMPLEX_FEATURE_REF = "complexFeatureRef";
    public const string SUBCAT_DISJUNCTION = "subcatDisjunction";
    public const string FIRST_REF = "firstRef";
    public const string REST_REF = "restRef";

    // feature template ref
    public const string TEMPLATE_ATT = "template";

    // subcat end values
    public const string SUBCAT_FIRST = "first";
    public const string SUBCAT_REST = "rest";

  }
}