﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace PCPATR.DataAccess.Parser
{
  public static class LogicalOperationNameMapper
  {
    public const string EXISTANCE = "existence";
    public const string NEGATION = "negation";
    public const string AND = "and";
    public const string OR = "or";
    public const string CONDITIONAL = "conditional";
    public const string BICONDITIONAL = "biconditional";
    public static string VOID = "";

    public static string EXISTANCE_MAPPED = "IS";
    public static string NEGATION_MAPPED = "IS NOT";
    public static string AND_MAPPED = "AND";
    public static string OR_MAPPED = "OR";
    public static string CONDITIONAL_MAPPED = "IF";
    public static string BICONDITIONAL_MAPPED = "IF AND ONLY IF";
    public static string VOID_MAPPED = "type not found";


    public static ObservableCollection<string> mapped_operation_types = new ObservableCollection<string>{
     EXISTANCE_MAPPED,
     NEGATION_MAPPED,
     AND_MAPPED,
     OR_MAPPED,
     CONDITIONAL_MAPPED,
     BICONDITIONAL_MAPPED
    };

    public static string map(string name)
    {
      switch (name) {
        case EXISTANCE:
          return EXISTANCE_MAPPED;
        case NEGATION:
          return NEGATION_MAPPED;
        case AND:
          return AND_MAPPED;
        case OR:
          return OR_MAPPED;
        case CONDITIONAL:
          return CONDITIONAL_MAPPED;
        case BICONDITIONAL:
          return BICONDITIONAL_MAPPED;
        default:
          return VOID_MAPPED;
      }
    }

    public static string map_back(string name)
    {
      if (name == EXISTANCE_MAPPED) return EXISTANCE;
      if (name == NEGATION_MAPPED) return NEGATION;
      if (name == AND_MAPPED) return AND;
      if (name == OR_MAPPED) return OR;
      if (name == CONDITIONAL_MAPPED) return CONDITIONAL;
      if (name == BICONDITIONAL_MAPPED) return BICONDITIONAL;
      return VOID;
    }
  }
}
