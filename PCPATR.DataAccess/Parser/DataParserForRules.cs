using System;
using System.IO;
using System.Linq;
using System.Xml;
using PCPATR.Common;
using PCPATR.Models;
using PCPATR.Models.Rules;
using PCPATR.Models.Rules.PSRElements;
using Microsoft.Practices.Unity;

namespace PCPATR.DataAccess.Parser
{
  public partial class DataParser
  {
    public RulesRepository ParseRulesRepository(XmlNode rules)
    {
      if (rules == null) throw new ArgumentNullException("rules");
      var nLRules = rules.SelectNodes("//rule");
      if (nLRules == null) throw new InvalidDataException("Rules xml definition contains no rule elements" + Environment.NewLine + rules);
      var rulesRep = _container.Resolve<RulesRepository>();
      rulesRep.AddRange(from XmlNode xRule in nLRules where RuleXmlIsValid(xRule,rulesRep) select ParseRule(xRule));
      return rulesRep;
    }


    public bool RuleXmlIsValid(XmlNode rule, RulesRepository rep)
    {
      var isValue = true;
      if (rule.Attributes == null)
      {
        rep.RuleParseErrors.Add("Rule Definition is missing attributes: " + rule);
        isValue = false;
      }
      else
      {
        if (rule.Attributes.GetNamedItem("id") == null)
        {
          rep.RuleParseErrors.Add("Rule Definition is id attributes: " + rule);
          isValue = false;
        }
      }

      return isValue;
    }

    public Rule ParseRule(XmlNode myXmlDef)
    {
      if (myXmlDef == null) throw new ArgumentNullException("myXmlDef");
      var rule = _container.Resolve<Rule>();
      ParseAttributes(myXmlDef, rule);
      if (myXmlDef.ChildNodes.Count > 0)
      {
        foreach (XmlNode ruleElem in myXmlDef.ChildNodes)
        {
          switch (ruleElem.Name)
          {
            case DataModelNames.COMMENT:
              rule.Comments.Add(ruleElem.InnerText);
              break;
            case DataModelNames.IDENTIFIER:
              rule.Identifier = ruleElem.InnerText;
              break;
            case DataModelNames.PHRASE_STRUCTURE_RULE:
              rule.PhraseStructureRule = ParsePhraseStructureRule(ruleElem);
              break;
            case DataModelNames.CONSTRAINTS:
              rule.Constraints = ParseConstraints(ruleElem, rule.ConstraintsComments);
              break;
          }
        }
      }
      return rule;
    }


    public PhraseStructureRule ParsePhraseStructureRule(XmlNode node)
    {
      if (node == null) throw new ArgumentNullException("node");
      var psr = _container.Resolve<PhraseStructureRule>();
      if (node.ChildNodes.Count > 0)
      {
        for (var i = 0; i < node.ChildNodes.Count; i++)
        {
          var psrElem = node.ChildNodes[i];
          switch (psrElem.Name)
          {
            case DataModelNames.LEFT_HAND_SIDE:
              psr.LeftHandSide = ParseSymbolRefToSymbolId(psrElem);
              break;
            case DataModelNames.RIGHT_HAND_SIDE:
              for (var x = 0; x < psrElem.ChildNodes.Count; x++)
              {
                var xRhs = psrElem.ChildNodes[x];
                switch (xRhs.Name)
                {
                  case DataModelNames.SYMBOL_REF:
                    psr.RightHandSide.Add(ParseSymbolRefToSymbolId(xRhs));
                    break;
                  case DataModelNames.OPTIONAL_SYMBOLS:
                    psr.RightHandSide.Add(ParseOptionalSymbols(xRhs));
                    break;
                  case DataModelNames.DISJUNCTION:
                    psr.RightHandSide.Add(ParseRuleElementDisjunction(xRhs));
                    break;
                  case DataModelNames.COMMENT:
                    psr.RightHandComments.Add(xRhs.InnerText);
                    break;
                }
              }
              break;
            case DataModelNames.COMMENT:
              psr.Comments.Add(psrElem.InnerText);
              break;
          }
        }
      }
      else throw new InvalidOperationException("phrase structure rule not missing elements or otherwise not valid.");

      return psr;
    }

    /// <summary>
    /// Parses the symbol ref to a (symbol, id) and inserts in <code>IPhraseStructureRule</code> parameter
    /// </summary>
    /// <param name="xmlDef">Data to parse</param>
    /// <returns>true of parsed correctly</returns>
    public SymbolRef ParseSymbolRefToSymbolId(XmlNode xmlDef)
    {
      if (xmlDef.Attributes == null) throw new InvalidDataException("Symbol Ref missing Identifier and Symbol Identifier");
      XmlNode id = xmlDef.Attributes.GetNamedItem(DataModelNames.ID);
      XmlNode symbol = xmlDef.Attributes.GetNamedItem(DataModelNames.SYMBOL);
      if (id == null) throw new InvalidDataException("Symbol Ref missing Identifier");
      if (symbol == null) throw new InvalidDataException("Symbol Ref missing Symbol Identifier");

      var symbolRef = _container.Resolve<SymbolRef>();
      symbolRef.Id = id.Value;
      symbolRef.Symbol.IdRef = symbol.Value;

      return symbolRef;
    }


    /// <summary>
    /// Parses Optional symbol Xml definition and inserts in <code>IPhraseStructureRule</code>.
    /// </summary>
    /// <param name="myXml">Data to parse (cannot be null)</param>
    /// <returns>true if completed correctly</returns>
    public OptionalSymbols ParseOptionalSymbols(XmlNode myXml)
    {
      if (myXml == null) throw new ArgumentNullException("myXml");
      var m = _container.Resolve<OptionalSymbols>();

      foreach (XmlNode optionalElem in myXml.ChildNodes)
      {
        switch (optionalElem.Name)
        {
          case DataModelNames.SYMBOL_REF:
            m.AddSymbol(ParseSymbolRefToSymbolId(optionalElem));
            break;
          case DataModelNames.OPTIONAL_SYMBOLS:
            m.AddSymbol(ParseOptionalSymbols(optionalElem));
            break;
          case DataModelNames.DISJUNCTION:
            m.AddSymbol(ParseRuleElementDisjunction(optionalElem));
            break;
        }
      }

      return m;
    }

    private Disjunction ParseRuleElementDisjunction(XmlNode xmlDef)
    {
      if (xmlDef == null) throw new ArgumentNullException("xmlDef");
      var m = _container.Resolve<Disjunction>();
      foreach (XmlNode disElem in xmlDef)
      {
        switch (disElem.Name)
        {
          case DataModelNames.SYMBOL_REF:
            m.LeftElements.Add(ParseSymbolRefToSymbolId(disElem));
            break;
          case DataModelNames.OPTIONAL_SYMBOLS:
            m.LeftElements.Add(ParseOptionalSymbols(disElem));
            break;
          case DataModelNames.DISJUNCTIVE_SYMBOLS:
            m.DisjunctiveSymbols = ParseDisjunctiveSymbols(disElem);
            break;
        }
      }
      return m;
    }


    public DisjunctiveSymbols ParseDisjunctiveSymbols(XmlNode xNode)
    {
      if (xNode == null) throw new ArgumentNullException("xNode");
      bool embeddedDisjunctiveSymbolsAdded = false;
      var disSymbols = _container.Resolve<DisjunctiveSymbols>();
      var childNodes = xNode.ChildNodes;
      for (var i = 0; i < childNodes.Count; i++)
      {
        var node = childNodes[i];
        switch (node.Name)
        {
          case DataModelNames.SYMBOL_REF:
            disSymbols.LeftElements.Add(ParseSymbolRefToSymbolId(node));
            break;
          case DataModelNames.OPTIONAL_SYMBOLS:
            disSymbols.LeftElements.Add(ParseOptionalSymbols(node));
            break;
          case DataModelNames.DISJUNCTIVE_SYMBOLS:
            if (embeddedDisjunctiveSymbolsAdded)
            {
              throw new ArgumentException(
                  "Disjunctive Symbols xml definition contains too many embedded Disjuctive Symbols elements.",
                  "xNode");
            }
            disSymbols.EmbeddedDisjuctiveSymbols = ParseDisjunctiveSymbols(node);
            embeddedDisjunctiveSymbolsAdded = true;
            break;
        }
      }

      return disSymbols;
    }

    public LexicalRulesRepository ParseLexicalRules(XmlNode xLexRules)
    {
      var lexRules = _container.Resolve<LexicalRulesRepository>();
      if (xLexRules != null) {
        if (xLexRules.HasChildNodes) {
          xLexRules.ChildNodes.Each(f => lexRules.Rules.Add(ParseLexicalRule(f)));
        }
      }
      
      return lexRules;
    }

    public LexicalRule ParseLexicalRule(XmlNode xLexRule)
    {
      if (xLexRule == null) throw new ArgumentNullException("xLexRule");
      if (xLexRule.HasChildNodes == false) throw new ArgumentException("Lexical Rule xml definition is missing required elements.", "xLexRule");
      var rule = _container.Resolve<LexicalRule>();

      var comments = xLexRule.SelectNodes("./" + DataModelNames.COMMENT);
      comments.Each(f => rule.Comments.Add(f.InnerText));
      
      var name = xLexRule.SelectSingleNode("./" + DataModelNames.NAME);
      rule.Name = name == null ? string.Empty : name.InnerText;

      var ruleDefinitions = xLexRule.SelectNodes("./" + DataModelNames.LEXICAL_RULE_DEFINITION);
      rule.LexicalRuleDefinitions = FEs.Map(ruleDefinitions, ParseLexicalRuleDefinition).ToList();

      //foreach (XmlNode node in xLexRule)
      //{
      //  switch (node.Name)
      //  {
      //    case DataModelNames.COMMENT:
      //      rule.Comments.Add(node.InnerText);
      //      break;
      //    case DataModelNames.NAME:
      //      rule.Name = node.InnerText;
      //      break;
      //    case DataModelNames.LEXICAL_RULE_DEFINITION:
      //      LexicalRuleDefinition lexicalRuleDef = ParseLexicalRuleDefinition(node);
      //      rule.LexicalRuleDefinitions.Add(lexicalRuleDef);
      //      break;
      //  }
      //}

      return rule;
    }

    public LexicalRuleDefinition ParseLexicalRuleDefinition(XmlNode node)
    {
      if (node == null) throw new ArgumentNullException("node");
      if (node.HasChildNodes == false) throw new ArgumentException("LexicalRuleDefinition is empty", "node");
      if (node.ChildNodes.Count != 2) throw new ArgumentException("Lexical Rule Definition has wrong number of terms.", "node");

      var ruleDef = new LexicalRuleDefinition { Left = ParseLexicalRulePath(node.ChildNodes[0]) };

      switch (node.ChildNodes[1].Name)
      {
        case DataModelNames.FEATURE_VALUE_REF:
          ruleDef.RightElement = ParseFeatureValueRef(node.ChildNodes[1]);
          break;
        case DataModelNames.LEXICAL_RULE_FEATURE_PATH:
          ruleDef.RightElement = ParseLexicalRulePath(node.ChildNodes[1]);
          break;
      }


      return ruleDef;
    }

    public LexicalRuleFeaturePath ParseLexicalRulePath(XmlNode xPath)
    {
      if (xPath == null) throw new ArgumentNullException("xPath");
      if (xPath.Attributes == null || xPath.Attributes.Count <= 0) throw new ArgumentException("Lexical Rule LogicalFeature Path is missing required elements.", "xPath");
      if (xPath.Attributes[DataModelNames.FEATURE_START] == null ||
          xPath.Attributes[DataModelNames.FEATURE_END] == null) throw new ArgumentException("Lexical Rule LogicalFeature Path is missing required attributes.", "xPath");

      var lexPath = new LexicalRuleFeaturePath
                    {
                      Start = xPath.Attributes[DataModelNames.FEATURE_START].Value,
                      End =xPath.Attributes[DataModelNames.FEATURE_END].Value
                    };


      return lexPath;
    }
  }
}