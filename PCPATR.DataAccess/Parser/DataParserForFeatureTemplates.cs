using System;
using System.Xml;
using PCPATR.Common;
using PCPATR.Models;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Models.MasterLists;
using Microsoft.Practices.Unity;
using System.Collections.Generic;

namespace PCPATR.DataAccess.Parser
{
  public partial class DataParser
  {
    public IEnumerable<FeatureTemplateModel> ParseFeatureTemplatesCollection(XmlNode xNode) {
      var templates = new List<FeatureTemplateModel>();

      if (xNode == null) return templates;
      if (xNode.ChildNodes.Count < 1)
        throw new ArgumentException("LogicalFeature Templates Collection xml definition has no feature templates",
                                    "xNode");

      

      foreach (XmlNode node in xNode.SelectNodes("./featureTemplate")) {
        templates.Add(ParseFeatureTemplate(node));
      }

      return templates;
    }

    public FeatureTemplateModel ParseFeatureTemplate(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.ChildNodes.Count < 1)
        throw new ArgumentException("LogicalFeature Template xml definition is missing required elements", "xNode");


      var template = _container.Resolve<FeatureTemplateModel>();

      ParseAttributes(xNode, template);

      foreach (XmlNode node in xNode) {
        switch (node.Name) {
          case DataModelNames.COMMENT:
            template.Comments.Add(node.InnerText);
            break;
          case DataModelNames.FEATURE_TEMPLATE_DEFINITION:
            var tup = ParseFeatureTemplateDefinition(node);
            template.Left = tup.Item1;
            template.RightValues = tup.Item2;
            break;
        }
      }
      return template;
    }

    public Tuple<FeatureTemplateIdBase, List<object>> ParseFeatureTemplateDefinition(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode", "The argument that was pass was null.");
      if (xNode.HasChildNodes == false || xNode.ChildNodes.Count < 2)
        throw new ArgumentException("LogicalFeature Template Definition element is missing required child elements.",
                                    "xNode");

      FeatureTemplateIdBase left = null;
      var right = new List<object>();

      foreach (XmlNode node in xNode) {
        switch (node.Name) {
          case DataModelNames.FEATURE_TEMPLATE_NAME:
            if (left != null)
              throw new ArgumentException(
                  "LogicalFeature Template definition has too many LogicalFeature template name and or Terminal Ref elements");
            left = ParseFeatureTemplateName(node);
            break;
          case DataModelNames.TERMINAL_REF:
            if (left != null)
              throw new ArgumentException(
                  "LogicalFeature Template definition has too many LogicalFeature template name and or Terminal Ref elements");
            left = ParseTerminalRef(node);
            break;
          case DataModelNames.TEMPLATE_FEATURE_STRUCTURE:
            right.Add(ParseTemplateFeatureStructure(node));
            break;
          case DataModelNames.TEMPLATE_FEATURE_DISJUNCTION:
            right.Add(ParseTemplateFeatureDisjunction(node));
            break;
          case DataModelNames.TEMPLATE_FEATURE_PATH:
            right.Add(ParseTemplateFeaturePath(node));
            break;
          case DataModelNames.TEMPLATE_FEATURE_PATH_DISJUNCTION:
            right.Add(ParseTemplateFeaturePathDisjunction(node));
            break;
          case DataModelNames.FEATURE_TEMPLATE_REF:
            right.Add(ParseFeatureTemplateRef(node));
            break;
          case DataModelNames.CAT_VALUE_PATH:
            right.Add(ParseCatValuePath(node));
            break;
          case DataModelNames.LEX_VALUE_PATH:
            right.Add(ParseXValuePath(node));
            break;
          case DataModelNames.GLOSS_VALUE_PATH:
            right.Add(ParseXValuePath(node));
            break;
          case DataModelNames.ROOT_GLOSS_VALUE_PATH:
            right.Add(ParseXValuePath(node));
            break;
          case DataModelNames.EMPTY:
            right.Add(ParseEmptyElement());
            break;
        }
      }

      return Tuple.Create(left, right);
    }

    private FeatureTemplateModelBase ParseEmptyElement() {
      return new EmptyElement();
    }


    public CollectionFeatureRef ParseCollectionFeatureRef(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.Attributes == null)
        throw new ArgumentException("Collection LogicalFeature Ref xml definition missing required attrubutes");
      var collectionFeaure = xNode.Attributes.GetNamedItem(DataModelNames.COLLECTION_FEATURE);

      if (collectionFeaure == null)
        throw new ArgumentException("Collection LogicalFeature Ref missing required Identifier reference", "xNode");
      if (xNode.ChildNodes.Count > 1)
        throw new ArgumentException("Collection LogicalFeature Ref has wrong number of child elements", "xNode");

      var colFeatRef = new CollectionFeatureRef { CollectionFeature = collectionFeaure.Value };

      var node = xNode.FirstChild;
      switch (node.Name) {
        case DataModelNames.NONE_REF:
          colFeatRef.EmbeddedPath = ParseNoneRef(node);
          break;
        case DataModelNames.CAT_VALUE:
          colFeatRef.EmbeddedPath = ParseCatValue(node);
          break;
        case DataModelNames.LEX_VALUE:
          colFeatRef.EmbeddedPath = ParseXValue(node);
          break;
        case DataModelNames.GLOSS_VALUE:
          colFeatRef.EmbeddedPath = ParseXValue(node);
          break;
        case DataModelNames.ROOT_GLOSS_VALUE:
          colFeatRef.EmbeddedPath = ParseXValue(node);
          break;
        case DataModelNames.TEMPLATE_FEATURE_STRUCTURE:
          colFeatRef.EmbeddedPath = ParseTemplateFeatureStructure(node);
          break;
        case DataModelNames.FEATURE_REF:
          colFeatRef.EmbeddedPath = ParseFeatureRef(node);
          break;
        case DataModelNames.FEATURE:
          colFeatRef.EmbeddedPath = ParseLogicalFeature(node);
          break;
        case DataModelNames.FEATURE_TEMPLATE_REF:
          colFeatRef.EmbeddedPath = ParseFeatureTemplateRef(node);
          break;
      }


      GetIsdefault(xNode, colFeatRef);

      return colFeatRef;
    }


    public FeatureTemplateFeatureStructure ParseTemplateFeatureStructure(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");

      var tfs = new FeatureTemplateFeatureStructure();

      foreach (XmlNode node in xNode) {
        switch (node.Name) {
          case DataModelNames.TEMPLATE_FEATURE_DISJUNCTION:
            tfs.Elements.Add(ParseTemplateFeatureDisjunction(node));
            break;
          case DataModelNames.COMPLEX_FEATURE_REF:
            tfs.Elements.Add(ParseComplexFeatureRef(node));
            break;
          case DataModelNames.FEATURE_REF:
            tfs.Elements.Add(ParseFeatureRef(node));
            break;
          case DataModelNames.FEATURE:
            tfs.Elements.Add(ParseLogicalFeature(node));
            break;
          case DataModelNames.CAT_VALUE:
            tfs.Elements.Add(ParseCatValue(node));
            break;
          case DataModelNames.LEX_VALUE:
            tfs.Elements.Add(ParseXValue(node));
            break;
          case DataModelNames.GLOSS_VALUE:
            tfs.Elements.Add(ParseXValue(node));
            break;
          case DataModelNames.ROOT_GLOSS_VALUE:
            tfs.Elements.Add(ParseXValue(node));
            break;
          case DataModelNames.COLLECTION_FEATURE_REF:
            tfs.Elements.Add(ParseCollectionFeatureRef(node));
            break;
          case DataModelNames.SUBCAT_REF:
            tfs.Elements.Add(ParseSubcatRef(node));
            break;
          default:
            throw new ApplicationException("cannot parse a node of type " + node.Name + " in a feature template disjunction");
        }
      }
      return tfs;
    }

    public ComplexFeatureRef ParseComplexFeatureRef(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.Attributes == null || xNode.Attributes.GetNamedItem(DataModelNames.FEATURE) == null)
        throw new ArgumentException("Complex LogicalFeature Ref is missing required Identifier Ref attribute.", "xNode");
      if (xNode.ChildNodes.Count < 1)
        throw new ArgumentException(
            "Complex LogicalFeature Ref is missing required LogicalFeature Ref or Subcat Ref elements", "xNode");

      var complex = new ComplexFeatureRef { Id = xNode.Attributes.GetNamedItem(DataModelNames.FEATURE).Value };

      foreach (XmlNode node in xNode) {
        switch (node.Name) {
          case DataModelNames.FEATURE_REF:
            complex.Values.Add(ParseFeatureRef(node));
            break;
          case DataModelNames.SUBCAT_REF:
            complex.Values.Add(ParseSubcatRef(node));
            break;
          case DataModelNames.COMPLEX_FEATURE_REF:
            complex.Values.Add(ParseComplexFeatureRef(node));
            break;
          case DataModelNames.COLLECTION_FEATURE_REF:
            complex.Values.Add(ParseCollectionFeatureRef(node));
            break;
          case DataModelNames.TEMPLATE_FEATURE_DISJUNCTION:
            complex.Values.Add(ParseTemplateFeatureDisjunction(node));
            break;
          case DataModelNames.CAT_VALUE:
            complex.Values.Add(ParseCatValue(node));
            break;
          case DataModelNames.FEATURE:
            complex.Values.Add(ParseLogicalFeature(node));
            break;
          case DataModelNames.COMMENT:
            complex.Comments.Add(node.InnerText);
            break;
          default:
            throw new ApplicationException("Cannot parse element of type " + node.Name + "as child of complexFeatureRef");
        }
      }

      return complex;
    }


    public FeatureRef ParseFeatureRef(XmlNode xRef) {
      if (xRef == null) throw new ArgumentNullException("xRef");

      if (xRef.Attributes != null) {
        var featureRef = new FeatureRef(xRef.Attributes[DataModelNames.FEATURE_START].Value,
                                        xRef.Attributes[DataModelNames.FEATURE_END].Value);
        GetIsdefault(xRef, featureRef);

        if (xRef.HasChildNodes) {
          var node = xRef.FirstChild;
          switch (node.Name) {
            case DataModelNames.FEATURE_REF:
              featureRef.Child = ParseFeatureRef(node);
              break;
            case DataModelNames.COLLECTION_FEATURE_REF:
              featureRef.Child = ParseCollectionFeatureRef(node);
              break;
            case DataModelNames.FEATURE_TEMPLATE_REF:
              featureRef.Child = ParseFeatureTemplateRef(node);
              break;
            case DataModelNames.TEMPLATE_FEATURE_DISJUNCTION:
              featureRef.Child = ParseTemplateFeatureDisjunction(node);
              break;
            default:
              throw new ApplicationException("Cannot parse node of type " + node.Name + " as child of Feature Ref");
          }

        }

        return featureRef;
      }
      throw new ApplicationException("Feature Ref is ill-formed and missing required data.");
    }

    public CatValue ParseCatValue(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.Attributes == null)
        throw new ArgumentException("Cat Name xml definition missing required attrubutes");

      if (xNode.Attributes[DataModelNames.SYMBOL] == null)
        throw new ArgumentException("Cat Name missing required Symbol attribute.", "xNode");

      var catVal = new CatValue { Symbol = new NameIdRef(xNode.Attributes[DataModelNames.SYMBOL].Value) };


      GetIsdefault(xNode, catVal);

      return catVal;
    }

    public NoneRef ParseNoneRef(XmlNode xNode) {
      return new NoneRef();
    }

    public FeatureTemplateModelBase ParseXValue(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      var isDefault = false;
      if (xNode.Attributes != null && xNode.Attributes[0] != null) {
        isDefault = xNode.Attributes[0].Value == "yes";
      }

      return MapNodeNameToValueClassType(xNode.Name, xNode.InnerText, isDefault);
    }



    public FeatureTemplateModelBase MapNodeNameToValueClassType(string nodeName, string value, bool isDefault) {
      switch (nodeName) {
        case DataModelNames.LEX_VALUE:
          var lex = _container.Resolve<LexValue>();
          lex.Value = value;
          lex.IsDefault = isDefault;

          return lex;
        case DataModelNames.GLOSS_VALUE:
          var gloss = _container.Resolve<GlossValue>();
          gloss.Value = value;
          gloss.IsDefault = isDefault;

          return gloss;
        case DataModelNames.ROOT_GLOSS_VALUE:
          var root = _container.Resolve<RootGlossValue>();
          root.Value = value;
          root.IsDefault = isDefault;

          return root;
      }

      return null;
    }

    public FeatureTemplateModelBase ParseXValuePath(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      var isDefault = false;
      if (xNode.Attributes != null && xNode.Attributes[0] != null) {
        isDefault = xNode.Attributes[0].Value == "yes";
      }

      return MppNodeNameToValuePathClass(xNode.Name, xNode.InnerText, isDefault);
    }

    public FeatureTemplateModelBase MppNodeNameToValuePathClass(string nodeName, string value, bool isDefault) {
      switch (nodeName) {
        case DataModelNames.LEX_VALUE_PATH:
          var lex = _container.Resolve<LexValuePath>();
          lex.Value = value;
          lex.IsDefault = isDefault;

          return lex;
        case DataModelNames.GLOSS_VALUE_PATH:
          var gloss = _container.Resolve<GlossValuePath>();
          gloss.Value = value;
          gloss.IsDefault = isDefault;

          return gloss;
        case DataModelNames.ROOT_GLOSS_VALUE_PATH:
          var root = _container.Resolve<RootGlossValuePath>();
          root.Value = value;
          root.IsDefault = isDefault;

          return root;
      }

      return null;
    }

    //public LexValue ParseLexValue(XmlNode xNode)
    //{
    //    if (xNode == null) throw new ArgumentNullException("xNode");
    //    //if (xNode.InnerText == null) throw new ArgumentException("Lex Name xml definition missing text");
    //    var lex = new LexValue
    //                  {
    //                      LexicalValue = xNode.InnerText
    //                  };


    //    if (xNode.Attributes[DataModelNames.IS_DEFAULT] == null)
    //    {
    //        lex.IsDefault = false;
    //    }
    //    lex.IsDefault = (xNode.Attributes[DataModelNames.IS_DEFAULT].Name == "yes");

    //    return lex;
    //}

    //public GlossValue ParseGlossValue(XmlNode xNode)
    //{
    //    if (xNode == null) throw new ArgumentNullException("xNode");
    //    //if (xNode.InnerText == null) throw new ArgumentException("Lex Name xml definition missing text");
    //    var lex = new GlossValue
    //                  {
    //                      Gloss = xNode.InnerText
    //                  };


    //    if (xNode.Attributes[DataModelNames.IS_DEFAULT] == null)
    //    {
    //        lex.IsDefault = false;
    //    }
    //    lex.IsDefault = (xNode.Attributes[DataModelNames.IS_DEFAULT].Name == "yes");

    //    return lex;
    //}

    //public RootGlossValue ParseRootGlossValue(XmlNode xNode)
    //{
    //    if (xNode == null) throw new ArgumentNullException("xNode");
    //    //if (xNode.InnerText == null) throw new ArgumentException("Lex Name xml definition missing text");
    //    var lex = new RootGlossValue
    //                  {
    //                      RootGloss = xNode.InnerText
    //                  };


    //    if (xNode.Attributes[DataModelNames.IS_DEFAULT] == null)
    //    {
    //        lex.IsDefault = false;
    //    }
    //    else
    //    {
    //        lex.IsDefault = (xNode.Attributes[DataModelNames.IS_DEFAULT].Name == "yes");
    //    }
    //    return lex;
    //}

    public SubcatRef ParseSubcatRef(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.HasChildNodes == false || xNode.ChildNodes.Count > 2)
        throw new ArgumentException("Subcat Ref xml definition is ill-formed", "xNode");

      var subcatRef = new SubcatRef();

      GetIsdefault(xNode, subcatRef);

      if (xNode.ChildNodes.Count == 2) {
        subcatRef.Value = ParseFirstOrRestRef(xNode.ChildNodes[0], xNode.ChildNodes[1]);
      } else {
        switch (xNode.ChildNodes[0].Name) {
          case DataModelNames.NONE_REF:
            subcatRef.Value = ParseNoneRef(xNode.ChildNodes[0]);
            break;
          case DataModelNames.SUBCAT_DISJUNCTION:
            subcatRef.Value = ParseSubcatDisjunction(xNode.ChildNodes[0]);
            break;
        }
      }

      return subcatRef;
    }

    private static void GetIsdefault(XmlNode xNode, FeatureTemplateModelBase element) {
      if (xNode.Attributes[DataModelNames.IS_DEFAULT] == null) {
        element.IsDefault = false;
      } else {
        element.IsDefault = (xNode.Attributes[DataModelNames.IS_DEFAULT].Value == "yes");
      }
    }

    private FeatureTemplateModelBase ParseSubcatDisjunction(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.HasChildNodes == false || xNode.ChildNodes.Count < 2)
        throw new ArgumentException("Subcat Ref xml definition is ill-formed", "xNode");

      var subDis = new SubcatDisjunction();

      GetIsdefault(xNode, subDis);

      int x = 1;
      switch (xNode.ChildNodes[0].Name) {
        case DataModelNames.FIRST_REF:
          subDis.FirstFactor = ParseFirstOrRestRef(xNode.ChildNodes[0], xNode.ChildNodes[1]);
          x++;
          break;
        case DataModelNames.REST_REF:
          throw new Exception("ParseSubcatDisjunctionMethod did not correctly parse First Rest Ref pair.");
        case DataModelNames.NONE_REF:
          subDis.FirstFactor = ParseNoneRef(xNode.ChildNodes[0]);
          break;
      }


      for (int i = x; i < xNode.ChildNodes.Count; i++) {
        switch (xNode.ChildNodes[i].Name) {
          case DataModelNames.FIRST_REF:
            subDis.OtherFactors.Add(ParseFirstOrRestRef(xNode.ChildNodes[i], xNode.ChildNodes[(i + 1)]));
            i++;
            break;
          case DataModelNames.REST_REF:
            throw new Exception("ParseSubcatDisjunctionMethod did not correctly parse First Rest Ref pair.");
          case DataModelNames.NONE_REF:
            subDis.OtherFactors.Add(ParseNoneRef(xNode.ChildNodes[i]));
            break;
        }
      }

      return subDis;
    }

    private FeatureTemplateModelBase ParseFirstOrRestRef(XmlNode xFirst, XmlNode xRest) {
      if (xFirst == null) throw new ArgumentNullException("xFirst");
      if (xRest == null) throw new ArgumentNullException("xRest");

      if (xFirst.HasChildNodes == false || xFirst.ChildNodes.Count > 1)
        throw new ArgumentException("First Ref xml definition is ill-formed", "xFirst");
      if (xRest.HasChildNodes == false || xRest.ChildNodes.Count > 1)
        throw new ArgumentException("Rest Ref xml definition is ill-formed", "xRest");

      var first = xFirst.ChildNodes[0];
      var rest = xRest.ChildNodes[0];
      if (first == null) throw new ArgumentException("First Ref is missing required elements.", "xFirst");
      if (rest == null) throw new ArgumentException("Rest Ref is missing required elements", "xRest");

      var firstRest = new SubcatFirstRest { First = SelectFirstRestElement(first), Rest = SelectFirstRestElement(rest) };


      return firstRest;
    }

    private FeatureTemplateModelBase SelectFirstRestElement(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");

      switch (xNode.Name) {
        case DataModelNames.COMPLEX_FEATURE_REF:
          return ParseComplexFeatureRef(xNode);
        case DataModelNames.FEATURE_REF:
          return ParseFeatureRef(xNode);
        case DataModelNames.TEMPLATE_FEATURE_DISJUNCTION:
          return ParseTemplateFeatureDisjunction(xNode);
        case DataModelNames.NONE_REF:
          return ParseNoneRef(xNode);
        case DataModelNames.FEATURE_TEMPLATE_REF:
          return ParseFeatureTemplateRef(xNode);
        case DataModelNames.TEMPLATE_FEATURE_STRUCTURE:
          return ParseTemplateFeatureStructure(xNode);
        case DataModelNames.SUBCAT_DISJUNCTION:
          return ParseSubcatDisjunction(xNode);
      }
      return null;
    }

    public FeatureTemplateFeatureDisjunction ParseTemplateFeatureDisjunction(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.HasChildNodes == false || xNode.ChildNodes.Count < 2)
        throw new ArgumentException("Template LogicalFeature Disjunction xml definition is ill-formed.", "xNode");

      var tempFeatDis = new FeatureTemplateFeatureDisjunction { LeftTerm = ChooseTemplateFeatureDisjunctionElement(xNode.ChildNodes[0]) };

      for (int i = 1; i < xNode.ChildNodes.Count; i++) {
        tempFeatDis.RightTerms.Add(ChooseTemplateFeatureDisjunctionElement(xNode.ChildNodes[i]));
      }

      return tempFeatDis;
    }

    private object ChooseTemplateFeatureDisjunctionElement(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      switch (xNode.Name) {
        case DataModelNames.TEMPLATE_FEATURE_STRUCTURE:
          return ParseTemplateFeatureStructure(xNode);
        case DataModelNames.FEATURE_TEMPLATE_REF:
          return ParseFeatureTemplateRef(xNode);
        case DataModelNames.COLLECTION_FEATURE_REF:
          return ParseCollectionFeatureRef(xNode);
        case DataModelNames.LEX_VALUE:
          return ParseXValue(xNode);
        case DataModelNames.FEATURE_VALUE_REF:
          return ParseFeatureValueRef(xNode);
        case DataModelNames.NONE_VALUE:
          return ParseNoneValue();
        default:
          throw new ApplicationException("cannot parse node of type " + xNode.Name + "as child of templateFeatureDisjunction");
      }
    }

    private static FeatureTemplateModelBase ParseFeatureTemplateRef(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      var xAtts = xNode.Attributes;
      if (xAtts == null)
        throw new ArgumentException("LogicalFeature Template Ref is missing required Identifier ref attribute.", "xNode");


      return new FeatureFeatureTemplateRef { Id = xAtts.GetNamedItem(DataModelNames.TEMPLATE_ATT).Value.ToNameIdRef() };
    }

    public TemplateFeaturePath ParseTemplateFeaturePath(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");

      if (xNode.Attributes == null || xNode.Attributes.Count > 3)
        throw new ArgumentException(
            "Template LogicalFeature Path is missing or has wrong number of required attributes.", "xNode");

      if (xNode.Attributes[DataModelNames.FEATURE_START] == null)
        throw new ArgumentException(
            "Template LogicalFeature Path xml definition is missing require LogicalFeature Start attribute.", "xNode");
      if (xNode.Attributes[DataModelNames.FEATURE_END] == null)
        throw new ArgumentException(
            "Template LogicalFeature Path xml definition is missing require LogicalFeature End attribute.", "xNode");

      var tempPath = new TemplateFeaturePath {
        Start = xNode.Attributes[DataModelNames.FEATURE_START].Value,
        End = xNode.Attributes[DataModelNames.FEATURE_END].Value
      };
      GetIsdefault(xNode, tempPath);


      if (xNode.HasChildNodes) {
        if (xNode.ChildNodes.Count != 1)
          throw new ArgumentException("Template LogicalFeature Path has wrong number of values.", "xNode");
        {
          var node = xNode.ChildNodes[0];
          switch (node.Name) {
            case DataModelNames.TEMPLATE_FEATURE_PATH:
              tempPath.Value = ParseTemplateFeaturePath(node);
              break;
            case DataModelNames.CAT_VALUE_PATH:
              tempPath.Value = ParseCatValuePath(node);
              break;
            case DataModelNames.ROOT_GLOSS_VALUE_PATH:
            // fall through
            case DataModelNames.LEX_VALUE_PATH:
            //fall through
            case DataModelNames.GLOSS_VALUE_PATH:
              tempPath.Value = ParseXValuePath(node);
              break;
            case DataModelNames.NONE_VALUE_PATH:
              tempPath.Value = ParseNoneValuePath(node);
              break;
            default:
              throw new ApplicationException("Cannot parse node of type " + node.Name + "as child of templateFeaturePath");
          }
        }
      }

      return tempPath;
    }

    private object ParseNoneValuePath(XmlNode node) {
      var isDefault = false;

      if (node.Attributes != null) {
        var isDefaultAtt = node.Attributes.GetNamedItem(DataModelNames.IS_DEFAULT);
        if (isDefaultAtt != null) {
          isDefault = isDefaultAtt.Value == "yes";
        }
      }

      return new NoneValuePath(node.InnerText) { IsDefault = isDefault };
    }


    public CatValuePath ParseCatValuePath(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      var path = new CatValuePath();

      if (xNode.Attributes == null || xNode.Attributes.Count < 1)
        throw new ArgumentException("Cat Name Path xml definition is missing required attributes.", "xNode");

      if (xNode.Attributes[DataModelNames.SYMBOL] == null)
        throw new ArgumentException(
            "Cat Name Path xml definition is missing required Symbol id ref attribute.", "xNode");
      path.Symbol = xNode.Attributes[DataModelNames.SYMBOL].Value.ToNameIdRef();

      GetIsdefault(xNode, path);

      return path;
    }

    //public LexValuePath ParseLexValuePath(XmlNode xNode)
    //{
    //    if (xNode == null) throw new ArgumentNullException("xNode");

    //    var path = new LexValuePath();

    //    if (xNode.Attributes[DataModelNames.IS_DEFAULT] == null)
    //    {
    //        path.IsDefault = false;
    //    }
    //    else
    //    {
    //        path.IsDefault = (xNode.Attributes[DataModelNames.IS_DEFAULT].Name == "yes");
    //    }
    //    return path;
    //}

    //public RootGlossValuePath ParseGlossValuePath(XmlNode xNode)
    //{
    //    if (xNode == null) throw new ArgumentNullException("xNode");

    //    var path = new RootGlossValuePath();

    //    if (xNode.Attributes[DataModelNames.IS_DEFAULT] == null)
    //    {
    //        path.IsDefault = false;
    //    }
    //    else
    //    {
    //        path.IsDefault = (xNode.Attributes[DataModelNames.IS_DEFAULT].Name == "yes");
    //    }
    //    return path;
    //}

    //public RootGlossValuePath ParseRootGlossValuePath(XmlNode xNode)
    //{
    //    if (xNode == null) throw new ArgumentNullException("xNode");

    //    var path = new RootGlossValuePath();

    //    if (xNode.Attributes[DataModelNames.IS_DEFAULT] == null)
    //    {
    //        path.IsDefault = false;
    //    }
    //    else
    //    {
    //        path.IsDefault = (xNode.Attributes[DataModelNames.IS_DEFAULT].Name == "yes");
    //    }
    //    return path;
    //}

    public FeatureTemplateFeaturePathDisjunction ParseTemplateFeaturePathDisjunction(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.Attributes == null || xNode.Attributes.Count != 2)
        throw new ArgumentException(
            "Template LogicalFeature Path Disjunction xml definition is missing required attributes");
      var featureStart = xNode.Attributes.GetNamedItem(DataModelNames.FEATURE_START);
      var featureEnd = xNode.Attributes.GetNamedItem(DataModelNames.FEATURE_END);
      var pathDis = new FeatureTemplateFeaturePathDisjunction {
        Start = featureStart.Value,
        End = featureEnd.Value
      };

      var firstChild = xNode.FirstChild;
      var disjunctsIndex = 1;

      switch (firstChild.Name) {
        case DataModelNames.FEATURE_VALUE_REF:
          pathDis.LeftPath = ParseFeatureValueRef(xNode.ChildNodes[0]);
          break;
        case DataModelNames.TEMPLATE_FEATURE_PATH:
          pathDis.TemplatePath = ParseTemplateFeaturePath(firstChild);
          pathDis.LeftPath = ParseFeatureValueRef(xNode.ChildNodes[1]);
          disjunctsIndex = 2;
          break;
        default:
          throw new ApplicationException("Cannot parse node of type" + firstChild.Name + " as first element in templateFeaturePathDisjunction.");
      }

      for (int i = disjunctsIndex; i < xNode.ChildNodes.Count; i++) {
        pathDis.RightPaths.Add(ParseFeatureValueRef(xNode.ChildNodes[i]));
      }
      return pathDis;
    }

    public FeatureTemplateNameModel ParseFeatureTemplateName(XmlNode xNode) {
      if (xNode == null) throw new ArgumentNullException("xNode");
      var tempName = new FeatureTemplateNameModel();
      if (xNode.Attributes != null) {
        if (xNode.Attributes[DataModelNames.ID] != null)
        {
          tempName.Id = xNode.Attributes[DataModelNames.ID].Value;
        }
      }

      tempName.Name = xNode.InnerText;

      return tempName;
    }

    public TerminalRefModel ParseTerminalRef(XmlNode xNode) {
      var terminalRef = new TerminalRefModel();
      
// ReSharper disable ConvertIfStatementToConditionalTernaryExpression
      if (xNode.Attributes == null)
// ReSharper restore ConvertIfStatementToConditionalTernaryExpression
      {
        terminalRef.Id = "ref id missing from data";
      } else
      {
        terminalRef.Id = xNode.Attributes[DataModelNames.TERMINAL].Value;        
      }

      return terminalRef;
    }
  }



}