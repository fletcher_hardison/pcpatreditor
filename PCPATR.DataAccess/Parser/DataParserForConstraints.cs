using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Microsoft.Practices.Unity;
using PCPATR.Common;
using PCPATR.Models;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Models.Rules;
using PCPATR.Models.Rules.Constraints;


namespace PCPATR.DataAccess.Parser
{
  public partial class DataParser
  {
    #region Properties

    public XmlDocument XmlDoc { get; set; }

    public string PathToGrammar { get; set; }

    private readonly IUnityContainer _container;

    #endregion

    public DataParser(IUnityContainer container)
    {
      _container = container;
    }

    #region Methods

    public void LoadGrammar(string pathToFile)
    {
      XmlDoc = new XmlDocument();
      XmlDoc.Load(pathToFile);
    }

    #region FeaturePaths

    public FeaturePathModel ParseFeaturePath(XmlNode myXmlDef)
    {
      if (myXmlDef == null) throw new ArgumentNullException("myXmlDef");

      var fPath = new FeaturePathModel();

      if (myXmlDef.Attributes != null && myXmlDef.Attributes.Count > 0)
      {
        foreach (XmlAttribute xAtt in myXmlDef.Attributes)
        {
          switch (xAtt.Name)
          {
            case DataModelNames.NODE:
              fPath.node = new NameIdRef(xAtt.Value);
              break;
            case DataModelNames.FEATURE_START:
              fPath.feature_start = new NameIdRef(xAtt.Value);
              break;
            case DataModelNames.FEATURE_END:
              fPath.feature_end = new NameIdRef(xAtt.Value);
              break;
          }
        }
      }
      else throw new InvalidOperationException("LogicalFeature Path Xml Definition missing required attributes");

      if (myXmlDef.HasChildNodes)
      {
        fPath.EmbeddedFeaturePath = ParseFeaturePathEmbeddedElement(myXmlDef);
      }

      if (fPath.node == null || fPath.feature_start == null) throw new InvalidOperationException("LogicalFeature Path Xml Definition missing required attributes");
      return fPath;
    }

    public object ParseFeaturePathEmbeddedElement(XmlNode parentNode)
    {
      var node = parentNode.FirstChild;
      switch (node.Name)
      {
        case DataModelNames.EMBEDDED_FEATURE_PATH:
          return ParseEmbeddedFeaturePath(node);
        case DataModelNames.CAT_VALUE_PATH:
          return ParseCatValuePath(node);
        case DataModelNames.LEX_VALUE_PATH:
        //fall through
        case DataModelNames.ROOT_GLOSS_VALUE_PATH:
        //fall through
        case DataModelNames.GLOSS_VALUE_PATH:
          return ParseXValuePath(node);
      }

      throw new ApplicationException("Not able to parse a node of type " + node.Name + " in a feature path.");
    }

    public object ParseEmbeddedFeaturePath(XmlNode node)
    {
      var startId = node.Attributes[DataModelNames.FEATURE_START].Value;
      var endId = string.Empty;
      var featureEnd = node.SelectSingleNode("./@" + DataModelNames.FEATURE_END);
      if (featureEnd != null)
      {
        endId = featureEnd.InnerText;
      }
      var result = new EmbeddedFeaturePath(startId, endId);

      if (node.HasChildNodes)
      {
        result.ChildPath = ParseEmbeddedFeaturePath(node.FirstChild) as EmbeddedFeaturePath;
      }

      return result;
    }

    public SubcatPathModel ParseSubcatPath(XmlNode myXmlDef)
    {
      if (myXmlDef == null) throw new ArgumentNullException("myXmlDef");

      var sPath = new SubcatPathModel();

      if (myXmlDef.Attributes != null && myXmlDef.Attributes.Count > 0)
      {
        foreach (XmlAttribute xAtt in myXmlDef.Attributes)
        {
          switch (xAtt.Name)
          {
            case DataModelNames.NODE:
              sPath.node = new NameIdRef(xAtt.Value);
              break;
            case DataModelNames.FEATURE_START:
              sPath.feature_start = new NameIdRef(xAtt.Value);
              break;
            case DataModelNames.FEATURE_END:
              sPath.feature_end = new NameIdRef(xAtt.Value);
              break;
            case DataModelNames.SUBCAT_END:
              sPath.subcat_end = _parse_subcat_end(xAtt.Value);
              break;
            case DataModelNames.FEATURE_AFTER_END:
              sPath.feature_after_start = new NameIdRef(xAtt.Value);
              break;
            case DataModelNames.FEATURE_AFTER_START:
              sPath.feature_after_start = new NameIdRef(xAtt.Value);
              break;
          }
        }
      }
      else throw new InvalidOperationException("LogicalFeature Path Xml Definition missing required attributes");

      if (!sPath.node.HasValue) throw new InvalidOperationException("LogicalFeature Path Xml Definition missing required attributes");

      return sPath;
    }

    public FeatureValueRefModel ParseFeatureValueRef(XmlNode xNode)
    {
      if (xNode == null) throw new ArgumentNullException("xNode");

      var fValRef = new FeatureValueRefModel();

      if (xNode.Attributes == null || xNode.Attributes[DataModelNames.VALUE] == null) throw new ArgumentException("LogicalFeature Name Ref is not defined in Xml file");

      fValRef.value = new NameIdRef(xNode.Attributes.GetNamedItem(DataModelNames.VALUE).Value);
      var isDefaultAtt = xNode.Attributes.GetNamedItem(DataModelNames.IS_DEFAULT);
      if (isDefaultAtt != null)
      {
        fValRef.IsDefault = isDefaultAtt.Value == "yes";
      }
      return fValRef;
    }

    public FeatureValueDisjunction ParseFeatureValueDisjunction(XmlNode node)
    {
      if (node == null) return null;
      var model = _container.Resolve<FeatureValueDisjunction>();
      if (node.ChildNodes.Count < 2) return null;
      //work on first child node
      model.LeftElement = ParseFeatureValueRef(node.ChildNodes[0]);

      //work on rest of child nodes
      for (var i = 1; i < node.ChildNodes.Count; i++)
      {
        var childNode = node.ChildNodes[i];
        model.RightElements.Add(ParseFeatureValueRef(childNode));
      }

      return model;
    }

    public NoneValueModel ParseNoneValue()
    {
      return new NoneValueModel();
    }


    private readonly Func<string, SubcatEndValues> _parse_subcat_end = str =>
    {
      switch (str)
      {
        case "noneValue":
          return SubcatEndValues.NoneValue;
        case "first":
          return SubcatEndValues.First;
        case "end":
          return SubcatEndValues.Rest;
        default:
          return SubcatEndValues.NoneValue;
      }
    };

    #endregion

    public CategoryValueRefModel ParseCategoryValueRef(XmlNode xNode)
    {
      if (xNode == null) throw new ArgumentNullException("xNode");
      var catRef = new CategoryValueRefModel();
      if (xNode.Attributes == null || xNode.Attributes.Count != 1) throw new InvalidOperationException("Category Name Ref is not defined in Xml file");

      var node = xNode.Attributes.GetNamedItem(DataModelNames.NODE);
      if (node != null)
      {
        catRef.value = new NameIdRef(node.Value);
      } else
      {
        catRef.value = new NameIdRef(xNode.Attributes[DataModelNames.SYMBOL].Value);
      }
      return catRef;
    }

    #region constraints

    public List<ConstraintModelBase> ParseConstraints(XmlNode xmlDef, List<string> comments)
    {
      if (xmlDef == null) throw new ArgumentNullException("xmlDef");
      if (xmlDef.ChildNodes.Count < 1) throw new ArgumentException("Xml definition of Constraints object is ill-formed", "xmlDef");

      var constraints = new List<ConstraintModelBase>();

      foreach (XmlNode node in xmlDef.ChildNodes)
      {
        switch (node.Name)
        {
          case DataModelNames.PERCOLATION_OPERATION:
            constraints.Add(ParsePercolationOperation(node));
            break;
          case DataModelNames.PRIORITY_UNION_OPERATION:
            constraints.Add(ParsePriorityUnionOperation(node));
            break;
          case DataModelNames.CONSTRAINT_DISJUNCTION:
            constraints.Add(ParseConstraintDisjunction(node));
            break;
          case DataModelNames.UNIFICATION_CONSTRAINT:
            constraints.Add(ParseUnificationConstraint(node));
            break;
          case DataModelNames.LOGICAL_CONSTRAINT:
            constraints.Add(ParseLogicalConstraint(node));
            break;
          case DataModelNames.COMMENT:
            comments.Add(node.InnerText);
            break;
        }
      }


      return constraints;
    }



    /// <summary>
    /// Parses attributes needed by all types of constraints
    /// </summary>
    /// <param name="xNode">Xml definition containing the attributes</param>
    /// <param name="constraint">rule whose attributes need to be set</param>
    protected virtual void ParseAttributes(XmlNode xNode, ConstraintModelBase constraint)
    {
      if (xNode == null) throw new InvalidOperationException("Cannot set rule attributes if deffinition is null");
      if (xNode.Attributes == null || xNode.Attributes.Count <= 0)
      {
        constraint.enabled = true;
        constraint.use_when_debugging = false;
      }
      else
      {
        if (xNode.Attributes.GetNamedItem(DataModelNames.ENABLED) != null) constraint.enabled = xNode.Attributes.GetNamedItem(DataModelNames.ENABLED).Value != "no";

        if (xNode.Attributes.GetNamedItem(DataModelNames.USE_WHEN_DEBUGGING) != null)
        {
          constraint.use_when_debugging =
            xNode.Attributes.GetNamedItem(DataModelNames.USE_WHEN_DEBUGGING).Value == "yes";
        }
      }
    }

    protected virtual void ParseAttributes(XmlNode xNode, Rule rule)
    {
      if (xNode == null) throw new InvalidOperationException("Cannot set rule attributes if deffinition is null");
      rule.enabled = true;
      rule.use_when_debugging = false;

      if (xNode.Attributes == null) return;

      var enabled  = xNode.Attributes.GetNamedItem(DataModelNames.ENABLED);
      var useWhenDebugging = xNode.Attributes.GetNamedItem(DataModelNames.USE_WHEN_DEBUGGING);
      var id = xNode.Attributes.GetNamedItem(DataModelNames.ID);

      if (id != null)
      {
        rule.Id = id.Value;
      } else
      {
        rule.Id = "noIdFound" + new Random().Next();
      }

      if (useWhenDebugging != null)
      {
        rule.use_when_debugging = useWhenDebugging.Value != "no";
      }
      if (enabled != null)
      {
        rule.enabled = enabled.Value == "yes";
      }
    }

    protected virtual void ParseAttributes(XmlNode xNode, FeatureTemplateModel template)
    {
      if (xNode == null) throw new ArgumentNullException("xNode");


      if (xNode.Attributes == null || xNode.Attributes.Count <= 0)
      {
        template.Enabled = true;
        template.UseWhenDebugging = false;
      }
      else
      {
        if (xNode.Attributes.GetNamedItem(DataModelNames.ENABLED) != null) template.Enabled = xNode.Attributes.GetNamedItem(DataModelNames.ENABLED).Value != "no";

        if (xNode.Attributes.GetNamedItem(DataModelNames.USE_WHEN_DEBUGGING) != null)
        {
          template.UseWhenDebugging =
            xNode.Attributes.GetNamedItem(DataModelNames.USE_WHEN_DEBUGGING).Value == "yes";
        }
      }
    }

    //Percolation Operations
    public PercolationOperationModel ParsePercolationOperation(XmlNode myXmldef)
    {
      var perOp = new PercolationOperationModel();

      ParseAttributes(myXmldef, perOp);

      for (int i = 0; i <= myXmldef.ChildNodes.Count - 1; i++)
      {
        XmlNode xNode = myXmldef.ChildNodes.Item(i);
        if (xNode == null) continue;

        switch (xNode.Name)
        {
          case DataModelNames.FEATURE_PATH:
            if (i == 0) perOp.left = ParseFeaturePath(xNode);
            else perOp.right = ParseFeaturePath(xNode);
            break;
          case DataModelNames.SUBCAT_PATH:
            if (i == 0) perOp.left = ParseSubcatPath(xNode);
            else perOp.right = ParseSubcatPath(xNode);
            break;
          case DataModelNames.COMMENT:
            perOp.comments.Add(xNode.InnerText);
            break;
        }
      }

      return perOp;
    }


    public PriorityUnionModel ParsePriorityUnionOperation(XmlNode xmlDef)
    {
      var pUnionOp = new PriorityUnionModel();

      if (xmlDef == null) throw new ArgumentNullException("xmlDef");
      if (xmlDef.ChildNodes == null && xmlDef.ChildNodes.Count <= 0) throw new InvalidOperationException("cannot parse Priority Union Operations if ");
      if (xmlDef.ChildNodes.Count < 2)
      {
        throw new InvalidOperationException(
          "Priority Union Operations has too few child feature paths or subcat paths");
      }

      ParseAttributes(xmlDef, pUnionOp);

      for (int i = 0; i <= xmlDef.ChildNodes.Count - 1; i++)
      {
        XmlNode xNode = xmlDef.ChildNodes.Item(i);
        if (xNode == null) continue;

        switch (xNode.Name)
        {
          case DataModelNames.FEATURE_PATH:
            if (i == 0) pUnionOp.left = ParseFeaturePath(xNode);
            else pUnionOp.right = ParseFeaturePath(xNode);
            break;
          case DataModelNames.SUBCAT_PATH:
            if (i == 0) pUnionOp.left = ParseSubcatPath(xNode);
            else pUnionOp.right = ParseSubcatPath(xNode);
            break;
          //The following can only be in the right side of priority union operaiont
          //they will be automatically discard from corrupt data.
          case DataModelNames.FEATURE_VALUE_REF:
            if (i == 1) pUnionOp.right = ParseFeatureValueRef(xNode);
            break;
          case DataModelNames.CATEGORY_VALUE_REF:
            if (i == 1) pUnionOp.right = ParseCategoryValueRef(xNode);
            break;
          case DataModelNames.FEATURE_VALUE_DISJUNCTION:
            if (i == 0) pUnionOp.right = ParseFeatureValueDisjunction(xNode);
            break;
          case DataModelNames.COMMENT:
            pUnionOp.comments.Add(xNode.InnerText);
            break;
        }
      }

      return pUnionOp;
    }

    public UnificationConstraintModel ParseUnificationConstraint(XmlNode xmlDef)
    {
      if (xmlDef == null) throw new ArgumentNullException("xmlDef");

      var uniCon = new UnificationConstraintModel();


      if (xmlDef.ChildNodes == null && xmlDef.ChildNodes.Count <= 0) throw new ArgumentException("cannot parse Unification Constraint if ", "xmlDef");
      if (xmlDef.ChildNodes.Count < 2)
      {
        throw new ArgumentException(
          "Unification Constraint has too few child feature paths or subcat paths", "xmlDef");
      }

      ParseAttributes(xmlDef, uniCon);

      for (int i = 0; i <= xmlDef.ChildNodes.Count - 1; i++)
      {
        XmlNode xNode = xmlDef.ChildNodes.Item(i);
        if (xNode == null) continue;

        switch (xNode.Name)
        {
          case DataModelNames.FEATURE_PATH:
            if (i == 0) uniCon.left = ParseFeaturePath(xNode);
            else uniCon.right = ParseFeaturePath(xNode);

            break;

          case DataModelNames.SUBCAT_PATH:
            if (i == 0) uniCon.left = ParseSubcatPath(xNode);
            else uniCon.right = ParseSubcatPath(xNode);
            break;
          //the following elements are only allowed on right side of a unification constraint. 
          // They will automatically be discared from corrupt data.
          case DataModelNames.FEATURE_VALUE_REF:
            if (i == 1) uniCon.right = ParseFeatureValueRef(xNode);
            break;
          case DataModelNames.CATEGORY_VALUE_REF:
            if (i == 1) uniCon.right = ParseCategoryValueRef(xNode);
            break;
          case DataModelNames.LEX_VALUE_PATH:
          //fall through
          case DataModelNames.GLOSS_VALUE_PATH:
          //fall through
          case DataModelNames.ROOT_GLOSS_VALUE_PATH:
            if (i == 1) uniCon.right = ParseXValuePath(xNode);
            break;
          case DataModelNames.TEMPLATE_FEATURE_STRUCTURE:
            if (i == 1) uniCon.right = ParseTemplateFeatureStructure(xNode);
            break;
          case DataModelNames.NONE_VALUE:
            if (i == 1) uniCon.right = ParseNoneValue();
            break;
          case DataModelNames.FEATURE_VALUE_DISJUNCTION:
            if (i == 1) uniCon.right = ParseFeatureValueDisjunction(xNode);
            break;
          case DataModelNames.COMMENT:
            uniCon.comments.Add(xNode.InnerText);
            break;
        }
      }

      return uniCon;
    }

    public LogicalConstraintRefModel ParseLogicalConstraintRef(XmlNode myXml)
    {
      if (myXml == null) throw new ArgumentNullException("myXml");

      if (myXml.Attributes == null || myXml.Attributes.Count <= 0)
      {
        throw new ArgumentException("Cannot Build Logical Constraint Ref from ill formed xml definition.",
                                    "myXml");
      }


      if (myXml.Attributes.GetNamedItem(DataModelNames.CONSTRAINT) == null)
      {
        throw new ArgumentException(
          "Constraint to reference attribute of Logical Constraint Ref cannot be null", "myXml");
      }     

      var logConstrRef = new LogicalConstraintRefModel
      {
        constraint = new NameIdRef(myXml.Attributes.GetNamedItem(DataModelNames.CONSTRAINT).Value),
      };


      return logConstrRef;
    }


    public IEnumerable<string> ParseLogicalConstraintRefToString(XmlNode myXml)
    {
      if (myXml == null) throw new ArgumentNullException("myXml");
      if (myXml.Attributes == null || myXml.Attributes.Count <= 0)
      {
        throw new ArgumentException("Cannot Build Logical Constraint Ref from ill formed xml definition.",
                                    "myXml");
      }
      XmlNode constraint_att = myXml.Attributes.GetNamedItem(DataModelNames.CONSTRAINT);
      XmlNode use_when_debugging = myXml.Attributes.GetNamedItem(DataModelNames.USE_WHEN_DEBUGGING);
      XmlNode enabled = myXml.Attributes.GetNamedItem(DataModelNames.ENABLED);

      if (constraint_att == null)
      {
        throw new ArgumentException(
          "Constraint to reference attribute of Logical Constraint Ref cannot be null", "myXml");
      }
      if (use_when_debugging == null)
      {
        throw new ArgumentException("Logical Constraint Ref missing required Use When Debugging attribute",
                                    "myXml");
      }
      if (enabled == null) throw new ArgumentException("Logical Constraint Ref missing required enabled attribute", "myXml");


      return string.Format(" [ref name:{0} enabled:{1} Use_when_debugging:{2} ] ", constraint_att.Value,
                           enabled.Value, use_when_debugging.Value).Split(' ');
    }

    public ConstraintDisjunctionModel ParseConstraintDisjunction(XmlNode xmlDef)
    {
      if (xmlDef == null) throw new ArgumentNullException("xmlDef");

      if (xmlDef.ChildNodes.Count < 2) throw new ArgumentException("Constraint disjunction xml is ill-formed", "xmlDef");
      var throwAwayComments = new List<string>();
      var conDis = new ConstraintDisjunctionModel { LeftConstraints = ParseConstraints(xmlDef.ChildNodes.Item(0), throwAwayComments).ToList() };

      for (int i = 1; i <= xmlDef.ChildNodes.Count - 1; i++)
      {
        var child = xmlDef.ChildNodes[i];
        if (child != null)
        {
          if (child.GetType() == typeof(XmlElement))
          {
            conDis.RightConstraints.Add(ParseConstraints(child, throwAwayComments).ToList());
          }
          else if (child.GetType() == typeof(XmlComment))
          {
            conDis.comments.Add(child.InnerText);
          }
        }
      }


      return conDis;
    }

    public LogicalConstraintModel ParseLogicalConstraint(XmlNode xNode)
    {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.ChildNodes.Count < 2) throw new ArgumentException("Logical Constraint Xml definition is missing required elements.", "xNode");

      var logCon = new LogicalConstraintModel();

      foreach (XmlNode node in xNode)
      {
        switch (node.Name)
        {
          case DataModelNames.FEATURE_PATH:
            logCon.left = ParseFeaturePath(node);
            break;
          case DataModelNames.LOGICAL_EXPRESSION:
            logCon.right = ParseLogicalExpression(node);
            break;
          case DataModelNames.LOGICAL_CONSTRAINT_REF:
            logCon.right = ParseLogicalConstraintRef(node);
            break;
          case DataModelNames.COMMENT:
            logCon.comments.Add(node.InnerText);
            break;
          default:
            throw new ArgumentException("Logical Constraint Xml Definition is ill-formed.", "xNode");
        }
      }
      ParseAttributes(xNode, logCon);

      return logCon;
    }


    public LogicalExpressionModel ParseLogicalExpression(XmlNode def)
    {
      Func<XmlNode, LogicalExpressionModel> parser_func = x =>
      {
        var logExp = new LogicalExpressionModel();
        var xOperation = x.ChildNodes[0];
        switch (xOperation.Name)
        {
          case DataModelNames.UNARY_OPERATION:
            logExp.operation = ParseUnaryOperation(xOperation);
            break;
          case DataModelNames.BINARY_OPERATION:
            logExp.operation = ParseBinaryOperation(xOperation);
            break;
        }

        return logExp;
      };
      return parser_func(def);
    }

    private object parse_factor(XmlNode x)
    {
      switch (x.Name)
      {
        case DataModelNames.FEATURE:
          return ParseLogicalFeature(x);
        case DataModelNames.LOGICAL_EXPRESSION:
          return ParseLogicalExpression(x);
        case DataModelNames.COLLECTION_FEATURE_REF:
          return ParseCollectionFeatureRef(x);
        case DataModelNames.COMPLEX_FEATURE_REF:
          return ParseComplexFeatureRef(x);
        case DataModelNames.CAT_VALUE:
          return ParseCatValue(x);
        case DataModelNames.LEX_VALUE:
        //fall through
        case DataModelNames.GLOSS_VALUE:
        //fall through
        case DataModelNames.ROOT_GLOSS_VALUE:
          return ParseXValue(x);
      }
      return null;
    }

    public UnaryOperationModel ParseUnaryOperation(XmlNode x)
    {
      if (x.Attributes != null)
      {
        var uom = new UnaryOperationModel
        {
          OperationType = x.Attributes.GetNamedItem("operation").Value
        };
        var child = x.SelectSingleNode("child::factor/child::*");
        uom.Factor1 = parse_factor(child);

        return uom;
      }

      return null;
    }

    public BinaryOperationModel ParseBinaryOperation(XmlNode x)
    {
      var bim = new BinaryOperationModel();
      if (x.Attributes != null) bim.OperationType = x.Attributes.GetNamedItem("operation").Value;
      var children = x.SelectNodes("./factor");
      //xpath hierarchy is ./factor/feature OR ./factor/logicalExpression
      if (children != null)
      {
        var factor1Child = children[0].ChildNodes[0];
        var factor2Child = children[1].ChildNodes[0];
      
        bim.Factor1 = parse_factor(factor1Child);
        bim.Factor2 = parse_factor(factor2Child);

      }

      return bim;
    }

    public LogicalFeatureModel ParseLogicalFeature(XmlNode x)
    {
      try
      {
        var start = x.Attributes[DataModelNames.FEATURE_START].Value;
        var end = x.Attributes[DataModelNames.FEATURE_END].Value;
        if (x.Attributes != null)
        {
          var index = x.Attributes.GetNamedItem(DataModelNames.INDEXED_VARIABLE);


          var logical_feature = new LogicalFeatureModel
          {
            feature_start = new NameIdRef(start),
            feature_end = new NameIdRef(end),
            indexed_variable = index == null ? "none" : index.Value
          };
          if (x.HasChildNodes)
            logical_feature.embedded_feature = ParseLogicalFeature(x.ChildNodes[0]);

          return logical_feature;
        }
      }
      catch (Exception)
      {
        return null;
      }

      return null;
    }


    #endregion

    #endregion // end constraints


    #region IDataParser Members

    public ConstraintTemplatesModel ParseConstraintTemplates(XmlNode xNode)
    {
      if (xNode == null) return null;
      //if (xNode.HasChildNodes == false) throw new ArgumentException("Constraint Templates collection xml definition is empty", "xNode");
      var templates = new ConstraintTemplatesModel();

      foreach (XmlNode node in xNode)
      {
        ConstraintTemplateModel cTemplate = ParseConstraintTemplate(node);

        templates.Add(cTemplate);
      }

      return templates;
    }

    public ConstraintTemplateModel ParseConstraintTemplate(XmlNode xNode)
    {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.HasChildNodes == false) throw new ArgumentException("Constraint Template is missing child element", "xNode");


      if (xNode.Attributes == null || xNode.Attributes[DataModelNames.ID] == null)
      {
        throw new ArgumentException(
          "Constraint Template xml definition is missing required Identifier attribute", "xNode");
      }

      //if (xNode.Attributes[(string)DataModelNames.ENABLED] == null) {
      //  throw new ArgumentException(
      //    "Constraint Template xml definition is missing requried enabled attribute", "xNode");
      //}

      //if (xNode.Attributes[(string)DataModelNames.USE_WHEN_DEBUGGING] == null) {
      //  throw new ArgumentException(
      //    "Constraint Template xml definition i smissisng required Use When Debugging Attribute.", "xNode");
      //}

      var template = new ConstraintTemplateModel(); // {
      //  enabled = xNode.Attributes.GetNamedItem(DataModelNames.ENABLED).Value == "yes",
      //  use_when_debugging =
      //    xNode.Attributes.GetNamedItem(DataModelNames.USE_WHEN_DEBUGGING).Value == "yes",
      //};

      var id = xNode.Attributes[DataModelNames.ID].Value;
      var name = string.Empty;
      foreach (XmlNode child in xNode)
      {
        switch (child.Name)
        {
          case DataModelNames.NAME:
            name = child.InnerText;
            break;
          case DataModelNames.LOGICAL_EXPRESSION:
            template.LogicalExpression = ParseLogicalExpression(child);
            break;
          case DataModelNames.COMMENT:
            template.Comments.Add(child.InnerText);
            break;
        }
      }

      template.Name = name;
      template.Id = id;

      if (template.Id == null || template.LogicalExpression == null)
      {
        throw new ArgumentException(
          "Constraint Template xml definition was missing its Name and/or Logical Expression element(s).",
          "xNode");
      }

      return template;
    }

    #endregion
  }
}