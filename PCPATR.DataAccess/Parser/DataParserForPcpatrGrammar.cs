using System;
using System.Collections.Generic;
using System.Xml;
using PCPATR.Models;


namespace PCPATR.DataAccess.Parser
{
    public partial class DataParser 
    {
        public PcpatrGrammar ParsePcpatrGrammar(XmlNode xNode)
        {
            if (xNode == null) throw new ArgumentNullException("xNode");
            if (xNode.HasChildNodes == false) throw new ArgumentException("Grammar's Xml definition is missing required elements");

            var grammar = new PcpatrGrammar();

            foreach (XmlNode node in xNode)
            {
                switch (node.Name)
                {
                    case DataModelNames.COMMENT:
                        grammar.Comments.Add(node.InnerText);
                        break;
                    case DataModelNames.FEATURE_TEMPLATES:
                        grammar.FeatureTemplates = ParseFeatureTemplatesCollection(node);
                        break;
                    case DataModelNames.CONSTRAINT_TEMPLATES:
                        grammar.ConstraintTemplates = ParseConstraintTemplates(node);
                        break;
                    case DataModelNames.RULES:
                        grammar.Rules = ParseRulesRepository(node);
                        break;
                    case DataModelNames.PARAMETERS:
                        grammar.Parameters = ParseParameters(node);
                        break;
                    case DataModelNames.LEXICAL_RULES:
                        grammar.LexicalRules = ParseLexicalRules(node);
                        break;
                    case  DataModelNames.MASTER_LISTS:
                        grammar.MasterLists = ParseMasterLists(node);
                        break;
                        

                }
            }

            return grammar;

        }

        
        public List<string>  GetIdList(XmlNode xmlNode)
        {
            if (xmlNode == null) throw new ArgumentNullException("xmlNode");

            var ids = xmlNode.SelectNodes("//@id");
            var idList = new List<string>();


            foreach (XmlAttribute id in ids) {
                idList.Add(id.Value);
            }

            return idList;
        }
    }
}
