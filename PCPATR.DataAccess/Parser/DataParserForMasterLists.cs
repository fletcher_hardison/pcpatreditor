using System;
using System.Collections.Generic;
using System.Xml;
using Microsoft.Practices.Unity;
using PCPATR.Common;
using PCPATR.Models;
using PCPATR.Models.FeatureSystem;
using PCPATR.Models.MasterLists;
using PCPATR.Models.Parameters;


namespace PCPATR.DataAccess.Parser
{
  public partial class DataParser
  {
    public MasterListsModel ParseMasterLists(XmlNode xMasterLists)
    {
      if (xMasterLists == null) throw new ArgumentNullException("xMasterLists");
      var masterList = new MasterListsModel();


      if (xMasterLists.ChildNodes.Count <= 0)
        throw
          new ArgumentException("Master Lists xml definition is ill-formed.");

      foreach (XmlNode xNode in xMasterLists) {
        switch (xNode.Name) {
          case DataModelNames.NONTERMINALS:
            masterList.NonTerminals = ParseSymbols(xNode);
            break;
          case DataModelNames.TERMINALS:
            masterList.Terminals = ParseSymbols(xNode);
            break;
          case DataModelNames.BUILT_IN_PRIMATIVES:
            masterList.GrammarBuildInPrimatives = ParseBuiltInPrimitives(xNode);
            break;
          case DataModelNames.FEATURE_SYSTEM:
            masterList.GrammarFeatureSystem = ParseFeatureSystem(xNode);
            break;
          case DataModelNames.COLLECTION_FEATURES:
            masterList.GrammarCollectionFeatures = ParseCollectionFeatures(xNode);
            break;
        }
      }


      return masterList;
    }


    public List<Symbol> ParseSymbols(XmlNode xSymbols)
    {
      if (xSymbols == null) throw new ArgumentNullException("xSymbols");


      if (xSymbols.ChildNodes.Count < 1) throw new ArgumentException("Terminal or Non-Terminal symbols xml definition is ill-formed");

      var grammarSymbols = new List<Symbol>();

      foreach (XmlNode xTs in xSymbols) {
        var symbol = _container.Resolve<Symbol>();
        if (xTs.Attributes == null || xTs.Attributes.GetNamedItem(DataModelNames.ID) == null)
          throw new ArgumentException(
            "Terminal or Non-Termiinal symbols contains illformed xml definition for symbol", "xSymbols");

        symbol.Id = xTs.Attributes.GetNamedItem(DataModelNames.ID).Value;

        foreach (XmlNode xNode in xTs) {
          switch (xNode.Name) {
            case DataModelNames.COMMENT:
              symbol.Comments.Add(xNode.InnerText);
              break;
            case DataModelNames.NAME:
              symbol.Name = xNode.InnerText;
              break;
            case DataModelNames.DESCRIPTION:
              symbol.Description = xNode.InnerText;
              break;
          }
        }

        if (grammarSymbols.Contains(symbol))
          throw new ArgumentException(
            "Terminal or Non-Terminal symbols contains symbols with non unique id.", "xSymbols");
        grammarSymbols.Add(symbol);
      }

      return grammarSymbols;
    }


    public CollectionFeatures ParseCollectionFeatures(XmlNode xCollectionFeatures)
    {
      var collectionFeats = _container.Resolve<CollectionFeatures>();

      if (xCollectionFeatures == null || xCollectionFeatures.ChildNodes.Count <= 0) return collectionFeats;

      var childNodes = xCollectionFeatures.SelectNodes("./" + DataModelNames.COLLECTION_FEATURE);
      collectionFeats.Features.AddRange(FEs.Map(childNodes, ParseCollectionFeature));

      
      return collectionFeats;
    }

    public CollectionFeature ParseCollectionFeature(XmlNode xFeature)
    {
      if (xFeature == null) throw new ArgumentNullException("xFeature");

      if (xFeature.HasChildNodes == false) throw new ArgumentException("Collection LogicalFeature xml Definition is empty", "xFeature");
      var colFeature = new CollectionFeature();

      if (xFeature.Attributes == null || xFeature.Attributes[DataModelNames.ID] == null)
        throw new ArgumentException("Collection Features xml definition is missing is required ID attribute",
                                    "xFeature");

      colFeature.Id = xFeature.Attributes[DataModelNames.ID].Value;

      foreach (XmlNode node in xFeature) {
        switch (node.Name) {
          case DataModelNames.COMMENT:
            colFeature.Comments.Add(node.InnerText);
            break;
          case DataModelNames.NAME:
            colFeature.Name = node.InnerText;
            break;
          case DataModelNames.DESCRIPTION:
            colFeature.Description = node.InnerText;
            break;
        }
      }

      return colFeature;
    }


    public FeatureSystem ParseFeatureSystem(XmlNode xFeatureSystem)
    {
      var featureSystem = _container.Resolve<FeatureSystem>();
      if (xFeatureSystem == null || xFeatureSystem.ChildNodes.Count < 1) return featureSystem;
      
      featureSystem.FeaturesXmlNode = xFeatureSystem;
      
      foreach (XmlNode xNode in xFeatureSystem) {
        switch (xNode.Name) {
          case DataModelNames.COMPLEX_FEATURE_DESC:
            var complex = ParseComplexFeatureDefinition(xNode);

            if (featureSystem.Features.Contains(complex))
              throw new ArgumentException("LogicalFeature system contains multiple elements with the same id.",
                                          "xFeatureSystem");
            featureSystem.Features.Add(complex);
            break;

          case DataModelNames.CLOSED_FEATURE_DESC:
            var closed = ParseClosedFeatureDefinition(xNode);
            if (featureSystem.Features.Contains(closed))
              throw new ArgumentException("LogicalFeature system contains multiple elements with the same id.",
                                          "xFeatureSystem");
            featureSystem.Features.Add(closed);

            break;
        }
      }

      return featureSystem;
    }

    public BuiltInPrimitives ParseBuiltInPrimitives(XmlNode xBip)
    {
      if (xBip == null) throw new ArgumentNullException("xBip");

      var bips = _container.Resolve<BuiltInPrimitives>();


      //XmlNode catId = xBip.SelectSingleNode("./cat");
      //XmlNode lexId = xBip.SelectSingleNode("./lex");
      //XmlNode glossId = xBip.SelectSingleNode("./gloss");
      //XmlNode rootGlossId = xBip.SelectSingleNode("./rootgloss");
      //XmlNode noneId = xBip.SelectSingleNode("./none");

      //if (catId == null || lexId == null || glossId == null || rootGlossId == null || noneId == null)
      //    throw new ArgumentException("Built in Primitives xml definition is missing required elements", "xBip");

      //if (catId.Attributes == null || catId.Attributes.GetNamedItem("id") == null)
      //    throw new ArgumentException("Built in Primitives Cat Identifier is ill-formed", "xBip");

      //bips.CatId = catId.Attributes.GetNamedItem("id").InnerText;


      //if (lexId.Attributes == null || lexId.Attributes.GetNamedItem("id") == null)
      //    throw new ArgumentException("Built in Primitives Lex Identifier is ill-formed", "xBip");

      //bips.LexId = new NameIdRef(lexId.Attributes.GetNamedItem("id").InnerText);


      //if (glossId.Attributes == null || glossId.Attributes.GetNamedItem("id") == null)
      //    throw new ArgumentException("Built in Primitives Gloss Identifier is ill-formed", "xBip");

      //bips.GlossId = glossId.Attributes.GetNamedItem("id").InnerText;


      //if (rootGlossId.Attributes == null || rootGlossId.Attributes.GetNamedItem("id") == null)
      //    throw new ArgumentException("Built in Primitives Root Gloss Identifier is ill-formed", "xBip");

      //bips.RootGlossId = rootGlossId.Attributes.GetNamedItem("id").InnerText;


      //if (noneId.Attributes == null || noneId.Attributes.GetNamedItem("id") == null)
      //    throw new ArgumentException("Built in Primitives None Identifier is ill-formed", "xBip");

      //bips.NoneId = noneId.Attributes.GetNamedItem("id").InnerText;


      return bips;
    }


    protected virtual void ParseAttributes(XmlNode xNode, Feature description)
    {
      if (xNode == null) {
        throw new ArgumentNullException("xNode", "Cannot set rule attributes if deffinition is null");
      }
      if (xNode.Attributes == null || xNode.Attributes.Count <= 0) throw new ArgumentException("LogicalFeature Descripion attributes are ill-formed");


      if (xNode.Attributes[DataModelNames.ID] != null) {
        description.Id = xNode.Attributes[DataModelNames.ID].Value;
      }
    }


    public Feature ParseComplexFeatureDefinition(XmlNode xmlComplex)
    {
      if (xmlComplex == null) throw new ArgumentNullException("xmlComplex");

      var complex = new Feature {IsComplex = true, IsReference = false};

      ParseAttributes(xmlComplex, complex);

      foreach (XmlNode xmlComplexElem in xmlComplex.ChildNodes) {
        switch (xmlComplexElem.Name) {
          case DataModelNames.NAME:
            complex.Name = xmlComplexElem.InnerText;
            break;
          case DataModelNames.COMMENT:
            complex.Comments.Add(xmlComplex.InnerText);
            break;
          case DataModelNames.CLOSED_FEATURE_DESC:

            var closedDesc = ParseClosedFeatureDefinition(xmlComplexElem);
            if (complex.Children.Contains(closedDesc)) {
              throw new ArgumentException(
                "Complex LogicalFeature description contains Closed LogicalFeature Description without an unique Identifier",
                "xmlComplex");
            }

            complex.Children.Add(closedDesc);

            break;
          case DataModelNames.COMPLEX_FEATURE_DESC:

            var embeddedComplex = ParseComplexFeatureDefinition(xmlComplexElem);
            if (complex.Children.Contains(embeddedComplex)) {
              throw new ArgumentException(
                "Complex LogicalFeature description contains Complex LogicalFeature Description without an unique Identifier",
                "xmlComplex");
            }

            complex.Children.Add(embeddedComplex);

            break;
          case DataModelNames.COMPLEX_FEATURE_DESC_REF:
            var complexFeatDesc = ParseComplexFeatureDefRef(xmlComplexElem);
            complex.Children.Add(complexFeatDesc);
            break;
        }
      }
      return complex;
    }

    public Feature ParseComplexFeatureDefRef(XmlNode xmlComplexRef)
    {
      if (xmlComplexRef == null) throw new ArgumentNullException("xmlComplexRef");

      if (xmlComplexRef.Attributes == null)
        throw new ArgumentException(
          "Complex LogicalFeature Ref xml definition is missing required attributes", "xmlComplexRef");

      var complexDefRef = new Feature {IsComplex = false, IsReference = true};

      foreach (XmlAttribute xAtt in xmlComplexRef.Attributes) {
        switch (xAtt.Name) {
          case DataModelNames.ID:
            complexDefRef.Id = xAtt.Value;
            break;
          case DataModelNames.COMPLEX_FEATURE_REFERENCED:
            complexDefRef.TargetId = xAtt.Value;
            break;
        }
      }

      if (complexDefRef.Id == null)
        throw new ArgumentException(
          "Complex LogicalFeature Definition Reference xml defintion is missing required ID attribute",
          "xmlComplexRef");

      if (complexDefRef.TargetId == null)
        throw new ArgumentException(
          "Complex LogicalFeature Definition Reference xml defintion is missing required Complex feature referenced id attribute",
          "xmlComplexRef");


      return complexDefRef;
    }


    public Feature ParseClosedFeatureDefinition(XmlNode xmlClosed)
    {
      if (xmlClosed == null) throw new ArgumentNullException("xmlClosed");

      var closedDef = new Feature {IsComplex = false, IsReference = false};

      ParseAttributes(xmlClosed, closedDef);

      foreach (XmlNode xmlFeatValDef in xmlClosed.ChildNodes) {
        switch (xmlFeatValDef.Name) {
          case DataModelNames.COMMENT:
            closedDef.Comments.Add(xmlFeatValDef.InnerText);
            break;
          case DataModelNames.NAME:
            closedDef.Name = xmlFeatValDef.InnerText;
            break;

          case DataModelNames.FEATURE_VALUE_DEFINITION:
            var valDef = ParseFeatureValueDefinition(xmlFeatValDef);

            closedDef.Children.Add(valDef);


            break;
        }
      }
      return closedDef;
    }


    public ValueDef ParseFeatureValueDefinition(XmlNode xmlDef)
    {
      if (xmlDef == null) throw new ArgumentNullException("xmlDef");

      var valDef = new ValueDef {IsComplex = false, IsReference = false};

      if (xmlDef.Attributes == null || xmlDef.Attributes.GetNamedItem(DataModelNames.ID) == null)
        throw new ArgumentException("LogicalFeature Name Description is missing required Identifier attributes.",
                                    "xmlDef");

      valDef.Id = xmlDef.Attributes.GetNamedItem(DataModelNames.ID).Value;


      foreach (XmlNode node in xmlDef) {
        switch (node.Name) {
          case DataModelNames.NAME:
            valDef.Name = node.InnerText;
            break;
          case DataModelNames.DESCRIPTION:
            valDef.Description = node.InnerText;
            break;
          case DataModelNames.COMMENT:
            valDef.Comments.Add(node.InnerText);
            break;
        }
      }

      return valDef;
    }




    public ParametersModel ParseParameters(XmlNode xNode)
    {
      var parameters = _container.Resolve<ParametersModel>();
      if (xNode == null) return parameters;
      foreach (XmlNode node in xNode.ChildNodes) {
        switch (node.Name) {
          case DataModelNames.COMMENT:
            parameters.Comments.Add(node.InnerText);
            break;
          case DataModelNames.ATTRIBUTE_ORDER:
            var attOrderResult = ParseAttributeOrder(node);
            parameters.AttributeOrder = attOrderResult.Item1;
            parameters.AttributeOrderComments = attOrderResult.Item2;
            break;
          case DataModelNames.START_SYMBOL:
            var startSymbolResult = ParseStartSymbol(node);
            parameters.StartSymbol = startSymbolResult.Item1;
            parameters.StartSymbolComments = startSymbolResult.Item2;
            break;
          case DataModelNames.CATEGORY_FEATURE:
            parameters.CategoryFeature = BuildToggledValue(node, ParameterFieldNames.LEX);
            break;
          case DataModelNames.LEXICAL_FEATURE:
            parameters.LexicalFeature = BuildToggledValue(node, ParameterFieldNames.LEX);
            break;
          case DataModelNames.GLOSS_FEATURE:
            parameters.GlossFeature = BuildToggledValue(node, ParameterFieldNames.GLOSS);
            break;
          case DataModelNames.ROOT_GLOSS_FEATURE:
            parameters.RootGlossFeature = BuildToggledValue(node, ParameterFieldNames.ROOT_GLOSS);
            break;
        }
      }

      return parameters;
    }

    //BuildToggledValue: node, valueName -> toggledValue
    //Purpose: computes a toggledValue from node and fieldName
    private ToggledValue BuildToggledValue(XmlNode node, string fieldName)
    {
      var name = node.SelectSingleNode("./name");
      if (name == null) return null;
      var comments = node.SelectNodes("./comment");
      var field = new ToggledValue {Value = name.InnerText, DisplayName = fieldName, HasValue = true};
      comments.Each(f => field.Comments.Add(f.InnerText));

      return field;
    }

    public Tuple<NameIdRef, List<string>> ParseStartSymbol(XmlNode xNode)
    {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.Attributes[DataModelNames.SYMBOL] == null) throw new ArgumentException("Start Symbol xml definition missing required  Symbol id ref attribute");

      var startSymbol = new NameIdRef(xNode.Attributes[DataModelNames.SYMBOL].Value);
      var comments = new List<string>();
      if (xNode.HasChildNodes) {
        foreach (XmlNode node in xNode) {
          if (node.Name != DataModelNames.COMMENT) throw new ArgumentException("Start Symbol xml definition contains illegal elements", "xNode");
          comments.Add(node.InnerText);
        }
      }

      return Tuple.Create(startSymbol, comments);
    }

    public Tuple<List<AttributeOrderItem>, List<string>> ParseAttributeOrder(XmlNode xNode)
    {

      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xNode.HasChildNodes == false) throw new ArgumentException("Attribute Order xml definition is missing required elements.", "xNode");

      var attOrder = new List<AttributeOrderItem>();
      var comments = new List<string>();

      foreach (XmlNode node in xNode) {
        switch (node.Name) {
          case DataModelNames.COMMENT:
            comments.Add(node.InnerText);
            break;
          default:
            var newAttOrder = _container.Resolve<AttributeOrderItem>();
			var hasAtt = (node.Attributes != null && node.Attributes.Count > 0 && node.Attributes[0] != null);
            if (hasAtt) {
              newAttOrder.IdRef = new NameIdRef(node.Attributes[0].Value);
			  newAttOrder.Name = MapXmlElementNameToAttributeName(node.Name);
              attOrder.Add(newAttOrder);
            }
			//will discard any malformed featureNameRefs
			if (!hasAtt & node.Name != DataModelNames.FEATURE_NAME_REF){
			  newAttOrder.Name = MapXmlElementNameToAttributeName(node.Name);
              attOrder.Add(newAttOrder);
			}           
            break;
        }
      }

      return Tuple.Create(attOrder, comments);
    }


    private string MapXmlElementNameToAttributeName(string attributeName)
    {
      switch (attributeName) {
        case DataModelNames.CAT_REF:

          return  ParameterAttributeOrderNames.CAT;
        case DataModelNames.LEX_REF:

          return ParameterAttributeOrderNames.LEX;
        case DataModelNames.GLOSS_REF:

          return ParameterAttributeOrderNames.GLOSS;
        case DataModelNames.ROOT_GLOSS_REF:

          return ParameterAttributeOrderNames.ROOT_GLOSS;
        case DataModelNames.NONE:

          return  ParameterAttributeOrderNames.NONE ;
        case DataModelNames.SUBCAT:

          return ParameterAttributeOrderNames.SUBCAT;
        case DataModelNames.FIRST:

          return  ParameterAttributeOrderNames.FIRST;
        case DataModelNames.REST:

          return ParameterAttributeOrderNames.REST ;

        case DataModelNames.FEATURE_NAME_REF:

          return ParameterAttributeOrderNames.FEATURE_NAME_REF;
        default:

          return null;
      }
    }
  }
}