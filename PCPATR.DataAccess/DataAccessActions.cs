﻿using PCPATR.Models.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using PCPATR.Common;
using PCPATR.Models.FeatureTemplates;
namespace PCPATR.DataAccess
{
  public class DataAccessActions : Actions
  {
    public static Func<RulesRepository, XmlDocument, IO<XmlNode>> SaveRulesFunction { get; set; }

    public Func<RulesRepository, XmlDocument, IO<XmlNode>> SaveRules
    {
      get
      {
        return SaveRulesFunction;
      }
    }
    public static Func<IEnumerable<ConstraintTemplateModel>, XmlDocument, IO<XmlNode>> MapConstraintTemplatesToXmlNode { get; set; }
    public Func<IEnumerable<ConstraintTemplateModel>, XmlDocument, IO<XmlNode>> MapConstraintTemplatesToXml { get { return MapConstraintTemplatesToXmlNode; } }
  }

  
}
