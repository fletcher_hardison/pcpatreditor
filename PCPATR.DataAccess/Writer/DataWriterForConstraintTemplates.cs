using System;
using System.Linq;
using System.Xml;
using PCPATR.Models;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Common;
using System.Collections.Generic;

namespace PCPATR.DataAccess.Writer
{
  public partial class DataWriter
  {
    public XmlNode WriteConstraintTemplates(XmlDocument doc, IEnumerable<ConstraintTemplateModel> cTemplates)
    {
      var countTemplates = 0;
      var xNode = doc.CreateElement(DataModelNames.CONSTRAINT_TEMPLATES);
      
      if (cTemplates != null) {
        foreach (XmlNode template in cTemplates.Select(f => WriteConstraintTemplate(doc, f))) {
          countTemplates++;
          xNode.AppendChild(template);
        }
      }
      
      if (countTemplates < 1) {
        RemoveElement(doc, "constraintTemplates");

        return null;
      }

      return xNode;
    }

    public XmlNode WriteConstraintTemplate(XmlDocument doc, ConstraintTemplateModel constraint)
    {
      if (constraint == null) throw new ArgumentNullException("constraint");

      var xNode = doc.CreateElement(DataModelNames.CONSTRAINT_TEMPLATE);

      WriteConstraintControlAttributes(constraint, xNode, doc);
      WriteIdAttribute(constraint, xNode, doc);

      var name = doc.CreateElement(DataModelNames.NAME);
      name.InnerText = constraint.Name;
      xNode.AppendChild(name);

      xNode.AppendChild(WriteLogicalExpression(doc, constraint.LogicalExpression));

      WriteComments(constraint.Comments, xNode, doc);

      return xNode;

    }
  }
}
