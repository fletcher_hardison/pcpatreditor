// /* ***************************************************************
// * File name: DataWriterForConstraints.cs
// * Project: PCPATR.DAL
// * Created by: 
// * Created on: 06,04,2012
// * For Christ's honor with thanks to God 
// */

using System;
using System.Xml;
using PCPATR.Models;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Models.Rules.Constraints;
using System.Collections.Generic;
using PCPATR.Common;
using PCPATR.DataAccess.Parser;

namespace PCPATR.DataAccess.Writer
{
  public partial class DataWriter
  {
    public XmlNode WriteConstraints(XmlDocument doc, IEnumerable<ConstraintModelBase> constraints, IEnumerable<string>  comments)
    {
      if (constraints == null) throw new ArgumentNullException("constraints");

      var xConstraints = doc.CreateElement(DataModelNames.CONSTRAINTS);

      foreach (var constraint in constraints) {
        if (constraint is PercolationOperationModel) {
          var node = WritePercolationOperation(doc, (PercolationOperationModel) constraint);
          xConstraints.AppendChild(node);
        } else if (constraint is UnificationConstraintModel) {
          var node = WriteUnificationConstraint(doc, (UnificationConstraintModel) constraint);
          xConstraints.AppendChild(node);
        } else if (constraint is PriorityUnionModel) {
          var node = WritePriorityUnionOperation(doc, (PriorityUnionModel) constraint);
          xConstraints.AppendChild(node);
        } else if (constraint is LogicalConstraintModel) {
          var node = WriteLogicalConstraint(doc, (LogicalConstraintModel) constraint);
          xConstraints.AppendChild(node);
        } else if (constraint is ConstraintDisjunctionModel) {
          var node = WriteConstraintDisjunction(doc, (ConstraintDisjunctionModel) constraint);
          xConstraints.AppendChild(node);
        } 
      }

      if ( comments != null) {
        WriteComments(comments,xConstraints,doc);
      }

      return xConstraints;
    }

    public XmlNode WriteUnificationConstraint(XmlDocument doc, UnificationConstraintModel constraint)
    {
      if (constraint == null) throw new ArgumentNullException("constraint");
      var xNode = doc.CreateElement(DataModelNames.UNIFICATION_CONSTRAINT);

      WriteConstraintControlAttributes(constraint, xNode, doc);
      xNode.AppendChild(WriteConstraintElement(doc, constraint.left));
      xNode.AppendChild(WriteConstraintElement(doc, constraint.right));
      WriteComments(constraint.comments, xNode, doc);

      return xNode;
    }


    public XmlNode WritePercolationOperation(XmlDocument doc, PercolationOperationModel percOp)
    {
      if (percOp == null) throw new ArgumentNullException("percOp");

      var xNode = doc.CreateElement(DataModelNames.PERCOLATION_OPERATION);

      WriteConstraintControlAttributes(percOp, xNode, doc);
      xNode.AppendChild(WriteConstraintElement(doc, percOp.left));
      xNode.AppendChild(WriteConstraintElement(doc, percOp.right));
      WriteComments(percOp.comments, xNode, doc);

      return xNode;
    }

    public XmlNode WritePriorityUnionOperation(XmlDocument doc, PriorityUnionModel p)
    {
      if (p == null) throw new ArgumentNullException("p");

      var xNode = doc.CreateElement(DataModelNames.PRIORITY_UNION_OPERATION);

      WriteConstraintControlAttributes(p, xNode, doc);
      xNode.AppendChild(WriteConstraintElement(doc, p.left));
      xNode.AppendChild(WriteConstraintElement(doc, p.right));
      WriteComments(p.comments, xNode, doc);

      return xNode;
    }

    public XmlNode WriteLogicalConstraint(XmlDocument doc, LogicalConstraintModel m)
    {
      if (m == null) throw new ArgumentNullException("m");

      var xNode = doc.CreateElement(DataModelNames.LOGICAL_CONSTRAINT);

      WriteConstraintControlAttributes(m, xNode, doc);
      xNode.AppendChild(WriteConstraintElement(doc, m.left));

      if (m.right is LogicalExpressionModel) {
        var node = WriteLogicalExpression(doc, (LogicalExpressionModel) m.right);
        xNode.AppendChild(node);
      } else if (m.right is LogicalConstraintRefModel) {
        var node = WriteLogicalConstraintRef(doc, (LogicalConstraintRefModel) m.right);
        xNode.AppendChild(node);
      }
      WriteComments(m.comments, xNode, doc);

      return xNode;
    }

    public XmlNode WriteConstraintDisjunction(XmlDocument doc, ConstraintDisjunctionModel constrDis)
    {
      if (constrDis == null) throw new ArgumentNullException("constrDis");

      var xNode = doc.CreateElement(DataModelNames.CONSTRAINT_DISJUNCTION);

      var xFirst = WriteConstraints(doc, constrDis.LeftConstraints, null);
      xNode.AppendChild(xFirst);

      foreach (var constraintsSet in constrDis.RightConstraints) {
        var node = WriteConstraints(doc, constraintsSet, null);
        xNode.AppendChild(node);
      }

      return xNode;
    }


    public XmlNode WriteConstraintElement(XmlDocument doc, object x)
    {
      var t = x.GetType();

      if (t == typeof (FeaturePathModel)) return WriteFeaturePath(doc, (FeaturePathModel) x);
      if (t == typeof (SubcatPathModel)) return WriteSubcatPath(doc, (SubcatPathModel) x);
      if (t == typeof (FeatureValueRefModel)) return WriteFeatureValueRef(doc, (FeatureValueRefModel) x);
      if (t == typeof (CategoryValueRefModel)) return WriteCategoryValueRef(doc, (CategoryValueRefModel) x);
      if (t == typeof (LexValuePath)) {
        var lexVal = x as LexValuePath;
        return (lexVal == null)
                 ? null
                 : WriteXValuePath(doc, DataModelNames.LEX_VALUE_PATH, lexVal.Value, lexVal.IsDefault);
      }
      if (t == typeof (GlossValuePath)) {
        var glossVal = x as GlossValuePath;
        return (glossVal == null)
                 ? null
                 : WriteXValuePath(doc, DataModelNames.GLOSS_VALUE_PATH, glossVal.Value, glossVal.IsDefault);
      }
      if (t == typeof (RootGlossValuePath)) {
        var rgVal = x as RootGlossValuePath;
        return (rgVal == null)
                 ? null
                 : WriteXValuePath(doc, DataModelNames.ROOT_GLOSS_VALUE_PATH, rgVal.Value, rgVal.IsDefault);
      }
      if (t == typeof (NoneValueModel)) return WriteNoneValue(doc, (NoneValueModel) x);
      if (t == typeof (FeatureTemplateFeatureStructure)) {
        var tfs = x as FeatureTemplateFeatureStructure;
        return (tfs == null)
                 ? null
                 : WriteTemplateFeatureStructure(doc, tfs);
      }
	  if (t == typeof(FeatureValueDisjunction)){
	   var dis = x as FeatureValueDisjunction;
	   return (dis == null)
	            ? null
				: WriteFeatureValueDisjunction(doc, dis);
	  }

      throw new ApplicationException("No save logical exists for constraint element of type " + t);
    }

    private XmlNode WriteFeatureValueRef(XmlDocument doc, FeatureValueRefModel featVal)
    {
      if (featVal == null) throw new ArgumentNullException("featVal");
      var xNode = doc.CreateElement(DataModelNames.FEATURE_VALUE_REF);

      var xValue = doc.CreateAttribute(DataModelNames.VALUE);
      xValue.Value = featVal.value.IdRef;
      xNode.SetAttributeNode(xValue);

      if (featVal.IsDefault)
      {
        var isDefault = doc.CreateElement(DataModelNames.IS_DEFAULT);
        isDefault.Value = "yes";
      }

      return xNode;
    }
    
	private XmlNode WriteFeatureValueDisjunction(XmlDocument doc, FeatureValueDisjunction dis){
	  if (dis == null) return null;
	  var node = doc.CreateElement(DataModelNames.FEATURE_VALUE_DISJUNCTION);
	  node.AppendChild(WriteFeatureValueRef(doc,dis.LeftElement));
	  
	  foreach(var featureValRef in dis.RightElements){
	    node.AppendChild(WriteFeatureValueRef(doc, featureValRef));
	  }
	  
	  return node;
	}
    
	private XmlNode WriteCategoryValueRef(XmlDocument doc, CategoryValueRefModel catValRef)
    {
      if (catValRef == null) throw new ArgumentNullException("catValRef");
      var xNode = doc.CreateElement(DataModelNames.CATEGORY_VALUE_REF);

      var xatt = doc.CreateAttribute(DataModelNames.NODE);
      xatt.Value = catValRef.value.IdRef;
      xNode.SetAttributeNode(xatt);

      return xNode;
    }

    private XmlNode WriteNoneValue(XmlDocument doc, NoneValueModel rightHandSide)
    {
      if (rightHandSide == null) throw new ArgumentNullException("rightHandSide");

      var xNode = doc.CreateElement(DataModelNames.NONE_VALUE);

      return xNode;
    }

    private XmlNode WriteFeaturePath(XmlDocument doc, FeaturePathModel featurePath)
    {
      if (featurePath == null) throw new ArgumentNullException("featurePath");

      var xNode = doc.CreateElement(DataModelNames.FEATURE_PATH);
      WriteNameIdToAttribute(featurePath.node, xNode, doc, DataModelNames.NODE);
      WriteNameIdToAttribute(featurePath.feature_start, xNode, doc, DataModelNames.FEATURE_START);
      WriteNameIdToAttribute(featurePath.feature_end, xNode, doc, DataModelNames.FEATURE_END);

      if (featurePath.EmbeddedFeaturePath != null)
      {
        xNode.AppendChild(WriteFeaturePathEmbeddedElement(doc, featurePath.EmbeddedFeaturePath));
      }

      return xNode;
    }

    private XmlNode WriteFeaturePathEmbeddedElement(XmlDocument doc, object embededElement)
    {
      var t = embededElement.GetType();
      if (t == typeof (EmbeddedFeaturePath))
      {
        return WriteEmbeddedFeaturePath(doc, embededElement as EmbeddedFeaturePath);
      }

      return CheckForAndWriteXValuePath(doc, embededElement);
    }

    private XmlNode WriteEmbeddedFeaturePath(XmlDocument doc, EmbeddedFeaturePath path)
    {
      var node = doc.CreateElement(DataModelNames.EMBEDDED_FEATURE_PATH);

      WriteNameIdToAttribute(path.feature_start,node,doc,DataModelNames.FEATURE_START);
      WriteNameIdToAttribute(path.featrure_end, node, doc,DataModelNames.FEATURE_END);

      if (path.ChildPath != null)
      {
        node.AppendChild(WriteEmbeddedFeaturePath(doc, path.ChildPath));
      }

      return node;
    }

    private void WriteNameIdToAttribute(NameIdRef value, XmlElement node, XmlDocument doc, string att_name)
    {
      if (value == null) return;
      var att = doc.CreateAttribute(att_name);
      att.Value = value.IdRef;
      node.SetAttributeNode(att);
    }

    private XmlNode WriteSubcatPath(XmlDocument doc, SubcatPathModel subcat)
    {
      if (subcat == null) throw new ArgumentNullException("subcat");

      var xNode = doc.CreateElement(DataModelNames.SUBCAT_PATH);

      var xNodeId = doc.CreateAttribute(DataModelNames.NODE);
      xNodeId.Value = subcat.node.IdRef;
      xNode.SetAttributeNode(xNodeId);

      WriteNameIdToAttribute(subcat.feature_start, xNode, doc, DataModelNames.FEATURE_START);
      WriteNameIdToAttribute(subcat.feature_end, xNode, doc, DataModelNames.FEATURE_END);

      var end_value = MapSubcatEndToString(subcat.subcat_end);
      if (!string.IsNullOrEmpty(end_value)) {
        var xSubcatEnd = doc.CreateAttribute(DataModelNames.SUBCAT_END);
        xSubcatEnd.Value = end_value;
        xNode.Attributes.Append(xSubcatEnd);
      }
      WriteNameIdToAttribute(subcat.feature_after_start, xNode, doc, DataModelNames.FEATURE_AFTER_START);
      WriteNameIdToAttribute(subcat.feature_after_end, xNode, doc, DataModelNames.FEATURE_AFTER_END);

      return xNode;
    }

    private string MapSubcatEndToString(SubcatEndValues x)
    {
      switch (x) {
        case SubcatEndValues.NoneValue:
          return DataModelNames.NONE;

        case SubcatEndValues.First:

          return DataModelNames.SUBCAT_FIRST;
        case SubcatEndValues.Rest:

          return DataModelNames.SUBCAT_REST;
        default: 
          return null;
      }

      throw new InvalidOperationException("no mappping exists for selected SubcatEndValue");
    }


    private XmlNode WriteLogicalExpression(XmlDocument doc, LogicalExpressionModel m)
    {
      if (m == null) throw new ArgumentNullException("m");
      var xNode = doc.CreateElement(DataModelNames.LOGICAL_EXPRESSION);

      if (m.operation is BinaryOperationModel) {
        XmlNode node = WriteBinaryOperation(doc, (BinaryOperationModel) m.operation);
        xNode.AppendChild(node);
      } else if (m.operation is UnaryOperationModel) {
        XmlNode node = WriteUnaryOperation(doc, (UnaryOperationModel) m.operation);
        xNode.AppendChild(node);
      }

      return xNode;
    }

    public XmlNode WriteUnaryOperation(XmlDocument doc, UnaryOperationModel op)
    {
      if (op == null) throw new ArgumentNullException("op");

      var xNode = doc.CreateElement(DataModelNames.UNARY_OPERATION);

      var xFactor = WriteFactor(doc, op.Factor1);
      xNode.AppendChild(xFactor);

      var xatt = doc.CreateAttribute(DataModelNames.OPERATION);
      xatt.Value = op.OperationType;
      xNode.SetAttributeNode(xatt);

      return xNode;
    }

    private XmlNode WriteBinaryOperation(XmlDocument doc, BinaryOperationModel op)
    {
      if (op == null) throw new ArgumentNullException("op");

      var xNode = doc.CreateElement(DataModelNames.BINARY_OPERATION);

      var xLeft = WriteFactor(doc, op.Factor1);
      xNode.AppendChild(xLeft);

      var xRight = WriteFactor(doc, op.Factor2);
      xNode.AppendChild(xRight);

      var xatt = doc.CreateAttribute(DataModelNames.OPERATION);
      xatt.Value = op.OperationType;
      xNode.SetAttributeNode(xatt);

      return xNode;
    }

    private XmlNode WriteFactor(XmlDocument doc, object factor)
    {
      if (factor == null) throw new ArgumentNullException("factor");

      var xNode = doc.CreateElement(DataModelNames.FACTOR);

      if (factor is LogicalExpressionModel) {
        var node = WriteLogicalExpression(doc, (LogicalExpressionModel) factor);
        xNode.AppendChild(node);
      } else if (factor is LogicalFeatureModel) {
        var node = WriteFeature(doc, (LogicalFeatureModel) factor);
        xNode.AppendChild(node);
      } else if (factor is CollectionFeatureRef) {
        var node = WriteCollectionFeatureRef(doc, (CollectionFeatureRef) factor);
        xNode.AppendChild(node);
      } else if (factor is ComplexFeatureRef)
      {
        xNode.AppendChild(WriteComplexFeatureRef(doc, (ComplexFeatureRef) factor));
      }
      else if (factor is CatValue) {
        var node = WriteCatValue(doc, factor as CatValue);
        xNode.AppendChild(node);
      }
      else if (factor is LexValue | factor is GlossValue | factor is RootGlossValue)
      {
        var node = WriteXValue(doc, factor);
        xNode.AppendChild(node);
      }
      else
      {
        throw new InvalidOperationException("Program cannot save a Binary operation with a " + factor.GetType() + " as a child");
      }

      return xNode;
    }

    private XmlNode WriteFeature(XmlDocument doc, LogicalFeatureModel m)
    {
      if (m == null) throw new ArgumentNullException("m");

      var xNode = doc.CreateElement(DataModelNames.FEATURE);

      var xStart = doc.CreateAttribute(DataModelNames.FEATURE_START);
      xStart.Value = m.feature_start.IdRef;
      xNode.SetAttributeNode(xStart);

      var xEnd = doc.CreateAttribute(DataModelNames.FEATURE_END);
      xEnd.Value = m.feature_end.IdRef;
      xNode.SetAttributeNode(xEnd);

      if (m.indexed_variable != "none") {
        var xIndexVar = doc.CreateAttribute(DataModelNames.INDEXED_VARIABLE);

        xIndexVar.Value = m.indexed_variable;
        xNode.SetAttributeNode(xIndexVar);
      }

      if (m.embedded_feature != null) {
        var embededFeature = WriteFeature(doc, m.embedded_feature);
        xNode.AppendChild(embededFeature);
      }

      return xNode;
    }

    private XmlNode WriteLogicalConstraintRef(XmlDocument doc, LogicalConstraintRefModel m)
    {
      if (m == null) throw new ArgumentNullException("m");

      var xNode = doc.CreateElement(DataModelNames.LOGICAL_CONSTRAINT_REF);
      WriteConstraintControlAttributes(m, xNode, doc);

      var xConstraint = doc.CreateAttribute(DataModelNames.CONSTRAINT);
      xConstraint.Value = m.constraint.IdRef;
      xNode.SetAttributeNode(xConstraint);

      return xNode;
    }
  }
}