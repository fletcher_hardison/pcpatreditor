// /* ***************************************************************
// * File name: DataWriter.cs
// * Project: PCPATR.DAL
// * Created by: TFH
// * Created on: 05,04,2012
// * For Christ's honor with thanks to God
// */

using System;
using System.Collections.Generic;
using System.Xml;
using PCPATR.Common.Interfaces;
using PCPATR.Models;

using PCPATR.Models.Rules.PSRElements;

namespace PCPATR.DataAccess.Writer
{
  public partial class DataWriter
  {
    public string PathToGrammarFile { get; set; }


    public void WriteComments(IEnumerable<string> comments, XmlNode node, XmlDocument xDoc)
    {
      if (node == null) throw new ArgumentNullException("node");

      foreach (string comment in comments)
      {
        XmlElement xComment = xDoc.CreateElement(DataModelNames.COMMENT);
        xComment.InnerText = comment;
        node.AppendChild(xComment);
      }
    }

    internal void WriteName(IHasName element, XmlNode node, XmlDocument xDoc)
    {
      if (element == null) throw new ArgumentNullException("element");
      if (node == null) throw new ArgumentNullException("node");
      if (xDoc == null) throw new ArgumentNullException("xDoc");

      XmlElement name = xDoc.CreateElement(DataModelNames.NAME);
      name.InnerText = element.Name;
      node.AppendChild(name);
    }

    internal void WriteIdAttribute(IHasId element, XmlNode node, XmlDocument xDoc)
    {
      if (element == null) throw new ArgumentNullException("element");
      if (node == null) throw new ArgumentNullException("node");
      if (xDoc == null) throw new ArgumentNullException("xDoc");
      if (string.IsNullOrEmpty(element.Id)) return;

      XmlAttribute id = xDoc.CreateAttribute(DataModelNames.ID);
      id.Value = element.Id;
      if (node.Attributes != null) node.Attributes.Append(id);
    }

    public void WriteConstraintControlAttributes(IHasConstraintControl element, XmlNode node, XmlDocument xDoc)
    {
      if (element == null) throw new ArgumentNullException("element");
      if (node == null) throw new ArgumentNullException("node");
      if (xDoc == null) throw new ArgumentNullException("xDoc");

      if (node.Attributes != null)
      {
        if (!element.enabled)
        {
          XmlAttribute enabled = xDoc.CreateAttribute(DataModelNames.ENABLED);
          enabled.Value = (element.enabled) ? "yes" : "no";
          node.Attributes.SetNamedItem(enabled);
        }

        if (element.use_when_debugging)
        {
          XmlAttribute useWhenDebugging = xDoc.CreateAttribute(DataModelNames.USE_WHEN_DEBUGGING);
          useWhenDebugging.Value = (element.use_when_debugging) ? "yes" : "no";
          node.Attributes.SetNamedItem(useWhenDebugging);
        }
      }
    }

    public void WriteConstraintControlAttributes(bool enabled, bool useWhenDebugging, XmlNode node, XmlDocument xDoc)
    {
      if (node == null) throw new ArgumentNullException("node");
      if (xDoc == null) throw new ArgumentNullException("xDoc");

      XmlAttribute enabledAtt = xDoc.CreateAttribute(DataModelNames.ENABLED);

      enabledAtt.Value = (enabled) ? "yes" : "no";

      XmlAttribute useWhenDebuggingAtt = xDoc.CreateAttribute(DataModelNames.USE_WHEN_DEBUGGING);

      useWhenDebuggingAtt.Value = (useWhenDebugging) ? "yes" : "no";

      if (node.Attributes != null) {
        node.Attributes.SetNamedItem(enabledAtt);

        node.Attributes.SetNamedItem(useWhenDebuggingAtt);
      }
    }

    internal void WriteDescription(IHasDescription element, XmlNode node, XmlDocument xDoc)
    {
      if (element == null) throw new ArgumentNullException("element");
      if (node == null) throw new ArgumentNullException("node");
      if (xDoc == null) throw new ArgumentNullException("xDoc");

      if (element.Description == null) return;

      XmlElement desc = xDoc.CreateElement(DataModelNames.DESCRIPTION);

      desc.InnerText = element.Description;

      node.AppendChild(desc);
    }

    internal void WriteSymbolRefAttributes(SymbolRef leftHandSide, XmlElement node, XmlDocument xDoc)
    {
      if (leftHandSide == null) throw new ArgumentNullException("leftHandSide");
      if (node == null) throw new ArgumentNullException("node");
      if (xDoc == null) throw new ArgumentNullException("xDoc");

      Func<string, string, XmlAttribute> createAtt = (p, v) =>
          {
            XmlAttribute a = xDoc.CreateAttribute(p);
            a.Value = v;
            return a;
          };

      node.SetAttributeNode(createAtt(DataModelNames.ID, leftHandSide.Id));
      node.SetAttributeNode(createAtt(DataModelNames.SYMBOL, leftHandSide.Symbol.IdRef));
    }
  }
}