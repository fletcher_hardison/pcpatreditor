// /* ***************************************************************
// * File name: DataWriterForRules.cs
// * Project: PCPATR.DAL
// * Created by: TFH
// * Created on: 05,04,2012
// * For Christ's honor with thanks to God 
// */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using PCPATR.Models;
using PCPATR.Models.Rules.PSRElements;

using PCPATR.Models.Rules;
using PCPATR.Common;


namespace PCPATR.DataAccess.Writer
{
  public partial class DataWriter
  {
    public Func<RulesRepository, XmlDocument, IO<XmlNode>> GetWriteRulesFunction() {
      return (rules, doc) => {
         if (doc == null) return null;

         try {
           return () => WriteRulesRepository(doc, rules);
         } catch {

           return null;
         }
       };
    }

    public XmlNode WriteRulesRepository(XmlDocument doc, IEnumerable<Rule> rules) {
      if (rules == null) throw new ArgumentNullException("rules");
      if (doc == null) throw new ArgumentNullException("doc");
      var xRules = doc.CreateElement(DataModelNames.RULES);

      foreach (var rule in rules) {
        xRules.AppendChild(WriteRule(doc, rule));
      }
      return xRules;
    }

    public XmlNode WriteRule(XmlDocument doc, Rule rule) {
      if (rule == null) throw new ArgumentNullException("rule");
      var xNode = doc.CreateElement(DataModelNames.RULE);

      WriteIdAttribute(rule, xNode, doc);

      WriteConstraintControlAttributes(rule, xNode, doc);

      WriteComments(rule.Comments, xNode, doc);

      if (rule.Identifier != null) {
        var ident = doc.CreateElement(DataModelNames.IDENTIFIER);
        ident.InnerText = rule.Identifier;

        xNode.AppendChild(ident);
      }

      XmlNode psr = WritePhraseStuctureRule(doc, rule.PhraseStructureRule);
      xNode.AppendChild(psr);

      if (rule.Constraints != null && rule.Constraints.Count() > 0) {
        XmlNode constraints = WriteConstraints(doc, rule.Constraints, rule.ConstraintsComments);
        xNode.AppendChild(constraints);
      }

      return xNode;

    }




    #region Phrase Structure Rules

    private XmlNode WritePhraseStuctureRule(XmlDocument doc, PhraseStructureRule phraseStructureRule) {
      if (phraseStructureRule == null) throw new ArgumentNullException("phraseStructureRule");

      var psr = doc.CreateElement(DataModelNames.PHRASE_STRUCTURE_RULE);

      var xLhs = WritePsrLeftHandSide(doc, phraseStructureRule.LeftHandSide);
      psr.AppendChild(xLhs);

      var xRhs = WriteRightHandside(doc, phraseStructureRule.RightHandSide, phraseStructureRule.RightHandComments);
      psr.AppendChild(xRhs);

      WriteComments(phraseStructureRule.Comments, psr, doc);

      return psr;

    }

    public XmlNode WriteRightHandside(XmlDocument doc, List<PsrElement> rightHandSide, IEnumerable<string> comments) {
      if (rightHandSide == null) throw new ArgumentNullException("rightHandSide");

      var xNode = doc.CreateElement(DataModelNames.RIGHT_HAND_SIDE);

      foreach (var rightHandElement in rightHandSide) {
        if (rightHandElement is SymbolRef) {
          var symbolRef = WriteSymbolRef(doc, (SymbolRef)rightHandElement);
          xNode.AppendChild(symbolRef);

        } else if (rightHandElement is OptionalSymbols) {
          XmlNode optionalSymbols = WriteOptionalSymbols(doc, (OptionalSymbols)rightHandElement);
          xNode.AppendChild(optionalSymbols);
        } else if (rightHandElement is Disjunction) {
          XmlNode disjunction = WriteDisjunction(doc, (Disjunction)rightHandElement);
          xNode.AppendChild(disjunction);
        }
      }

      WriteComments(comments, xNode, doc);

      return xNode;

    }

    private XmlNode WriteDisjunction(XmlDocument doc, Disjunction disjunction) {
      if (disjunction == null) throw new ArgumentNullException("disjunction");
      string nodeName;

      nodeName = disjunction.DisjunctiveSymbols == null ? DataModelNames.DISJUNCTIVE_SYMBOLS : DataModelNames.DISJUNCTION;

      var xNode = doc.CreateElement(nodeName);

      foreach (var left in disjunction.LeftElements) {
        if (left is SymbolRef) {
          var symbolRef = WriteSymbolRef(doc, (SymbolRef)left);
          xNode.AppendChild(symbolRef);
        } else if (left is OptionalSymbols) {
          var optionals = WriteOptionalSymbols(doc, (OptionalSymbols)left);
          xNode.AppendChild(optionals);
        }
      }

      if (disjunction.DisjunctiveSymbols != null) {
        var disjunctiveSymbols = WriteDisjunctiveSymbols(doc, disjunction.DisjunctiveSymbols); //WriteDisjunctiveSymbols(disjunction.DisjunctiveSymbols);
        xNode.AppendChild(disjunctiveSymbols);
      }

      return xNode;
    }

    private XmlNode WriteDisjunctiveSymbols(XmlDocument doc, DisjunctiveSymbols disSymbols) {
      if (disSymbols == null) throw new ArgumentNullException("disSymbols");

      var xNode = doc.CreateElement(DataModelNames.DISJUNCTIVE_SYMBOLS);

      foreach (var symbol in disSymbols.LeftElements) {
        if (symbol is SymbolRef) {
          var xRef = WriteSymbolRef(doc, (SymbolRef)symbol);
          xNode.AppendChild(xRef);
        } else if (symbol is OptionalSymbols) {
          var xOptional = WriteOptionalSymbols(doc, (OptionalSymbols)symbol);
          xNode.AppendChild(xOptional);
        }
      }

      if (disSymbols.EmbeddedDisjuctiveSymbols != null) {
        var embeddedDisSymbols = WriteDisjunctiveSymbols(doc, disSymbols.EmbeddedDisjuctiveSymbols);
        xNode.AppendChild(embeddedDisSymbols);
      }

      return xNode;
    }

    private XmlNode WriteOptionalSymbols(XmlDocument doc, OptionalSymbols optionalSymbols) {
      if (optionalSymbols == null) throw new ArgumentNullException("optionalSymbols");

      var optionals = doc.CreateElement(DataModelNames.OPTIONAL_SYMBOLS);


      foreach (var optSym in optionalSymbols.Elements) {
        if (optSym is SymbolRef) {
          var symbolRef = WriteSymbolRef(doc, (SymbolRef)optSym);
          optionals.AppendChild(symbolRef);
        } else if (optSym is OptionalSymbols) {
          var subOptSymbols = WriteOptionalSymbols(doc, (OptionalSymbols)optSym);
          optionals.AppendChild(subOptSymbols);
        } else if (optSym is Disjunction) {
          var disjunction = WriteDisjunction(doc, (Disjunction)optSym);
          optionals.AppendChild(disjunction);
        }
      }

      return optionals;
    }

    private XmlNode WriteSymbolRef(XmlDocument doc, SymbolRef symbolRef) {
      if (symbolRef == null) throw new ArgumentNullException("symbolRef");
      var xNode = doc.CreateElement(DataModelNames.SYMBOL_REF);
      WriteSymbolRefAttributes(symbolRef, xNode, doc);

      return xNode;
    }

    private XmlNode WritePsrLeftHandSide(XmlDocument doc, SymbolRef symbolRef) {
      if (symbolRef == null) throw new ArgumentNullException("symbolRef");
      var xNode = doc.CreateElement(DataModelNames.LEFT_HAND_SIDE);
      WriteSymbolRefAttributes(symbolRef, xNode, doc);

      return xNode;
    }

    #endregion
  }
}