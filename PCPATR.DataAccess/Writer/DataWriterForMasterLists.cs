// /* ***************************************************************
// * File name: DataWriterForMasterLists.cs
// * Project: PCPATR.DAL
// * Created by: TFH
// * Created on: 05,04,2012
// * For Christ's honor with thanks to God 
// */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using PCPATR.Common;
using PCPATR.Models;
using PCPATR.Models.FeatureSystem;
using PCPATR.Models.MasterLists;

using PCPATR.Models.Parameters;

namespace PCPATR.DataAccess.Writer
{
  public partial class DataWriter
  {
    public XmlNode WriteFeatureSystem(XmlDocument doc, FeatureSystem featureSystem)
    {
      if (featureSystem == null) throw new ArgumentNullException("featureSystem");

      var xNode = doc.CreateElement(DataModelNames.FEATURE_SYSTEM);

      foreach (Feature description in featureSystem.Features)
      {
        if (description.IsComplex & description.IsReference == false)
        {
          xNode.AppendChild(WriteComplexFeatureDefinition(doc, description));
        }
        else if (description.IsReference == false)
        {
          xNode.AppendChild(WriteClosedFeatureDefinition(doc, description));
        }
      }

      return xNode;
    }


    #region LogicalFeature System
    #region ClosedFeatureDefintions

    public XmlNode WriteClosedFeatureDefinition(XmlDocument doc, Feature description)
    {
      if (description == null) throw new ArgumentNullException("description");

      var closed = doc.CreateElement(DataModelNames.CLOSED_FEATURE_DESC);

      WriteIdAttribute(description, closed, doc);
      WriteComments(description.Comments, closed, doc);
      WriteName(description, closed, doc);
      WriteDescription(description, closed, doc);

      foreach (Feature valDef in description.Children)
      {
        var val = valDef;
        closed.AppendChild(WriteFeatureValueDefinition(doc, val));
      }

      return closed;
    }


    public XmlNode WriteFeatureValueDefinition(XmlDocument doc, Feature valDef)
    {
      if (valDef == null) throw new ArgumentNullException("valDef");

      var xNode = doc.CreateElement(DataModelNames.FEATURE_VALUE_DEFINITION);

      WriteIdAttribute(valDef, xNode, doc);
      WriteComments(valDef.Comments, xNode, doc);

      var name = doc.CreateElement(DataModelNames.NAME);
      name.InnerText = valDef.Name;
      xNode.AppendChild(name);
      WriteDescription(valDef, xNode, doc);

      return xNode;
    }

    #endregion

    #region complexFeatureDefinition

    public XmlNode WriteComplexFeatureDefinition(XmlDocument doc, Feature description)
    {
      if (description == null) throw new ArgumentNullException("description");
      var complex = doc.CreateElement(DataModelNames.COMPLEX_FEATURE_DESC);

      WriteIdAttribute(description, complex, doc);

      WriteComments(description.Comments, complex, doc);

      WriteName(description, complex, doc);

      WriteDescription(description, complex, doc);

      foreach (var elem in description.Children)
      {
        if (elem.IsComplex)
        {
          var subComplex = WriteComplexFeatureDefinition(doc, elem);
          complex.AppendChild(subComplex);
        }
        else if (elem.IsComplex == false & elem.IsReference == false)
        {


          var subClosed = WriteClosedFeatureDefinition(doc, elem);
          complex.AppendChild(subClosed);
        }
        else if (elem.IsReference)
        {
          //var complexRef = (IComplexFeatureRef)elem;
          var complexRef = elem;
          var subComplexRef = WriteComplexFeatureDefinitionRef(doc, complexRef);
          complex.AppendChild(subComplexRef);
        }
      }

      return complex;
    }


    public XmlNode WriteComplexFeatureDefinitionRef(XmlDocument doc, Feature elem)
    {
      if (elem == null) throw new ArgumentNullException("elem");

      var xRef = doc.CreateElement(DataModelNames.COMPLEX_FEATURE_DESC_REF);

      WriteIdAttribute(elem, xRef, doc);

      var idRef = doc.CreateAttribute(DataModelNames.COMPLEX);
      idRef.Value = elem.TargetId;
      xRef.SetAttributeNode(idRef);

      return xRef;
    }

    #endregion
    #endregion
    #region Collection Features

    public XmlNode WriteCollectionFeatures(XmlDocument doc, CollectionFeatures collection)
    {
      if (collection == null) throw new ArgumentNullException("collection");
      var xNode = doc.CreateElement(DataModelNames.COLLECTION_FEATURES);

      foreach (var feature in collection.Features)
      {
        xNode.AppendChild(WriteCollectionFeature(doc, feature));
      }

      return xNode;
    }

    private XmlNode WriteCollectionFeature(XmlDocument doc, CollectionFeature feature)
    {
      if (feature == null) throw new ArgumentNullException("feature");
      var xNode = doc.CreateElement(DataModelNames.COLLECTION_FEATURE);

      WriteIdAttribute(feature, xNode, doc);

      WriteComments(feature.Comments, xNode, doc);

      WriteName(feature, xNode, doc);

      return xNode;
    }

    #endregion

    #region Terminals and NonTerminals
    public XmlNode WriteSymbols(XmlDocument doc, List<Symbol> symbols, string symbolName)
    {
      if (symbols == null) throw new ArgumentNullException("symbols");
      XmlNode xNode = null;

      if (symbolName == DataModelNames.TERMINAL)
      {
        xNode = doc.CreateElement(DataModelNames.TERMINALS);
      }
      else if (symbolName == DataModelNames.NONTERMINAL)
      {
        xNode = doc.CreateElement(DataModelNames.NONTERMINALS);
      }

      if (xNode == null) throw new ArgumentException("Symbol type could not be determined", "symbolName");
      foreach (var symbol in symbols)
      {
        var node = WriteSymbol(doc, symbol, symbolName);
        xNode.AppendChild(node);

      }

      return xNode;
    }

    public XmlNode WriteTerminalSymbols(XmlDocument doc, List<Symbol> symbols)
    {
      if (symbols == null) throw new ArgumentNullException("symbols");
      var xNode = doc.CreateElement(DataModelNames.TERMINALS);
      foreach (var symbol in symbols)
      {
        xNode.AppendChild(WriteSymbol(doc, symbol, DataModelNames.TERMINAL));
      }

      return xNode;
    }

    public XmlNode WriteNonTerminalSymbols(XmlDocument doc, List<Symbol> symbols)
    {
      if (symbols == null) throw new ArgumentNullException("symbols");
      var xNode = doc.CreateElement(DataModelNames.NONTERMINALS);
      foreach (var symbol in symbols)
      {
        xNode.AppendChild(WriteSymbol(doc, symbol, DataModelNames.NONTERMINAL));
      }

      return xNode;
    }


    private XmlNode WriteSymbol(XmlDocument doc, Symbol symbol, string symbolType)
    {
      if (symbol == null) throw new ArgumentNullException("symbol");
      var xNode = doc.CreateElement(symbolType);

      WriteIdAttribute(symbol, xNode, doc);
      WriteComments(symbol.Comments, xNode, doc);
      WriteName(symbol, xNode, doc);
      WriteDescription(symbol, xNode, doc);

      return xNode;
    }

    #endregion

    public XmlNode WriteBuiltInPrimitives(XmlDocument doc, BuiltInPrimitives builtInPrimatives)
    {
      if (builtInPrimatives == null) throw new ArgumentNullException("builtInPrimatives");

      var xPrimitives = doc.CreateElement(DataModelNames.BUILT_IN_PRIMATIVES);

      var xCat = doc.CreateElement(DataModelNames.CAT);
      var xCatId = doc.CreateAttribute(DataModelNames.ID);
      xCatId.Value = builtInPrimatives.CatId.id;
      xCat.SetAttributeNode(xCatId);
      xPrimitives.AppendChild(xCat);

      var xLex = doc.CreateElement(DataModelNames.LEX);
      var xLexId = doc.CreateAttribute(DataModelNames.ID);
      xLexId.Value = builtInPrimatives.LexId.id;
      xLex.SetAttributeNode(xLexId);
      xPrimitives.AppendChild(xLex);

      var xGloss = doc.CreateElement(DataModelNames.GLOSS);
      var xGlossId = doc.CreateAttribute(DataModelNames.ID);
      xGlossId.Value = builtInPrimatives.GlossId.id;
      xGloss.SetAttributeNode(xGlossId);
      xPrimitives.AppendChild(xGloss);

      var xRootGloss = doc.CreateElement(DataModelNames.ROOT_GLOSS);
      var xRootGlossId = doc.CreateAttribute(DataModelNames.ID);
      xRootGlossId.Value = builtInPrimatives.RootGlossId.id;
      xRootGloss.SetAttributeNode(xRootGlossId);
      xPrimitives.AppendChild(xRootGloss);

      var xNone = doc.CreateElement(DataModelNames.NONE);
      var xNoneId = doc.CreateAttribute(DataModelNames.ID);
      xNoneId.Value = builtInPrimatives.NoneId.id;
      xNone.SetAttributeNode(xNoneId);
      xPrimitives.AppendChild(xNone);

      return xPrimitives;
    }

    public XmlNode WriteParametersAttributeOrder(XmlDocument doc, IEnumerable<AttributeOrderItem> attribues)
    {
      var attributeOrderItems = attribues as AttributeOrderItem[] ?? attribues.ToArray();
      if (attribues == null || !(attributeOrderItems.Any())) return null;

      var attributeOrder = doc.CreateElement(DataModelNames.ATTRIBUTE_ORDER);
      foreach (var att in attributeOrderItems)
      {
        var attXml = WriteParameterAttribute(doc, att);
        if (attXml == null) continue;
        attributeOrder.AppendChild(attXml);
      }

      return attributeOrder;
    }

    public XmlElement WriteParameterAttribute(XmlDocument doc, AttributeOrderItem att)
    {
      var elementName = MapAttributeNameToXmlElementName(att.Name);
      var attName = MapAttributeOrderNameToXmlAttributeName(att.Name);

      var attXml = doc.CreateElement(elementName);
      if (!string.IsNullOrEmpty(attName))
      {
        var attXmlAtt = doc.CreateAttribute(attName);
        if (att.IdRef == null) return null;
        attXmlAtt.Value = att.IdRef.IdRef;
        attXml.Attributes.Append(attXmlAtt);
      }
      return attXml;
    }

    private string MapAttributeNameToXmlElementName(string attributeName)
    {
      switch (attributeName)
      {
        case ParameterAttributeOrderNames.CAT:

          return DataModelNames.CAT_REF;
        case ParameterAttributeOrderNames.LEX:

          return DataModelNames.LEX_REF;
        case ParameterAttributeOrderNames.GLOSS:

          return DataModelNames.GLOSS_REF;
        case ParameterAttributeOrderNames.ROOT_GLOSS:

          return DataModelNames.ROOT_GLOSS_REF;
        case ParameterAttributeOrderNames.NONE:

          return DataModelNames.NONE;
        case ParameterAttributeOrderNames.SUBCAT:

          return DataModelNames.SUBCAT;
        case ParameterAttributeOrderNames.FIRST:

          return DataModelNames.FIRST;
        case ParameterAttributeOrderNames.REST:

          return DataModelNames.REST;
        default:
          return "featureNameRef";
      }
    }

    private string MapAttributeOrderNameToXmlAttributeName(string attributeName)
    {
      switch (attributeName)
      {
        case ParameterAttributeOrderNames.CAT:

          return DataModelNames.CAT;
        case ParameterAttributeOrderNames.LEX:

          return DataModelNames.LEX;
        case ParameterAttributeOrderNames.GLOSS:

          return DataModelNames.GLOSS;
        case ParameterAttributeOrderNames.ROOT_GLOSS:

          return DataModelNames.ROOT_GLOSS;
        case ParameterAttributeOrderNames.NONE:

          return DataModelNames.NONE;
        case ParameterAttributeOrderNames.FEATURE_NAME_REF:

          return DataModelNames.FEATURE;
        default:

          return null;
      }
    }

    public XmlNode WriteParameters(XmlDocument doc, ParametersModel parameters_model)
    {
      var xNode = doc.CreateElement(DataModelNames.PARAMETERS);

      if (parameters_model != null) {
        if (parameters_model.StartSymbol != null) {
          var startSymbol = WriteStartSymbol(doc, parameters_model.StartSymbol, parameters_model.StartSymbolComments);
          xNode.AppendChild(startSymbol);
        }

        var attOrder = WriteParametersAttributeOrder(doc, parameters_model.AttributeOrder);
        if (attOrder != null) {
          xNode.AppendChild(attOrder);
        }
        AppendToggleFieldIfNotNull(doc, xNode, parameters_model.CategoryFeature);
        AppendToggleFieldIfNotNull(doc, xNode, parameters_model.LexicalFeature);
        AppendToggleFieldIfNotNull(doc, xNode, parameters_model.GlossFeature);
        AppendToggleFieldIfNotNull(doc, xNode, parameters_model.RootGlossFeature);
        WriteComments(parameters_model.Comments, xNode, doc);
      }

      if (!xNode.HasChildNodes) {
        RemoveElement(doc, "parameters");
        return null;
      }

      return xNode;
    }

    private void AppendToggleFieldIfNotNull(XmlDocument doc, XmlNode parametersXml, ToggledValue field)
    {
      var xml = WriteToggleField(doc, field);
      if (xml != null)
      {
        parametersXml.AppendChild(xml);
      }
    }

    public XmlNode WriteToggleField(XmlDocument doc, ToggledValue toggle)
    {
      if (!toggle.HasValue) return null;
      var name = MapParameterFieldNamesToXmlElementNames(toggle.DisplayName);
      if (string.IsNullOrEmpty(name)) return null;

      var xmlElement = doc.CreateElement(name);
      var xmlNameElement = doc.CreateElement(DataModelNames.NAME);
      xmlNameElement.InnerText = toggle.Value;
      xmlElement.AppendChild(xmlNameElement);
      WriteComments(toggle.Comments, xmlElement, doc);

      return xmlElement;
    }

    private string MapParameterFieldNamesToXmlElementNames(string fieldName)
    {
      switch (fieldName)
      {
        case ParameterFieldNames.CAT:

          return DataModelNames.CATEGORY_FEATURE;
        case ParameterFieldNames.LEX:

          return DataModelNames.LEXICAL_FEATURE;
        case ParameterFieldNames.GLOSS:

          return DataModelNames.GLOSS_FEATURE;

        case ParameterFieldNames.ROOT_GLOSS:

          return DataModelNames.ROOT_GLOSS_FEATURE;
      }

      return null;
    }

    public XmlNode WriteStartSymbol(XmlDocument doc, NameIdRef start, IEnumerable<string> comments)
    {
      if (start == null) throw new ArgumentNullException("start");

      var xNode = doc.CreateElement(DataModelNames.START_SYMBOL);
      var xAtt = doc.CreateAttribute(DataModelNames.SYMBOL);
      xAtt.Value = start.IdRef;
      xNode.SetAttributeNode(xAtt);
      WriteComments(comments, xNode, doc);

      return xNode;
    }
  }
}
