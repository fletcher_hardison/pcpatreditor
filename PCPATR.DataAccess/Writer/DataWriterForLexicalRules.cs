// /* ***************************************************************
// * File name: DataWriterForLexicalRules.cs
// * Project: PCPATR.DAL
// * Created by: 
// * Created on: 13,04,2012
// * In the name of Christ with thanks to God 
// */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using PCPATR.Models;
using PCPATR.Models.Rules;
using PCPATR.Models.Rules.Constraints;

namespace PCPATR.DataAccess.Writer
{
  public partial class DataWriter
  {
    public XmlNode WriteLexicalRules(XmlDocument doc, IEnumerable<LexicalRule> lexRules)
    {
     var xNode = doc.CreateElement(DataModelNames.LEXICAL_RULES);

      foreach (var rule in lexRules)
      {
        xNode.AppendChild(WriteLexicalRule(doc, rule));
      }      


      if (xNode.ChildNodes.Count < 1)
      {
        RemoveElement(doc, "lexicalRules");
        return null;
      }

      return xNode;
    }

    /// <summary>
    /// computes the xml representation of the grammar without lexical rules. 
    /// </summary>
    /// <returns>true if rules were removed, false otherwise </returns>
    private bool RemoveElement(XmlDocument doc, string elementName)
    {
      var element = doc.SelectSingleNode("//" + elementName);
      var elementParent = doc.SelectSingleNode("//*[" + elementName +"]");
      if (elementParent == null) return false;
      if (element == null) return false;
      elementParent.RemoveChild(element);

      return true;
    }

    public XmlNode WriteLexicalRule(XmlDocument doc, LexicalRule lexRule)
    {
      if (lexRule == null) throw new ArgumentNullException("lexRule");
      var xNode = doc.CreateElement(DataModelNames.LEXICAL_RULE);
      WriteComments(lexRule.Comments, xNode, doc);
      WriteName(lexRule, xNode, doc);
      lexRule.LexicalRuleDefinitions.ForEach(f => xNode.AppendChild(WriteLexicalRuleDefinition(doc, f)));

      return xNode;
    }

    public XmlNode WriteLexicalRuleDefinition(XmlDocument doc, LexicalRuleDefinition def)
    {
      if (def == null) throw new ArgumentNullException("def");
      var xNode = doc.CreateElement(DataModelNames.LEXICAL_RULE_DEFINITION);
      xNode.AppendChild(WriteLexicalRuleFeaturePath(doc, def.Left));

      if (def.RightElement is FeatureValueRefModel)
        xNode.AppendChild(WriteFeatureValueRef(doc, def.RightElement as FeatureValueRefModel));
      else if (def.RightElement is LexicalRuleFeaturePath)
        xNode.AppendChild(WriteLexicalRuleFeaturePath(doc, def.RightElement as LexicalRuleFeaturePath));

      return xNode;
    }

    public XmlNode WriteLexicalRuleFeaturePath(XmlDocument doc, LexicalRuleFeaturePath lexPath)
    {
      if (lexPath == null) throw new ArgumentNullException("lexPath");
      var xNode = doc.CreateElement(DataModelNames.LEXICAL_RULE_FEATURE_PATH);
      WriteFeatureStartEndAttributes(lexPath, xNode, doc);

      return xNode;
    }
  }
}