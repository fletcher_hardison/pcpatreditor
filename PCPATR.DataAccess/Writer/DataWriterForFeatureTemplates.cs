// /* ***************************************************************
// * File name: DataWriterForFeatureTemplates.cs
// * Project: PCPATR.DAL
// * Created by: TFH
// * Created on: 12,04,2012
// * In the name of Christ with thanks to God 
// */

using System;
using System.Linq;
using System.Xml;
using PCPATR.Common;
using PCPATR.Models;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Models.MasterLists;

using System.Collections.Generic;
using PCPATR.Models.Rules.Constraints;

namespace PCPATR.DataAccess.Writer
{
  public partial class DataWriter
  {
    public XmlNode WriteFeatureTemplatesCollection(XmlDocument doc, IEnumerable<FeatureTemplateModel> featureTemplates)
    {
      var featureTemplateModels = featureTemplates as FeatureTemplateModel[] ?? featureTemplates.ToArray();
      if (featureTemplates == null ||!featureTemplateModels.Any()) {
        RemoveElement(doc, "featureTemplates");
        return null;
      }

      var xNode = doc.CreateElement(DataModelNames.FEATURE_TEMPLATES);
      
      foreach (var node in featureTemplateModels.Select(template => WriteFeatureTemplate(doc, template))) {
        xNode.AppendChild(node);
      }

      return xNode;
    }

    

    public XmlNode WriteFeatureTemplate(XmlDocument doc, FeatureTemplateModel template)
    {

      if (template == null) throw new ArgumentNullException("template");
      var xNode = doc.CreateElement(DataModelNames.FEATURE_TEMPLATE);

      WriteConstraintControlAttributes(template.Enabled, template.UseWhenDebugging, xNode, doc);

      WriteComments(template.Comments, xNode, doc);

      xNode.AppendChild(WriteFeatureTemplateDefinition(doc, template));

      return xNode;

    }

    private XmlNode WriteFeatureTemplateDefinition(XmlDocument doc, FeatureTemplateModel def)
    {
      if (def == null) throw new ArgumentNullException("def");
      var xNode = doc.CreateElement(DataModelNames.FEATURE_TEMPLATE_DEFINITION);

      xNode.AppendChild(WriteFeatureTemplateDefLeftSide(doc, def.Left));


      foreach (var element in def.RightValues)
      {
        xNode.AppendChild(WriteFeatureDefRightSideValue(doc, element));
      }

      return xNode;
    }

    private XmlNode WriteFeatureDefRightSideValue(XmlDocument doc, object element)
    {

      if (element == null) throw new ArgumentNullException("element");
      //going to have to figure out what structure to create based on what type 
      // the structure is   
      if (element is FeatureTemplateFeatureStructure)
      {
        return WriteTemplateFeatureStructure(doc, (FeatureTemplateFeatureStructure)element);
      }
      if (element is FeatureTemplateFeatureDisjunction)
      {
        return WriteTemplateFeatureDisjunction(doc, (FeatureTemplateFeatureDisjunction)element);
      }
      if (element is TemplateFeaturePath)
      {
        return WriteTemplateFeaturePath(doc, (TemplateFeaturePath)element);
      }
      if (element is FeatureTemplateFeaturePathDisjunction)
      {
        return WriteTemplateFeaturePathDisjunction(doc, (FeatureTemplateFeaturePathDisjunction)element);
      }
      if (element is FeatureFeatureTemplateRef)
      {
        return WriteFeatureTemplateRef(doc, (FeatureFeatureTemplateRef)element);
      }
      var pathTypeResult = CheckForAndWriteXValuePath(doc, element);
      if (pathTypeResult != null) return pathTypeResult;
      if (element is EmptyElement)
      {
        return WriteEmptyElement(doc);
      }

      return null;
    }

    private XmlNode CheckForAndWriteXValuePath(XmlDocument doc, object element)
    {
      if (element is CatValuePath)
      {
        return WriteCatValuePath(doc, (CatValuePath)element);
      }
      if (element is LexValuePath)
      {
        var lvp = element as LexValuePath;
        return WriteXValuePath(doc, DataModelNames.LEX_VALUE_PATH, lvp.Value, lvp.IsDefault);
      }
      if (element is GlossValuePath)
      {
        var gvp = element as GlossValuePath;
        return WriteXValuePath(doc, DataModelNames.GLOSS_VALUE_PATH, gvp.Value, gvp.IsDefault);
      }
      if (element is RootGlossValuePath)
      {
        var rgvp = element as RootGlossValuePath;
        return WriteXValuePath(doc, DataModelNames.ROOT_GLOSS_VALUE_PATH, rgvp.Value, rgvp.IsDefault);
      }
      return null;
    }

    private XmlNode CheckForAndWriteXValue(XmlDocument doc, object element)
    {
      if (element is CatValue)
      {
        return WriteCatValue(doc, (CatValue)element);
      }
      if (element is LexValue)
      {
        var lvp = element as LexValue;
        return WriteXValue(doc, DataModelNames.LEX_VALUE, lvp.Value, lvp.IsDefault);
      }
      if (element is GlossValue)
      {
        var gvp = element as GlossValue;
        return WriteXValue(doc, DataModelNames.GLOSS_VALUE, gvp.Value, gvp.IsDefault);
      }
      if (element is RootGlossValue)
      {
        var rgvp = element as RootGlossValue;
        return WriteXValue(doc, DataModelNames.ROOT_GLOSS_VALUE, rgvp.Value, rgvp.IsDefault);
      }
      return null;
    }


    private XmlNode WriteCatValuePath(XmlDocument doc, CatValuePath path)
    {
      if (path == null) throw new ArgumentNullException("path");
      var xNode = doc.CreateElement(DataModelNames.CAT_VALUE_PATH);

      var symbol = doc.CreateAttribute(DataModelNames.SYMBOL);
      symbol.Value = path.Symbol.IdRef;
      xNode.SetAttributeNode(symbol);

      SetIsDefault(doc, xNode, path);

      return xNode;
    }

    private void SetIsDefault(XmlDocument doc, XmlElement xNode, FeatureTemplateModelBase element)
    {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (element == null) throw new ArgumentNullException("element");

      if (!element.IsDefault) return;

      var isDefault = doc.CreateAttribute(DataModelNames.IS_DEFAULT);
      isDefault.Value = (element.IsDefault) ? "yes" : "no";
      xNode.SetAttributeNode(isDefault);
    }

    private void SetIsDefault(XmlDocument doc, XmlElement xNode, bool isDefault)
    {
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (!isDefault) return;

      var isDefaultXml = doc.CreateAttribute(DataModelNames.IS_DEFAULT);
      isDefaultXml.Value = "yes";
      xNode.SetAttributeNode(isDefaultXml);
    }

    private XmlNode WriteXValuePath(XmlDocument doc, string elementName, string elementValue, bool isDefault)
    {
      var xNode = doc.CreateElement(elementName);
      xNode.InnerText = elementValue;

      SetIsDefault(doc, xNode, isDefault);

      return xNode;
    }

    public XmlNode WriteXValue(XmlDocument doc, string valueName, string valueValue, bool isDefault)
    {
      

      var xNode = doc.CreateElement(valueName);

      xNode.InnerText = valueValue;

      SetIsDefault(doc, xNode, isDefault);

      return xNode;
    }

    public XmlNode WriteXValue(XmlDocument doc, object element) {
      var nodeName = string.Empty;
      if (element is LexValue) {
        nodeName = DataModelNames.LEX_VALUE;
      } else if (element is GlossValue) {
        nodeName = DataModelNames.LEX_VALUE;
      } else if (element is RootGlossValue) {
        nodeName = DataModelNames.LEX_VALUE;
      }
      var value = element as FeatureTemplateTerminalValueModelBase;
      if (value == null) return null;
      if (string.IsNullOrEmpty(nodeName)) return null;

      var node = doc.CreateElement(nodeName);
      node.InnerText = value.Value;
      
      return node;
    }

    private XmlNode WriteEmptyElement(XmlDocument doc)
    {
      return doc.CreateElement(DataModelNames.EMPTY);
    }

    private XmlNode WriteFeatureTemplateRef(XmlDocument doc, FeatureFeatureTemplateRef element)
    {
      if (element == null) throw new ArgumentNullException("element");
      var xNode = doc.CreateElement(DataModelNames.FEATURE_TEMPLATE_REF);

      var xAtt = doc.CreateAttribute(DataModelNames.TEMPLATE_ATT);
      xAtt.Value = element.Id.IdRef;
      xNode.SetAttributeNode(xAtt);

      return xNode;
    }

    public XmlNode WriteTemplateFeaturePathDisjunction(XmlDocument doc, FeatureTemplateFeaturePathDisjunction element)
    {
      if (element == null) throw new ArgumentNullException("element");
      var xNode = doc.CreateElement(DataModelNames.TEMPLATE_FEATURE_PATH_DISJUNCTION);

      xNode.AppendChild(WriteFeatureValueRef(doc, element.LeftPath));

      foreach (var valueRef in element.RightPaths)
      {
        xNode.AppendChild(WriteFeatureValueRef(doc, valueRef));
      }
      WriteFeatureStartEndAttributes(element, xNode, doc);

      return xNode;
    }

    public void WriteFeatureStartEndAttributes(IHasStartAndEndFields startAndEnd, XmlElement xNode, XmlDocument xDoc)
    {
      if (startAndEnd == null) throw new ArgumentNullException("startAndEnd");
      if (xNode == null) throw new ArgumentNullException("xNode");
      if (xDoc == null) throw new ArgumentNullException("xDoc");

      var start = xDoc.CreateAttribute(DataModelNames.FEATURE_START);
      start.Value = startAndEnd.Start;
      xNode.SetAttributeNode(start);

      var end = xDoc.CreateAttribute(DataModelNames.FEATURE_END);
      end.Value = startAndEnd.End;
      xNode.SetAttributeNode(end);
    }


    private XmlNode WriteTemplateFeaturePath(XmlDocument doc, TemplateFeaturePath element)
    {
      if (element == null) throw new ArgumentNullException("element");
      var xNode = doc.CreateElement(DataModelNames.TEMPLATE_FEATURE_PATH);
      SetIsDefault(doc, xNode, element);
      WriteFeatureStartEndAttributes(element, xNode, doc);

      if (element.Value != null)
      {
        if (element.Value is TemplateFeaturePath)
        {
          xNode.AppendChild(WriteTemplateFeaturePath(doc, (TemplateFeaturePath)element.Value));
        }
        var pathtypeResult = CheckForAndWriteXValuePath(doc, element.Value);
        if (pathtypeResult != null) xNode.AppendChild(pathtypeResult);
      }

      return xNode;
    }

    private XmlNode WriteTemplateFeatureDisjunction(XmlDocument doc, FeatureTemplateFeatureDisjunction element)
    {
      if (element == null) throw new ArgumentNullException("element");
      var xNode = doc.CreateElement(DataModelNames.TEMPLATE_FEATURE_DISJUNCTION);

      if (element.LeftTerm is FeatureTemplateFeatureStructure)
      {
        xNode.AppendChild(WriteTemplateFeatureStructure(doc, (FeatureTemplateFeatureStructure)element.LeftTerm));
      }
      else if (element.LeftTerm is FeatureFeatureTemplateRef)
      {
        xNode.AppendChild(WriteFeatureTemplateRef(doc, (FeatureFeatureTemplateRef)element.LeftTerm));
      }
      else if (element.LeftTerm is CollectionFeatureRef)
      {
        xNode.AppendChild(WriteCollectionFeatureRef(doc, (CollectionFeatureRef)element.LeftTerm));
      }

      foreach (FeatureTemplateModelBase term in element.RightTerms)
      {
        if (term is FeatureTemplateFeatureStructure)
        {
          xNode.AppendChild(WriteTemplateFeatureStructure(doc, (FeatureTemplateFeatureStructure)term));
        }
        else if (term is FeatureFeatureTemplateRef)
        {
          xNode.AppendChild(WriteFeatureTemplateRef(doc, (FeatureFeatureTemplateRef)term));
        }
        else if (term is CollectionFeatureRef)
        {
          xNode.AppendChild(WriteCollectionFeatureRef(doc, (CollectionFeatureRef)term));
        }
      }

      return xNode;
    }

    private XmlNode WriteCollectionFeatureRef(XmlDocument doc, CollectionFeatureRef leftTerm)
    {
      if (leftTerm == null) throw new ArgumentNullException("leftTerm");
      var xNode = doc.CreateElement(DataModelNames.COLLECTION_FEATURE_REF);


      var xRef = doc.CreateAttribute(DataModelNames.COLLECTION_FEATURE);
      xRef.Value = leftTerm.CollectionFeature;
      xNode.SetAttributeNode(xRef);

      SetIsDefault(doc, xNode, leftTerm);
      var embededPath = leftTerm.EmbeddedPath;
      if (embededPath is NoneRef)
      {
        xNode.AppendChild(WriteNoneRef(doc));
      }
      else if (embededPath is FeatureRef)
      {
        xNode.AppendChild(WriteFeatureRef(doc, (FeatureRef)embededPath));
      }
      else if (embededPath is FeatureTemplateFeatureStructure)
      {
        xNode.AppendChild(WriteTemplateFeatureStructure(doc, (FeatureTemplateFeatureStructure)embededPath));
      } else if (embededPath is FeatureFeatureTemplateRef)
      {
        xNode.AppendChild(WriteFeatureTemplateRef(doc, embededPath as FeatureFeatureTemplateRef));
      }
      var checkForValue = CheckForAndWriteXValue(doc, embededPath);
      if (checkForValue != null) xNode.AppendChild(checkForValue);

      return xNode;
    }

    private XmlNode WriteFeatureRef(XmlDocument doc, FeatureRef featureRef)
    {
      if (featureRef == null) throw new ArgumentNullException("featureRef");
      var xNode = doc.CreateElement(DataModelNames.FEATURE_REF);
      WriteFeatureStartEndAttributes(featureRef, xNode, doc);

      SetIsDefault(doc, xNode, featureRef);

      if (featureRef.Child != null) {
        var t = featureRef.Child.GetType();
        if (t == typeof (FeatureRef)) {
          xNode.AppendChild(WriteFeatureRef(doc, featureRef.Child as FeatureRef));  
        } else if (t == typeof (CollectionFeatureRef)) {
          xNode.AppendChild(WriteCollectionFeatureRef(doc, featureRef.Child as CollectionFeatureRef));
        } else if (t == typeof (FeatureTemplateFeatureDisjunction)) {
          xNode.AppendChild(WriteTemplateFeatureDisjunction(doc, featureRef.Child as FeatureTemplateFeatureDisjunction));
        } else if (t == typeof (FeatureFeatureTemplateRef)) {
          xNode.AppendChild(WriteFeatureTemplateRef(doc, featureRef.Child as FeatureFeatureTemplateRef));
        } else {
          throw new ApplicationException("Cannot write " + t + " as child of featureRef");
        }
        
      }
      return xNode;
    }

    private XmlNode WriteNoneRef(XmlDocument doc)
    {
      return doc.CreateElement(DataModelNames.NONE_REF);
    }

    private XmlNode WriteTemplateFeatureStructure(XmlDocument doc, FeatureTemplateFeatureStructure structure)
    {
      if (structure == null) throw new ArgumentNullException("structure");
      var xNode = doc.CreateElement(DataModelNames.TEMPLATE_FEATURE_STRUCTURE);

      foreach (var elem in structure.Elements)
      {
        
        if (elem is ComplexFeatureRef)
        {
          xNode.AppendChild(WriteComplexFeatureRef(doc, (ComplexFeatureRef)elem));
        }
        else if (elem is CollectionFeatureRef)
        {
          xNode.AppendChild(WriteCollectionFeatureRef(doc, (CollectionFeatureRef)elem));
        } 
        else if (elem is FeatureRef)
        {
          xNode.AppendChild(WriteFeatureRef(doc, (FeatureRef)elem));
        }
        else if (elem is FeatureFeatureTemplateRef)
        {
          xNode.AppendChild(WriteFeatureTemplateRef(doc, elem as FeatureFeatureTemplateRef));
        }
        else if (elem is LogicalFeatureModel)
        {
          xNode.AppendChild(WriteFeature(doc, elem as LogicalFeatureModel));
        }
        else if (elem is FeatureTemplateFeatureDisjunction)
        {
          xNode.AppendChild(WriteTemplateFeatureDisjunction(doc, (FeatureTemplateFeatureDisjunction)elem));
        }
        else if (elem is SubcatRef)
        {
          xNode.AppendChild(WriteSubcatRef(doc, (SubcatRef)elem));
        }
       
        else
        {
          var checkForValue = CheckForAndWriteXValue(doc, elem);
          if (checkForValue != null)
          {
            xNode.AppendChild(checkForValue);
          } else
          {
            throw new InvalidOperationException("Feature structure cannot contain an element of type: " + elem.GetType());
          } 
        }
        
      }

      return xNode;
    }

    private XmlNode WriteCatValue(XmlDocument doc, CatValue elem)
    {
      if (elem == null) throw new ArgumentNullException("elem");
      var xNode = doc.CreateElement(DataModelNames.CAT_VALUE);

      var symbol = doc.CreateAttribute(DataModelNames.SYMBOL);
      symbol.Value = elem.Symbol.IdRef;
      xNode.SetAttributeNode(symbol);

      SetIsDefault(doc, xNode, elem);

      return xNode;
    }

    private XmlNode WriteSubcatRef(XmlDocument doc, SubcatRef elem)
    {
      if (elem == null) throw new ArgumentNullException("elem");
      var xNode = doc.CreateElement(DataModelNames.SUBCAT_REF);

      SetIsDefault(doc, xNode, elem);

      if (elem.Value is SubcatFirstRest)
      {
        WriteSubcatFirstRest(doc, xNode, (SubcatFirstRest)elem.Value);
      }
      else if (elem.Value is NoneRef)
      {
        xNode.AppendChild(WriteNoneRef(doc));
      }
      else if (elem.Value is SubcatDisjunction)
      {
        xNode.AppendChild(WriteSubcatDisjunction(doc, (SubcatDisjunction)elem.Value));
      }

      return xNode;
    }

    private XmlNode WriteSubcatDisjunction(XmlDocument doc, SubcatDisjunction value)
    {
      if (value == null) throw new ArgumentNullException();
      var xNode = doc.CreateElement(DataModelNames.SUBCAT_DISJUNCTION);

      if (value.FirstFactor is NoneRef)
      {
        xNode.AppendChild(WriteNoneRef(doc));
      }
      else if (value.FirstFactor is SubcatFirstRest)
      {
        WriteSubcatFirstRest(doc, xNode, (SubcatFirstRest)value.FirstFactor);
      }

      foreach (var factor in value.OtherFactors)
      {
        if (factor is NoneRef)
        {
          xNode.AppendChild(WriteNoneRef(doc));
        }
        else if (factor is SubcatFirstRest)
        {
          WriteSubcatFirstRest(doc, xNode, (SubcatFirstRest)factor);
        }
      }
      return xNode;
    }

    private void WriteSubcatFirstRest(XmlDocument doc, XmlNode xNode, SubcatFirstRest value)
    {
      if (value == null) throw new ArgumentNullException("value");


      xNode.AppendChild(WriteFirstRestElement(doc, DataModelNames.FIRST_REF, value.First));

      xNode.AppendChild(WriteFirstRestElement(doc, DataModelNames.REST_REF, value.Rest));
    }

    public XmlNode WriteFirstRestElement(XmlDocument doc, string wrapperNodeName, object firstRest)
    {
      if (firstRest == null) throw new ArgumentNullException("firstRest");
      var wrapperNode = doc.CreateElement(wrapperNodeName);

      if (firstRest is ComplexFeatureRef)
        wrapperNode.AppendChild(WriteComplexFeatureRef(doc, (ComplexFeatureRef)firstRest));
      if (firstRest is FeatureRef)
        wrapperNode.AppendChild(WriteFeatureRef(doc, (FeatureRef)firstRest));
      if (firstRest is FeatureTemplateFeatureDisjunction)
        wrapperNode.AppendChild(WriteTemplateFeatureDisjunction(doc, (FeatureTemplateFeatureDisjunction)firstRest));
      if (firstRest is NoneRef)
        wrapperNode.AppendChild(WriteNoneRef(doc));
      if (firstRest is FeatureFeatureTemplateRef)
        wrapperNode.AppendChild(WriteFeatureTemplateRef(doc, (FeatureFeatureTemplateRef)firstRest));
      if (firstRest is FeatureTemplateFeatureStructure)
        wrapperNode.AppendChild(WriteTemplateFeatureStructure(doc, (FeatureTemplateFeatureStructure)firstRest));
      if (firstRest is SubcatDisjunction)
        wrapperNode.AppendChild(WriteSubcatDisjunction(doc, (SubcatDisjunction)firstRest));

      return wrapperNode;
    }


    private XmlNode WriteComplexFeatureRef(XmlDocument doc, ComplexFeatureRef elem)
    {
      if (elem == null) throw new ArgumentNullException("elem");
      var xNode = doc.CreateElement(DataModelNames.COMPLEX_FEATURE_REF);

      var xAtt = doc.CreateAttribute(DataModelNames.FEATURE);
      xAtt.Value = elem.Id;
      xNode.SetAttributeNode(xAtt);
      
      foreach (var element in elem.Values)
      {
        if (element is FeatureRef)
        {
          xNode.AppendChild(WriteFeatureRef(doc, (FeatureRef) element));
        } else if (element is SubcatRef)
        {
          xNode.AppendChild(WriteSubcatRef(doc, (SubcatRef) element));
        } else if(element is ComplexFeatureRef)
        {
          xNode.AppendChild(WriteComplexFeatureRef(doc, element as ComplexFeatureRef));
        } else if (element is CollectionFeatureRef)
        {
          xNode.AppendChild(WriteCollectionFeatureRef(doc, element as CollectionFeatureRef));
        } else if (element is FeatureTemplateFeatureDisjunction)
        {
          xNode.AppendChild(WriteTemplateFeatureDisjunction(doc, element as FeatureTemplateFeatureDisjunction));
        } else if (element is CatValue)
        {
          xNode.AppendChild(WriteCatValue(doc, element as CatValue));
        }
      }

      WriteComments(elem.Comments,xNode,doc);

      return xNode;
    }

    private XmlNode WriteFeatureTemplateDefLeftSide(XmlDocument doc, FeatureTemplateIdBase leftSide)
    {
      if (leftSide == null) throw new ArgumentNullException("leftSide");

      return
          (leftSide is TerminalRefModel)
              ? WriteTerminalRef(doc, (TerminalRefModel)leftSide)
              : WriteFeatureTemplateName(doc, (FeatureTemplateNameModel)leftSide);
    }

    private XmlNode WriteFeatureTemplateName(XmlDocument doc, FeatureTemplateNameModel leftSide)
    {
      if (leftSide == null) throw new ArgumentNullException("leftSide");
      var xNode = doc.CreateElement(DataModelNames.FEATURE_TEMPLATE_NAME);

      xNode.InnerText = leftSide.Name;

      WriteIdAttribute(leftSide, xNode, doc);

      //var xId = doc.CreateAttribute(DataModelNames.ID);
      //xId.Value = leftSide.Id;
      //xNode.SetAttributeNode(xId);

      return xNode;
    }

    private XmlNode WriteTerminalRef(XmlDocument doc, TerminalRefModel leftSide)
    {
      if (leftSide == null) throw new ArgumentNullException("leftSide");
      var xNode = doc.CreateElement(DataModelNames.TERMINAL_REF);

      var xId = doc.CreateAttribute(DataModelNames.TERMINAL);
      xId.Value = leftSide.Id;
      xNode.SetAttributeNode(xId);

      return xNode;
    }
  }
}