﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using PCPATR.DataAccess.Parser;

namespace PCPATR.DataAccess
{
  public class DataAccessModule : IModule
  {
    public DataAccessModule(IUnityContainer container)
    {
      _container = container;
    }

    public void Initialize()
    {
      _container.RegisterType<DataParser>(new ContainerControlledLifetimeManager());
      var writer = new DataAccess.Writer.DataWriter();
      
      _container.RegisterType<DataAccess.Writer.DataWriter>(new ContainerControlledLifetimeManager());
      DataAccessActions.SaveRulesFunction = writer.GetWriteRulesFunction();
      
      _container.RegisterType<DataAccessActions>(new ContainerControlledLifetimeManager());
      
    }

    private readonly IUnityContainer _container;
  }
}
