﻿using System;
using NUnit.Framework;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Models.Rules;


namespace PCPATR.Tests.Services
{
  [TestFixture()]
  public class ModelServiceTest
  {
    private ModelService<Rule> _sut = new ModelService<Rule>(new NewIdService());
    private Rule _rule1;
    [SetUp()]
    public void setUpTest()
    {
      
      _rule1 = new Rule();
      _rule1.Id = null;
      _rule1.Identifier = "Rule 1";

     
    }

    [Test()]
    public void ATest()
    {
      _sut.PersistModel(_rule1);
      var idQuery = _sut.FormatIdentiferIdString(_rule1);
      var result = _sut.GetModelFromIdentiferIdString(idQuery);
      Console.WriteLine(idQuery);
      Assert.IsNotNull(result);
    }

  }

}
