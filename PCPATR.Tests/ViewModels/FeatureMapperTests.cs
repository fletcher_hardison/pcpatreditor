﻿using NUnit.Framework;
using PCPATR.Editor.MasterLists;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Editor.MasterLists.ViewModels;


using PCPATR.Models.FeatureSystem;
using Rhino.Mocks;

namespace PCPATR.Tests.ViewModels
{
  [TestFixture()]
  public class FeatureMapperTest
  {

    private FakeUnityContainer _fake;
    [SetUp()]
    public void setUpTest()
    {
      _fake = new FakeUnityContainer();

      _fake.FakeRegistration(typeof(FeatureViewModel), () => new FeatureViewModel( () => "newId", MockRepository.GenerateMock<FeatureCreatorWithArg>(),MockRepository.GenerateMock<NewIdService>()));
      _fake.FakeRegistration(typeof(Feature), ()  => new Feature());
    }

    [Test()]
    public void TestFeatureSystemFeatureMapper()
    {
      var mapper = new FeatureMapper(_fake);
      var dummyData = new Feature { Id = "agr" };
      var gender = new Feature { Id = "gender" };
      var number = new Feature { Id = "number" };
      dummyData.Children.Add(gender);
      dummyData.Children.Add(number);

      var result = mapper.MapToViewModel(dummyData);

      Assert.IsNotNull(result);

      Assert.AreEqual(dummyData.Id, result.Id);
      Assert.AreEqual(dummyData.Children.Count, result.Children.Count);
      Assert.AreEqual(dummyData.Children[0].Id, result.Children[0].Id);
      Assert.AreEqual(dummyData.Children[1].Id, result.Children[1].Id);
    }

    [Test()]
    public void TestFeatureSystemFeatureMapperToModel()
    {
      var mapper = new FeatureMapper(_fake);
      var dummyData = new FeatureViewModel(null, null, null) { Id = "agr" };
      var gender = new FeatureViewModel(null, null, null) { Id = "gender" };
      var number =new FeatureViewModel(null, null, null) { Id = "number" };
      dummyData.Children.Add(gender);
      dummyData.Children.Add(number);

      var result = mapper.MapToModel(dummyData);

      Assert.IsNotNull(result);

      Assert.AreEqual(dummyData.Id, result.Id);
      Assert.AreEqual(dummyData.Children.Count, result.Children.Count);
      Assert.AreEqual(dummyData.Children[0].Id, result.Children[0].Id);
      Assert.AreEqual(dummyData.Children[1].Id, result.Children[1].Id);
    }

    [Test]
    public void SearchFeaturesTest()
    {
      var dummyData = new FeatureViewModel(null, null, null) { Id = "agr" };
      var gender = new FeatureViewModel(null, null, null) { Id = "gender" };
      var masc = new FeatureViewModel(null, null, null) { Id = "masc" };
      var fem = new FeatureViewModel(null, null, null) { Id = "feam" };
      gender.Children.Add(masc);
      gender.Children.Add(fem);
      var number = new FeatureViewModel(null, null, null) { Id = "number" };
      var sg = new FeatureViewModel(null, null, null) { Id = "sg" };
      var pl = new FeatureViewModel(null, null, null) { Id = "pl" };
      number.Children.Add(sg);
      number.Children.Add(pl);
      dummyData.Children.Add(gender);
      dummyData.Children.Add(number);

      var result = FeatureSystemViewModel.SearchFeaturesForParnetOfTarget(dummyData, sg);


      Assert.IsNotNull(result);
      Assert.AreEqual("number", result.Id);
    }

  }
}
