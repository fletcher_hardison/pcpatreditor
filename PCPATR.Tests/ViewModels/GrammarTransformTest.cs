﻿//  ***************************************************************
//  File name: GrammarTransformTest.cs
//  Project: PCPATR.Tests
//  Created by: Fletcher Hardison
//  Created on: 31,01,2013
// ***************************************************************

using System.Collections.Generic;
using PCPATR.Editor.MasterLists.ViewModels;
using NUnit.Framework;
using System.IO;
namespace PCPATR.Tests.ViewModels
{
  [TestFixture]
  public class GrammarTransformTest
  {
    private const string GRAMMAR_PATH = @"..\..\..\PCPATR.Editor\HabMiscTest.xml";
    private const string OUTPUT_PATH = @"..\..\..\PCPATR.Editor\HabMiscTest.grm";
    private const string TRANSFORM_LOCATION = @"..\..\..\PCPATR.Editor.FeatureSystem\PatrParser\PcPatrGrammar.xsl";

    [Test]
    public void TestGrammarXmlToGrmTransform()
    {
      IOHelperMethods.TransformGrammarXmlToGrmFile(GRAMMAR_PATH,TRANSFORM_LOCATION,OUTPUT_PATH,false,new Dictionary<string, string>());
      Assert.That(File.Exists(OUTPUT_PATH));
      //clean up
      //if (File.Exists(OUTPUT_PATH)) File.Delete(OUTPUT_PATH);
    }

    [Test]
    public void TestBuildMinimumGrammar()
    {
      var doc = IOHelperMethods.GenerateMinimumGrammarXmlDocument();

      Assert.IsNotNull(doc);
    }

  }
}