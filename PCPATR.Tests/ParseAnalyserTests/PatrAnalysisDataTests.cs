﻿using System.Collections.Generic;
using NUnit.Framework;
using PCPATR.Editor.MasterLists.PatrParser.DataModel;
using Rhino.Mocks; 

namespace PCPATR.Tests.ParseAnalyserTests
{
  [TestFixture()]
  public class PatrAnalysisDataTests
  {
    [SetUp()]
    public void SetUpTest()
    {
    }

    [Test()]
    public void ATest()
    {
      

      var childList = new List<PatrParseFeature>(new[]
      {
        new PatrParseFeature("person", "person_1", "1st", new List<PatrParseFeature>()),
        new PatrParseFeature("number", "number_1", "sg", new List<PatrParseFeature>())


      });
      var feature = new PatrParseFeature("agr", "agr_0_1", "", childList);

      var head = new PatrParseFeature("head", "", "", new List<PatrParseFeature>(new[] {feature, feature}));
      var result = PatrParseAnalysisMethods.FormatPatrParseFeature(head,1);
      const string EXPECTED = "agr : [person : 1st\r\nnumber : sg]";
      
      System.Diagnostics.Debug.Write(result);
      Assert.Pass(result);
      
      //Assert.AreEqual(EXPECTED, result);
      

    }

  }

}