﻿using System;
using System.Xml;
using NUnit.Framework;
using PCPATR.Editor.MasterLists;
using PCPATR.Editor.MasterLists.PatrParser;
using PCPATR.Tests.Properties;
using SIL.PcPatrBrowser;

namespace PCPATR.Tests.ParseAnalyserTests
{
  [TestFixture()]
  public class ParseComparisonTests
  {
    [SetUp()]
    public void setUpTest() { }

    [Test()]
    public void TestFullAnalysisAndHtmlOutput()
    {
      var doc = new XmlDocument();
      doc.LoadXml(Resources.ParseComparisonTestData);
      var analyser = new ParseAnalyser(OutsideWorld.GetPathToShowFsTransForm());
      var sentence = new PcPatrSentence(doc.DocumentElement);

      analyser.CompareParsesForSentence(sentence);
    }

    [Test]
    public void TestNoneComparison()
    {
      var doc = new XmlDocument();
      var doc2 = new XmlDocument();

      doc.LoadXml(Resources.SingleNodeComparisonTestData);
      doc2.LoadXml(Resources.SingleNodeComparisonTestData);

      var differenceNode = doc2.CreateElement("F");
      var nameAtt = doc2.CreateAttribute("name");
      nameAtt.Value = "animalType";
      differenceNode.Attributes.Append(nameAtt);
      var str = doc2.CreateElement("Str");
      str.InnerText = "fish";
      differenceNode.AppendChild(str);
      var node = doc2.SelectSingleNode(".//Node");
      node.AppendChild(differenceNode);

      var parses = new XmlNode[2];
      parses[0] = doc.DocumentElement;
      parses[1] = doc2.DocumentElement;

      var levels = ParseAnalyser.GetNodeLevels(parses);

      ParseAnalyser.compareLevels(levels);

      var levelsAfter = ParseAnalyser.GetNodeLevels(parses);

      var secondNode = parses[1];

      var secondNodeChildFeatures = secondNode.SelectNodes(".//F");
      Console.Write(secondNode.InnerXml);
      Assert.AreEqual(1, secondNodeChildFeatures.Count);
      



    }



  }

}