﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using PCPATR.Editor.MasterLists.PatrParser;


namespace PCPATR.Tests.ParseAnalyserTests
{
  [TestFixture()]
  public class InputFileCheckerTests
  {
    [SetUp()]
    public void setUpTest() {}

    [Test()]
    public void CheckCorrectChunkingOfLexiconFile()
    {
      var lexiconFile = string.Format("\\w uther\r\n\\g uther\r\n\\w sleep\r\n\\g sleep\r\n\\w knight\r\n\\g knight");
      
      var outputList = PatrInputCheckerMethods.GetListOfWordsInLexicon(lexiconFile,"\\w");
      
      var lexiconAsList = new List<string> {"uther", "sleep", "knight"};
      
      var list = outputList.ToArray();
      var listCount = list.Count();
      
      Assert.AreEqual(lexiconAsList.Count,listCount);
      for(var i = 0; i< listCount; i +=1)
      {
        Assert.AreEqual(lexiconAsList[i],list[i]);
        System.Console.WriteLine(list[i] + " was in lex as " + lexiconAsList[i]);
      }
    }

    [Test()]
    public void CheckCorrectChunkingOfInputFile()
    {
      var testData = string.Format("uther sleeps\r\nuther sleep\r\nknight sleep\r\n knights sleep");
      var expectedResult = new List<string> {"uther", "sleeps", "sleep", "knight", "knights"};

      var outputList = PatrInputCheckerMethods.GetListOfWordsInDataFile(testData, '|');
      
      var list = outputList.ToArray();
      var listCount = list.Count();

      
      for (var i = 0; i < listCount; i += 1)
      {
        Assert.AreEqual(expectedResult[i], list[i]);
        System.Console.WriteLine(list[i] + " was in expected results as " + expectedResult[i]);
      }
      Assert.AreEqual(expectedResult.Count, listCount);
    }

  }

}