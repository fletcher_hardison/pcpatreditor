﻿using System.Collections.Generic;
using NUnit.Framework;
using PCPATR.Editor.MasterLists.PatrParser;
using PCPATR.Editor.MasterLists.ViewModels;
using Rhino.Mocks; 

namespace PCPATR.Tests.ParseAnalyserTests
{
  [TestFixture()]
  public class ParserControlViewModelTests
  {

    [Test()]
    public void TestRunParserWithTransformedGrammar()
    {
      var control = new PatrParserControl();
      var analyser = new ParseAnalyser("");
      const string pathToLexFile = @"..\..\..\DummyData\ENGLISH.LEX";
      const string pathToDataFile = @"..\..\..\DummyData\AP.txt";
      const string pathToXmlFile = @"..\..\shieber1Xml.xml";
      string grmPath = pathToXmlFile.Replace(".xml", ".grm");

      string BetterNotEqual = string.Format("|\r\n|\r\n|  Phrase structure rules\r\n|\r\n");

      IOHelperMethods.TransformGrammarXmlToGrmFile(pathToXmlFile,@"..\..\..\PCPATR.EDITOR\Assets\PcPatrGrammar.xsl",pathToXmlFile.Replace(".xml",".grm"),false,new Dictionary<string, string>());
        
      //var output = ParserControlViewModel.RunParserOnSelectedFile(pathToDataFile, pathToLexFile, pathToXmlFile, () => 1.ToString(), analyser,
        //                                             control,new ParseOptionsViewModel(), new Dictionary<string, string>(), 1,);

      var fileContents = System.IO.File.ReadAllText(grmPath);
      System.Console.Write(fileContents);
      Assert.AreNotEqual(BetterNotEqual,fileContents);
    }

  }

}