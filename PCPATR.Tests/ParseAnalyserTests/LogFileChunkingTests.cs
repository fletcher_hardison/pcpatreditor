﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using PCPATR.Editor.MasterLists.PatrParser;
using PCPATR.Editor.MasterLists.PatrParser.DataModel;

namespace PCPATR.Tests.ParseAnalyserTests
{
  [TestFixture]
  public class LogFileChunkingTests
  {
    private const string DUMMY_DATA =
      "the and nice doctor likes glasses\r\n**** Not able to parse this sentence ****\r\n**** Building the largest parse \"bush\" ****";

    [Test]
    public void TestCommentLoader()
    {
      var currentIndex = 0;
      var result = ParseAnalyser.LoadSentenceAndCommentsIntoClass(DUMMY_DATA, new PatrAnalysisData(),false, ref currentIndex,new List<string>());

      Assert.AreEqual("the and nice doctor likes glasses", result.Sentence.Trim());
      Assert.AreEqual(2,result.Comments.Count);
    }

    

    [Test]
    public void TestMovePastEndOfFile()
    {
      const string FILE_HEADER = "   Grammar file used: C:\\englishgb\\Testing\\ENGLISH.GRM\r\n| AP conjunction ";

      var index = ParseAnalyser.MovePastFileHeader(FILE_HEADER);
      Assert.That(index > FILE_HEADER.IndexOf("\r\n"));
    }

    [Test]
    public void TestParseAnalyser()
    {
      var analyser = new ParseAnalyser("");
      const string FILE_PATH = @"..\..\ParseAnalyserTests\ParseLogResource.txt";
      var result = analyser.AnalysePatrOutput(FILE_PATH,false,'|');

      Assert.AreEqual(8,result.PatrSentences.Count);

    }

  }
}
