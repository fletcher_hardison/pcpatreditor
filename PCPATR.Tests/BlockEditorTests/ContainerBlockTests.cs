﻿using System;
using NUnit.Framework;
using PCPATR.Editor.MasterLists.BlockEditor;


namespace PCPATR.Tests.BlockEditorTests
{
  [TestFixture()]
  public class ContainerBlockTests
  {
    [SetUp()]
    public void setUpTest()
    {
      
    }

    [Test()]
    public void EmptyContainerBlockInsertListIsItsOwnFirstList()
    {
      var grammar = BlockGrammar.GetInstance();

      grammar.AddRule("ChildBlock", con =>
      {
        return new ContainerBlock
        {
        TypeName = "ChildBlock",
        GrammarRule = "ChildBlock -> ChildBlock, ParentBlock"
        };
      });
      
      var parentBlock = new ContainerBlock
      {
        TypeName = "ParentBlock",
        GrammarRule = "ParentBlock -> ChildBlock, ChildBlock, ChildBlock"
      };

      var childBlock1 = new ContainerBlock
      {
        TypeName = "ChildBlock",
        GrammarRule = "ChildBlock -> GrandChildBlock, ParentBlock"
      };

      parentBlock.AppendChild(childBlock1,0);

      parentBlock.SelectedBlock = childBlock1;
      var childInsertListFirstElement = childBlock1.InsertList[0];
      
      Assert.AreEqual("GrandChildBlock",childInsertListFirstElement);
      Console.WriteLine("Child insert List");
      foreach (var item in childBlock1.InsertList)
      {
        Console.WriteLine(item);
      }
    }
    
    [Test]
    public void CheckAppendChildToContainerWithHigherIndexChildWorksCorrectly()
    {
      var grammar = BlockGrammar.GetInstance();

      grammar.AddRule("ChildBlock", con =>
      {
        return new ContainerBlock
        {
          TypeName = "ChildBlock",
          GrammarRule = "ChildBlock -> ChildBlock, ParentBlock"
        };
      });

      var parentBlock = new ContainerBlock
      {
        TypeName = "ParentBlock",
        GrammarRule = "ParentBlock -> ChildBlock, ChildBlock, ChildBlock"
      };

      var childBlock1 = new ContainerBlock
      {
        TypeName = "ChildBlock",
        GrammarRule = "ChildBlock -> GrandChildBlock, ParentBlock"
      }; 
  
      var newChild = new ContainerBlock
      {
        TypeName = "ChildBlock",
        GrammarRule = "ChildBlock -> GrandChildBlock, ParentBlock"
      }; 
      
      parentBlock.AppendChild(childBlock1,1);
      
      //act
      parentBlock.AppendChild(newChild,0);
      
      //assert
      Assert.AreEqual(newChild,parentBlock[0]);
      Assert.AreEqual(childBlock1,parentBlock[1]);
    }
  }

}