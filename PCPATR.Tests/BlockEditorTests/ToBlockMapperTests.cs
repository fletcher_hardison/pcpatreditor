﻿using NUnit.Framework;
using PCPATR.Common;
using PCPATR.Editor.MasterLists.BlockEditor;
using PCPATR.Models.Rules.Constraints;

namespace PCPATR.Tests.BlockEditorTests
{
  [TestFixture()]
  public class ToBlockMapperTests
  {
    private BlockGrammar _grammar = BlockGrammar.GetInstance();

    [SetUp()]
    public void setUpTest()
    {
      BlockGrammarLoader.ConfigureFeatureElements(_grammar);
      BlockGrammarLoader.ConfigureFeatureStructureAndElements(_grammar);
      BlockGrammarLoader.ConfigureLexicalReferenceElements(_grammar);
      BlockGrammarLoader.ConfigureLogicalConstraintTemplateElements(_grammar);
      BlockGrammarLoader.ConfigureRuleConstraints(_grammar);
      BlockGrammarLoader.ConfigureRuleConstraintsPathElements(_grammar);            
    }

    [Test()]
    public void TestMappingOfSimpleUnificationConstraint()
    {
      var mapper = new ObjectToBlockMapper();

      var testData = new UnificationConstraintModel();
      var path1 = new FeaturePathModel
      {
        node = new NameIdRef("V"),
        feature_start = new NameIdRef("head"),
        feature_end = new NameIdRef("agr"),
        EmbeddedFeaturePath = new EmbeddedFeaturePath
        {
          feature_start = new NameIdRef("number"),
          feature_end = new NameIdRef("sg")
        }
      };
      var path2 = new SubcatPathModel
      {
        node = new NameIdRef("VP"),
        feature_start = new NameIdRef("head")
      };

      testData.left = path1;
      testData.right = path2;

      var block = mapper.MapObjectToBlock(testData);

      Assert.IsNotNull(block);
      Assert.IsInstanceOf<ContainerBlock>(block);
      var container = block as ContainerBlock;
      Assert.AreEqual(2, container.Children.Count);
    }

  }

}