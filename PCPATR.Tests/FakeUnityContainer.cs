﻿//  ***************************************************************
//  File name: FakeUnityContainer.cs
//  Project: PCPATR.Tests
//  Created by: 
//  Created on: 04,10,2012
// ***************************************************************

using System;
using System.Collections.Generic;
using Microsoft.Practices.Unity;

namespace PCPATR.Tests
{
    public class FakeUnityContainer : IUnityContainer
    {
        private Dictionary<string, Delegate> _fakeResgistrations;

        private IUnityContainer _parent;

        private IEnumerable<ContainerRegistration> _registrations;

        public void FakeRegistration<T>(Type t, Func<T> creatorFunction)
        {
            if (this._fakeResgistrations == null) this._fakeResgistrations = new Dictionary<string, Delegate>();

            if (creatorFunction == null) throw new ArgumentNullException("creatorFunction");

            if (!this._fakeResgistrations.ContainsKey(t.Name))
            {
                this._fakeResgistrations.Add(t.Name, creatorFunction);
            }
        }


        public IUnityContainer RegisterType(Type @from, Type to, string name, LifetimeManager lifetimeManager, params InjectionMember[] injectionMembers)
        {
            throw new NotImplementedException();
        }

        public IUnityContainer RegisterInstance(Type t, string name, object instance, LifetimeManager lifetime)
        {
            throw new NotImplementedException();
        }



        public T Resolve<T>(string name, params ResolverOverride[] resolverOverrides)
         {
             if (_fakeResgistrations.ContainsKey(typeof(T).Name)) {
                 throw new InvalidOperationException("Fake Unity container does not contian registration for the given type");
             }
             var creatorFunction = this._fakeResgistrations[typeof(T).Name];

             return (T) creatorFunction.DynamicInvoke();
         }

        public object Resolve( Type t,string name, params ResolverOverride[] resolverOverrides)
        {

            if (!_fakeResgistrations.ContainsKey(t.Name))
            {
                throw new InvalidOperationException("Fake Unity container does not contian registration for the given type");
            }
            var creatorFunction = this._fakeResgistrations[t.Name];

            return creatorFunction.DynamicInvoke();
        }

        public IEnumerable<object> ResolveAll(Type t, params ResolverOverride[] resolverOverrides)
        {
            throw new NotImplementedException();
        }

        public object BuildUp(Type t, object existing, string name, params ResolverOverride[] resolverOverrides)
        {
            throw new NotImplementedException();
        }

        public void Teardown(object o)
        {
            throw new NotImplementedException();
        }

        public IUnityContainer AddExtension(UnityContainerExtension extension)
        {
            throw new NotImplementedException();
        }

        public object Configure(Type configurationInterface)
        {
            throw new NotImplementedException();
        }

        public IUnityContainer RemoveAllExtensions()
        {
            throw new NotImplementedException();
        }

        public IUnityContainer CreateChildContainer()
        {
            throw new NotImplementedException();
        }

        public IUnityContainer Parent
        {
            get { return this._parent; }
        }

        public IEnumerable<ContainerRegistration> Registrations
        {
            get { return this._registrations; }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}