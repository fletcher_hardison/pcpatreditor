﻿using System.Xml;
using NUnit.Framework;
using PCPATR.Common;
using PCPATR.DataAccess.Writer;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Models.Rules.Constraints;
using Rhino.Mocks;

namespace PCPATR.Tests.DAL
{
  [TestFixture()]
  public class ConstraintWriterTests
  {

    [Test()]
    public void ATest()
    {
      var writer = new DataWriter();

      var data = new UnaryOperationModel();
      data.OperationType = "IS NOT";
      data.Factor1 = new LogicalFeatureModel
      {
        feature_start = new NameIdRef("embedded"),
        feature_end = new NameIdRef("embedded"),
      };


      var outputNode = writer.WriteUnaryOperation(new XmlDocument(), data);

      System.Diagnostics.Debug.Write(outputNode.InnerXml);
    }

  }

}