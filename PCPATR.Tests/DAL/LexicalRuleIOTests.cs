﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using NUnit.Framework;
using PCPATR.Common;
using PCPATR.DataAccess.Writer;
using PCPATR.Models.Rules;
using PCPATR.Models.Rules.Constraints;

namespace PCPATR.Tests.DAL
{
  [TestFixture]
  public class LexicalRuleIOTests
  {
    private DataWriter _sut;
    
    [SetUp]
    public void SetUpTest()
    {
      _sut = new DataWriter();
    }

    [Test]
    public void TestWriteLexicalRule()
    {
      const string EXPECTED = "<lexicalRule><name>Transitive</name><lexicalRuleDefinition><lexicalRuleFeaturePath featureStart=\"head\" featureEnd=\"transitive\" /><featureValueRef value=\"transitiveplus\" /></lexicalRuleDefinition></lexicalRule>";
      var lexicalRule = new LexicalRule {Name = "Transitive"};
      var definition1 = new LexicalRuleDefinition
      {
        Left = new LexicalRuleFeaturePath { Start = "head", End = "transitive" }
      };
      definition1.RightElement = new FeatureValueRefModel { value = new NameIdRef("transitiveplus") };

      lexicalRule.LexicalRuleDefinitions.Add(definition1);

      var swriter = new StringWriter();
      var xmlWriter = new XmlTextWriter(swriter);
      var doc = new XmlDocument();

      var result = _sut.WriteLexicalRule(doc, lexicalRule);
      doc.AppendChild(result);
      doc.WriteContentTo(xmlWriter);

      Assert.AreEqual(EXPECTED, swriter.ToString() );
    }
  }
}
