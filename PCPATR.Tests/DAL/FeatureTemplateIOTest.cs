﻿using System.IO;
using System.Reflection;
using System.Xml;
using NUnit.Framework;
using PCPATR.Common;
using PCPATR.DataAccess.Writer;
using PCPATR.Models.FeatureTemplates;
using PCPATR.Models.Rules.Constraints;

using Rhino.Mocks; 

namespace PCPATR.Tests.DAL
{
  [TestFixture()]
  public class FeatureTemplateIOTest
  {
    private DataWriter _sut;
    private string _templateFeaturePathDisjunctionData =
      "<templateFeaturePathDisjunction featureStart=\"head\" featureEnd=\"case\"><featureValueRef value=\"accusative\" /><featureValueRef value=\"genitive\" /></templateFeaturePathDisjunction>";
    [SetUp()]
    public void setUpTest()
    {
      _sut = new DataWriter();
    }

    [Test]
    public void TestFeaturePathDisjunctionWrite()
    {
      var model = new FeatureTemplateFeaturePathDisjunction {Start = "head", End ="case"};
      var valRef1 = new FeatureValueRefModel {value = new NameIdRef("accusative")};
      var valRef2 = new FeatureValueRefModel { value = new NameIdRef("genitive") };
      model.LeftPath = valRef1;
      model.RightPaths.Add(valRef2);
      var swriter = new StringWriter();
      var xmlwriter = new XmlTextWriter(swriter);
      var doc = new XmlDocument();
      var xmlResult = _sut.WriteTemplateFeaturePathDisjunction(doc, model);
      doc.AppendChild(xmlResult);
      doc.WriteContentTo(xmlwriter);
      Assert.AreEqual(_templateFeaturePathDisjunctionData, swriter.ToString());
    }

    [Test()]
    public void TestWriteFeatureTemplate()
    {
      
      var testData = Properties.Resources.animate_absolutive_template;

      var data = new FeatureTemplateModel();
      var name = new FeatureTemplateNameModel {Name = "animate_absolutive"};
      var disjunct = new FeatureTemplateFeatureDisjunction();
      var leftStructure = new FeatureTemplateFeatureStructure();
      var complexRef = new ComplexFeatureRef {Id = "head"};
      var featureRef1 = new FeatureRef
      {
        Start = "object",
        End = "objecthead",
        Child = new FeatureRef { Start = "head", End = "animateplus" }
      };

      var featureRef2 = new FeatureRef {Start = "type", End = "transitiveplus"};
      complexRef.Values.Add(featureRef1);
      complexRef.Values.Add(featureRef2);
      leftStructure.Elements.Add(complexRef);
      disjunct.LeftTerm = leftStructure;

      var complexRef2 = new ComplexFeatureRef { Id = "head" };
      var featureRef3 = new FeatureRef
      {
        Start = "subject",
        End = "subjecthead",
        Child = new FeatureRef { Start = "head", End = "animateplus"}
      };

      var featureRef4 = new FeatureRef { Start = "type", End = "transitiveminus" };
      complexRef2.Values.Add(featureRef3);
      complexRef2.Values.Add(featureRef4);

      var rightStructure = new FeatureTemplateFeatureStructure();
      rightStructure.Elements.Add(complexRef2);

      disjunct.RightTerms.Add(rightStructure);
      data.Left = name;
      data.RightValues.Add(disjunct);

      var doc = new XmlDocument();
      var xml = _sut.WriteFeatureTemplate(doc, data);
      

      Assert.AreEqual(testData,"<featureTemplate>" +xml.InnerXml + "</featureTemplate>");

    }

  }

}