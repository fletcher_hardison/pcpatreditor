﻿using System.Xml;
using NUnit.Framework;
using PCPATR.Common;
using PCPATR.DataAccess.Writer;
using PCPATR.Editor.MasterLists.Services;
using PCPATR.Editor.MasterLists.ViewModels;
using PCPATR.Models.Parameters;
using Rhino.Mocks; 

namespace PCPATR.Tests.DAL
{
  [TestFixture()]
  public class ParametersIOTests
  {
    private DataWriter _sut;
    
    [SetUp()]
    public void setUpTest()
    {
      _sut = new DataWriter();
    }

    [Test()]
    public void TestWriteCategoryRefAttributeOrderElement()
    {
      var catRefAtt = new AttributeOrderItem {IdRef = new NameIdRef("ctId"), Name = ParameterAttributeOrderNames.CAT};
      var output = _sut.WriteParameterAttribute(new XmlDocument(), catRefAtt);

      Assert.AreEqual("catRef", output.Name);
      Assert.That(output.Attributes != null && output.Attributes.Count == 1);
      var outAtt = output.Attributes[0];
      Assert.AreEqual("cat", outAtt.Name);
      Assert.AreEqual("ctId", outAtt.Value);
    }

    [Test()]
    public void TestParametersMapper()
    {
      var container = new FakeUnityContainer();
      container.FakeRegistration(typeof(ParametersModel), () => new ParametersModel());
      ParametersMapper.InitializeParametersMapper(container);
    }

    [Test()]
    public void TestMappingViewModelToParametersModel()
    {
      var viewModel = new ParametersViewModel(() => new NameIdRef(""), () => new NameIdRef(""));
      viewModel.StartSymbol = new NameIdRef("s");
      viewModel.AttributeOrder.Add(new AttributeOrderViewModel{Name = "Lex"});
      var container = new FakeUnityContainer();
      container.FakeRegistration(typeof(ParametersModel), () => new ParametersModel());
      var mapper = new ParametersMapper(container);
      var model = mapper.MapToModel(viewModel);

      Assert.IsInstanceOf<ParametersModel>(model);
      Assert.IsNotNull(model);
      Assert.AreEqual("s", model.StartSymbol.IdRef);
      var att = model.AttributeOrder[0];
      Assert.AreEqual("Lex", att.Name);
    }

  }

}