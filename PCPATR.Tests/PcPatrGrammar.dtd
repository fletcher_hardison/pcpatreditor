<!--                                                              -->
<!-- *******************     PCPATR.DTD     *******************   -->
<!--                                                              -->
<!-- A formal specification of the conceptual model of a PC-PATR  -->
<!-- grammar file.                                                -->
<!--                                                              -->
<!-- VERSION 0.2.0                                                -->
<!--                                                              -->
<!-- This file is maintained by Andy Black.   Send comments or    -->
<!-- suggested refinements by email to: andy_black@sil.org 	  -->
<!--                                                              -->
<!-- The revision history is maintained at the end of the file.   -->
<!--                                                              -->

<!ENTITY % constraintControl "
    enabled (yes|no) 'yes'
    useWhenDebugging (yes | no) 'yes'"
>
<!-- enabled indicates whether or not the rule or constraint is currently being used.  
              This lets one keep older ideas around and makes it easy to restore them.
       useWhenDebugging indicates whether the item is to be included in a debug run or not.  
           By design, for useWhenDebugging most rules are "off" while most constraints are "on" since this is the normal case.
-->

<!--                                                              -->
<!-- **********************  PC-PATR GRAMMAR  ******************* -->
<!--                                                              -->

<!ELEMENT patrGrammar (comment*, featureTemplates?, constraintTemplates?, rules, parameters?, lexicalRules?, masterLists) >

<!-- The top-level element is named "patrGrammar" for "PC-PATR grammar".  It consists of a set of rules and optional
     feature templates, parameters, lexical rules, and constraint templates.  There also are a set of required master lists.
 -->
<!ELEMENT masterLists (nonTerminals, terminals, builtInPrimitives, featureSystem?, collectionFeatures?) >
<!ELEMENT builtInPrimitives (cat, lex, gloss, rootgloss, none) >
<!-- The built-in primitives are items that PC-PATR always has.
        These should appear in every XML file that conforms to this DTD.
        We suggest using ids of pcpatrGrammarCat, pcpatrGrammarLex, pcpatrGrammarGloss, pcpatrGrammarRootGloss,
          and pcpatrGrammarNone for these items.
 -->
<!ELEMENT cat EMPTY >
<!ATTLIST cat
    id ID  #REQUIRED
>
<!ELEMENT lex EMPTY >
<!ATTLIST lex
    id ID  #REQUIRED
>
<!ELEMENT gloss EMPTY >
<!ATTLIST gloss
    id ID  #REQUIRED
>
<!ELEMENT rootgloss EMPTY >
<!ATTLIST rootgloss
    id ID  #REQUIRED
>
<!ELEMENT none EMPTY >
<!ATTLIST none
    id ID  #REQUIRED
>

<!--                                                              -->
<!-- ************************   RULE  *************************** -->
<!--                                                              -->
<!ELEMENT rules (rule+) >
<!ELEMENT rule (comment*, identifier?, phraseStructureRule, constraints?) >
<!ATTLIST rule
    id ID #REQUIRED
    enabled (yes|no) "yes"
    useWhenDebugging (yes | no) "no"
>
<!-- A rule consists of zero or more comment lines,
     an optional rule identifier (used more as a comment than anything else)
     a phrase structure rule, zero or more constraints.

     id is a unique ID for this rule.
     enabled indicates whether it is being used or not.
     useWhenDebugging indicates whether the item is to be included in a debug run or not.        
-->

<!ELEMENT phraseStructureRule (leftHandSide, rightHandSide, comment*) >
<!-- A phrase structure rule consists of a left hand side,
     a right hand side, and an optional comment. -->

<!ELEMENT comment     (#PCDATA) >
<!-- A comment. -->

<!ELEMENT identifier  (#PCDATA) >
<!-- A user-defined identifier for the rule. -->

<!ELEMENT leftHandSide EMPTY >
<!ATTLIST leftHandSide
    id ID #REQUIRED
   symbol IDREF #REQUIRED
>
<!-- A left hand side is always a nonTerminal. -->

<!ELEMENT rightHandSide  (symbolRef | optionalSymbols | disjunction | comment)* >
<!-- A right hand side is a list of terminal and non-terminal symbols, optional symbols and/or disjunctive symbols.
       optional and disjunctive symbols may be nested to any depth.
-->

<!ELEMENT symbolRef EMPTY >
<!ATTLIST symbolRef
    id ID #REQUIRED
   symbol IDREF #REQUIRED
>
<!-- a symbolRef refers to a terminal or a nonTerminal symbol -->

<!ELEMENT optionalSymbols ((symbolRef | optionalSymbols | disjunction)+) >
<!ELEMENT disjunction ((symbolRef | optionalSymbols)+, disjunctiveSymbols) >
<!ELEMENT disjunctiveSymbols ((symbolRef | optionalSymbols)+, disjunctiveSymbols?) >

<!--                                                              -->
<!-- **************   CONSTRAINTS  **************** -->
<!--                                                              -->
<!ELEMENT constraints (percolationOperation | unificationConstraint | priorityUnionOperation | logicalConstraint | constraintDisjunction | comment)* >
<!-- There are four kinds of constraints in this mark-up (PC-PATR only has three, but we are treating 
        percolation operations specially): percolation operations, unification constraints, priority union
        operations, and logical constraints.
        Any of these may occur in a disjunction of constraints.
-->
<!ELEMENT constraintDisjunction (constraints, constraints+) >
<!ELEMENT percolationOperation ((featurePath | subcatPath), (featurePath | subcatPath), comment*) >
<!ATTLIST percolationOperation
    %constraintControl; 
>
<!-- A percolation operation has two parts: 
        the first is a path whose symbol is the left hand side symbol of the phrase structure rule; 
        the second is a path whose symbol is one of the right hand side symbols of the phrase structure rule -->

<!ELEMENT featurePath ((embeddedFeaturePath | catValuePath | glossValuePath | lexValuePath | rootGlossValuePath)?)>
<!ATTLIST featurePath 
  node IDREF #REQUIRED
  featureStart IDREF #REQUIRED
  featureEnd IDREF #IMPLIED
>
<!-- a feature path consists of 
a reference to a node in a phrase structure rule (to a symbolRef);
a reference to a starting complex or closed feature or subcat or collection feature; and
a reference to an ending complex or closed feature or a feature value or collection feature or a part of a subcat list (i.e. subcat, first, or rest)
(The intermediate nodes are implicit in the feature system.)
-->
<!ELEMENT embeddedFeaturePath (embeddedFeaturePath?)>
<!ATTLIST embeddedFeaturePath 
  featureStart IDREF #REQUIRED
  featureEnd IDREF #IMPLIED
>
<!-- an embedded feature path consists of 
a reference to a starting complex or closed feature or subcat or collection feature; and
a reference to an ending complex or closed feature or a feature value or collection feature or a part of a subcat list (i.e. subcat, first, or rest)
(The intermediate nodes are implicit in the feature system.)
-->
<!ELEMENT subcatPath EMPTY>
<!ATTLIST subcatPath 
  node IDREF #REQUIRED
  featureStart IDREF #IMPLIED
  featureEnd IDREF #IMPLIED
  subcatEnd (noneValue | first |rest) #IMPLIED
  featureAfterStart IDREF #IMPLIED
  featureAfterEnd IDREF #IMPLIED
>
<!-- a subcat path consists of 
 a reference to a node in a phrase structure rule (to a symbolRef);
 any preceding feature path items bounded by 
    a reference to a starting complex or closed feature; and
    a reference to an ending complex or closed feature or a feature value
    (The intermediate nodes are implicit in the feature system.)
subcat (output automatically)
any further refinement of the subcat path (e.g. first or rest)
any following feature path (featureAfterStart and featureAfterEnd)
-->

<!ELEMENT unificationConstraint ((featurePath | subcatPath), (featurePath | featureValueRef | categoryValueRef | featureValueDisjunction | subcatPath | lexValuePath | glossValuePath | rootGlossValuePath| noneValue | templateFeatureStructure), comment*) >
<!ATTLIST unificationConstraint
    %constraintControl; 
>
<!-- A unification constraint has two parts: the first is a feature path;
        the second is either another feature path or a feature value -->
        
<!ELEMENT categoryValueRef EMPTY>
<!ATTLIST categoryValueRef
   node IDREF #REQUIRED
>
<!-- a categoryValueRef refers to node in the phrase structure rule -->

<!ELEMENT featureValueDisjunction (featureValueRef, featureValueRef+) >

<!ELEMENT priorityUnionOperation ((featurePath | subcatPath),(featurePath | featureValueRef | categoryValueRef | featureValueDisjunction | noneValue | templateFeatureStructure), comment*) >
<!ATTLIST priorityUnionOperation
    %constraintControl; 
>
<!-- A priority union operation has two parts:
        the first is a feature path whose symbol is the left hand side symbol of the phrase structure rule; 
        the second is a feature path whose symbol is one of the right hand side symbols of the phrase structure rule -->

<!ELEMENT logicalConstraint (featurePath, (logicalExpression | logicalConstraintRef), comment*) >
<!ATTLIST logicalConstraint
    %constraintControl; 
>
<!-- A logical constraint operation has two parts:
        the first is a feature path whose symbol is one of the symbols of the phrase structure rule; 
        the second is a logical expression -->
<!ELEMENT  logicalConstraintRef EMPTY >
<!ATTLIST logicalConstraintRef
  constraint IDREF #REQUIRED
>
<!-- a logicalConstraintRef refers to a constraintTemplate -->
<!ELEMENT logicalExpression (unaryOperation | binaryOperation) >
<!ELEMENT unaryOperation (factor) >
<!ATTLIST unaryOperation
   operation (existence | negation) "existence"
>
<!ELEMENT binaryOperation (factor, comment?, factor) >
<!ATTLIST binaryOperation
   operation (and | or | conditional | bicondtional) "and"
>
<!ELEMENT factor (feature | complexFeatureRef | collectionFeatureRef | logicalExpression | catValue | lexValue | glossValue | rootGlossValue) >

<!ELEMENT feature (feature?) >
<!ATTLIST feature
    featureStart IDREF #REQUIRED
    featureEnd IDREF #REQUIRED
    indexedVariable (none | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9) "none"
>
<!-- a feature consists of 
a reference to a starting complex or closed feature; and
a reference to an ending complex or closed feature or a feature value
(The intermediate nodes are implicit in the feature system.)
an optional indexedVariable which acts as a variable for the value 
   of the feature indicated by the closed feature referred to by featureEnd
-->

<!--                                                              -->
<!-- ******************  FEATURE SYSTEM  ********************** -->
<!--                                                              -->

<!ELEMENT featureSystem ((complexFeatureDefinition | closedFeatureDefinition)+) >

<!ELEMENT complexFeatureDefinition (comment*, name, description?, (complexFeatureDefinition | closedFeatureDefinition | complexFeatureDefinitionRef )+) >
<!ATTLIST complexFeatureDefinition
   id ID #REQUIRED
>
<!-- a complexFeatureDefinition consists of one or more other features, either closed or complex.
        The description is not used by PC-PATR but can help a user remember what the feature is for.
-->
<!ELEMENT complexFeatureDefinitionRef EMPTY >
<!ATTLIST complexFeatureDefinitionRef
   id ID #REQUIRED
   complex IDREF #REQUIRED
>
<!-- a complexFeatureDefinitionRef refers to another complexFeatureDefinition defined elsewhere in the feature system.
       It provides a way to reuse parts of the feature system.
-->
<!ELEMENT closedFeatureDefinition (comment*, name, description?, featureValueDefinition+) >
<!ATTLIST closedFeatureDefinition
   id ID #REQUIRED
>
<!-- a closedFeatureDefinition is a simple feature with one or more values.
        The description is not used by PC-PATR but can help a user remember what the feature is for.
-->
<!ELEMENT featureValueDefinition (comment*, name, description?) >
<!ATTLIST featureValueDefinition
   id ID #REQUIRED
>
<!-- a featureValueDefinition defines a value of a (closed) feature.
        The description is not used by PC-PATR but can help a user remember what the feature is for.
-->

<!ELEMENT name (#PCDATA) >
<!-- The name of the feature or other item. -->

<!--                                                              -->
<!-- ******************  FEATURE TEMPLATE  ********************** -->
<!--                                                              -->
<!ELEMENT featureTemplates ((featureTemplate | comment)*) >
<!ELEMENT featureTemplate (comment*, featureTemplateDefinition) >
<!ATTLIST featureTemplate
    %constraintControl;
>
<!-- A feature template has optional comment lines and
     the feature definition.
 -->
<!ELEMENT featureTemplateDefinition ((featureTemplateName | terminalRef), 
                                                                    (templateFeatureStructure | templateFeatureDisjunction | templateFeaturePath | templateFeaturePathDisjunction | featureTemplateRef | 
                                                                      catValuePath | lexValuePath | glossValuePath | rootGlossValuePath | empty)+) >
<!-- A feature definition consists of two parts.
      The first is a name or a reference to a terminal symbol.
      The second is the content of the definition.
-->

<!ELEMENT featureTemplateName (#PCDATA) >
<!ATTLIST featureTemplateName
    id ID #IMPLIED
>
<!-- a featureTemplateName is the name of the feature template.
       It may have an id for when another feature template refers to it.
-->
<!ELEMENT templateFeatureStructure ((complexFeatureRef | featureRef | feature | templateFeatureDisjunction | subcatRef | catValue | lexValue | glossValue | rootGlossValue
                                                                  | collectionFeatureRef | featureTemplateRef | comment)+)>
<!-- a templateFeatureStructure defines a feature structure -->
<!ELEMENT featureRef ((featureRef | collectionFeatureRef | templateFeatureDisjunction | featureTemplateRef)?) >
<!ATTLIST featureRef
    featureStart IDREF #REQUIRED
    featureEnd IDREF #REQUIRED
  isDefault (no | yes) "no"
>
<!-- a featureRef refers to a feature path within in the feature system.
     It can be embedded in which case the featureEnd of the parent is a complexFeatureDefinitionRef and
     the startFeature of the embedded featureRef refers to the same feature as the complexFeatureDefinitionRef  reference
     of its parent's featureEnd.
-->
<!ELEMENT collectionFeatureRef (noneRef | catValue | lexValue | glossValue | rootGlossValue | templateFeatureStructure | featureRef | featureTemplateRef) >
<!ATTLIST collectionFeatureRef
    collectionFeature IDREF #REQUIRED
    isDefault (no | yes) "no"
>
<!-- a collectionFeatureRef refers to a collection feature and has a path to other items -->
<!ELEMENT catValue EMPTY >
<!ATTLIST catValue
    symbol IDREF #REQUIRED
    isDefault (no | yes) "no"
>
<!-- a catValue refers to terminal or non-terminal symbol -->
<!ELEMENT lexValue (#PCDATA) >
<!ATTLIST lexValue
    isDefault (no | yes) "no"
>
<!ELEMENT glossValue (#PCDATA) >
<!ATTLIST glossValue
    isDefault (no | yes) "no"
>
<!ELEMENT rootGlossValue (#PCDATA) >
<!ATTLIST rootGlossValue
    isDefault (no | yes) "no"
>
<!ELEMENT subcatRef (noneRef | (firstRef, restRef) | subcatDisjunction) >
<!ATTLIST subcatRef
    isDefault (no | yes) "no"
>
<!-- a subcatRef is used for the special case of subcategorization lists.
      They have either a (first, rest) pair or a value of none (to end the list).
      It can also have a disjunctive value.
-->
<!ELEMENT noneRef EMPTY >
<!ELEMENT firstRef (complexFeatureRef | featureRef | templateFeatureDisjunction | noneRef | featureTemplateRef | templateFeatureStructure | subcatDisjunction) >
<!ELEMENT restRef (complexFeatureRef | featureRef | templateFeatureDisjunction | noneRef | featureTemplateRef | templateFeatureStructure | subcatDisjunction) >
<!-- firstRef and restRef elements contain the contents of first element of a subcategorization list and the rest of the list, respecitively. -->
<!ELEMENT subcatDisjunction ((noneRef | (firstRef, restRef)), (noneRef | (firstRef, restRef))+) >
<!ELEMENT complexFeatureRef ((complexFeatureRef | featureRef | feature | collectionFeatureRef | templateFeatureDisjunction | subcatRef | catValue | comment)+) >
<!ATTLIST complexFeatureRef
    feature IDREF #REQUIRED
>
<!-- a complexFeatureRef is used to build a complex feature in a feature template.
       The feature attribute refers to a complexFeatureDefinition.
-->
<!ELEMENT templateFeatureDisjunction ((templateFeatureStructure | featureTemplateRef | collectionFeatureRef | noneValue | featureValueRef | lexValue), 
                                                                       (templateFeatureStructure | featureTemplateRef | collectionFeatureRef | noneValue | featureValueRef | lexValue | comment)+) >
<!ELEMENT templateFeaturePath ((templateFeaturePath | catValuePath | noneValuePath | glossValuePath | lexValuePath | rootGlossValuePath)?)>
<!ATTLIST templateFeaturePath 
  featureStart IDREF #REQUIRED
  featureEnd IDREF #REQUIRED
  isDefault (no | yes) "no"
>
<!-- a feature path consists of 
a reference to a non-terminal or terminal symbol;
a reference to a starting complex or closed feature; and
a reference to an ending complex or closed feature or a feature value
(The intermediate nodes are implicit in the feature system.)
-->
<!ELEMENT templateFeaturePathDisjunction (templateFeaturePath?, featureValueRef, featureValueRef+) >
<!ATTLIST templateFeaturePathDisjunction 
  featureStart IDREF #REQUIRED
  featureEnd IDREF #REQUIRED
>
<!ELEMENT catValuePath EMPTY >
<!ATTLIST catValuePath
    symbol IDREF #REQUIRED
    isDefault (no | yes) "no"
>
<!-- a catValue refers to terminal or non-terminal symbol -->
<!ELEMENT noneValuePath (#PCDATA) >
<!ATTLIST noneValuePath
    isDefault (no | yes) "no"
>
<!ELEMENT lexValuePath (#PCDATA) >
<!ATTLIST lexValuePath
    isDefault (no | yes) "no"
>
<!ELEMENT glossValuePath (#PCDATA) >
<!ATTLIST glossValuePath
    isDefault (no | yes) "no"
>
<!ELEMENT rootGlossValuePath (#PCDATA) >
<!ATTLIST rootGlossValuePath
    isDefault (no | yes) "no"
>
<!ELEMENT featureValueRef EMPTY >
<!ATTLIST featureValueRef
    value IDREF #REQUIRED
    isDefault (no | yes) "no"
>
<!ELEMENT featureTemplateRef EMPTY >
<!ATTLIST featureTemplateRef
    template IDREF #REQUIRED
>
<!-- a featureTemplateRef refers to a feature template defined earlier in the file. -->
<!--                                                              -->
<!-- *********************** PARAMETERS  ************************* -->
<!--                                                              -->

<!ELEMENT parameters (comment*, startSymbol?, restrictor?, attributeOrder?,  categoryFeature?, lexicalFeature?, glossFeature?, rootGlossFeature?, comment*) >
<!-- A parameter setting consists of zero or more comment lines,
     the parameter name, the parameter value, and an optional
     comment. -->

<!ELEMENT startSymbol (comment*)>
<!ATTLIST startSymbol
    symbol IDREF #REQUIRED
>
<!ELEMENT restrictor (templateFeaturePath+, comment*)>
<!ELEMENT attributeOrder ((catRef | lexRef | glossRef | rootGlossRef | featureNameRef | subcat | first | rest)+, comment*) >
<!ELEMENT catRef EMPTY >
<!ATTLIST catRef
    cat IDREF  #REQUIRED
>
<!ELEMENT lexRef EMPTY >
<!ATTLIST lexRef
    lex IDREF  #REQUIRED
>
<!ELEMENT glossRef EMPTY >
<!ATTLIST glossRef
    gloss IDREF  #REQUIRED
>
<!ELEMENT rootGlossRef EMPTY >
<!ATTLIST rootGlossRef
    rootGloss IDREF  #REQUIRED
>
<!ELEMENT featureNameRef EMPTY >
<!ATTLIST featureNameRef
   feature IDREF #REQUIRED
>
<!ELEMENT subcat EMPTY >
<!ELEMENT first EMPTY >
<!ELEMENT rest EMPTY >
<!ELEMENT noneValue EMPTY >
<!ELEMENT empty EMPTY >
<!ELEMENT categoryFeature (name, comment*) >
<!ELEMENT lexicalFeature (name, comment*) >
<!ELEMENT glossFeature (name, comment*) >
<!ELEMENT rootGlossFeature (name, comment*) >
<!--                                                              -->
<!-- ********************** LEXICAL RULE ************************ -->
<!--                                                              -->
<!ELEMENT lexicalRules (lexicalRule*) >
<!ELEMENT lexicalRule     (comment*, name, lexicalRuleDefinition+) >
<!-- A lexical rule has an name and a definition. -->
<!ELEMENT lexicalRuleDefinition (lexicalRuleFeaturePath, (lexicalRuleFeaturePath | featureValueRef), comment?) >
<!ELEMENT lexicalRuleFeaturePath EMPTY >
<!ATTLIST lexicalRuleFeaturePath
  featureStart IDREF #REQUIRED
  featureEnd IDREF #REQUIRED
>
<!--                                                              -->
<!-- *******************  CONSTRAINT TEMPLATE  ****************** -->
<!--                                                              -->
<!ELEMENT constraintTemplates (constraintTemplate*) >
<!ELEMENT  constraintTemplate (name, logicalExpression, comment*) >
<!ATTLIST  constraintTemplate
   id ID #REQUIRED
>
<!-- A constraint template has a name and a logical expression. -->

<!--                                                              -->
<!-- *******************  NON-TERMINAL ****************** -->
<!--                                                              -->
<!ELEMENT nonTerminals (nonTerminal+) >
<!ELEMENT  nonTerminal (comment*, name, description?) >
<!ATTLIST nonTerminal
  id ID #REQUIRED
>
<!-- A non-terminal has a name and definition and a required ID. -->

<!--                                                              -->
<!-- *******************  TERMINAL ****************** -->
<!--                                                              -->
<!ELEMENT terminals (terminal+) >
<!ELEMENT  terminal (comment*, name, description?) >
<!ATTLIST terminal
  id ID #REQUIRED
>
<!-- A terminal symbol has a name and definition and a required ID. -->
<!ELEMENT description (#PCDATA) >
<!ELEMENT terminalRef EMPTY >
<!ATTLIST terminalRef
    terminal IDREF #REQUIRED
>
<!--                                                              -->
<!-- *******************  COLLECTION FEATURES ****************** -->
<!--                                                              -->
<!ELEMENT collectionFeatures (collectionFeature*) >
<!ELEMENT  collectionFeature (comment*, name, description?) >
<!ATTLIST collectionFeature
  id ID #REQUIRED
>
<!-- a collectFeature is a feature that can contain various things: 
        a complex feature, a category, none.
        Therefore it cannot fit into the feature system and needs to be handled specially.
-->
<!-- *******************   REVISION HISTORY   ******************* -->
<!--                                                              -->
<!-- Version 0.2.0, January 10, 2012 -->
<!--     Flesh out constituent objects -->
<!-- Version 0.1.0, February 11, 2000 -->
<!--     First draft by Andy Black -->
<!--     This is for Stage 1: a simplified model of rules and
         their constraints.  Stage 2 will deal with detailing
         rule contents in terms of their constituent objects. -->

