﻿//  ***************************************************************
//  File name: TestHelperMethods.cs
//  Project: PCPATR.Tests
//  Created by: 
//  Created on: 18,10,2012
// ***************************************************************

using System.Collections.ObjectModel;

namespace PCPATR.Tests
{
    public static  class TestHelperMethods
    {
         public  static string flatten(this ObservableCollection<string> col)
         {
             var result = string.Empty;

             foreach (var s in col) {
                 result += s + ' ';
             }
             return result.Trim();
         }
    }
}