﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using PCPATR.Common.Properties;

namespace PCPATR.Common
{
  public class NameIdRef : INotifyPropertyChanged
  {
    
    public NameIdRef(string idRef)
    {
      _idRef = idRef;
      IdRefsInGrammar.Add(this);
    }

    public void NotifyChanges() {
      RaisePropertyChanged("IdRef");
      RaisePropertyChanged("Name");
    }

    public string IdRef
    {
      get { return _idRef; }
      set
      {
        _idRef = value;
        RaisePropertyChanged("IdRef");
        RaisePropertyChanged("Name");
      }
    }

    public string Name
    {
      get
      {
        if (NameId.id_and_names.ContainsKey(_idRef))
        {
          return NameId.id_and_names[_idRef];
        }

        return _idRef;
      }
    }

   public bool HasValue
    {
      get { return !string.IsNullOrEmpty(_idRef); }
    }


    private string _idRef;

    public static List<NameIdRef> IdRefsInGrammar = new List<NameIdRef>();

    public static void UpdateRefsInGrammar(string oldId, string newId) {
      var list = IdRefsInGrammar.Where(f => f.IdRef == oldId);
      foreach (var idRef in list) {
        idRef.IdRef = newId;
      }
    }

    protected bool Equals(NameIdRef other) {
      return string.Equals(_idRef, other._idRef);
    }

    public override int GetHashCode() {
      return (_idRef != null ? _idRef.GetHashCode() : 0);
    }


    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void RaisePropertyChanged(string property_name)
    {
      PropertyChangedEventHandler handler = PropertyChanged;
      if (handler != null) handler(this, new PropertyChangedEventArgs(property_name));
    }

    public override bool Equals(object obj)
    {
      var x = obj as NameIdRef;
      return (x != null) && (string.Equals(_idRef, x.IdRef));
    }

    public override string ToString() {
      return string.IsNullOrEmpty(_idRef) ?  "idRef": Name;
    }
  }


  public static class NameIdRefExtensions
  {
    public static string GetId(this NameIdRef x)
    {
      return x == null ? string.Empty: x.IdRef;
    }

    public static NameIdRef ToNameIdRef(this string x)
    {
      return new NameIdRef(x);
    }
  }
}
