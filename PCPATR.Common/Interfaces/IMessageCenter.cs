namespace PCPATR.Common.Interfaces
{
    public interface IMessageCenter {
        IMessageBus MessageBus { get; set; }

        IMessageRepository MessageRepository { get; set; }
    }
}