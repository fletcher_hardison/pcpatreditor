using System.Collections.Generic;

namespace PCPATR.Common.Interfaces
{
    public interface ISymbol {
        List<string> Comments { get; set; }

        string Id { get; set; }

        string Name { get; set; }

        string Description { get; set; }

        object Clone();
    }
}