using System.Collections.Generic;

namespace PCPATR.Common.Interfaces
{
    public interface IFeatureDescription {
        List<string> Comments { get; set; }

        string Name { get; set; }

        string Id { get; set; }

        string Description { get; set; }
    }
}