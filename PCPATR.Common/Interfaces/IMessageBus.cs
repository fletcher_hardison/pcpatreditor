using System;

namespace PCPATR.Common.Interfaces
{
    public interface IMessageBus
    {
        void Subscribe<TMessage>(Action<TMessage> handler);
        
        void Unsubscribe<TMessage>(Action<TMessage> handler);
        
        void Publish<TMessage>(TMessage message);

        void Publish<TMessage>(TMessage message, Action<TMessage> callback);
        
        void Publish(Object message);

        void Publish(Object message, Action<Object> callback);
    }
}