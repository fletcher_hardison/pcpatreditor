using System;

namespace PCPATR.Common.Interfaces
{
    public interface IMessageRepository {
        void RegisterMessage<T>( Func<T> messageCreator);

        void UnregisterMessage<T>();

        T GetMessage<T>();
    }
}