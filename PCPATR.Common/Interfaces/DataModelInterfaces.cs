namespace PCPATR.Common.Interfaces
{
    public interface IHasStartAndEnd
    {
        string Start { get; set; }
        string End { get; set; }
    }

    public interface IHasName
    {
        string Name { get; set; }
    }
    public interface IHasId
    {
        string Id { get; set; }
    }

   
    public interface IHasDescription : IHasName
    {
        string Description { get; set; }
        
    }

    public interface IHasConstraintControl
    {
      bool use_when_debugging { get; set; }

      bool enabled { get; set; }
    }
}
