﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCPATR.Common
{
  public class Option<T>
  {
    public static Option<T> Nothing = new Option<T>(false, default(T));

    public Option(T value) : this(true, value) { }

    public Option(bool hasValue, T value)
    {
      HasValue = hasValue;
      Value = value;
    }

    public bool HasValue { get; private set; }
    public T Value { get; private set; }
  }

  public static class OptionExtensions
  {
    public static  Option<U> Bind<T, U>(this Option<T> x, Func<T, Option<U>> func)
    {
      return x.HasValue ? func(x.Value) : Option<U>.Nothing;
    }

    public static Option<U> FMap<T, U>(this Option<T> x, Func<T, U> func)
    {
      return x.HasValue ? new Option<U>(func(x.Value)) : Option<U>.Nothing;
    }

    public static Option<T> ToOption<T>(this T value)
    {
      return new Option<T>(value);
    }
  }


  public class Exceptional<T>
  {
    public Exceptional(Exception ex)
    {
      HasException = true;
      Exception = ex;
    }

    public Exceptional(T value)
    {
      HasException = false;
      Value = value;
    }

    public bool HasException { get; private set; }
    public Exception Exception { get; private set; }
    public T Value {get;private set;}
  }

  public static class ExceptionalExtensions
  {
    public static Exceptional<U> Bind<T, U>(this Exceptional<T> x, Func<T, Exceptional<U>> func)
    {
      return x.HasException ? new Exceptional<U>(x.Exception) :  func(x.Value) ;
    }

    public static Exceptional<U> FMap<T, U>(this Exceptional<T> x, Func<T, U> func)
    {
      return x.HasException ? new Exceptional<U>(x.Exception) : new Exceptional<U>(func(x.Value));
    }

    public static  Exceptional<T> ToExceptional<T>(this T value)
    {
      return new Exceptional<T>(value);
    }
  }
}
