using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PCPATR.Common
{
  public class NameId : INotifyPropertyChanged
  {
// ReSharper disable InconsistentNaming
    public static Dictionary<string, string> id_and_names = new Dictionary<string, string>();
// ReSharper restore InconsistentNaming

    public NameId(string name, string id)
    {
      _name = name;
      _id = id;
    }

    public bool has_value
    {
      get { return !string.IsNullOrEmpty(id); }
    }

    public string name
    {
      get
      {
        if (id_and_names.ContainsKey(_id)) return id_and_names[_id];
        return _name;
      }
      set
      {
        update_name(value);
      }
    }

    public string id
    {
      get { return _id; }
      set { update_id(value); }
    }

    //Assumption - if two things have the same id, they refer to the same thing.
    private void update_id(string new_id)
    {
      NameIdRef.UpdateRefsInGrammar(_id,new_id); //syncs changes accross classes
      _id = new_id;
      if (!id_and_names.ContainsKey(new_id))
      {
        id_and_names.Add(new_id, new_id);
      }
    }

    public void update_name(string new_name)
    {
      if (id_and_names.ContainsKey(_id))
      {
        id_and_names[_id] = new_name;
      }
      _name = new_name;

      notify_name_changed();
    }

    public static string resolve(string id)
    {
      return id_and_names[id];
    }

    public static NameId create(string id)
    {
      return new NameId("", id);
    }

    public static void register(string name, string id)
    {
      id_and_names.Add(id, name);
    }

    public static Tuple<string, string> remove(string id)
    {
      if (!id_and_names.ContainsKey(id)) return null;
      Tuple<string, string> x = Tuple.Create(id_and_names[id], id);
      id_and_names.Remove(id);
      return x;
    }

    public static void update_name(string id, string name)
    {
      id_and_names.Add(id, name);
    }


    public void try_register()
    {
      if (id_and_names.ContainsKey(id)) id_and_names[id] = name;
      else register();
    }

    public void register()
    {
      id_and_names.Add(id, name);
    }

    public void refresh_name()
    {
      _name = id_and_names[id];
      notify_name_changed();
    }

    public void notify_name_changed()
    {
      if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("name"));
    }



    public override bool Equals(object obj)
    {
      if (obj == null) return false;


      var cast = obj as NameId;
      if (cast == null) return false;

      return (id == cast.id);
    }

    public override int GetHashCode()
    {
// ReSharper disable NonReadonlyFieldInGetHashCode
      return _id.GetHashCode();
// ReSharper restore NonReadonlyFieldInGetHashCode
    }

    private string _id;
    private string _name;

    #region Implementation of INotifyPropertyChanged

    public event PropertyChangedEventHandler PropertyChanged;

    #endregion
  }


  public static class NameIdExtensions
  {
    public static NameId ToNameId(this string value)
    {
      return new NameId(string.Empty, value);
    }

    public static string GetId(this NameId x)
    {
      if (x == null) return string.Empty;

      return x.id;
    }
  }
}