﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace PCPATR.Common
{
  public static class FEs
  {
    public static TResult Pipe<T, TResult>(this T x, Func<T, TResult> func)
    {
      return func(x);
    }

    public static void Each(this XmlNodeList list, Action<XmlNode> func)
    {
      if (list == null) return;
      foreach (XmlNode node in list) {
        func(node);
      }
    }

    public static IEnumerable<TU> Map<TU>(XmlNodeList list, Func<XmlNode, TU> func)
    {
      var output = new List<TU>();
      foreach (XmlNode node in list) {
        output.Add(func(node));
      }

      return output;
    }


    public static IEnumerable<TU> Map<T, TU>(IEnumerable<T> list, Func<T, TU> func)
    {
      return from x in list
      select func(x);
    }
  }
}
