using System;
using System.Collections.Generic;
using System.Xml;

namespace PCPATR.Common
{
  public static class ActionsExtensions
  {
    public static IO<TU> Bind<T, TU>(this IO<T> io, Func<T, IO<TU>> func)
    {
      return func(io());
    }
  }

  public delegate T IO<out T>();

  public class Actions
  {
    public delegate T ListSelectMonad<T>(IEnumerable<T> list);

    public static IO<XmlDocument> LoadXml;

    public IO<XmlDocument> load_xml_monad
    {
      get
      {
        return LoadXml;
      }
      set
      {
        LoadXml = value;
      }
    }

    /// <summary>
    /// Takes a list of NameId inputs allows user to select on and returns select NameId
    /// </summary>
    //public static Func<List<NameId>, NameId> choose_nameid_from_list { get; set; }

    public static Func<NameIdRef> choose_symbol_from_master_lists { get; set; }

    /// <summary>
    /// Takes an string id and gets all child features of that id, lets user select one and returns result;
    /// </summary>
    public static Func<string, NameId> edit_feature_path_feature { get; set; }

    public static Func<NameId> choose_index_variable_function { get; set; }

    //public Func<NameId> SelectFeature
    //{
    //  get
    //  {
    //    return () => edit_feature_path_feature(string.Empty);
    //  }
    //}

    //public Func<string, NameId> choose_feature
    //{
    //  get
    //  {
    //    return edit_feature_path_feature;
    //  }
    //}

    public Func<NameIdRef> choose_symbol
    {
      get
      {
        return choose_symbol_from_master_lists;
      }
    }

    public Func<NameId> choose_index_variable
    {
      get { return choose_index_variable_function; }
    }

  }
}
